var md5 = require('MD5');
var async = require('async');
var _ = require('underscore');
var ServiceCurriculum = require('../../Service/curriculum');
var ServiceCustomer = require('../../Service/customer');
var ServiceTopic = require('../../Service/topic');
var ServiceTopicCategory = require('../../Service/topicCategory');
var ServiceTopicBinding = require('../../Service/topicBinding');
var ServicePackage = require('../../Service/package');
var ServiceChildMapper = require('../../Service/childParentMapper');
var ServiceTopicCurriculumMapper = require('../../Service/topicCurriculumMapping');
var constants = require('../../Library/appConstants');
var fs = require('fs');
var parseCsv = require('csv-parse');
var Path = require('path');
var Templates = require('../../Templates/superAdminRegister');
var UploadManager = require('../../Library/uploadManager');
var NotificationManager = require('../../Library/notificationManager');
var TemplatesBlock = require('../../Templates/blockCustomer');
var ServiceCustomerBuyPackage = require('../../Service/customerBuyPackages');
var TemplatesPublish = require('../../Templates/publishTopics');
var TemplatesSubAgent = require('../../Templates/subAgentRegister');

var integrateCustomersIntoSystem = function (item, callback) {
	var agentId = null;
	var superParentId = null;
	var timezoneOffset = null;
	var customerData = null;
	var str = item.emailId;
	var res = str.substr(0, 3);
	var str1 = item.mobileNo;
	var res1 = str1.substr(0, 5);
	async.series([

        function (cb) {

			var query = {
				$or: [{
					emailId: item.emailId
                }]
			};
			ServiceCustomer.getCustomer(query, {}, {
				lean: true
			}, function (error, data) {
				if (error) {
					cb(error);
				} else {
					if (data && data.length > 0) {
						if (data[0].phoneVerified == true) {
							cb(constants.STATUS_MSG.ERROR.USER_ALREADY_REGISTERED)
						} else {
							ServiceCustomer.deleteCustomer({
								_id: data[0]._id
							}, function (err, updatedData) {
								if (err) cb(err)
								else cb(null);
							})
						}
					} else {
						cb(null);
					}
				}
			});
        },
        
        function (cb) {
			//Insert Into DB
var dataToSave={};
			dataToSave.mobileNo = item.mobileNo;
			dataToSave.password = md5(md5(res + res1));
			dataToSave.registrationDate = new Date().toISOString();
			dataToSave.lastName = item.lastName;
			dataToSave.firstName = item.firstName;
			dataToSave.curriculum = item.curriculum;
//			dataToSave.grade = dataToSave.grade.split(',');
			dataToSave.dob = item.dob;
			dataToSave.addressValue = item.addressValue;
			dataToSave.emailId = item.emailId;
			dataToSave.countryCode = item.countryCode;
			dataToSave.userType = item.userType;
			dataToSave.location = {
				"coordinates": [0, 0],
				"type": "Point"

			}

			ServiceCustomer.createCustomer(dataToSave, function (err, customerDataFromDB) {
				logging.consolelog('hello', err, customerDataFromDB)
				if (err) {
					if (err.code == 11000 && err.message.indexOf('customers.$mobileNo_1') > -1) {
						cb(constants.STATUS_MSG.ERROR.PHONE_NO_EXIST);

					} else if (err.code == 11000 && err.message.indexOf('customers.$emailId_1') > -1) {
						cb(constants.STATUS_MSG.ERROR.EMAIL_ALREADY_EXIST);

					} else {
						cb(err)
					}
				} else {
					customerData = customerDataFromDB;
					cb();
				}
			})
        },
        function (cb) {
			ServiceCustomer.getCustomer({
				emailId: item.agent,
				userType: constants.USER_TYPE.AGENT
			}, {}, {}, function (err, data) {
				if (!err) {
					if (data.length > 0) {
						agentId = data[0]._id;
					}
					cb(null);
				} else {
					cb(err);
				}
				//get Id of agent//then check in cild mapper table
			});

        },
//        function (cb) {
//			if (agentId) {
//				ServiceChildMapper.getChildParentMapper({
//					childCustomerId: agentId
//				}, {
//					superParentCustomerId: 1
//				}, {}, function (err, data) {
//					if (data.length > 0) {
//						superParentId = data[0].superParentId;
//					}
//					cb(null);
//				});
//
//			} else {
//				cb(null);
//			}
//        },
        function (cb) {
			if (agentId) {
				var data = {
					childCustomerId: customerData._id,
					childCustomerType: item.userType,
					parentCustomerId: agentId,
					parentCustomerType: constants.USER_TYPE.AGENT,
					superParentCustomerId: agentId
				}
				ServiceChildMapper.createChildParentMapper(data, function (err, data) {
					cb(null);
				})


			} else {
				cb(null);
			}




        },
		function (cb) {
			if (agentId) {
				/*Update License counter in new customer*/
				var query = {
					_id: customerData._id
				}

				var update = {
					$push: {
						license: {
							$each: [{
								parentCustomerId: superParentId,
								allocatedLicense: 0,
								usedLicense: 0,
								isActive: true
							}]
						}
					}
				}
				logging.consolelog("logging",query, update);
				ServiceCustomer.updateCustomer(query, update, {
					lean: true,
					safe: true
				}, function (err, data) {
					logging.consolelog(err, data, "new assign license to customer");
					if (!err) {
						cb(null);
					} else {
						cb(err);
					}
				})

			} else {
				cb(null);
			}


			},
		function (cb) {
			/* send email to customer*/
			var emailSend = {
				emailId: item.emailId,
				password: res + res1,
				type: item.userType
			}
			var text;
			text = TemplatesSubAgent.register(emailSend);
			NotificationManager.sendEmailToUser(item.emailId, "Congratulations!! Login to www.pixelsed.com", text, function (err, data) {
				if (err) {
					cb(err);
				} else {
					cb(null);
				}
			});
						},
	function (cb) {

			var variableData = {


			}
			NotificationManager.sendNotifications(customerData._id, 'ADD_CUSTOMER', variableData, function (err, data) {

			})
			cb(null);


	}
    ], function (err, data) {
		if (!err) {
			callback(null);
		} else {
			callback(err);
		}
	})
}


var manageCSVData = function (data, callbackRoute) {
	var CSVData = data;
	logging.consolelog("logging","",CSVData);
	var arrangedData = [];
	async.series([
        function (cb) {
			var DataObject = {};
			var constants = [];
			if (CSVData) {
				var taskInSubcat = [];
				for (var key in CSVData) {
					(function (key) {
						taskInSubcat.push((function (key) {
							return function (cb) {
								if (key == 0) {
									for (var i = 0; i < CSVData[key].length; i++) {
										constants.push(CSVData[key][i]);
										if (i == CSVData[key].length - 1) {
											cb();
										}
									}
								} else {

									for (var i = 0; i < CSVData[key].length; i++) {
										DataObject[constants[i]] = CSVData[key][i]
										if (i == CSVData[key].length - 1) {
											arrangedData.push(DataObject);
											logging.consolelog("logging","",arrangedData);
											DataObject = {};
											cb()
										}
									}
								}
							}
						})(key))
					}(key));
				}
				async.parallel(taskInSubcat, function (err, result) {
					logging.consolelog("arrangedData :::::::::::: ", err, result);
					if (err) cb(err)
					else {
						cb(null);
					}
				});
			}
        },
        function (cb) {
			logging.consolelog(arrangedData,"", "CSV Data");
			for (var i = 0; i < arrangedData.length; i++) {
				var Row = i + 1;
				var error = {};
				logging.consolelog(i, arrangedData.length, "test bug")
					//				if (arrangedData[i].agent == ""|| ) {
					//					error.statusCode = 400;
					//					error.customMessage = "Agent Name Cannot Be Empty at Row:" + Row;
					//					error.type = 'CURRICULUM_ERROR_CSV'
					//					cb(error);
					//				}
				if (arrangedData[i].customerName == "") {
					error.statusCode = 400;
					error.customMessage = "Customer Name Field Cannot Be Empty at Row:" + Row
					error.type = 'CURRICULUM_ERROR_CSV'
					cb(error);
				}

				if (arrangedData[i].gender == "") {
					error.statusCode = 400;
					error.customMessage = "Gender Field Cannot Be Empty at Row:" + Row
					error.type = 'CURRICULUM_ERROR_CSV'
					cb(error);
				}

				if (arrangedData[i].emailId == "") {
					error.statusCode = 400;
					error.customMessage = "Email Id Field Cannot Be Empty at Row:" + Row
					error.type = 'CURRICULUM_ERROR_CSV'
					cb(error);
				}
				if (arrangedData[i].userType == "") {
					error.statusCode = 400;
					error.customMessage = "User Type Field Cannot Be Empty at Row:" + Row
					error.type = 'CURRICULUM_ERROR_CSV'
					cb(error);
				}
				if (arrangedData[i].mobileNo == "") {
					error.statusCode = 400;
					error.customMessage = "Mobile Number Field Cannot Be Empty at Row:" + Row
					error.type = 'CURRICULUM_ERROR_CSV'
					cb(error);
				}
				if (i == arrangedData.length - 1) {
					cb()
				}
			}
			// cb(null)
        },
        function (cb) {
			var counter = 0;
			logging.consolelog("arrangedData::","" ,arrangedData);
			async.forEach(arrangedData, function (item, callback) {
				counter++;
				integrateCustomersIntoSystem(item, function (data) {
					if (data) callback(null);
					else {
						callback(null)
					}
				})


			}, function (err) {
				if (counter == arrangedData.length) {
					cb(null);
				}
			});
        }
    ], function (err, response) {
		if (err) callbackRoute(err);
		else callbackRoute(null);
	});
}
var customersUploadViaCSV = function (request, response) {
	async.series([
        function (cb) {
			var csvFile = request.files.UploadCsv;
			if (csvFile && csvFile.originalFilename) {
				var path = Path.resolve("..") + "/uploads/" + "file.csv";
				UploadManager.saveCSVFile(csvFile.path, path, function (err, result) {
					if (err) {
						cb(constants.STATUS_MSG.ERROR.IMP_ERROR)
					} else {
						var file = path;

						fs.readFile(file, function (err, data) { // Read the contents of the file
							if (err) {
								cb(constants.STATUS_MSG.ERROR.IMP_ERROR);
							} else {
								parseCsv(data.toString(), {
									comment: '#'
								}, function (err, data) {

									if (err) {
										cb(err);
									} else {
										if (data != undefined && data != null) {

											manageCSVData(data, function (err, response) {
												if (err) {

													cb(err);
												} else {
													cb()
												}
											})
										} else {
											cb();
										}

									}
								});
							}
						});
					}
				})
			} else {
				cb();
			}
        }
    ], function (err, data) {
		if (!err) {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.DEFAULT);
		} else {
			response.send(err);
		}
	})


}



var getCustomer = function (request, response) {
	var final = [];
	var customersCount = null;
	var userType = [];
	var gender = [];
	var country = [];
	var grade = [];
	var query = {};
	if (request.body.userType&& request.body.userType.length>0) {

		for (var i = 0; i < request.body.userType.length; i++) {
			userType.push(request.body.userType[i].userType);
		}
		if (userType.length > 0) {
			query.userType = {
				$in: userType,
				$nin: [constants.USER_TYPE.AGENT, constants.USER_TYPE.SCHOOL, constants.USER_TYPE.SUB_AGENT]

			}
		}

	} else {
		query.userType = {
			$nin: [constants.USER_TYPE.AGENT, constants.USER_TYPE.SCHOOL, constants.USER_TYPE.SUB_AGENT]
		}
	}
	if (request.body.gender) {
		for (var i = 0; i < request.body.gender.length; i++) {
			gender.push(request.body.gender[i].gender);
		}
		if (gender.length > 0) {
			query.gender = {
				$in: gender
			}
		}

	}
	if (request.body.country) {
		for (var i = 0; i < request.body.country.length; i++) {
			country.push(request.body.country[i].country);
		}
		if (country.length > 0) {
			query.country = {
				$in: country
			}
		}

	}

	if (request.body.grade) {
		for (var i = 0; i < request.body.grade.length; i++) {
			grade.push(request.body.grade[i].grade);
		}
		if (grade.length > 0) {
			query.grade = {
				$in: grade
			}


		}

	}
	if (request.body.fromDate && request.body.toDate) {
		query.registrationDate = {
			$gte: request.body.fromDate,
			$lte: request.body.toDate

		}

	}
	if (request.body.search) {
		// query["$text"] = {
		// 	$search: request.body.search
		// };
		query['$or'] = [
			{
				emailId: {
					$regex: '.*' + request.body.search + '.*',
					$options: "i"
				}
			},
			{
				firstName: {
					$regex: '.*' + request.body.search + '.*',
					$options: "i"
				}
			},
			{
				lastName: {
					$regex: '.*' + request.body.search + '.*',
					$options: "i"
				}
			},
			{
				mobileNo: {
					$regex: '.*' + request.body.search + '.*',
					$options: "i"
				}
			}
		]
	}
	async.series([
        function (cb) {

			var projection = {
				firstName: 1,
				lastName: 1,
				emailId: 1,
				mobileNo: 1,
				gender: 1,
				country: 1,
				grade: 1,
				userType: 1,
				registrationDate: 1,
				addressValue: 1,
				block: 1
			}
			var options = {
				registrationDate: 'desc'
			}

			var setOptions = {
				limit: parseInt(request.body.limit),
				skip: parseInt(request.body.skip),
				lean: true
			};
logging.consolelog("query in get customers",query,projection);

			ServiceCustomer.getAllCustomer(query, projection, options, setOptions, function (err, data) {
				logging.consolelog(err, data, "get ALl custoners");
				if (!err) {
					var i = 0;
					var length = data.length;
					for (i = 0; i < length; i++) {

						final.push({
							customerId: data[i]._id,
							name: data[i].firstName + ' ' + data[i].lastName,
							gender: data[i].gender,
							country: data[i].country,
							userType: data[i].userType,
							grade: data[i].grade,
							curriculum: data[i].curriculum,
							registrationDate: data[i].registrationDate,
							emailId: data[i].emailId,
							mobileNo: data[i].mobileNo,
							location: data[i].addressValue,
							block: data[i].block
						});
					}
					cb(null);
				} else {
					cb(err);
				}
			})

        },
        function (cb) {
			ServiceCustomer.getCustomerCount(query, function (err, data) {
				if (!err) {
					logging.consolelog(err, data, "count");
					customersCount = data;
					cb(null);
				} else {
					cb(err);
				}
			})
        },
        function (cb) {
			ServiceCustomer.getDistinctCustomer({}, 'country', function (err, data) {
				if (!err) {
					for (var i = 0; i < data.length; i++) {
						country.push({
							country: data[i]
						});
					}
					cb(null);
				} else {
					cb(err);
				}

			})
        },
        function (cb) {
			ServiceCustomer.getCustomer({}, {
				'grade': 1
			}, {}, function (err, data) {

				for (var i = 0; i < data.length; i++) {
					if (data[i].grade) {
						for (var j = 0; j < data[i].grade.length; j++) {
							grade.push({
								grade: data[i].grade[j]
							});
						}
					}

				}
				cb(null);
			});
        },
        function (cb) {
			var flags = [],
				output = [],
				l = grade.length,
				i;
			for (i = 0; i < l; i++) {
				if (flags[grade[i].grade]) continue;
				flags[grade[i].grade] = true;
				output.push({
					grade: grade[i].grade
				});
			}
			grade = output;
			cb(null);

        }
    ], function (err, data) {
		if (!err) {
			response.status(200).send({
				data: final,
				statusCode: 200,
				count: customersCount,
				grade: grade,
				country: country
			})
		} else {
			response.send(err);
		}
	})
}


var blockCustomer = function (request, response) {
	var userDetails=request.userData;
	var emailId;
	async.series([
        function (cb) {
			var query = {
				_id: request.body.customerId
			}
			ServiceCustomer.updateCustomer(query, {
				$set: {
					block: true
				}
			}, {}, function (err, data) {
				if (!err) {
					cb(null);
				} else {
					cb(err);
				}
			})
        },
		function (cb) {
			ServiceCustomer.getCustomer({
				_id: request.body.customerId
			}, {}, {}, function (err, data) {
				if (!err) {
					if (data.length > 0) {
						emailId = data[0].emailId
					}
					cb(null)
				} else {
					cb(err);
				}
			})
		},
	function (cb) {

			var emailSend = {
				link: constants.COMMON_VARIABLES.link,
				action: 'blocked'
			}
			var text;
			text = TemplatesBlock.action(emailSend);
			NotificationManager.sendEmailToUser(emailId, 'Blocked by admin!!', text, function (err, data) {
				if (!err) {
					logging.consolelog('mail', err, data);
				}

			});

			cb(null);
		}


    ], function (err, result) {
		if (!err) {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.CUSTOMER_BLOCKED);
		} else {
			response.send(err);
		}
	})
}


var unblockCustomer = function (request, response) {
	var emailId;
	async.series([
        function (cb) {
			var query = {
				_id: request.body.customerId
			}
			ServiceCustomer.updateCustomer(query, {
				$set: {
					block: false
				}
			}, {}, function (err, data) {
				if (!err) {
					cb(null);
				} else {
					cb(err);
				}
			})
        },
		function (cb) {
			ServiceCustomer.getCustomer({
				_id: request.body.customerId
			}, {}, {}, function (err, data) {
				if (!err) {
					if (data.length > 0) {
						emailId = data[0].emailId
					}
					cb(null)
				} else {
					cb(err);
				}
			})
		},
	function (cb) {

			var emailSend = {
				link: constants.COMMON_VARIABLES.link,
				action: 'unblocked'
			}
			var text;
			text = TemplatesBlock.action(emailSend);
			NotificationManager.sendEmailToUser(emailId, 'Unblocked by admin!!', text, function (err, data) {
				if (!err) {
					logging.consolelog('mail', err, data);
				}

			});

			cb(null);




		}
    ], function (err, result) {
		if (!err) {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.CUSTOMER_UNBLOCKED);
		} else {
			response.send(err);
		}
	})
}


// var categoryList = function (item, callback) {
// 	var categoryName = [];
// 	var contentName = [];
// 	var query = {
// 		topicId: item
// 	};
// 	var path = 'categoryId';
// 	var select = 'name content isDeleted'
// 	var populate = {
// 		path: path,
// 		match: {},
// 		select: select,
// 		options: {
// 			lean: true
// 		}
// 	};
// 	var options = {
//
// 	};
// 	var projection = {
// 		categoryId: 1
// 	}
//
// 	ServiceTopicBinding.topicBindingPopulate(query, projection, populate, {}, {}, function (err, data) {
// 		logging.consolelog(err, data, "category Dtaa");
// 		if (!err) {
// 			for (var i = 0; i < data.length; i++) {
// 				if (data[i].categoryId.name == 'Video' && data[i].categoryId.isDeleted == false) {
// 					for (var j = 0; j < data[i].categoryId.content.length; j++) {
// 						if (data[i].categoryId.content[j].isDeleted == false && data[i].categoryId.content[j].isVideo == true) {
// 							contentName.push(data[i].categoryId.content[j].typeOfVideo)
// 						}
// 					}
// 				} else if (data[i].categoryId.isDeleted == false) {
// 					categoryName.push(data[i].categoryId.name);
// 				}
//
// 			}
// 			callback(null, {
// 				contentName: contentName,
// 				categoryName: categoryName
// 			});
// 		} else {
// 			callback(err);
// 		}
// 	});
//
// }
var topicListingForPublishing = function (request, response) {
	var list = [];
	var topicListObj = {};
	var topicList = [];
	var language = [];
	var languageList = [];
	var boardList = [];
	var gradeList = [];
	var subjectList = [];
	var chapterList = [];
	var count = null;
	var board = [];
	var grade = [];
	var subject = [];
	var chapter = [];
	var topicId = [];
	var categoryName = [];
	var contentName = [];
	var curriculumId = [];


	if (request.body.language) {
		for (var i = 0; i < request.body.language.length; i++) {
			language.push(request.body.language[i].language)
		}
	}
	if (request.body.board) {
		for (var i = 0; i < request.body.board.length; i++) {
			board.push(request.body.board[i].board)
		}
	}
	if (request.body.grade) {
		for (var i = 0; i < request.body.grade.length; i++) {
			grade.push(request.body.grade[i].grade)
		}
	}
	if (request.body.subject) {
		for (var i = 0; i < request.body.subject.length; i++) {
			subject.push(request.body.subject[i].subject)
		}

	}
	if (request.body.chapter) {
		for (var i = 0; i < request.body.chapter.length; i++) {
			chapter.push(request.body.chapter[i].chapter)
		}
	}

	async.auto({
		board_list: function (cb) {
			var query = {};
			if (language.length > 0) {
				query.language = {
						$in: language
					},
					query.isRemoved = false;
				ServiceCurriculum.getUniqueCurriculum(query, 'board', function (err, data) {
					if (!err) {
						for (var i = 0; i < data.length; i++) {
							boardList.push({
								board: data[i].board
							});
						}

						cb(null);
					} else {
						cb(err);

					}
				})
			} else {
				cb(null);
			}
		},
		grade_list:function (cb) {
			var query = {};
			if (language.length > 0 && board.length > 0) {
				query.language = {
					$in: language
				}
				query.isRemoved = false;
				query.board = {
					$in: board
				}
				ServiceCurriculum.getUniqueCurriculum(query, 'grade', function (err, data) {
					if (!err) {
						for (var i = 0; i < data.length; i++) {
							gradeList.push({
								grade: data[i].grade
							});
						}

						cb(null);
					} else {
						cb(err);

					}
				})
			} else {
				cb(null);
			}
		},
		subject_list: function (cb) {
			var query = {};
			if (language.length > 0 && board.length > 0 && grade.length > 0) {
				query.language = {
					$in: language
				}
				query.isRemoved = false;
				query.board = {
					$in: board
				}
				query.grade = {
					$in: grade
				}
				ServiceCurriculum.getUniqueCurriculum(query, 'subject', function (err, data) {
					if (!err) {
						for (var i = 0; i < data.length; i++) {
							subjectList.push({
								subject: data[i].subject
							});
						}

						cb(null);
					} else {
						cb(err);

					}
				})
			} else {
				cb(null);
			}
		},
		chapter_list: function (cb) {
			var query = {};
			if (language.length > 0 && board.length > 0 && grade.length > 0 && subject.length > 0) {
				query.language = {
					$in: language
				}
				query.isRemoved = false;
				query.board = {
					$in: board
				}
				query.grade = {
					$in: grade
				}
				query.subject = {
					$in: subject
				}
				ServiceCurriculum.getUniqueCurriculum(query, 'chapter', function (err, data) {
					if (!err) {
						for (var i = 0; i < data.length; i++) {
							chapterList.push({
								chapter: data[i].chapter
							});
						}

						cb(null);
					} else {
						cb(err);

					}
				})
			} else {
				cb(null);
			}
		},
		language_list: function (cb) {
			ServiceCurriculum.getUniqueCurriculum({
				isRemoved: false
			}, 'language', function (err, data) {
				if (!err) {
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							languageList.push({
								language: data[i].language
							});
						}
						cb(null);
					} else {
						cb(constants.STATUS_MSG.ERROR.EMPTY_CURRICULUM);
					}

				} else {
					cb(err);
				}
			});
		},
		curriculum_list: function (cb) {

			var projection = {
				'result.topicId': 1

			}
			var query = {};
			if (language.length > 0) {
				query.language = {
						$in: language
					},
					query.isRemoved = "false";

			}
			if (board.length > 0) {

				query.board = {
					$in: board
				}
			}
			if (grade.length > 0) {

				query.grade = {
					$in: grade
				}
			}
			if (subject.length > 0) {

				query.subject = {
					$in: subject
				}
			}
			if (chapter.length > 0) {

				query.chapter = {
					$in: chapter
				}
			}
			if (!(language.length > 0)) {
				query.board = constants.COMMON_VARIABLES.UNIVERSAL_BOARD_NAME;
				query.isRemoved = "false";

			}

			logging.consolelog("query", query);
			var limit = {
				limit: request.body.limit ? request.body.limit : 10,
				offset: request.body.skip ? request.body.skip : 0
			}
			ServiceCurriculum.getCurriculumAndTopic(query, limit, {}, function (err, data) {
				if (!err) {
					if (data.length) {
						for (var i = 0; i < data.length; i++) {
							topicListObj[data[i].name] = {
								language: data[i].topicLanguage.split(","),
								topicName: data[i].name,
								topicId: data[i]["_id"],
								board: data[i].board,
								grade: data[i].grade,
								chapter: data[i].chapter,
								subject: data[i].subject
							}
							topicId = topicId.concat(data[i].ids.split(","));
						}
						for (var i in topicListObj) {
							topicList.push(topicListObj[i]);
						}
						ServiceCurriculum.getCurriculumAndTopicCount(query, limit, {}, function (err, data) {
							if (!err) {
								logging.consolelog("logging","",data[0]);
								if (data.length && data[0].cnt) {
									count = data[0].cnt;
								}
								cb(null);
							} else {
								cb(err);
							}
						});
					} else {
						cb(null)
					}
				} else {
					cb(err);
				}
			});

		},
		//		topic_categories: ['curriculum_list', function (cb) {
		//
		//			topicId = _.unique(topicId);
		//			if (topicId.length > 0) {
		//				async.forEach(topicId, function (item, callback) {
		//					categoryList(item, function (err, result) {
		//
		//						if (!err) {
		//							categoryName.push(result.categoryName);
		//							contentName.push(result.contentName);
		//							callback(null);
		//						} else {
		//							callback(err);
		//						}
		//					});
		//				}, function (err) {
		//					if (!err) {
		//
		//						cb(null);
		//					} else {
		//
		//						cb(err);
		//					}
		//				});
		//			} else {
		//				cb(null);
		//			}
		//
		//        }],
		unique:function (cb) {
				ServiceTopicCategory.getTopicCategory({
					isDeleted: false
				}, {}, {}, function (err, data) {
					if (!err) {
						for (var i = 0; i < data.length; i++) {
							if (data[i].name == 'Video' && data[i].isDeleted == false) {
								for (var j = 0; j < data[i].content.length; j++) {
									if (data[i].content[j].isDeleted == false && data[i].content[j].isVideo == true) {
										contentName.push(data[i].content[j].typeOfVideo)
									}
								}
							} else if (data[i].isDeleted == false) {
								categoryName.push(data[i].name);
							}

						}

						contentName = _.uniq(contentName);
						categoryName = _.uniq(categoryName);
						cb(null);
					} else {
						cb(err);
					}
				})

			}
			//		unique_category_list: ['topic_categories', function (cb) {
			//			categoryName = categoryName.toString().split(',');
			//			categoryName = _.unique(categoryName);
			//			contentName = contentName.toString().split(',');
			//			contentName = _.unique(contentName);
			//			cb(null);
			//        }]
	}, function (err, data) {
		if (!err) {
			response.status(200).send({
				categoryList: categoryName,
				contentList: contentName,
				statusCode: 200,
				boardList: boardList,
				gradeList: gradeList,
				chapterList: chapterList,
				subjectList: subjectList,
				topicList: topicList,
				languageList: languageList,
				count: count

			})
		} else {
			response.send(err);
		}
	})
}

var publishTopics = function (request, response) {
	logging.consolelog("request","",request.body);
	var topicIds = [];
	var language = [];
	for (var i = 0; i < request.body.language.length; i++) {
		language.push(request.body.language[i].language);
	}
	var board = [];
	var grade = [];
	var subject = [];
	var chapter = [];
	var emailId = [];
	if (request.body.board) {
		for (var i = 0; i < request.body.board.length; i++) {
			board.push(request.body.board[i].board)
		}
	}
	if (request.body.grade) {
		for (var i = 0; i < request.body.grade.length; i++) {
			grade.push(request.body.grade[i].grade)
		}
	}
	if (request.body.subject) {
		for (var i = 0; i < request.body.subject.length; i++) {
			subject.push(request.body.subject[i].subject)
		}

	}
	if (request.body.chapter) {
		for (var i = 0; i < request.body.chapter.length; i++) {
			chapter.push(request.body.chapter[i].chapter)
		}
	}


	async.auto({
		select_all_functionality: function (cb) {

			if (request.body.selectAll) {
				var projection = {
					'result.topicId': 1

				}
				var query = {};
				if (language.length > 0) {
					query.language = {
							$in: language
						},
						query.isRemoved = "false";

				}
				if (board.length > 0) {

					query.board = {
						$in: board
					}
				}
				if (grade.length > 0) {

					query.grade = {
						$in: grade
					}
				}
				if (subject.length > 0) {

					query.subject = {
						$in: subject
					}
				}
				if (chapter.length > 0) {

					query.chapter = {
						$in: chapter
					}
				}
				if (!(language.length > 0)) {
					query.board = constants.COMMON_VARIABLES.UNIVERSAL_BOARD_NAME;
					query.isRemoved = "false";

				}

				logging.consolelog("query","", query);
				var limit = {
					limit: request.body.limit ? request.body.limit : 10000,
					offset: request.body.skip ? request.body.skip : 0
				}
				ServiceCurriculum.getCurriculumAndTopic(query, limit, {}, function (err, data) {
					if (!err) {
						if (data.length) {
							for (var i = 0; i < data.length; i++) {
								request.body.topicNames.push(data[i].name);
							}
							cb(null);

						} else {
							cb(err);
						}
					} else {
						cb(err);
					}
				});
			} else {
				cb(null);
			}
		},
		topic_Id: function (cb) {
				var query = {
					name: {
						$in: request.body.topicNames
					},
					language: {
						$in: language
					}
				}
				ServiceTopic.getTopic(query, {}, {}, function (err, data) {
					if (!err) {
						for (var i = 0; i < data.length; i++) {
							topicIds.push(data[i]._id);
						}
						cb(null);
					} else {
						cb(err);
					}
				})
		},
			publish_topics: ['topic_Id', function (cb) {
				var query = {
					_id: {
						$in: topicIds
					}
				}

				ServiceTopic.updateTopic(query, {
					isPublish: true
				}, {
					multi: true
				}, function (err, data) {

					if (!err) {
						cb(null);
					} else {
						cb(err);
					}
				})
        }],
			publish_category: ['topic_Id', function (cb) {
				logging.consolelog("topic id::","" ,topicIds);
				var query = {
					topicId: {
						$in: topicIds
					}
				}
				var path = 'categoryId';
				var select = 'name'
				var populate = {
					path: path,
					match: {},
					select: select,
					options: {
						lean: true
					},
				};
				var options = {

				};
				var projection = {
					categoryId: 1,
					topicId: 1

				}
				ServiceTopicBinding.getAllTopicBinding(query, projection, populate, {}, {}, function (err, data) {
					logging.consolelog(err, data, "data of category")
					if (!err) {
						for (var i = 0; i < data.length; i++) {
							logging.consolelog('check>>>>>>content',request.body.categoryList.indexOf(data[i].categoryId.name) ,request.body.contentList.length)
							if ((request.body.categoryList.indexOf(data[i].categoryId.name)!= -1) ||request.body.contentList.length) {
								ServiceTopicCategory.updateTopicCategory({
									_id: data[i].categoryId._id
								}, {
									$set: {
										isPublish: true
									}
								}, {}, function (err, data) {
									logging.consolelog(err, data, "category publish");
								})
							}
							else {
								continue;
							}

						}
						cb(null);

					} else {
						cb(err);
					}

				})
        }],
			publish_content: ['topic_Id', function (cb) {
				if (request.body.contentList) {
					var query = {
						topicId: {
							$in: topicIds
						}
					}
					var path = 'categoryId';
					var select = 'name content'
					var populate = {
						path: path,
						match: {},
						select: select,
						options: {
							lean: true
						},
					};
					var options = {

					};
					var projection = {
						categoryId: 1,
						topicId: 1

					}
					ServiceTopicBinding.getAllTopicBinding(query, projection, populate, {}, {}, function (err, data) {
						if (!err) {
							for (var i = 0; i < data.length; i++) {
								if (data[i].categoryId.name == 'Video') {
									for (var j = 0; j < data[i].categoryId.content.length; j++) {
										if (request.body.contentList.indexOf(data[i].categoryId.content[j].typeOfVideo) == -1) {

										} else {
											var query = {
												_id: data[i].categoryId._id,
												content: {
													$elemMatch: {
														_id: data[i].categoryId.content[j]._id
													}
												}
											}
											var dataToupdate = {
												$set: {
													'content.$.isPublish': true
												}
											}
											ServiceTopicCategory.updateTopicCategory(query, dataToupdate, {}, function (err, data) {
												logging.consolelog(err, data, "content publish");
											})

										}
									}

								}

							}
							cb(null);

						} else {
							cb(err);
						}

					})
				} else {
					cb(null);
				}
        }],
		get_customers: ['publish_content',function (cb) {

			ServiceCustomer.getCustomer({
				userType: constants.USER_TYPE.STUDENT
			}, {
				emailId: 1
			}, {}, function (err, data) {

				if (!err) {
					for (var i = 0; i < data.length; i++) {
						emailId.push(data[i].emailId);
					}
					cb(null);
				} else {
					cb(err);
				}
			})
		}],
		email: ['get_customers', function (cb) {
			for (var i = 0; i < emailId.length; i++) {
				var emailSend = {
					emailId: emailId[i],
					link: constants.COMMON_VARIABLES.link,
					topics: request.body.topicNames
				}
				var text;
				text = TemplatesPublish.package(emailSend);
				NotificationManager.sendEmailToUser(emailId[i], 'New Content Added!!', text, function (err, data) {
					if (!err) {
						logging.consolelog('mail', err, data);
					}

				});
			}
			cb(null);
		}]
	},
	function (err, data) {
		if (!err) {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.PUBLISH_SUCCESSFULLY)
		} else {
			response.send(err);
		}
	})
}



var customerDetail = function (request, response) {
	var details = null;
	var buyDetails = [];
	var packageDetails = [];
	var packages = {};
	async.auto({
		get_customer_data:function (cb) {
			var projection = {
				profilePicUrl: 1,
				emailId: 1,
				userType: 1,
				grade: 1,
				addressValue: 1,
				curriculum: 1,
				sessionTo: 1,
				sessionFrom: 1,
				mobileNo: 1,
				registrationDate: 1,
				dob: 1,
				gender: 1,
			}
			ServiceCustomer.getCustomer({
				_id: request.body.customerId
			}, projection, {}, function (err, data) {
				if (!err) {
					if (data.length > 0) {
						details = data[0];
					}
					cb(null);
				} else {
					cb(err);
				}
			})
		},
		purchased_data: function (cb) {
			var query = {
				customerId: request.body.customerId
			}

			var populate = [{
				path: 'package.packageId',
				match: {},
				select: 'curriculum language grade',
				options: {
					lean: true
				}
            }];
			ServiceCustomerBuyPackage.getPackages(query, {
				packageName: 1,
				topic: 1,
				buyDate: 1,
				endDate: 1,
				payments: 1,
				topicPayments: 1,
				package: 1
			}, populate, {}, function (err, data) {
				//                logging.consolelog(err, data, data[0].package);
				if (!err) {
					for (var i = 0; i < data.length; i++) {
						if (data[i].topic.length > 0) {
							buyDetails.push({
								packageName: data[i].packageName,
								noOfTopics: data[i].topic.length,
								purchasedDate: data[i].buyDate,
								expiredOn: data[i].topic[0].endDate,
								topicPayments: data[i].topicPayments.totalPrice,
								packageId: data[i]._id,
								topic: true
							});
						}
						for (var j = 0; j < data[i].package.length; j++) {
							packageDetails.push({
								board: data[i].package[j].packageId.curriculum,
								grade: data[i].package[j].packageId.grade,
								"curriculum.language": data[i].package[j].packageId.language
							})
							packages[data[i].package[j].packageId.curriculum + "||" + data[i].package[j].packageId.grade + "||" + data[i].package[j].packageId.language + "||" + data[i].package[j].packageId._id] = {
								packageId: data[i].package[j].packageId._id,
								packageName: data[i].package[j].packageId.grade,
								topic: false,
								purchasedDate: data[i].buyDate,
								expiredOn: data[i].package[j].endDate,
								topicPayments: data[i].package[j].finalPrice

							}
						}

					}
					ServicePackage.getPackageDetails(packageDetails, function (err, data) {
						if (data && data.length) {
							for (var i in data) {
								var key = data[i].board + "||" + data[i].grade + "||" + data[i].language;
								for (var j in packages) {
									if (j.indexOf(key) != -1) {
										packages[j]["price"] = data[i].price;
										packages[j]["noOfTopics"] = data[i].topicCount;
										packages[j]["subjectCount"] = data[i].subjectCount;
										packages[j]["chapterCount"] = data[i].chapterCount;
										packages[j]["board"] = data[i].board;
										packages[j]["grade"] = data[i].grade;
										packages[j]["language"] = data[i].language;
									}
								}

							}
						}

						for (var k in packages) {
							buyDetails.push({
								packageName: packages[k].packageName,
								noOfTopics: packages[k].noOfTopics,
								purchasedDate: packages[k].purchasedDate,
								expiredOn: packages[k].expiredOn,
								topicPayments: packages[k].topicPayments,
								packageId: packages[k].packageId,
								topic: false

							})
						}

						cb(null);

					})
				} else {
					cb(err);
				}
			})

		}
	}, function (err, data) {
		if (!err) {
			response.status(200).send({
				statusCode: 200,
				details: details,
				buyDetails: buyDetails
			})
		} else {
			response.send(err);
		}
	})
}


var packageDetails = function (request, response) {
	var topicIds = [];
	var package = {};
	var topicListObj = {};
	var topicList = [];
	async.auto({
		get_detail: function (cb) {
			if (request.body.topic) {
				ServiceCustomerBuyPackage.getTransaction({
					_id: request.body.packageId
				}, {
					topic: 1
				}, {
					lean: true
				}, function (err, data) {
					if (!err) {
						for (var i = 0; i < data[0].topic.length; i++) {
							topicIds.push(data[0].topic[i].topicId);
						}
						cb(null);
					} else {
						cb(err);
					}
				});
			} else {
				var query = {
					_id: request.body.packageId
				}
				var projection = {
					curriculum: 1,
					grade: 1,
					language: 1
				}
				ServicePackage.getPackageDetails(query, projection, {
					lean: true
				}, function (err, data) {
					if (!err) {
						if (data.length > 0) {
							package.curriculum = data[0].curriculum;
							package.grade = data[0].grade;
							package.language = data[0].language;
						}
						cb(null);
					} else {
						cb(err);
					}
				})

			}
		},
		get_topic_details:  function (cb) {
			if (request.body.topic) {
				var query = {
					"topic._id": {
						$in: topicIds
					},
					isUniversal: true
				}
				ServiceTopicCurriculumMapper.getTopicAndCurriculumDetails(query, function (err, data) {
					if (!err) {
						if (data.length) {
							for (var i = 0; i < data.length; i++) {

								topicListObj[data[i].name] = {
									language: data[i].topicLanguage,
									topicName: data[i].name,
									board: data[i].board,
									grade: data[i].grade,
									chapter: data[i].chapter,
									subject: data[i].subject
								}
							}
							for (var i in topicListObj) {
								topicList.push(topicListObj[i]);
							}
							cb(null);
						} else {
							cb(null)
						}
					} else {
						cb(err);
					}
				})

			} else {
				var query = {};
				query.board = package.curriculum;
				query.language = package.language;
				query.grade = package.grade;

				var limit = {
					limit: request.body.limit ? request.body.limit : 10,
					offset: request.body.skip ? request.body.skip : 0
				}
				ServiceCurriculum.getCurriculumAndTopic(query, limit, {}, function (err, data) {
					if (!err) {
						if (data.length) {
							for (var i = 0; i < data.length; i++) {
								topicListObj[data[i].name] = {
									language: data[i].topicLanguage.split(","),
									topicName: data[i].name,
									board: data[i].board,
									grade: data[i].grade,
									chapter: data[i].chapter,
									subject: data[i].subject
								}
							}
							for (var i in topicListObj) {
								topicList.push(topicListObj[i]);
							}
							cb(null);
						} else {
							cb(null)
						}
					} else {
						cb(err);
					}
				})



			}

        }
	}, function (err, data) {
		if (!err) {
			response.status(200).send({
				topicList: topicList,
				statusCode: 200
			})
		} else {
			response.send(err);
		}
	})
}



module.exports = {
	customersUploadViaCSV: customersUploadViaCSV,
	getCustomer: getCustomer,
	blockCustomer: blockCustomer,
	unblockCustomer: unblockCustomer,
	topicListingForPublishing: topicListingForPublishing,
	publishTopics: publishTopics,
	customerDetail: customerDetail,
	packageDetails: packageDetails


}