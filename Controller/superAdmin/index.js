/**
 * Created by sterlingpixels on 18/03/17.
 */
module.exports = {
    superAdminCGCSModule: require('./superAdminCGCSModule'),
    superAdminManageAgents: require('./superAdminManageAgents'),
    superAdminManageContent: require('./superAdminManageContent'),
    superAdminManageCustomers: require('./superAdminManageCustomers'),
    superAdminManagePackages: require('./superAdminManagePackages'),
    superAdminTopicModule: require('./superAdminTopicModule'),
    superAdminOnboarding: require('./superAdminOnboarding'),
    superAdminOfflineManagement: require('./superAdminOfflineManagement')
};