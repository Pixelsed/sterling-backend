// // *
//  * Created by priya on 8/3/16.
//  */

'use strict';
var async = require('async');
var ServiceCurriculum = require('../../Service/curriculum');
var ServiceCustomer = require('../../Service/customer');
var ServiceTopic = require('../../Service/topic');
var ServiceTopicCategory = require('../../Service/topicCategory');
var ServiceTopicBinding = require('../../Service/topicBinding');
var ServiceChildMapper = require('../../Service/childParentMapper');
var constants = require('../../Library/appConstants');
var fs = require('fs');
var parseCsv = require('csv-parse');
var Path = require('path');
var Templates = require('../../Templates/superAdminRegister');
var TemplatesForAgent = require('../../Templates/subAgentRegister');
var UploadManager = require('../../Library/uploadManager');
var NotificationManager = require('../../Library/notificationManager');
var ServiceCustomerBuyPackages = require('../../Service/customerBuyPackages');
var AwsConfig = Config.get('awsConfig');
var UniversalFunctions = require('../../Library/universalFuctions');
var _ = require('underscore');
var ServiceCustomerBuyPackage = require('../../Service/customerBuyPackages');
var ServiceAssignDataToCustomer = require('../../Service/assignDataToCustomer');
var ServiceTempAgentAssignData= require('../../Service/tempAgentAssignData');
var pendingAgents = function (request, response) {
	var query = {};
	var country = [];

	var final = [];
	var countries = [];
	async.auto({
		get_bookings: function (cb) {
			/* Getting agents who has requested for purchase from admin.. */
			var query = {
				paymentStatus: constants.DATABASE.STATUS.PENDING
			}
			var path = 'customerId';
			var select = 'firstName lastName emailId mobileNo addressValue country registrationDate'
			var populate = {
				path: path,
				match: {},
				select: select,
				options: {
					lean: true
				}
			};
			if (request.body.country) {
				for (var i = 0; i < request.body.country.length; i++) {
					country.push(request.body.country[i].country)
				}
				if (country.length > 0) {
					populate["match"]["country"] = {
						$in: country
					}
				}
			}
			populate["match"]["userType"] = constants.USER_TYPE.AGENT;

			if (request.body.registrationDateTo && request.body.registrationDateFrom) {
				populate["match"]["registrationDate"] = {
					$gte: request.body.registrationDateTo,
					$lte: request.body.registrationDateFrom

				}

			}


			if (request.body.search) {
				populate["match"]["$text"] = {
					$search: request.body.search
				};
			}
			var options = {
				buyDate:-1
			};
			var projection = {
				customerId: 1,
				transactionId: 1,
				buyDate: 1,
				packageName: 1,
				bookingStatus: 1
			}
			logging.consolelog(query, projection, populate)
			ServiceCustomerBuyPackage.getPackages(query, projection, populate, options, function (err, data) {
				logging.consolelog("logging",err, JSON.stringify(data));
				if (!err) {
					for (var i = 0; i < data.length; i++) {
						if (data[i].customerId != null) {
							final.push({
								bookingId: data[i]._id,
								customerId: data[i].customerId._id,
								name: data[i].customerId.firstName + ' ' + data[i].customerId.lastName,
								country: data[i].customerId.country,
								userType: data[i].customerId.userType,
								registrationDate: data[i].customerId.registrationDate,
								emailId: data[i].customerId.emailId,
								mobileNo: data[i].customerId.mobileNo,
								location: data[i].customerId.addressValue,
								block: data[i].customerId.block,
								transactionId: data[i].transactionId,
								packageName: data[i].packageName,
								bookingDate: data[i].buyDate,
								bookingStatus: data[i].bookingStatus
							})
						}


					}
					cb(null)
				} else {
					cb(err);
				}
			})



		},
		get_countries: ['get_bookings', function (cb) {
			/* Get distinct countries */
			ServiceCustomer.getDistinctCustomer({}, 'country', function (err, data) {
				if (!err) {
					for (var i = 0; i < data.length; i++) {
						countries.push({
							country: data[i]
						});
					}
					cb(null);
				} else {
					cb(err);
				}

			})
        }]
	}, function (err, data) {
		if (!err) {
			response.status(200).send({
				final: final,
				statusCode: 200,
				country: countries
			})
		} else {
			response.send(err);
		}
	})
}


var approvedAgents = function (request, response) {
	var query = {};
	var country = [];

	var final = [];
	var countries = [];
	async.auto({
		get_bookings: function (cb) {
			/* Getting agents whose  purchased request has accepted by super admin. */
			var query = {
				paymentStatus: constants.DATABASE.STATUS.COMPLETED
			};
			if(request.body.search){
				query['$or']=[
					{
						packageName:{
							$regex: '.*' + request.body.search + '.*',
								$options: "i"
						}
					},
					{
						transactionId:{
							$regex: '.*' + request.body.search + '.*',
								$options: "i"
						}
					}
				]
			}
			var path = 'customerId';
			var select = 'firstName lastName emailId mobileNo addressValue country registrationDate'
			var populate = {
				path: path,
				match: {},
				select: select,
				options: {
					lean: true
				}
			};
			if (request.body.country) {
				for (var i = 0; i < request.body.country.length; i++) {
					country.push(request.body.country[i].country)
				}
				if (country.length > 0) {
					populate["match"]["country"] = {
						$in: country
					}
				}
			}
			populate["match"]["userType"] = constants.USER_TYPE.AGENT;

			if (request.body.registrationDateTo && request.body.registrationDateFrom) {
				populate["match"]["registrationDate"] = {
					$gte: request.body.registrationDateTo,
					$lte: request.body.registrationDateFrom

				}

			}


			if (request.body.search) {
				// populate["match"]["$text"] = {
				// 	$search: request.body.search
				// };
				populate["match"]={
					"$or":[
						{
							firstName: {
								$regex: '.*' + request.body.search + '.*',
								$options: "i"
							}
						},
						{
							lastName: {
								$regex: '.*' + request.body.search + '.*',
								$options: "i"
							}
						},
						{
							emailId: {
								$regex: '.*' + request.body.search + '.*',
								$options: "i"
							}
						},
						{
							mobileNo: {
								$regex: '.*' + request.body.search + '.*',
								$options: "i"
							}
						},
						{
							registrationDate: {
								$regex: '.*' + request.body.search + '.*',
								$options: "i"
							}
						},
					]
				}
			}
			var options = {
				buyDate:-1
			};
			var projection = {
				customerId: 1,
				transactionId: 1,
				buyDate: 1,
				packageName: 1,
				bookingStatus: 1
			}
			logging.consolelog(query, projection, populate)
			ServiceCustomerBuyPackage.getPackages(query, projection, populate, options, function (err, data) {
				logging.consolelog("------->get_bookings::", err, JSON.stringify(data));
				if (!err) {
					for (var i = 0; i < data.length; i++) {
						if (data[i].customerId != null) {
							final.push({
								bookingId: data[i]._id,
								customerId: data[i].customerId._id,
								name: data[i].customerId.firstName + ' ' + data[i].customerId.lastName,
								country: data[i].customerId.country,
								userType: data[i].customerId.userType,
								registrationDate: data[i].customerId.registrationDate,
								emailId: data[i].customerId.emailId,
								mobileNo: data[i].customerId.mobileNo,
								location: data[i].customerId.addressValue,
								block: data[i].customerId.block,
								transactionId: data[i].transactionId,
								packageName: data[i].packageName,
								bookingDate: data[i].buyDate,
								bookingStatus: data[i].bookingStatus
							})
						}


					}
					cb(null)
				} else {
					cb(err);
				}
			})



		},
		get_countries: ['get_bookings', function (cb) {
			ServiceCustomer.getDistinctCustomer({}, 'country', function (err, data) {
				/* Get distinct countries */
				if (!err) {
					for (var i = 0; i < data.length; i++) {
						countries.push({
							country: data[i]
						});
					}
					cb(null);
				} else {
					cb(err);
				}

			})
        }]
	}, function (err, data) {
		if (!err) {
			response.status(200).send({
				final: final,
				statusCode: 200,
				country: countries
			})
		} else {
			response.send(err);
		}
	})
}

var addAgents = function (request, response) {
	var filename1 = null;
	var customerId = null;
	request.body.password = UniversalFunctions.encryptData(UniversalFunctions.generateRandomStringAndNumbers());
	async.auto({
		unique_check_verify: 											/*
			check unique check of email Id
			*/
        function (cb) {
				if (request.body.socialId) {
					var query = {
						$or: [{
							emailId: request.body.emailId
                    }, {
							socialId: request.body.socialId
                    }]
					}
					ServiceCustomer.getCustomer(query, {}, {
						lean: true
					}, function (error, data) {
						logging.consolelog("-------->getCustomer", error, data);
						if (error) {
							cb(error);
						} else {
							if (data && data.length > 0) {
								if (data[0].phoneVerified == true) {
									cb(constants.STATUS_MSG.ERROR.USER_ALREADY_REGISTERED);
								} else {
									ServiceCustomer.deleteCustomer({
										_id: data[0]._id
									}, function (err, updatedData) {
										if (err) cb(err)
										else cb(null);
									});
								}
							} else {
								cb(null);
							}
						}
					});
				} else {
					var query = {
						$or: [{
							emailId: request.body.emailId
                    }]
					};
					ServiceCustomer.getCustomer(query, {}, {
						lean: true
					}, function (error, data) {
							logging.consolelog("-------->getCustomer", error, data);
						if (error) {
							cb(error);
						} else {
							if (data && data.length > 0) {
								if (data[0].phoneVerified == true) {
									cb(constants.STATUS_MSG.ERROR.USER_ALREADY_REGISTERED)
								} else {
									ServiceCustomer.deleteCustomer({
										_id: data[0]._id
									}, function (err, updatedData) {
										if (err) cb(err)
										else cb(null);
									})
								}
							} else {
								cb(null);
							}
						}
					});
				}

        },
		profile_url: ['unique_check_verify', function (cb) {
			
				/*
			Upload Pic to s3 Bucket
			*/
			logging.consolelog('request file',request.files)
			if (request.files) {
				UploadManager.uploadPicToS3Bucket(request.files.profilePic,  AwsConfig.s3BucketCredentials.folder.profile, function (filename) {
					logging.consolelog("-------->uploading add agent:::","",filename);
					if (filename == "No File") {
						cb(constants.STATUS_MSG.ERROR.UPLOAD_IMAGE_ERROR);
					} else {
						filename1 = AwsConfig.s3BucketCredentials.s3URL +'/'+ AwsConfig.s3BucketCredentials.folder.profile+'/'+ filename;
						cb(null);
					}
				});
			} else {
				cb(null);
			}
		}],
		add_agent: ['profile_url', function (cb) {

			/*
			Add agent in the database()
			*/
			var dataToSave = {};
			dataToSave.mobileNo = request.body.mobileNo;
			dataToSave.registrationDate = new Date().toISOString();
			dataToSave.lastName = request.body.lastName;
			dataToSave.firstName = request.body.firstName;
			dataToSave.profilePicUrl = filename1;
			dataToSave.domainName = request.body.domainName;
			dataToSave.addressValue = request.body.addressValue;
			dataToSave.state=request.body.state;
			dataToSave.zip=request.body.zip;
			dataToSave.city= request.body.city;
			// dataToSave.gender= request.body.gender;

			if (request.body.longitude && request.body.latitude) {
				dataToSave.location = {
					"coordinates": [request.body.longitude, request.body.latitude],
					"type": "Point"
				}
			} else {
				request.body.longitude = 0;
				request.body.latitude = 0;
				dataToSave.location = {
					"coordinates": [request.body.longitude, request.body.latitude],
					"type": "Point"

				}
			}
			dataToSave.password = request.body.password;
			dataToSave.emailId = request.body.emailId;
			dataToSave.countryCode = request.body.countryCode;
			dataToSave.userType = request.body.userType;
			dataToSave.phoneVerified = true;
			dataToSave.isApproved = true;
			dataToSave.accessRights = constants.AGENT_ACCESS_RIGHTS;


			dataToSave.country = request.body.country;
			if (request.body.accountNumber) {
				dataToSave.accountDetails = [];
				dataToSave.accountDetails.push({
					accountNumber: UniversalFunctions.encryptData(request.body.accountNumber),
					ifscCode: request.body.ifscCode,
					bankName: request.body.bankName,
					branchName: request.body.branchName,
					isActive: false
				})
			}
			ServiceCustomer.createCustomer(dataToSave, function (err, data) {
				logging.consolelog("------------->create customer-->",err, data);
				if (!err) {
					customerId = data._id;
					cb(null);
				} else {
					cb(constants.STATUS_MSG.ERROR.ALREADY_REGISTERED_EMAIL);
				}
			})
        }],
		add_account_info: ['add_agent', function (cb) {
			var dataToSave = {};
      /*
			License update()
			*/
			dataToSave.license = [];
			dataToSave.license.push({
				parentCustomerId: customerId,
				allocatedLicense: request.body.allocatedLicense ? request.body.allocatedLicense : 0,
				usedLicense: 0,
				isActive: true
			})


			var update = {
				$addToSet: {

					license: {
						$each: dataToSave.license
					},
				}
			}
			
			ServiceCustomer.updateCustomer({
				_id: customerId
			}, update, {}, function (err, data) {
				logging.consolelog("------->update License ", err, data);
				if (!err) {
					cb(null);
				} else {
					cb(err);
				}
			})

        }],
		add_mapper: ['add_account_info', function (cb) {
			var dataToSave = {
				'childCustomerId': customerId,
				'superParentCustomerId': customerId,
				'parentCustomerType': constants.USER_TYPE.AGENT
			};
			ServiceChildMapper.createChildParentMapper(dataToSave, function (err, data) {
				logging.consolelog("------->createChildParentMapper", err, data);
				cb(null);
			})
		}],
		send_notification_to_agent: ['add_mapper', function (cb) {

			var emailSend = {
				emailId: request.body.emailId,
				password: UniversalFunctions.decryptData(request.body.password),
				type: request.body.userType,
				name: request.body.firstName + ' ' + request.body.lastName,
				assign_name: 'Admin'

			}
			var text;
			text = TemplatesForAgent.register(emailSend);
			NotificationManager.sendEmailToUser(request.body.emailId, "Congratulations!! Login to www.pixelsed.com", text, function (err, data) {

				cb(null);

			});



}]

	}, function (err, data) {
		if (!err) {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.AGENT_ADDED);
		} else {
			response.send(err);
		}
	})
}

var addGeoAgents = function (request, response) {
	var filename1 = null;
	var customerId = null;
    request.body.password = UniversalFunctions.encryptData(UniversalFunctions.generateRandomStringAndNumbers());
	async.auto({
		profile_url: function (cb) {
			if (request.files.profilePic) {
				UploadManager.uploadPicToS3Bucket(request.files.profilePic, "profile", function (filename) {
					if (filename == "No File") {
						cb(constants.STATUS_MSG.ERROR.UPLOAD_IMAGE_ERROR);
					} else {
						filename1 = AwsConfig.s3BucketCredentials.s3URL + filename;
						cb(null);
					}
				});
			} else {
				cb(null);
			}


		},
		add_agent: ['profile_url', function (cb) {

			var dataToSave = {};
			dataToSave.mobileNo = request.body.mobileNo;
			dataToSave.registrationDate = new Date().toISOString();
			dataToSave.lastName = request.body.lastName;
			dataToSave.firstName = request.body.firstName;
			dataToSave.profilePicUrl = filename1;
			dataToSave.addressValue = request.body.addressValue;
			// dataToSave.gender= request.body.gender
			// dataToSave.geoLocation.coordinates = [payload.coordinates],
			dataToSave.password = request.body.password;
			dataToSave.emailId = request.body.emailId;
			dataToSave.countryCode = request.body.countryCode;
			dataToSave.userType = 'DISTRICT';
			dataToSave.phoneVerified = true;
			dataToSave.isApproved = true;
			dataToSave.country = request.body.country;
			logging.consolelog("coordinates","",request.body.coordinates);
			if (request.body.coordinates) {
				dataToSave.geoSetup = [];
				for (var i = 0; i < request.body.coordinates.length; i++) {
					dataToSave.geoSetup.push({
						geoLocation: {
							coordinates: [request.body.coordinates[i].latLong]
						},
						networkAddress: request.body.coordinates[i].networkAddress
					})
				}


				//				if(typeof dataToSave.geoSetup[0].geoLocation.coordinates[0] != "object"){
				//				dataToSave.geoSetup[0].geoLocation.coordinates[0] = JSON.parse(dataToSave.geoSetup[0].geoLocation.coordinates[0]);
				//				}
			}
			if (request.body.accountNumber) {
				dataToSave.accountDetails = [];
				dataToSave.accountDetails.push({
					accountNumber: UniversalFunctions.encryptData(request.body.accountNumber),
					ifscCode: request.body.ifscCode,
					bankName: request.body.bankName,
					branchName: request.body.branchName,
					isActive: false
				})
			}

			ServiceCustomer.createCustomer(dataToSave, function (err, data) {
				if (!err) {
					customerId = data._id;
					cb(null);
				} else {
					cb(constants.STATUS_MSG.ERROR.ALREADY_REGISTERED_EMAIL);
				}
			})
        }],
		add_account_info: ['add_agent', function (cb) {
			var dataToSave = {};
			if (request.body.allocatedLicense != undefined) {
				dataToSave.license = [];
				dataToSave.license.push({
					parentCustomerId: customerId,
					allocatedLicense: request.body.allocatedLicense,
					usedLicense: 0,
					isActive: true
				})


				var update = {
					$addToSet: {

						license: {
							$each: dataToSave.license
						},
					}
				}
				logging.consolelog("update@@@@@@@@@@@","", update)
				ServiceCustomer.updateCustomer({
					_id: customerId
				}, update, {}, function (err, data) {
					if (!err) {
						cb(null);
					} else {
						cb(err);
					}
				})
			} else {
				cb(null);
			}
        }]
	}, function (err, data) {
		if (!err) {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.AGENT_ADDED);
		} else {
			response.send(err);
		}
	})
}

var editAgent = function (request, response) {
	var update = {};

	async.auto({
		edit_profilePic: function (cb) {
			
			if (request.files) {
				if (request.files.profilePic) {
					UploadManager.uploadPicToS3Bucket(request.files.profilePic, "profile", function (filename) {
						logging.consolelog("-------->::uploadPicToS3Bucket::------>","", filename);
						if (filename == "No File") {
							cb(null);
						} else {
							update.profilePicUrl = awsS3Config.s3BucketCredentials.s3URL + 'profile' + '/' + filename;
							cb(null);
						}
					});
				} else {
					cb(null);
				}
			} else {
				cb(null);
			}

		},
		edit_agent: ['edit_profilePic', function (cb) {
			var query = {
				_id: request.body.agentId
			}
			if (request.body.domainName && request.body.domainName.length) {
				update.domainName = request.body.domainName;
			}
			if (request.body.firstName != undefined) {
				update.firstName = request.body.firstName;
			}
			if (request.body.lastName != undefined) {
				update.lastName = request.body.lastName;
			}
			if (request.body.addressValue != undefined) {
				update.addressValue = request.body.addressValue;
			}
			if (request.body.countryCode != undefined) {
				update.countryCode = request.body.countryCode;
			}
			if (request.body.mobileNo != undefined) {
				update.mobileNo = request.body.mobileNo;
			}
			if (request.body.grade != undefined) {
				update.grade = request.body.grade;
			}
			if (request.body.dob != undefined) {
				update.dob = request.body.dob;
			}
			if (request.body.sessionTo != undefined) {
				update.sessionTo = request.body.sessionTo;
			}
			if (request.body.sessionFrom != undefined) {
				update.sessionFrom = request.body.sessionFrom;
			}
			if (request.body.state != undefined) {
				update.state = request.body.state;
			}
			if (request.body.zip != undefined) {
				update.zip = request.body.zip;
			}
			if (request.body.country != undefined) {
				update.country = request.body.country;
			}
			if (request.body.city != undefined) {
				update.city = request.body.city;
			}
			if (request.body.gender != undefined) {
				update.gender = request.body.gender;
			}

			logging.consolelog("update values>>>>>>>>>>>>>>>>>>>>>>>>>>>>>",query, update);
			ServiceCustomer.updateCustomer(query, update, {
				lean: true
			}, function (err, data) {
				logging.consolelog("-------->::updateCustomer::------>", err, data);
				if (!err) {
					cb(null);
				} else {
					cb(err);
				}
			})
		}],
		upadate_license: ['edit_agent', function (cb) {

			if (request.body.license) {
				var conditions = {
					_id: request.body.agentId,
					'license.parentCustomerId': request.body.agentId
				};

				var update = {
					'license.$.allocatedLicense': request.body.license
				};
				
				ServiceCustomer.getCustomer(conditions,{license:1},{},function(err,license){
					if(err){
						cb(err)
					}else{
						logging.consolelog('licenses',license,'>>>>>>>>>>>>>')
						if(license.length){
							ServiceCustomer.updateCustomer(conditions, update, {}, function (err, data) {
								logging.consolelog("-------->::updateCustomer1::------>", err, data);
								if (!err) {
									cb(null);
								} else {
									cb(err);
								}
							})
						}else{
							let query = {
								_id:request.body.agentId
							}
							let dataToUpdate = {
								$push:{
									license:{
										parentCustomerId:request.body.agentId,
										isActive:true,
										usedLicense:0,
										allocatedLicense:request.body.license
									}
								}
							}
							ServiceCustomer.updateCustomer(query, dataToUpdate, {}, function (err, data) {
								logging.consolelog("-------->::updateCustomer2::------>", err, data);
								if (!err) {
									cb(null);
								} else {
									cb(err);
								}
							})
						}
					}
				})
			} else {
				cb(null);
			}
		}],
		update_account: ['edit_agent', function (cb) {
			if (request.body.accountNumber) {
				var query = {
					_id: request.body.agentId
				}
				var update = {
					$push: {
						accountDetails: {
							accountNumber: UniversalFunctions.encryptData(request.body.accountNumber),
							ifscCode: request.body.ifscCode,
							bankName: request.body.bankName,
							branchName: request.body.branchName,
							isActive: false
						},
						domainName: request.body.domainName
					}
				}
				ServiceCustomer.updateCustomer(query, update, {
					lean: true
				}, function (err, data) {
					logging.consolelog("-------->::updateCustomer::------>", err, data);
					if (!err) {
						cb(null);
					} else {
						cb(err);
					}
				})
			} else {
				cb(null);
			}
		}]
	}, function (err, data) {
		if (!err) {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.EDIT_SUCCESSFULLY);
		} else {
			response.send(err);
		}
	})
}


var getTopic = function (item, callback) {
	var query = {
		_id: item.topicId
	}
	ServiceTopic.getTopic(query, {
		name: 1,
		averageRating: 1,
		icon: 1
	}, {}, function (err, data) {

		if (!err) {
			if (data.length > 0) {
				callback(null, {
					name: data[0].name,
					rating: data[0].averageRating,
					icon: data[0].icon,
					language: data[0].language
				});
			}

		} else {
			callback(err);
		}
	});
}

function findParentOfChild(customerId, type, temp, callback) {
	logging.consolelog(customerId, type,temp);
	if (type == constants.USER_TYPE.AGENT) {
		findChildOfParent1([customerId], temp, function (err, data) {
			if (!err) {
				callback(null, data);
			}
		});

	} else if (type != constants.USER_TYPE.SCHOOL) {
		var query = {
			childCustomerId: customerId
		}
		ServiceChildMapper.getChildParentMapper(query, {
			parentCustomerId: 1,
			parentCustomerType: 1
		}, {
			lean: true
		}, function (err, data) {
			if (!err) {
				if (data.length > 0) {

					findParentOfChild(data[0].parentCustomerId, data[0].parentCustomerType, temp, callback);
				}
			} else {
				callback(err);
			}
		})
	} else {
		findChildOfParent1([customerId], temp, function (err, data) {
			if (!err) {
				var x = data;
				logging.consolelog("----------->findChildOfParent1","",x)
				callback(null, x);
			}
		});
	}
}

function findChildOfParent(customerId, temp, callback) {
	logging.consolelog("------------>findChildOfParent","", customerId);
	var childCustomers = []
	var query = {
		parentCustomerId: {
			$in: customerId
		}
	}
	ServiceChildMapper.getChildParentMapper(query, {
		childCustomerId: 1
	}, {
		lean: true
	}, function (err, data) {
		if (!err) {
			logging.consolelog("------------->getChildParentMapper", data, temp);
			if (data.length > 0) {

				for (var i = 0; i < data.length; i++) {
					temp.push(data[i].childCustomerId);
					childCustomers.push(data[i].childCustomerId);
				}
				findChildOfParent(childCustomers, temp, callback);
			} else {
				callback(null, temp);
			}

		} else {
			callback(err);
		}
	});

}


function findChildOfParent1(customerId, temp, cb) {
	logging.consolelog("---------->findChildOfParent","", customerId);
	var childCustomers = []
	var query = {
		parentCustomerId: {
			$in: customerId
		}
	}
	ServiceChildMapper.getChildParentMapper(query, {
		childCustomerId: 1
	}, {
		lean: true
	}, function (err, data) {
		if (!err) {
			logging.consolelog("------------>getChildParentMapper", data, temp);
			if (data.length > 0) {

				for (var i = 0; i < data.length; i++) {
					temp.push(data[i].childCustomerId);
					childCustomers.push(data[i].childCustomerId);
				}
				findChildOfParent1(childCustomers, temp, cb);
			} else {
				logging.consolelog("---------------->temp final value","",temp)
				cb(null, temp);
			}

		} else {
			cb(err);
		}
	});

}





var getChildCustomersOfAgent = function (userId, callback) {

	var parentCustomerId = null;
	var parentType = null;
	var customers = [];
	var length = 0;
	var accessRights = false;
	var x = [];
	logging.consolelog( "-------->agent id","",userId)
	async.series([
		function (cb) {

			ServiceCustomer.getCustomer({
				_id: userId
			}, {
				accessRights: 1
			}, {
				lean: true
			}, function (err, data) {
				logging.consolelog("get customer",err, data);
				if (!err) {
					if (data.length > 0) {
						for (var i = 0; i < data[0].accessRights.length; i++) {
							if (data[0].accessRights[i].key == 'MANAGE_CUSTOMERS') {
								accessRights = data[0].accessRights[i].peer_view;
								break;
							} else {
								continue;
							}
						}
					}

					cb(null);
				} else {
					cb(err);
				}
			})

		},
			function (cb) {
			// with customerId find it super agent(main root of tree)
			ServiceChildMapper.getChildParentMapper({
				childCustomerId: userId
			}, {
				parentCustomerId: 1,
				parentCustomerType: 1
			}, {
				lean: true
			}, function (err, data) {
				if (!err) {
					if (data.length > 0) {
						logging.consolelog( "err,data",err, data);
						if (data[0].parentCustomerId == null) {
							logging.consolelog("hello","","");
							parentCustomerId = userId;
							parentType = constants.USER_TYPE.AGENT;
						} else {
							parentCustomerId = data[0].parentCustomerId;
							parentType = data[0].parentCustomerType;
						}

					}
					cb(null);
				} else {
					cb(err);
				}
			});

        },
		function (cb) {

			var customerIds = [];
			var temp = [];

			logging.consolelog("accessRights","",accessRights)
			if (accessRights == false) {
				findChildOfParent([parentCustomerId], temp, function (err, data) {
					if (!err) {
						x = data;
						cb(null);
					}
				});
			} else {
				findParentOfChild(parentCustomerId, parentType, temp, function (err, data) {
					if (!err) {
						x = data;
						cb(null);
					}
				});

			}

		},
			function (cb) {
			logging.consolelog("x child data","" ,x)
			if (x.length > 0) {
				var query = {
					_id: {
						$in: x
					}
				}

				query.userType = {
					$in: [constants.USER_TYPE.SCHOOL, constants.USER_TYPE.TEACHER, constants.USER_TYPE.SUB_AGENT, constants.USER_TYPE.SCHOOL]
				}

				var projection = {
					firstName: 1,
					lastName: 1,
					emailId: 1,
					mobileNo: 1,
					curriculum: 1,
					grade: 1,
					userType: 1,
					registrationDate: 1,
					addressValue: 1,
					accessRights: 1,
					profilePicUrl: 1,
					block: 1
				}
				ServiceCustomer.getCustomer(query, projection, {
					lean: true
				}, function (err, data) {
					if (!err) {
						if (data.length > 0) {
							for (var i = 0; i < data.length; i++) {
								customers.push({
									customerId: data[i]._id,
									name: data[i].firstName + ' ' + data[i].lastName,
									emailId: data[i].emailId,
									grade: data[i].grade,
									curriculum: data[i].curriculum,
									addressValue: data[i].addressValue,
									registerationDate: data[i].registrationDate,
									userType: data[i].userType,
									mobileNo: data[i].mobileNo,
									accessRights: data[i].accessRights,
									profilePic: data[i].profilePicUrl,
									block: data[i].block
								})
							}
							cb(null);
						} else {
							cb(null);
						}
					}

				})
			} else {
				cb(null);
			}

			}], function (err, data) {
		if (!err) {
			callback(null, customers);
		} else {
			callback(err);
		}
	})
}


var getAgentDetail = function (request, response) {
	var agentData = {};
	var purchased = [];
	var output = [];
	var shop = [];
	var childData = [];
	async.auto({
		get_agent_detail: function (cb) {
			var query = {
				_id: request.body.agentId
			}
			var projection = {
				emailId: 1,
				mobileNo: 1,
				country: 1,
				addressValue: 1,
				license: 1,
				accountDetails: 1,
				geoSetup: 1,
				domainName: 1,
				firstName: 1,
				lastName: 1,
				registrationDate: 1,
				block: 1,
				city:1,
				zip:1,
				state:1,
				location:1,
				profilePicUrl:1
			}
			var options = {
				lean: true
			}
			ServiceCustomer.getCustomer(query, projection, options, function (err, data) {
				logging.consolelog("--------> get customer---",err,data);
				if (!err) {
					if (data.length > 0) {

						agentData.emailId = data[0].emailId;
						agentData.mobileNo = data[0].mobileNo;
						agentData.country = data[0].country;
						agentData.firstName = data[0].firstName;
						agentData.lastName = data[0].lastName;
						agentData.addressValue = data[0].addressValue;
						agentData.city= data[0].city;
						agentData.zip= data[0].zip;
						agentData.state= data[0].state;
						agentData.location= data[0].location;
						agentData.profilePicUrl=data[0].profilePicUrl;
						for (var i = 0; i < data[0].license.length; i++) {
							if (data[0].license[i].parentCustomerId == request.body.agentId) {
								agentData.license = data[0].license[i].allocatedLicense;
								break;
							}
						}
						agentData.accountNumber = [];
						for (var i = 0; i < data[0].accountDetails.length; i++) {
							agentData.accountNumber.push({
								accountNumber: UniversalFunctions.decryptData(data[0].accountDetails[i].accountNumber),
								ifscCode: data[0].accountDetails[i].ifscCode,
								bankName: data[0].accountDetails[i].bankName,
								branchName: data[0].accountDetails[i].branchName,
								isActive: data[0].accountDetails[i].isActive
							})
						}

						agentData.geoSetup = data[0].geoSetup;
						agentData.domainName = data[0].domainName;
						agentData.registrationDate = data[0].registrationDate;
						agentData.block = data[0].block;


						cb(null);
					} else {
						cb(constants.STATUS_MSG.ERROR.INVALID_TOKEN);
					}
				} else {
					cb(null);
				}
			})
		},
		child_customers: function (cb) {

			getChildCustomersOfAgent(request.body.agentId, function (err, data) {
				logging.consolelog(err, data, "--------->child data");
				if (!err) {
					childData = data;
					cb(null);
				} else {
					cb(err);
				}
			});
		},

		purchased_topics: ['get_agent_detail', 'child_customers', function (cb) {
			var path = 'content.categoryId';
			var select = 'name isDeleted isPublish content'
			var populate = {
				path: path,
				match: {},
				select: select,
				options: {
					lean: true
				}
			};

			ServiceCustomerBuyPackage.getPackages({
				customerId: request.body.agentId,
				paymentStatus: constants.DATABASE.STATUS.COMPLETED
			}, {
				content: 1,
				topic: 1
			}, populate, {

			}, function (err, data) {
				logging.consolelog(err, JSON.stringify(data), "--------->populate data");
				if (!err) {
					if (data.length > 0) {
						for (var k = 0; k < data.length; k++) {
							for (var i = 0; i < data[k].content.length; i++) {
								if (data[k].content[i].contentType) {
									for (var j = 0; j < data[k].content[i].categoryId.content.length; j++) {

										if ((data[k].content[i].categoryId.content[j]._id).toString() == (data[k].content[i].contentType).toString()) {
											if (data[k].content[i].isActive == true || data[k].content[i].isActive == undefined) {
												purchased.push({
													topicId: data[k].content[i].topicId,
													categoryId: data[k].content[i].categoryId._id,
													categoryName: data[k].content[i].categoryId.name,
													contentId: data[k].content[i].contentId,
													contentType: data[k].content[i].categoryId.content[j].typeOfVideo
												});
											}

										}
									}

								} else {
									if (data[k].content[i].isActive == true || data[k].content[i].isActive == undefined) {
										purchased.push({
											topicId: data[k].content[i].topicId,
											categoryId: data[k].content[i].categoryId._id,
											categoryName: data[k].content[i].categoryId.name
										});
									}

								}

							}
						}

					}

					cb(null);
				} else {
					cb(err);
				}
			});

		}],
		get_topic_details: ['purchased_topics', function (cb) {
			var mergedObject = {};
			var mergerdArray = [];
			logging.consolelog("--------->purchased data","", purchased);
			if (purchased.length > 0) {
				for (var i in purchased) {
					if (mergedObject.hasOwnProperty(purchased[i]["topicId"])) {
						if (purchased[i]["contentType"]) {
							if (mergedObject[purchased[i]["topicId"]]["contentType"].indexOf(purchased[i].contentType) == -1) {
								mergedObject[purchased[i]["topicId"]]["contentType"].push(purchased[i]["contentType"]);
							}

						} else {
							if (mergedObject[purchased[i]["topicId"]]["categoryName"].indexOf(purchased[i].categoryName) == -1) {
								mergedObject[purchased[i]["topicId"]]["categoryName"].push(purchased[i]["categoryName"]);
							}

						}

					} else {
						mergedObject[purchased[i]["topicId"]] = {
							contentType: purchased[i]["contentType"] ? [purchased[i]["contentType"]] : [],
							topicId: purchased[i]["topicId"],
							categoryName: purchased[i]["contentType"] ? [] : [purchased[i]["categoryName"]]
						}
					}
				}
				for (var key in mergedObject) {
					mergerdArray.push(mergedObject[key]);
				}

				logging.consolelog(mergerdArray,"", "---------->mergedArray");

				async.forEach(mergerdArray, function (item, callback) {
					getTopic(item, function (err, result) {
						if (!err) {
							output = output.concat({
								topicId: item.topicId,
								name: result.name,
								rating: result.rating,
								language: result.language,
								icon: result.icon,
								contentType: item.contentType,
								categoryName: item.categoryName
							});
							callback(null);
						} else {
							callback(err);
						}
					});
				}, function (err) {
					if (!err) {
						var object = {};
						logging.consolelog("--------->output array get cat array wise!!!!!!!!","" ,output)
						for (var i = 0; i < output.length; i++) {
							logging.consolelog("output::" ,"",i)
							if (object.hasOwnProperty(output[i]["name"])) {
								object[output[i]["name"]]["rating"].push(output[i]["rating"]);

								object[output[i]["name"]]["mergedObject"].push({
									topicId: output[i]["topicId"],
									categoryName: output[i]["categoryName"],
									contentType: output[i]["contentType"],
									isAll: (output[i]["categoryName"]).indexOf(undefined) == -1 ? false : true,
									language: output[i]["language"]
								});

							} else {
								object[output[i]["name"]] = {
									name: output[i]["name"],
									icon: output[i]["icon"],
									rating: output[i]["rating"] ? [output[i]["rating"]] : [],
									mergedObject: [{
										topicId: output[i]["topicId"],
										isAll: (output[i]["categoryName"]).indexOf(undefined) == -1 ? false : true,
										categoryName: output[i]["categoryName"],
										contentType: output[i]["contentType"],
										language: output[i]["language"]
											}]
								}
							}
						}
						var resultArray = [];
						for (var key in object) {
							resultArray.push(object[key]);
						}
						logging.consolelog(resultArray,"", "--------->result array");
						logging.consolelog(object,"", "----------->::: object :::");
						shop = resultArray;
						cb(null);
					} else {
						cb(err);
					}
				});
			} else {
				cb(null);
			}
				}],
		license_count: ['get_agent_detail', 'get_topic_details', function (cb) {
			var licenseData = [];
			var conditions = {
				'license.parentCustomerId': request.body.agentId
			};

			ServiceCustomer.getCustomer(conditions, {
				firstName: 1,
				license: 1,
				lastName: 1
			}, {
				lean: true
			}, function (err, data) {
				if (!err) {
					for (var i = 0; i < data.length; i++) {
						licenseData.push({
							name: data[i].firstName + '' + data[i].lastName,
							allocatedLicense: data[i].license[0].allocatedLicense,
							usedLicense: data[i].license[0].usedLicense
						})
					}
					agentData.licenseData = licenseData;
					cb(null);
				} else {
					cb(err);
				}


			})
		}],


	}, function (err, data) {
		logging.consolelog(agentData, output, childData);
		if (!err) {
			response.status(200).send({
				agentData: agentData,
				purchased: shop,
				childData: childData,
				statusCode: 200
			});
		} else {
			response.send(err);
		}
	})
}


var categoryList = function (item, categoryList, contentList, categoryCheck, callback) {
	logging.consolelog("verify categories","","")
	var counter = 0;
	var categoryName = [];
	var contentName = [];
	async.forEach(item, function (item, embeddedcb) {
		counter++

		var query = {
			topicId: item
		};
		var path = 'categoryId';
		var select = 'name content isDeleted'
		var populate = {
			path: path,
			match: {},
			select: select,
			options: {
				lean: true
			}
		};
		var options = {

		};
		var projection = {
			categoryId: 1
		}

		ServiceTopicBinding.topicBindingPopulate(query, projection, populate, {}, {}, function (err, data) {
			logging.consolelog(err, data, "category Dtaa");
			if (!err) {
				for (var i = 0; i < data.length; i++) {
					if (data[i].categoryId.name == 'Video' && data[i].categoryId.isDeleted == false) {
						for (var j = 0; j < data[i].categoryId.content.length; j++) {
							if (data[i].categoryId.content[j].isDeleted == false && data[i].categoryId.content[j].isVideo == true) {
								if (categoryCheck == true) {
									// if (contentList.indexOf(data[i].categoryId.content[j].typeOfVideo) != -1) {
									// 	contentName.push(data[i].categoryId.content[j].typeOfVideo);
									// }
									contentName.push(data[i].categoryId.content[j].typeOfVideo);
								} else {
									contentName.push(data[i].categoryId.content[j].typeOfVideo);
								}

							}
						}
					} else if (data[i].categoryId.isDeleted == false) {
						if (categoryCheck == true) {
							// if (categoryList.indexOf(data[i].categoryId.name) != -1) {
							// 	categoryName.push(data[i].categoryId.name);
							// }

							categoryName.push(data[i].categoryId.name);
						} else {
							categoryName.push(data[i].categoryId.name);
						}

					}

				}
				contentName = _.unique(contentName);
				categoryName = _.unique(categoryName);
				embeddedcb(null);
			} else {
				embeddedcb(err);
			}
		});

	}, function (err) {
		if (counter == item.length) {

			callback(null, {
				contentName: contentName,
				categoryName: categoryName
			});
		}

	});
}


var categoryList1 = function (item, categoryList, contentList, categoryCheck, callback) {
	logging.consolelog("verigfy categories","","")
	var counter = 0;
	var categoryName = [];
	var contentName = [];


	var query = {
		topicId: item
	};
	var path = 'categoryId';
	var select = 'name content isDeleted'
	var populate = {
		path: path,
		match: {},
		select: select,
		options: {
			lean: true
		}
	};
	var options = {

	};
	var projection = {
		categoryId: 1
	}

	ServiceTopicBinding.topicBindingPopulate(query, projection, populate, {}, {}, function (err, data) {
		logging.consolelog(err, data, "category Dtaa");
		if (!err) {
			for (var i = 0; i < data.length; i++) {
				if (data[i].categoryId.name == 'Video' && data[i].categoryId.isDeleted == false) {
					for (var j = 0; j < data[i].categoryId.content.length; j++) {
						if (data[i].categoryId.content[j].isDeleted == false && data[i].categoryId.content[j].isVideo == true) {
							if (categoryCheck == true) {
								if (contentList.indexOf(data[i].categoryId.content[j].typeOfVideo) != -1) {
									contentName.push(data[i].categoryId.content[j].typeOfVideo);
								}
							} else {
								contentName.push(data[i].categoryId.content[j].typeOfVideo);
							}

						}
					}
				} else if (data[i].categoryId.isDeleted == false) {
					if (categoryCheck == true) {
						if (categoryList.indexOf(data[i].categoryId.name) != -1) {
							categoryName.push(data[i].categoryId.name);
						}
					} else {
						categoryName.push(data[i].categoryId.name);
					}

				}

			}
			contentName = _.unique(contentName);
			categoryName = _.unique(categoryName);
			callback(null, {
				contentName: contentName,
				categoryName: categoryName
			});
		} else {
			callback(err);
		}
	});


}


// var prepareFilter = function (key, data, query) {
// 	if (data) {
// 		try {
// 			data = JSON.parse(data);
// 		} catch (e) {}
// 		if (!Array.isArray(data)) {
// 			data = [data]
// 		}
// 		if (data && data.length) {
// 			query[key] = {
// 				$in: data
// 			}
// 		}
// 	}
// }


var getTopicListData = function (request, response) {
	var list = [];
	var topicListObj = {};
	var topicList = [];
	var language = [];
	var languageList = [];
	var boardList = [];
	var gradeList = [];
	var subjectList = [];
	var chapterList = [];
	var count = null;
	var board = [];
	var grade = [];
	var subject = [];
	var chapter = [];
	var curriculumId = [];
	var topicId = [];
	var final = [];
	var topics = [];
	var categoryL = [];
	var contentL = [];
	var categoryPool = [];
	var contentPool = [];
	if (request.body.language) {
		for (var i = 0; i < request.body.language.length; i++) {
			language.push(request.body.language[i].language)
		}
	}
	if (request.body.board) {
		for (var i = 0; i < request.body.board.length; i++) {
			board.push(request.body.board[i].board)
		}
	}
	if (request.body.grade) {
		for (var i = 0; i < request.body.grade.length; i++) {
			grade.push(request.body.grade[i].grade)
		}
	}
	if (request.body.subject) {
		for (var i = 0; i < request.body.subject.length; i++) {
			subject.push(request.body.subject[i].subject)
		}

	}
	if (request.body.chapter) {
		for (var i = 0; i < request.body.chapter.length; i++) {
			chapter.push(request.body.chapter[i].chapter)
		}
	}
	var purchased = [];
	var shop = [];
	var output = {};
	async.auto({
		board_list: function (cb) {
			var query = {};
			if (language.length > 0) {
				query.language = {
						$in: language
					},
					query.isRemoved = "false";
				ServiceCurriculum.getUniqueCurriculum(query, 'board', function (err, data) {
					if (!err) {
						for (var i = 0; i < data.length; i++) {
							boardList.push({
								board: data[i].board
							});
						}

						cb(null);
					} else {
						cb(err);

					}
				})
			} else {
				cb(null);
			}


		},
		grade_list: function (cb) {
			var query = {};
			if (language.length > 0 && board.length > 0) {
				query.language = {
					$in: language
				}
				query.isRemoved = "false";
				query.board = {
					$in: board
				}
				ServiceCurriculum.getUniqueCurriculum(query, 'grade', function (err, data) {
					if (!err) {
						for (var i = 0; i < data.length; i++) {
							gradeList.push({
								grade: data[i].grade
							});
						}

						cb(null);
					} else {
						cb(err);

					}
				})
			} else {
				cb(null);
			}
		},
		subject_list: function (cb) {
			var query = {};
			if (language.length > 0 && board.length > 0 && grade.length > 0) {
				query.language = {
					$in: language
				}
				query.isRemoved = "false";
				query.board = {
					$in: board
				}
				query.grade = {
					$in: grade
				}
				ServiceCurriculum.getUniqueCurriculum(query, 'subject', function (err, data) {
					if (!err) {
						for (var i = 0; i < data.length; i++) {
							subjectList.push({
								subject: data[i].subject
							});
						}

						cb(null);
					} else {
						cb(err);

					}
				})
			} else {
				cb(null);
			}
		},

		chapter_list: function (cb) {
			var query = {};
			if (language.length > 0 && board.length > 0 && grade.length > 0 && subject.length > 0) {
				query.language = {
					$in: language
				}
				query.isRemoved = "false";
				query.board = {
					$in: board
				}
				query.grade = {
					$in: grade
				}
				query.subject = {
					$in: subject
				}
				ServiceCurriculum.getUniqueCurriculum(query, 'chapter', function (err, data) {
					if (!err) {
						for (var i = 0; i < data.length; i++) {
							chapterList.push({
								chapter: data[i].chapter
							});
						}

						cb(null);
					} else {
						cb(err);

					}
				})
			} else {
				cb(null);
			}
		},
		language_list: function (cb) {

			ServiceCurriculum.getUniqueCurriculum({
				isRemoved: false
			}, 'language', function (err, data) {
				if (!err) {
					logging.consolelog("language data::", err, JSON.stringify(data));
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							languageList.push({
								language: data[i].language
							});
						}
						cb(null);
					} else {
						cb(constants.STATUS_MSG.ERROR.EMPTY_CURRICULUM);
					}

				} else {
					cb(err);
				}
			})
		},
		purchased_topics: ['language_list', function (cb) {
			var path = 'content.categoryId';
			var select = 'name isDeleted isPublish content'
			var populate = {
				path: path,
				match: {},
				select: select,
				options: {
					lean: true
				}
			};

			ServiceCustomerBuyPackage.getPackages({
				customerId: request.body.agentId,
				paymentStatus: constants.DATABASE.STATUS.COMPLETED
			}, {
				content: 1,
				topic: 1
			}, populate, {

			}, function (err, data) {
				logging.consolelog("populate data>>>>>>>",err, JSON.stringify(data));
				if (!err) {
					if (data.length > 0) {
						for (var k = 0; k < data.length; k++) {
							for (var i = 0; i < data[k].content.length; i++) {
								if (data[k].content[i].contentType) {
									for (var j = 0; j < data[k].content[i].categoryId.content.length; j++) {

										if ((data[k].content[i].categoryId.content[j]._id).toString() == (data[k].content[i].contentType).toString()) {
											if (data[k].content[i].isActive == true || data[k].content[i].isActive == undefined) {
												purchased.push({
													topicId: data[k].content[i].topicId,
													categoryId: data[k].content[i].categoryId._id,
													categoryName: data[k].content[i].categoryId.name,
													contentId: data[k].content[i].contentId,
													contentType: data[k].content[i].categoryId.content[j].typeOfVideo
												});
											}

										}
									}

								} else {
									if (data[k].content[i].isActive == true || data[k].content[i].isActive == undefined) {
										purchased.push({
											topicId: data[k].content[i].topicId,
											categoryId: data[k].content[i].categoryId._id,
											categoryName: data[k].content[i].categoryId.name
										});
									}

								}

							}
						}

					}

					cb(null);
				} else {
					cb(err);
				}
			});

		}],
		get_topic_details: ['purchased_topics', function (cb) {
			var mergedObject = {};
			var mergerdArray = [];
			logging.consolelog("purchased data","", purchased);
			if (purchased.length > 0) {
				for (var i in purchased) {
					if (mergedObject.hasOwnProperty(purchased[i]["topicId"])) {
						if (purchased[i]["contentType"]) {
							if (mergedObject[purchased[i]["topicId"]]["contentType"].indexOf(purchased[i].contentType) == -1) {
								mergedObject[purchased[i]["topicId"]]["contentType"].push(purchased[i]["contentType"]);
							}

						} else {
							if (mergedObject[purchased[i]["topicId"]]["categoryName"].indexOf(purchased[i].categoryName) == -1) {
								mergedObject[purchased[i]["topicId"]]["categoryName"].push(purchased[i]["categoryName"]);
							}

						}

					} else {
						mergedObject[purchased[i]["topicId"]] = {
							contentType: purchased[i]["contentType"] ? [purchased[i]["contentType"]] : [],
							topicId: purchased[i]["topicId"],
							categoryName: purchased[i]["contentType"] ? [] : [purchased[i]["categoryName"]]
						}
					}
				}
				for (var key in mergedObject) {
					mergerdArray.push(mergedObject[key]);
				}

				logging.consolelog(mergerdArray, "","mergedArray");

				async.forEach(mergerdArray, function (item, callback) {
					getTopic(item, function (err, result) {
						if (!err) {
							output[item.topicId] = {
								topicId: item.topicId,
								name: result.name,

								language: result.language,

								VideoContent: item.contentType,
								categoryName: item.categoryName
							}

							callback(null);
						} else {
							callback(err);
						}
					});
				}, function (err) {
					if (!err) {

						logging.consolelog("output array get cat array wise!!!!!!!!", "",output)
						cb(null);
					} else {
						cb(err);
					}
				});
			} else {
				cb(null);
			}
				}],
		curriculum_list: function (cb) {

			var projection = {
				'result.topicId': 1

			}
			var query = {
				'board': constants.COMMON_VARIABLES.UNIVERSAL_BOARD_NAME,
				'isDeleted': false,
				'isPublish': true,

			}

			if (language.length > 0) {
				//        query.curriculum={}
				query['curriculum.language'] = {
					$in: language
				}
			}


			if (board.length > 0) {

				query.board = {
					$in: board
				}
			}
			if (grade.length > 0) {

				query.grade = {
					$in: grade
				}
			}
			if (subject.length > 0) {

				query.subject = {
					$in: subject
				}
			}
			if (chapter.length > 0) {

				query.chapter = {
					$in: chapter
				}
			}

			logging.consolelog("query", "",query);
			var limit = {
				limit: request.body.limit ? request.body.limit : 10,
				offset: request.body.skip ? request.body.skip : 0
			}
			var search = "";
			if (request.body.search) {
				search = ' and name like "%' + request.body.search + '%" ';
			}
			var sort = {
				sortBy: "sequenceOrder"
			};
			ServiceTopic.getTopicsByCurriculum(query, sort, limit, search, function (err, curriculum) {
				if (!err) {
					logging.consolelog(err, curriculum, "%%%%%% get curriculum data %%%%%%");
					if (curriculum.length) {
						for (var i = 0; i < curriculum.length; i++) {

							topics.push({
								id: curriculum[i]["_id"],
								name: curriculum[i].name,
								language: curriculum[i].topicLanguage
							});
						}



						ServiceTopic.getTopicsByCurriculumCount(query, {}, {}, search, function (err, countData) {
							logging.consolelog(err, countData, "check count");
							if (!err) {
								count = countData ? countData[0]["cnt"] : 0;
							}
							cb(null);
						});

					} else {
						cb(null)
					}
				} else {
					cb(err);
				}
			});

		},
		topic_categories: ['curriculum_list', function (cb) {
			logging.consolelog(topics,"", "topic id");
			if (topics.length > 0) {
				logging.consolelog("get categories of topics","","");
				async.forEach(topics, function (item, callback) {
					categoryList1(item.id, '', '', false, function (err, result) {
						logging.consolelog(err, result, "categoryList");
						if (!err) {
							final.push({
								categoryName: result.categoryName,
								VideoContent: result.contentName,
								topicName: item.name,
								topicId: item.id,
								language: item.language

							});
							callback(null);
						} else {
							callback(err);
						}
					});
				}, function (err) {
					if (!err) {

						cb(null);
					} else {

						cb(err);
					}
				});
			} else {
				cb(null);
			}

        }],
		unique_data: ['topic_categories', function (cb) {

			for (var i = 0; i < final.length; i++) {
				categoryL = categoryL.concat(final[i].categoryName);
				contentL = contentL.concat(final[i].VideoContent);
			}

			categoryL = _.uniq(categoryL);
			contentL = _.uniq(contentL);
			cb(null);
		}],
		get_all_categories: function (cb) {
			ServiceTopicCategory.getTopicCategory({
				isDeleted: false
			}, {}, {}, function (err, data) {
				if (!err) {
					for (var i = 0; i < data.length; i++) {
						if (data[i].name == 'Video' && data[i].isDeleted == false) {
							for (var j = 0; j < data[i].content.length; j++) {
								if (data[i].content[j].isDeleted == false && data[i].content[j].isVideo == true) {
									contentPool.push(data[i].content[j].typeOfVideo)
								}
							}
						} else if (data[i].isDeleted == false) {
							categoryPool.push(data[i].name);
						}

					}

					contentPool = _.uniq(contentPool);
					categoryPool = _.uniq(categoryPool);
					cb(null);
				} else {
					cb(err);
				}
			})
		}
	}, function (err, data) {
		if (!err) {
			response.status(200).send({
				final: final,
				categoryList: categoryL,
				contentList: contentL,
				count: count,
				categoryPool: categoryPool,
				contentPool: contentPool,
				purchased: output,
				statusCode: 200
			})
		} else {
			response.send(err);
		}
	})
}


var addCollection = function (item, language, startDate, endDate,agentId,callback) {
	var topicIds = [];
	var final = [];
	var temp=[];
	var flag= false;
	var pass=[];
   console.log(item,language, startDate, endDate,agentId )
	async.auto({
		get_topics: function (cb) {
			var query = {
				name: item.topicName,
				language: {
					$in: language
				},
				isDeleted: false
			}

			console.log("====> query", query)
			ServiceTopic.getTopic(query, {
				_id: 1
			}, {
				lean: true
			}, function (err, data) {
				logging.consolelog(err, data, item);
				if (!err) {
					for (var i = 0; i < data.length; i++) {
						topicIds.push(data[i]._id);
					}
					cb(null);
				} else {
					cb(err);
				}
			});

		},
		get_category: ['get_topics', function (cb) {

			console.log("topicIds", topicIds);
			if (topicIds.length > 0) {
				var query = {
					topicId: {
						$in: topicIds
					}
				}
				var path = 'categoryId';
				var select = 'name content isDeleted'
				var populate = {
					path: path,
					match: {},
					select: select,
					options: {
						lean: true
					}
				};
				var options = {

				};
				var projection = {
					categoryId: 1,
					topicId: 1
				}
				ServiceTopicBinding.getAllTopicBinding(query, projection, populate, {}, {}, function (err, data) {
					console.log(err, JSON.stringify(data), "=====> topic binding");
					if (!err) {
						var obj={};
						for (var i = 0; i < data.length; i++) {
							var categoryNames=[];
							var contentList=[];
							logging.consolelog(data[i].categoryId.name,"", "======>category name");
							if (data[i].categoryId.name == 'Video') {
								for (var j = 0; j < data[i].categoryId.content.length; j++) {
									logging.consolelog(data[i].categoryId.content[j].typeOfVideo, item.contentList, "content video check")
									contentList.push(data[i].categoryId.content[j].typeOfVideo);
									if (item.contentList.indexOf(data[i].categoryId.content[j].typeOfVideo) == -1) {
										
									} else {
										final.push({
											topicId: data[i].topicId,
											contentId: data[i].categoryId.content[j]._id,
											categoryId: data[i].categoryId._id,
											contentType: data[i].categoryId.content[j]._id,
											startDate: startDate,
											endDate: endDate
										});


									}
								}

							} else {
								categoryNames.push(data[i].categoryId.name);
								if (item.categoryList.indexOf(data[i].categoryId.name) == -1) {
									
									
									
								} else {
									final.push({
										topicId: data[i].topicId,
										categoryId: data[i].categoryId._id,
										startDate: startDate,
										endDate: endDate
									});


								}


							}
							console.log("=====> obj", categoryNames, contentList, data[i]["topicId"]);
							if (obj.hasOwnProperty(data[i]["topicId"])) {
							if(categoryNames && categoryNames.length>0)	obj[data[i]["topicId"]]["categoryName"].push(categoryNames[0]);
							if(contentList && contentList.length>0)		obj[data[i]["topicId"]]["contentList"].push(contentList[0]);

							}else{
								obj[data[i]["topicId"]] = {
									contentList: contentList[0] ? [contentList[0]] : [],
									topicId: data[i]["topicId"],
									categoryName: categoryNames[0] ? [categoryNames[0] ]: []
								}
							}
							
							
							

						}

						for (var key in obj) {
							pass.push(obj[key]);
						}
                      
						console.log("========> object pass", pass);
						cb(null);
						
					} else {
						cb(err);
					}
				})
			} else {
				cb(null);
			}

		}],
		"checking_for_agent_temp":["get_category",(cb)=>{
			var obj={};
			console.log("==========>pass", pass, item.categoryList,item.contentList , pass.length, item.categoryList.length,item.contentList.length )
			   for(var k=0; k< pass.length; k++){

				for(var i=0; i<item.categoryList.length;i++ ){
					console.log("pass[k][categoryName]", pass[k]["categoryName"]);
					console.log(item.categoryList[i]);
					if(pass[k]["categoryName"].indexOf(item.categoryList[i]==-1)){ 

					if (obj.hasOwnProperty(pass[k]["topicId"])) {
						
						obj[pass[k]["topicId"]]["category"].push(item.categoryList[i]);

					}else{
						obj[pass[k]["topicId"]] = {
							content: [],
							topicId: pass[k]["topicId"],
							category: item.categoryList[i] ? [item.categoryList[i]] : [],
							startDate: startDate,
							endDate: endDate,
							agentId:agentId,
							
						}
					}

					}

					}
					for(var j=0; j< item.contentList.length; j++){
						if(pass[k]["contentList"].indexOf(item.contentList[j]==-1)){

					if (obj.hasOwnProperty(pass[k]["topicId"])) {
						
						obj[pass[k]["topicId"]]["content"].push(item.contentList[j]);

					}else{
						obj[pass[k]["topicId"]] = {
							content: item.contentList[j] ? [item.contentList[j]] : [],
							topicId: pass[k]["topicId"],
							category: [],
							startDate: startDate,
							endDate: endDate,
							agentId:agentId,
							
						}
					}
						

					}
					else{
						if (obj.hasOwnProperty(pass[k]["topicId"])) {
							
							obj[pass[k]["topicId"]]["content"].push(item.contentList[j]);
	
						}else{
							obj[pass[k]["topicId"]] = {
								content: item.contentList[j] ? [item.contentList[j]] : [],
								topicId: pass[k]["topicId"],
								category: [],
								startDate: startDate,
								endDate: endDate,
								agentId:agentId,
								
							}
						}
					}

					}



			   }
			   for (var key in obj) {
				temp.push(obj[key]);
			}

			cb(null);


			
       

		}]



	}, function (err, data) {
		if (!err) {

			console.log("====> final temp", final, temp);
			callback(null, {final:final, temp:temp} );
		

		} else {
			callback(err);
		}
	})
}


var agentPackages = function (request, response) {

	var board = [];
	var grade = [];
	var subject = [];
	var chapter = [];
	var final = [];
	var topics = [];
	var language = [];
	var temp=[];
	if (request.body.language) {
		for (var i = 0; i < request.body.language.length; i++) {
			language.push(request.body.language[i].language)
		}
	}
	if (request.body.board) {
		for (var i = 0; i < request.body.board.length; i++) {
			board.push(request.body.board[i].board)
		}
	}
	if (request.body.grade) {
		for (var i = 0; i < request.body.grade.length; i++) {
			grade.push(request.body.grade[i].grade)
		}
	}
	if (request.body.subject) {
		for (var i = 0; i < request.body.subject.length; i++) {
			subject.push(request.body.subject[i].subject)
		}

	}
	if (request.body.chapter) {
		for (var i = 0; i < request.body.chapter.length; i++) {
			chapter.push(request.body.chapter[i].chapter)
		}
	}
	async.auto({
		check_select_all_option: function (cb) {

				if (request.body.selectAll) {
					// var projection = {
					// 	'result.topicId': 1
                    //
					// }
					var query = {};
					if (language.length > 0) {
						query.language = {
								$in: language
							},
							query.isRemoved = "false";

					}
					if (board.length > 0) {

						query.board = {
							$in: board
						}
					}
					if (grade.length > 0) {

						query.grade = {
							$in: grade
						}
					}
					if (subject.length > 0) {

						query.subject = {
							$in: subject
						}
					}
					if (chapter.length > 0) {

						query.chapter = {
							$in: chapter
						}
					}
					if (!(language.length > 0)) {
						query.board = constants.COMMON_VARIABLES.UNIVERSAL_BOARD_NAME;
						query.isRemoved = "false";

					}

					logging.consolelog("query","", query);
					var limit = {
						limit: request.body.limit ? request.body.limit : 5000,
						offset: request.body.skip ? request.body.skip : 0
					}
					var search = "";
					if (request.body.search) {
						search = ' and name like "%' + request.body.search + '%" ';
					}
					ServiceCurriculum.getCurriculumAndTopic(query, limit, search, function (err, data) {
						if (!err) {

							if (data.length) {
								for (var i = 0; i < data.length; i++) {
									logging.consolelog("check deletd topics ", data[i].name, data[i].ids.split(","))

									topics.push({
										name: data[i].name,
										language: data[i].topicLanguage,
										id: data[i].ids.split(",")
									})



								}
								cb(null);
							}else{
								cb(null)
							}
						} else {
							cb(err);
						}
					});
				} else {
					cb(null);

				}
		},
		if_select_all_then_fetch_topic_categories: ['check_select_all_option', function (cb) {

			console.log("check_select_all_option", request.body.selectAll,topics.length );
				if (request.body.selectAll) {
					if (topics.length > 0) {
						async.forEach(topics, function (item, callback) {
							categoryList(item.id, request.body.categoryList, request.body.contentList, true, function (err, result) {
								logging.consolelog(err, result, "categoryList");
								if (!err) {
									// if (result.categoryName.length > 0 || result.contentName.length > 0) {
									// 	request.body.topicArray.push({
									// 		categoryList: result.categoryName,
									// 		contentList: result.contentName,
									// 		topicName: item.name,
									// 		topicId: item.id,
									// 		language: item.language

									// 	});
									// }
									request.body.topicArray.push({
												categoryList: request.body.categoryList,
												contentList:  request.body.contentList,
												topicName: item.name,
												topicId: item.id,
												language: item.language
	
											});

									callback(null);
								} else {
									callback(err);
								}
							});
						}, function (err) {
							if (!err) {

								cb(null);
							} else {

								cb(err);
							}
						});
					} else {
						cb(null);
					}
				} else {
					cb(null);
				}
						}],
		get_topi_aggregation: ['if_select_all_then_fetch_topic_categories', function (cb) {
				logging.consolelog("topicArray values", request.body.topicArray, request.body)
				var language = [];
				for (var i = 0; i < request.body.language.length; i++) {
					language.push(request.body.language[i].language);
				}


				async.forEach(request.body.topicArray, function (item, callback) {
					addCollection(item, language, request.body.startDate, request.body.endDate,request.body.agentId, function (err, result) {

						if (!err) {
							final = final.concat(result.final);
							temp= temp.concat(result.temp);
							callback(null);
						} else {
							callback(err);
						}
					});
				}, function (err) {
					if (!err) {

						cb(null);
					} else {

						cb(err);
					}
				});
		}],
		save_data: ['get_topi_aggregation', function (cb) {
				logging.consolelog("merged array result","", final);
				if (request.body.transactionId) {
					var query = {
						transactionId: request.body.transactionId
					}
					ServiceCustomerBuyPackage.getTransaction(query, {
						content: 1,
						customerId: 1
					}, {}, function (err, data) {
						if (!err) {
							data[0].content = data[0].content.concat(final);
							var update = {
								$set: {
									'content': data[0].content
								}
							}
							ServiceCustomerBuyPackage.updateTransaction(query, update, {}, function (err, updateData) {
								cb(null);
							})
						} else {
							cb(err);
						}
					})
				} else {
					var data = {
						content: final,
						amount: request.body.amount,
						customerId: request.body.agentId,
						packageName: "Package Created on" + ' ' + new Date().getDate() + '/' + new Date().getMonth() + '/' + new Date().getFullYear(),
						transactionId: "txn" + Date.now(),
						buyDate: Date.now(),
						paymentCardId: '0847',
						paymentStatus: constants.DATABASE.STATUS.COMPLETED,
						payments: {},
                        embeddedLink : request.body.embeddedLink,
                    	domainName : request.body.domainName

					}
					request.body.transactionId= data.transactionId;
					data.payments.subTotalPrice = 0;
					data.payments.discountPrice = 0;
					data.payments.totalPrice = request.body.amount;
					ServiceCustomerBuyPackages.createTransaction(data, function (err, data) {
						if (!err) {
							cb(null);
						} else {
							cb(err);
						}
					})
				}
		}],
		"temp_save_data":["save_data",function(cb){
			  var insertData=[];
			 console.log("====> temp data::: temp_save_data",temp );
			 if(temp.length>0){
				for(var i=0; i< temp.length; i++){
					insertData.push({
						transactionId:request.body.transactionId,
						topicId:temp[i]["topicId"],
						category:temp[i]["category"],
						content:temp[i]["content"],
						agentId:temp[i]["agentId"],
						startDate: temp[i]["startDate"],
						endDate: temp[i]["endDate"],
						language:language,
						board:board,
						grade:grade,
						subject:subject,
						chapter:chapter
							})

		}

			 }else{

				insertData.push({
					transactionId:request.body.transactionId,
						
						category:request.body.categoryList,
						content: request.body.contentList,
						agentId:request.body.agentId,
						startDate: request.body.startDate,
						endDate: request.body["endDate"],
						language:language,
						board:board,
						grade:grade,
						subject:subject,
						chapter:chapter
						
				})
			 }
			
			

			if(insertData.length>0){

				console.log("====> insert data", insertData);
				ServiceTempAgentAssignData.insertData(insertData, (err,saveData)=>{
					if(!err){
							console.log("=====> create suoper admin", err, saveData);
							cb(null);
					}else{
						cb(err);
					}
				})
			}else{

				





				cb(null);
			}
			

		}]

		},
		function (err, data) {
			if (!err) {
				response.status(200).send(constants.STATUS_MSG.SUCCESS.DEFAULT);
			} else {
				response.send(err);
			}
		})
}

var getTopic = function (item, callback) {


	var query = {
		_id: item.topicId
	}
	ServiceTopic.getTopic(query, {
		name: 1,
		averageRating: 1,
		icon: 1,
		language: 1
	}, {}, function (err, data) {

		if (!err) {
			if (data.length > 0) {
				callback(null, {
					name: data[0].name,
					rating: data[0].averageRating,
					icon: data[0].icon,
					price: data[0].price,
					language: data[0].language
				});
			}

		} else {
			callback(err);
		}
	});
}

function add(a, b) {
	return a + b;
}


var getBookingDetails = function (request, response) {
	var topicFinal = [];
	var objToSave = {};
	var mergedObject = {};
	var mergerdArray = [];
	var price = 0;
	var customerId = request.userData.id;
	var license = 0;
	async.auto({
		get_license: function (cb) {
			ServiceCustomer.getCustomer({
				_id: request.body.customerId
			}, {
				license: 1
			}, {}, function (err, data) {
				if (!err) {
					if (data.length > 0 && data[0].license.length > 0)
						license = data[0].license[0].allocatedLicense - data[0].license[0].usedLicense;
					cb(null);
				} else {
					cb(err);
				}
			});
		},
		get_booking: ['get_license', function (cb) {

				var query = {
					transactionId: request.body.transactionId
				}
				var path = 'customerId content.categoryId';
				var select = 'firstName lastName emailId mobileNo addressValue country registrationDate name isDeleted isPublish content price'
				var populate = {
					path: path,
					match: {},
					select: select,
					options: {
						lean: true
					}
				};
				var options = {

				};
				var projection = {
					customerId: 1,
					transactionId: 1,
					packageName: 1,
					content: 1,
					buyDate: 1,
					topic: 1,
					embeddedLink: 1,
					domainName: 1,
					topicValidity: 1,
					paymentStatus: 1,
					payments: 1,
					license: 1

				}
				logging.consolelog(query, projection, populate)
				ServiceCustomerBuyPackage.getPackages(query, projection, populate, options, function (err, data) {
					logging.consolelog("data",err, JSON.stringify(data));
					if (!err) {
						if (data.length)
							license = data[0].license;
						if (data.length > 0 && data[0].content) {
							for (var i = 0; i < data[0].content.length; i++) {
								if (data[0].content[i].contentId) {
									for (var j = 0; j < data[0].content[i].categoryId.content.length; j++) {
										if (data[0].content[i].categoryId.content[j]._id == data[0].content[i].contentId) {
											topicFinal.push({
												topicId: data[0].content[i].topicId,
												categoryId: data[0].content[i].categoryId._id,
												categoryName: data[0].content[i].categoryId.name,
												categoryPrice: data[0].content[i].categoryId.price,
												contentId: data[0].content[i].contentId,
												startDate: data[0].content[i].startDate,
												endDate: data[0].content[i].endDate,
												contentType: data[0].content[i].categoryId.content[j].typeOfVideo,
												contentPrice: data[0].content[i].categoryId.content[j].price
											});
										}
									}

								} else {
									if (data[0].content[i].isActive == true)
										topicFinal.push({
											topicId: data[0].content[i].topicId,
											startDate: data[0].content[i].startDate,
											endDate: data[0].content[i].endDate,
											categoryId: data[0].content[i].categoryId._id,
											categoryName: data[0].content[i].categoryId.name,
											categoryPrice: data[0].content[i].categoryId.price,
										});
								}

							}
						}
						if (data[0].topic) {
							for (var i = 0; i < data[0].topic.length; i++) {
								topicFinal.push({
									topicId: data[0].topic[i].topicId
								})
							}
						}

						for (var i = 0; i < data.length; i++) {

							if (data[i].customerId != null) {
								objToSave.agent = {
									bookingId: data[i]._id,
									customerId: data[i].customerId._id,
									name: data[i].customerId.firstName + ' ' + data[i].customerId.lastName,
									country: data[i].customerId.country,
									userType: data[i].customerId.userType,
									registrationDate: data[i].customerId.registrationDate,
									emailId: data[i].customerId.emailId,
									mobileNo: data[i].customerId.mobileNo,
									location: data[i].customerId.addressValue,
									block: data[i].customerId.block,
									transactionId: data[i].transactionId,
									packageName: data[i].packageName,
									bookingDate: data[i].buyDate,
									embeddedLink: data[i].embeddedLink,
									domainName: data[i].domainName,
									topicValidity: data[i].topicValidity,
									paymentStatus: data[i].paymentStatus,
									payments: data[i].payments
								}

							}


						}
						cb(null)
					} else {
						cb(err);
					}
				})
		}],
		get_data: ['get_booking', function (cb) {

				var output = [];
				if (topicFinal.length) {
					for (var i in topicFinal) {

						if (mergedObject.hasOwnProperty(topicFinal[i]["topicId"])) {
							if (topicFinal[i]["contentId"]) {
								if (mergedObject[topicFinal[i]["topicId"]]["contentType"].indexOf(topicFinal[i].contentType) == -1) {
									mergedObject[topicFinal[i]["topicId"]]["contentType"].push(topicFinal[i]["contentType"]);
									mergedObject[topicFinal[i]["topicId"]]["contentPrice"].push(topicFinal[i]["contentPrice"]);
								}

							} else {
								if (mergedObject[topicFinal[i]["topicId"]]["categoryName"].indexOf(topicFinal[i].categoryName) == -1) {
									mergedObject[topicFinal[i]["topicId"]]["categoryName"].push(topicFinal[i]["categoryName"]);
									mergedObject[topicFinal[i]["topicId"]]["categoryPrice"].push(topicFinal[i]["categoryPrice"]);
								}

							}

						} else {
							mergedObject[topicFinal[i]["topicId"]] = {
								contentType: topicFinal[i]["contentType"] ? [topicFinal[i]["contentType"]] : [],
								topicId: topicFinal[i]["topicId"],
								categoryName: topicFinal[i]["contentType"] ? [] : [topicFinal[i]["categoryName"]],
								categoryPrice: topicFinal[i]["contentType"] ? [] : [topicFinal[i]["categoryPrice"]],
								// categoryPrice: [topicFinal[i]["categoryPrice"]],
								contentPrice: [topicFinal[i]["contentPrice"]],
								startDate: topicFinal[i]["startDate"],
								endDate: topicFinal[i]["endDate"]
							}
						}
					}
					for (var key in mergedObject) {
						mergerdArray.push(mergedObject[key]);
					}

					logging.consolelog(mergerdArray,"" ,"merged Array")

					async.forEach(mergerdArray, function (item, callback) {
						getTopic(item, function (err, result) {
							if (!err) {
								output = output.concat({
									topicId: item.topicId,
									name: result.name,
									rating: result.rating,
									icon: result.icon,
									price: item.price,
									language: result.language,
									startDate: item.startDate,
									endDate: item.endDate,
									contentType: item.contentType,
									categoryName: item.categoryName,
									topicPrice: result.price
								})


								if (item.categoryName.length > 0) {
									var a = 0;
									a = item.categoryPrice.reduce(add, 0);
									price += a;
									logging.consolelog(a, "a value","")
								}
								if (item.contentType.length > 0) {
									var b = 0;
									b = item.contentType.reduce(add, 0);
									price += b;
									logging.consolelog(b, "b value","")
								} else {
									logging.consolelog(a, b, price);
									price += result.price;

								}
								callback(null);
							} else {
								callback(err);
							}
						});
					}, function (err) {
						if (!err) {
							objToSave.topic = output;
							if (license > 0) {
								price = license * price;
							}

							cb(null);
						} else {

							cb(err);
						}
					});
				} else {
					cb(null);
				}

				}],
		},
		function (err, data) {
			if (!err) {
				response.status(200).send({
					final: objToSave,
					estimatedPrice: price,
					statusCode: 200,
					license: license
				});
			} else {
				response.send(err);
			}
		})
}

var bookingAction = function (request, response) {
	var customerId = null;
	var topicIds = [];
	async.auto({
		edit_booking:  function (cb) {


			var query = {
				transactionId: request.body.transactionId
			}
			if (request.body.action == 'APPROVED') {
				var update = {
					paymentStatus: constants.DATABASE.STATUS.COMPLETED,
					payments: {}

				}
				update.payments.subTotalPrice = 0;
				update.payments.discountPrice = 0;
				update.payments.totalPrice = request.body.price;

			}
			if (request.body.action == 'DECLINED') {
				var update = {
					paymentStatus: constants.DATABASE.STATUS.CANCELLED,
					payments: {}
				}
				update.payments.subTotalPrice = 0;
				update.payments.discountPrice = 0;
				update.payments.totalPrice = 0;

			}




			ServiceCustomerBuyPackage.updateTransaction(query, update, {
				lean: true
			}, function (err, data) {

				if (!err) {
					cb(null);
				} else {
					cb(err);
				}
			})


		},
		get_booking_detail: ['edit_booking', function (cb) {
			var query = {
				transactionId: request.body.transactionId
			}
			ServiceCustomerBuyPackage.getTransaction(query, {}, {}, function (err, data) {
				if (!err) {
					if (data.length > 0) {
						customerId = data[0].customerId
						if (data.length > 0 && data[0].content && data[0].content.length > 0) {
							for (var i = 0; i < data[0].content.length; i++) {
								topicIds.push(data[0].content[i].topicId);
							}

						}
					};
					cb(null);
				} else {
					cb(err);
				}
			})
		}],
		change_date: ['get_booking_detail', function (cb) {

			var query = {
				transactionId: request.body.transactionId
			}
			ServiceCustomerBuyPackage.getTransaction(query, {
				content: 1,
				topicValidity: 1
			}, {}, function (err, data) {
				if (!err) {
					var topicValidity = data[0].topicValidity;
					var date = new Date();

					var endDate = new Date(date.setMonth(date.getMonth() + parseFloat(topicValidity)));
					logging.consolelog(endDate,"", "endDate");
					if (data && data.length) {
						data[0].content.forEach(function (singleContent) {
							logging.consolelog('before>>>>>', "",singleContent.startDate)
							singleContent.startDate = new Date();
							singleContent.endDate = endDate;
							logging.consolelog('after>>>>>',"" ,singleContent.startDate)
						})
					}


					logging.consolelog(data[0].content, "","yello print kro")
					var update = {
						$set: {
							'content': data[0].content
						}
					}
					ServiceCustomerBuyPackage.updateTransaction(query, update, {}, function (err, updateData) {
						cb(null);
					})
				} else {
					cb(err);
				}
			})
		}],
		update_license: ['get_booking_detail', function (cb) {
				if (request.body.action == 'APPROVED') {

					var conditions = {
						_id: customerId,
						'license.parentCustomerId': customerId

					};
					var update = {
						$inc: {
							'license.$.allocatedLicense': request.body.license
						}
					};


					ServiceCustomer.updateCustomer(conditions, update, {
						lean: true
					}, function (err, data) {
						if (!err) {
							cb(null);
						} else {
							cb(err);
						}
					})
				} else {
					cb(null);
				}

		}],
		notify:["get_booking_detail",function (cb) {
			NotificationManager.sendNotifications(customerId, 'NOTIFY_ADMIN', {action:request.body.action});
			cb(null);

		}]

	}, function (err, data) {
		if (!err) {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.APPROVED);
		} else {
			response.send(err);
		}
	})
}



var getAgents = function (request, response) {
	var start, end;
	var final = [];
	var filter = [];
	var customersCount = null;
	var userType = [];
	var gender = [];
	var country = [];
	var grade = [];
	var query = {};

	/*
	Filtering all the data for query hit in the db*/
	if (request.body.gender) {
		for (var i = 0; i < request.body.gender.length; i++) {
			gender.push(request.body.gender[i].gender);
		}
		if (gender.length > 0) {
			query.gender = {
				$in: gender
			}
		}

	}
	if (request.body.country) {
		for (var i = 0; i < request.body.country.length; i++) {
			country.push(request.body.country[i].country);
		}
		if (country.length > 0) {
			query.country = {
				$in: country
			}
		}

	}

	if (request.body.registrationDateTo && request.body.registerationDateFrom) {
		query.registrationDate = {
			$gte: request.body.registrationDateTo,
			$lte: request.body.registerationDateFrom

		}

	}
	if (request.body.search) {
		// query["$text"] = {
		// 	$search: request.body.search
		// };
		query['$or'] = [
			{
				emailId: {
					$regex: '.*' + request.body.search + '.*',
					$options: "i"
				}
			},
			{
				firstName: {
					$regex: '.*' + request.body.search + '.*',
					$options: "i"
				}
			},
			{
				lastName: {
					$regex: '.*' + request.body.search + '.*',
					$options: "i"
				}
			},
			{
				mobileNo: {
					$regex: '.*' + request.body.search + '.*',
					$options: "i"
				}
			}
		]

	}
	async.series([
        function (cb) {
			/* getAllCustomer */
			var projection = {
				firstName: 1,
				lastName: 1,
				emailId: 1,
				mobileNo: 1,
				gender: 1,
				country: 1,
				grade: 1,
				userType: 1,
				registrationDate: 1,
				addressValue: 1,
				block: 1
			}
			var options = {
				registrationDate: 'desc'
			}

			var setOptions = {
				limit: parseInt(request.body.limit),
				skip: parseInt(request.body.skip),
				lean: true
			};
			query.userType = constants.USER_TYPE.AGENT;
			ServiceCustomer.getAllCustomer(query, projection, options, setOptions, function (err, data) {
				logging.consolelog(err, data, "------->get ALl customers");
				if (!err) {
					var i = 0;
					var length = data.length;
					for (i = 0; i < length; i++) {


						final.push({
							customerId: data[i]._id,
							name: data[i].firstName + ' ' + data[i].lastName,
							gender: data[i].gender,
							country: data[i].country,
							userType: data[i].userType,
							grade: data[i].grade,
							registrationDate: data[i].registrationDate,
							emailId: data[i].emailId,
							mobileNo: data[i].mobileNo,
							location: data[i].addressValue,
							block: data[i].block
						});
					}
					cb(null);
				} else {
					cb(err);
				}
			})

        },
        function (cb) {
			ServiceCustomer.getCustomerCount(query, function (err, data) {
				logging.consolelog(err, data, "--------->count");
				if (!err) {

					customersCount = data;
					cb(null);
				} else {
					cb(err);
				}
			})
        },
        function (cb) {
			ServiceCustomer.getDistinctCustomer({}, 'country', function (err, data) {
				logging.consolelog(err, data, "--------->country");
				if (!err) {
					for (var i = 0; i < data.length; i++) {
						country.push({
							country: data[i]
						});
					}
					cb(null);
				} else {
					cb(err);
				}

			})
        }
    ], function (err, data) {
		if (!err) {
			response.status(200).send({
				data: final,
				statusCode: 200,
				count: customersCount,
				country: country
			})
		} else {
			response.send(err);
		}
	})
}

var integrateAgentIntoSystem = function (item, callback) {
	var uniqueCode = null;
	var customerId = null;
	async.auto({
		unique_check_verify: function (cb) {

			var query = {
				$or: [{
					emailId: item.emailId
                    }]
			};
			ServiceCustomer.getCustomer(query, {}, {
				lean: true
			}, function (error, data) {
				if (error) {
					cb(error);
				} else {
					if (data && data.length > 0) {
						if (data[0].phoneVerified == true) {
							cb(constants.STATUS_MSG.ERROR.USER_ALREADY_REGISTERED)
						} else {
							ServiceCustomer.deleteCustomer({
								_id: data[0]._id
							}, function (err, updatedData) {
								if (err) cb(err)
								else cb(null);
							})
						}
					} else {
						cb(null);
					}
				}
			});


		},

		add_agent: ['unique_check_verify', function (cb) {

			var dataToSave = {};
			dataToSave.mobileNo = item.mobileNo;
			dataToSave.registrationDate = new Date().toISOString();
			dataToSave.lastName = item.lastName;
			dataToSave.firstName = item.firstName;
			if (item.longitude && item.latitude) {
				dataToSave.location = {
					"coordinates": [item.longitude, item.latitude],
					"type": "Point"
				}
			} else {

				dataToSave.location = {
					"coordinates": [0, 0],
					"type": "Point"

				}
			}
			uniqueCode = Math.floor(Math.random() * 900000) + 100000;
			dataToSave.password = UniversalFunctions.CryptData(uniqueCode);
			dataToSave.emailId = item.emailId;
			dataToSave.userType = constants.USER_TYPE.AGENT;
			dataToSave.phoneVerified = true;
			dataToSave.isApproved = true;
			dataToSave.accessRights = [
				{
					"key": "MANAGE_TOPICS",
					"peer_view": false,
					"assign": true,
					"delete": false,
					"add": false,
					"edit": false,
					"view": true
        },
				{
					"key": "MANAGE_CUSTOMERS",
					"peer_view": true,
					"assign": true,
					"delete": true,
					"add": true,
					"edit": true,
					"view": true
        },
				{
					"key": "SETTINGS",
					"peer_view": true,
					"assign": true,
					"delete": true,
					"add": true,
					"edit": true,
					"view": true
        }
    ]
			ServiceCustomer.createCustomer(dataToSave, function (err, data) {
				logging.consolelog(err, data, "create customer");
				if (!err) {
					customerId = data._id;
					cb(null);
				} else {
					cb(constants.STATUS_MSG.ERROR.ALREADY_REGISTERED_EMAIL);
				}
			})
        }],
		add_account_info: ['add_agent', function (cb) {
			var dataToSave = {};

			dataToSave.license = [];
			dataToSave.license.push({
				parentCustomerId: customerId,
				allocatedLicense: 0,
				usedLicense: 0,
				isActive: true
			})


			var update = {
				$addToSet: {

					license: {
						$each: dataToSave.license
					},
				}
			}
			logging.consolelog("update@@@@@@@@@@@","" ,update)
			ServiceCustomer.updateCustomer({
				_id: customerId
			}, update, {}, function (err, data) {
				if (!err) {
					cb(null);
				} else {
					cb(err);
				}
			})

        }],
		add_mapper: ['add_account_info', function (cb) {
			var dataToSave = {
				'childCustomerId': customerId,
				'superParentCustomerId': customerId,
				'parentCustomerType': constants.USER_TYPE.AGENT
			};
			ServiceChildMapper.createChildParentMapper(dataToSave, function (err, data) {
				if (err) {
					logging.consolelog("logging",err,"");
				}
				cb(null);
			})
		}]
	}, function (err, data) {
		if (!err) {
			callback(null);
		} else {
			callback(err);
		}
	})



}

var manageCSVData = function (data, callbackRoute) {
	var CSVData = data;
	logging.consolelog(CSVData,"" ,"!!!!!!!!!!!!!!!!!!!!!!!");
	var arrangedData = [];
	async.series([
        function (cb) {
			var DataObject = {};
			var constants = [];
			if (CSVData) {
				var taskInSubcat = [];
				for (var key in CSVData) {
					(function (key) {
						taskInSubcat.push((function (key) {
							return function (cb) {
								if (key == 0) {
									for (var i = 0; i < CSVData[key].length; i++) {
										constants.push(CSVData[key][i]);
										if (i == CSVData[key].length - 1) {
											cb();
										}
									}
								} else {

									for (var i = 0; i < CSVData[key].length; i++) {
										DataObject[constants[i]] = CSVData[key][i]
										if (i == CSVData[key].length - 1) {
											arrangedData.push(DataObject);
											logging.consolelog("arrange",arrangedData,"");
											DataObject = {};
											cb()
										}
									}
								}
							}
						})(key))
					}(key));
				}
				async.parallel(taskInSubcat, function (err, result) {
					logging.consolelog("arrangedData :::::::::::: ", err, result);
					if (err) cb(err)
					else {
						cb(null);
					}
				});
			}
        },
        function (cb) {
			logging.consolelog(arrangedData, "","CSV Data");
			for (var i = 0; i < arrangedData.length; i++) {
				var Row = i + 1;
				var error = {};
				logging.consolelog(i, arrangedData.length, "test bug")
				if (arrangedData[i].firstName == "") {
					error.statusCode = 400;
					error.customMessage = "Agent Name Cannot Be Empty at Row:" + Row;
					error.type = 'CURRICULUM_ERROR_CSV'
					cb(error);
				}
				if (arrangedData[i].lastName == "") {
					error.statusCode = 400;
					error.customMessage = "Customer Name Field Cannot Be Empty at Row:" + Row
					error.type = 'CURRICULUM_ERROR_CSV'
					cb(error);
				}

				if (arrangedData[i].emailId == "") {
					error.statusCode = 400;
					error.customMessage = "Gender Field Cannot Be Empty at Row:" + Row
					error.type = 'CURRICULUM_ERROR_CSV'
					cb(error);
				}

				if (arrangedData[i].mobileNo == "") {
					error.statusCode = 400;
					error.customMessage = "Email Id Field Cannot Be Empty at Row:" + Row
					error.type = 'CURRICULUM_ERROR_CSV'
					cb(error);
				}

				if (i == arrangedData.length - 1) {
					cb()
				}
			}
			// cb(null)
        },
        function (cb) {
			var counter = 0;
			logging.consolelog("arrangedData::","", arrangedData);
			async.forEach(arrangedData, function (item, callback) {
				counter++;
				integrateAgentIntoSystem(item, function (data) {
					logging.consolelog("data err","", data)
					callback(null);
				})
			}, function (err) {
				if (counter == arrangedData.length) {
					cb(null);
				}
			});
        }
    ], function (err, response) {
		logging.consolelog("data err2", err, response)
		if (err) callbackRoute(err);
		else callbackRoute(null);
	});
}


var agentUploadViaCSV = function (request, response) {
	async.series([
        function (cb) {
			var csvFile = request.files.UploadCsv;
			logging.consolelog("-----> csv file check::agentUploadViaCSVAPI", csvFile, request.files.UploadCsv);
			if (csvFile && csvFile.originalFilename) {
				var path = Path.resolve("..") + "/uploads/" + "file.csv";
				UploadManager.saveCSVFile(csvFile.path, path, function (err, result) {
					logging.consolelog("-----> saveCSVFile::agentUploadViaCSVAPI", err, result);
					if (err) {
						cb(constants.STATUS_MSG.ERROR.IMP_ERROR)
					} else {
						var file = path;

						fs.readFile(file, function (err, data) { // Read the contents of the file
							if (err) {
								cb(constants.STATUS_MSG.ERROR.IMP_ERROR);
							} else {
								parseCsv(data.toString(), {
									comment: '#'
								}, function (err, data) {

									if (err) {
										cb(err);
									} else {
										if (data != undefined && data != null) {
											manageCSVData(data, function (err, response) {
												logging.consolelog("-----> manageCSVData::agentUploadViaCSVAPI", err, response);
												if (err) {
													cb(err);
												} else {
													cb()
												}
											})
										} else {
											cb();
										}

									}
								});
							}
						});
					}
				})
			} else {
				cb();
			}
        }
    ], function (err, data) {
		if (!err) {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.DEFAULT);
		} else {
			response.send(err);
		}
	})


}



var purchasedPackageView = function (request, response) {
	var topicData = [];
	var agentPackage = [];
	async.auto({
		seggregate_packages: function (cb) {
			/*
			Package detail of an agent purchased packages.*/
			var query = {
				'customerId': request.body.agentId,
				'paymentStatus': constants.DATABASE.STATUS.COMPLETED
			};
			var path = 'package.packageId';
			var select = 'name icon language  price curriculum grade';
			var populate = {
				path: path,
				match: {},
				select: select,
				options: {
					lean: true
				}
			};
			if (request.body.search) {
				populate["match"]["$text"] = {
					$search: request.body.search
				};
			}

			ServiceCustomerBuyPackages.getPackages(query, {
				topic: 1,
				package: 1,
				customPackage: 1,
				packageName: 1,
				buyDate: 1,
				customerId: 1,
				content: 1
			}, populate, {}, function (err, data) {

				logging.consolelog("------->getPackages 2nd fuction::purchasedPackageViewAPI", err, JSON.stringify(data));
				if (err) {
					cb(err);
				} else {
					if (data.length) {
						for (var i in data) {
							var topicCount = 0;
							var topicData = [];
							/*
							checking delete topic
							*/

							if (data && data.length && data[i].content.length > 0) {
								for (var k = 0; k < data[i].content.length; k++) {
									if (data[i].content[k].isActive == true || data[i].content[k].isActive == undefined) {
										topicData.push(data[i].content[k].topicId);
									}

								}
							}
							topicData = _.unique(topicData);
							for (var m in topicData) {
								topicCount++;
							}
							if (topicCount && data[i].packageName) {
								agentPackage.push({
									name: data[i].packageName,
									topic: topicCount,
									type: "AGENT_PACKAGE",
									id: data[i]["_id"],
									endDate: data[i].content[0] ? data[i].content[0].endDate : false
								});
							}
						}
					}
				}
				cb(null);
			});

		}

	}, function (err, data) {
		if (!err) {
			response.status(200).send({
				statusCode: 200,
				agentPackages: agentPackage
			})
		} else {
			response.send(err);
		}
	})
}


var deletePackage = function (request, response) {

	async.auto({
		delete_package: function (cb) {

			/*
package delete with change in the payment status to cancel.Pass transaction Id as booking id.
*/
			var query = {
				_id: request.body.transactionId
			}

			var update = {
				paymentStatus: constants.DATABASE.STATUS.CANCELLED,

			}


			ServiceCustomerBuyPackage.updateTransaction(query, update, {
				lean: true
			}, function (err, data) {
				logging.consolelog("------>updateTransaction::deletePackageAPI", err, data);

				if (!err) {
					cb(null);
				} else {
					cb(err);
				}
			})

		}

	}, function (err, data) {
		if (!err) {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.PACKAGE_DELETED);
		} else {
			response.status(err);
		}
	})
}

var editPurchasedPackageView = function (request, response) {
	async.auto({
		editPackage:  function (cb) {

			/*
			Edit Package:: Can edit package Name and end date
			*/

			var query = {
				_id: request.body.transactionId
			}
			ServiceCustomerBuyPackage.getTransaction(query, {
				content: 1
			}, {}, function (err, data) {
				logging.consolelog(err, data, "--->get transaction purchased pacakge view");
				if (!err) {
					var topicValidity = data[0].topicValidity;
					var date = new Date(request.body.endDate);


					logging.consolelog(date, "","---->endDate");
					if (data && data.length) {
						data[0].content.forEach(function (singleContent) {
							logging.consolelog('before>>>>>',"", singleContent.endDate)
							singleContent.endDate = date;
							logging.consolelog('after>>>>>', "",singleContent.endDate)
						})
					}


					logging.consolelog(data[0].content, "","------>Content")
					var update = {
						$set: {}
					}
					if (request.body.endDate) {
						update["$set"]["content"] = data[0].content;
					}
					if (request.body.packageName) {
						update["$set"]["packageName"] = request.body.packageName;
					}

					ServiceCustomerBuyPackage.updateTransaction(query, update, {}, function (err, updateData) {
						logging.consolelog("-------->updated values", updateData, err);
						cb(null);
					})
				} else {
					cb(err);
				}
			})

		}

	}, function (err, data) {
		if (!err) {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.PACKAGE_EDIT);
		} else {
			response.send(err);
		}
	})
}


var purchasedPackageDetail = function (request, response) {
	var topicData = [];
	var final = {
		data: []
	}
	var purchased = [];
	var output = [];
	var shop = [];
	async.auto({
		purchased_topics: function (cb) {
			/*
			Pass transaction id as booking id.. and get package detail with populate, if payment status is completed it means it is purchased package. 
			*/
			var path = 'content.categoryId';
			var select = 'name isDeleted isPublish content'
			var populate = {
				path: path,
				match: {},
				select: select,
				options: {
					lean: true
				}
			};

			ServiceCustomerBuyPackage.getPackages({
				paymentStatus: constants.DATABASE.STATUS.COMPLETED,
				'_id': request.body.transactionId
			}, {
				content: 1,
				topic: 1
			}, populate, {

			}, function (err, data) {
				logging.consolelog(err, JSON.stringify(data), "-->responsepopulate data::purchased_topics::ServiceCustomerBuyPackage");
				if (!err) {
					if (data.length > 0) {
						for (var k = 0; k < data.length; k++) {
							for (var i = 0; i < data[k].content.length; i++) {
								if (data[k].content[i].contentType) {
									for (var j = 0; j < data[k].content[i].categoryId.content.length; j++) {

										if ((data[k].content[i].categoryId.content[j]._id).toString() == (data[k].content[i].contentType).toString()) {
											if (data[k].content[i].isActive == true || data[k].content[i].isActive == undefined) {
												/*checking delete topics, if deleted dont send to frontend, getting only topicIds, use next function for topic details*/
												purchased.push({
													topicId: data[k].content[i].topicId,
													categoryId: data[k].content[i].categoryId._id,
													categoryName: data[k].content[i].categoryId.name,
													contentId: data[k].content[i].contentId,
													contentType: data[k].content[i].categoryId.content[j].typeOfVideo
												});
											}

										}
									}

								} else {
									if (data[k].content[i].isActive == true || data[k].content[i].isActive == undefined) {
										/*checking delete topics, if deleted dont send to frontend, getting only topicIds, use next function for topic details*/
										purchased.push({
											topicId: data[k].content[i].topicId,
											categoryId: data[k].content[i].categoryId._id,
											categoryName: data[k].content[i].categoryId.name
										});
									}

								}

							}
						}

					}

					cb(null);
				} else {
					cb(err);
				}
			});

		},
		get_topic_details: ['purchased_topics', function (cb) {
			var mergedObject = {};
			var mergerdArray = [];
			logging.consolelog("-->get_topic_details::purchased data", "",purchased);
			/*
			for one topic, merging the category annd content in one array, content type is only for video category
			*/
			if (purchased.length > 0) {
				for (var i in purchased) {
					if (mergedObject.hasOwnProperty(purchased[i]["topicId"])) {
						if (purchased[i]["contentType"]) {
							if (mergedObject[purchased[i]["topicId"]]["contentType"].indexOf(purchased[i].contentType) == -1) {
								mergedObject[purchased[i]["topicId"]]["contentType"].push(purchased[i]["contentType"]);
							}

						} else {
							if (mergedObject[purchased[i]["topicId"]]["categoryName"].indexOf(purchased[i].categoryName) == -1) {
								mergedObject[purchased[i]["topicId"]]["categoryName"].push(purchased[i]["categoryName"]);
							}

						}

					} else {
						mergedObject[purchased[i]["topicId"]] = {
							contentType: purchased[i]["contentType"] ? [purchased[i]["contentType"]] : [],
							topicId: purchased[i]["topicId"],
							categoryName: purchased[i]["contentType"] ? [] : [purchased[i]["categoryName"]]
						}
					}
				}
				for (var key in mergedObject) {
					mergerdArray.push(mergedObject[key]);
				}

				logging.consolelog(mergerdArray,"", "-->mergedArray");

				/*
				get topic details
				*/
				async.forEach(mergerdArray, function (item, callback) {
					getTopic(item, function (err, result) {
						logging.consolelog(err, result, "---->response of get topic::get_topic_details");
						if (!err) {
							output = output.concat({
								topicId: item.topicId,
								name: result.name,
								rating: result.rating,
								language: result.language,
								icon: result.icon,
								contentType: item.contentType,
								categoryName: item.categoryName
							});
							callback(null);
						} else {
							callback(err);
						}
					});
				}, function (err) {
					if (!err) {
						var object = {};
						/*
		 if one topic is in different languages then send as one object and use merge object in output object for handling different languages.
				*/
						logging.consolelog("--->output array get cat array wise!!!!!!!!","", output)
						for (var i = 0; i < output.length; i++) {
							logging.consolelog("output::" ,"", i)
							if (object.hasOwnProperty(output[i]["name"])) {
								object[output[i]["name"]]["rating"].push(output[i]["rating"]);

								object[output[i]["name"]]["mergedObject"].push({
									topicId: output[i]["topicId"],
									categoryName: output[i]["categoryName"],
									contentType: output[i]["contentType"],
									isAll: (output[i]["categoryName"]).indexOf(undefined) == -1 ? false : true,
									language: output[i]["language"]
								});

							} else {
								object[output[i]["name"]] = {
									name: output[i]["name"],
									icon: output[i]["icon"],
									rating: output[i]["rating"] ? [output[i]["rating"]] : [],
									mergedObject: [{
										topicId: output[i]["topicId"],
										isAll: (output[i]["categoryName"]).indexOf(undefined) == -1 ? false : true,
										categoryName: output[i]["categoryName"],
										contentType: output[i]["contentType"],
										language: output[i]["language"]
											}]
								}
							}
						}
						var resultArray = [];
						for (var key in object) {
							resultArray.push(object[key]);
						}
						logging.consolelog(resultArray,"", "------>result array");

						shop = resultArray;
						cb(null);
					} else {
						cb(err);
					}
				});
			} else {
				cb(null);
			}
				}],

	}, function (err, data) {
		if (!err) {
			response.status(200).send({
				final: shop,
				statusCode: 200
			})
		} else {
			response.status(err);
		}
	});
}



var deletePurchasedTopic = function (request, response) {
	var agentId = null;
	async.auto({
			/*
			Verfication of token :: authorization
			*/
			delete_topic:function (cb) {
				/*
			Pass transaction id from front end is booking id and get topic Id from content table to which admin wants to delete.
			*/
				var query = {
					_id: request.body.transactionId
				}
				ServiceCustomerBuyPackage.getTransaction(query, {
					content: 1,
					customerId: 1
				}, {}, function (err, data) {
					logging.consolelog(err, data, "Response of ServiceCustomerBuyPackage,delete_topic")
					if (!err) {
						if (data && data.length) {
							agentId = data[0].customerId;
							data[0].content.forEach(function (singleContent) {
								if (request.body.topicId.indexOf(singleContent.topicId) != -1) {
									singleContent.isActive = false;
								}
							})
						}
						logging.consolelog(data[0].content, "","Response of ServiceCustomerBuyPackage,content updated");
						var update = {
							$set: {
								'content': data[0].content
							}
						}
						ServiceCustomerBuyPackage.updateTransaction(query, update, {}, function (err, updateData) {
							logging.consolelog(err, updateData, "Response of updateTransaction,content updated");
							cb(null);
						})
					} else {
						cb(err);
					}
				})
		},
		delete_in_children: ["delete_topic", function (cb) {
				/*
		If deleting a topic from agent then also delete topic from agent's child.
			*/
				ServiceAssignDataToCustomer.getAssignDataToCustomer({
					superParentCustomerId: agentId
				}, {
					content: 1
				}, {
					lean: true
				}, function (err, data) {
					logging.consolelog(err, data, " response of delete_in_children,ServiceAssignDataToCustomer");
					if (!err) {
						if (data.length > 0) {
							var counter = 0;
							async.forEach(data, function (item, embeddedCb) {
									counter++;
									item.content.forEach(function (singleContent) {
										if (request.body.topicId.indexOf(singleContent.topicId) != -1) {
											singleContent.isActive = false;
										}
									})
									var update = {
										$set: {
											'content': item.content
										}
									}
									ServiceAssignDataToCustomer.updateAssignDataToCustomer({
										_id: item._id
									}, update, {
										lean: true,
										safe: true
									}, function (err, innerdata) {
										logging.consolelog(err, innerdata, "update of child data");
										embeddedCb(null);
									})

								},
								function (err) {
									if (!err) {
										if (counter == data.length) {
											cb(null);
										}
									} else {
										cb(err);
									}
								});
						} else {
							cb(null);
						}
					} else {
						cb(null);
					}

				});
						}]
		},
		function (err, data) {
			if (!err) {
				response.status(200).send(constants.STATUS_MSG.SUCCESS.TOPIC_DELETE);
			} else {
				response.send(err);
			}
		})
}
module.exports = {
	pendingAgents: pendingAgents,
	approvedAgents: approvedAgents,
	addAgents: addAgents,
	editAgent: editAgent,
	addGeoAgents: addGeoAgents,
	getAgentDetail: getAgentDetail,
	addCollection: addCollection,
	getTopicListData: getTopicListData,
	agentPackages: agentPackages,
	getBookingDetails: getBookingDetails,
	bookingAction: bookingAction,
	getAgents: getAgents,
	agentUploadViaCSV: agentUploadViaCSV,
	categoryList: categoryList,
	purchasedPackageView: purchasedPackageView,
	deletePackage: deletePackage,
	editPurchasedPackageView: editPurchasedPackageView,
	purchasedPackageDetail: purchasedPackageDetail,
	deletePurchasedTopic: deletePurchasedTopic
		//	approveAgent:approveAgent
}