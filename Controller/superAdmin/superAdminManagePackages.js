var async = require('async');
var ServiceTopic = require('../../Service/topic');
var ServicePackage = require('../../Service/package');
var constants = require('../../Library/appConstants');
var PackageType = constants.DATABASE.PACKAGE_TYPE;
var fs = require('fs');
var Templates = require('../../Templates/superAdminRegister');
var UploadManager = require('../../Library/uploadManager');
var NotificationManager = require('../../Library/notificationManager');
var AwsConfig = Config.get('awsConfig');
var ServiceCustomer = require('../../Service/customer');
var TemplatesPackage = require('../../Templates/packageAdvertisement');
var ServiceCurriculum = require('../../Service/curriculum');

var createPackage = function (request, response) {
    var filename1 = null;
    var emailId = [];
    var customerId = [];
    var deviceTokens=[];
    async.series([
        function (cb) {
            //upload pacakge icon:: its  optional
            if (request.files.packageIcon) {
                UploadManager.uploadPicToS3Bucket(request.files.packageIcon, "profile", function (filename) {
                    if (filename == "No File") {
                        cb(null);
                    } else {
                        filename1 = AwsConfig.s3BucketCredentials.s3URL + "/profile/" + filename;
                        cb(null);
                    }
                });
            } else {
                cb(null);
            }
        },
        function (cb) {
            //there are three types of packages:: open pacakage, package(closed) and customised package:::
            //this api is handling open package and package(closed)
            if (request.body.packageType == PackageType.OPEN_PACKAGE) {


                var dataToSave = {
                    packageType: PackageType.OPEN_PACKAGE,
                    noOfTopics: request.body.noOfTopics,
                    monthlyDiscount: request.body.monthlyDiscount,
                    yearlyDiscount: request.body.yearlyDiscount,
                    monthlyPrice: request.body.monthlyPrice,
                    yearlyPrice: request.body.yearlyPrice,
                    typeOfDiscount: request.body.typeOfDiscount,
                    icon: filename1

                }

                ServicePackage.createPackage(dataToSave, function (err, data) {
                    if (!err) {
                        cb(null);
                    } else {
                        cb(err);
                    }
                })

            } else if (request.body.packageType == PackageType.PACKAGE) {
                var dataToSave = {
                    packageType: PackageType.PACKAGE,
                    monthlyDiscount: request.body.monthlyDiscount,
                    yearlyDiscount: request.body.yearlyDiscount,
                    curriculum: request.body.curriculum,
                    language: request.body.language,
                    grade: request.body.grade,
                    monthlyPrice: request.body.monthlyPrice,
                    yearlyPrice: request.body.yearlyPrice,
                    typeOfDiscount: request.body.typeOfDiscount,
                    icon: filename1
                }

                ServicePackage.createPackage(dataToSave, function (err, data) {
                    if (!err) {
                        cb(null);
                    } else {
                        cb(err);
                    }
                })
            } else {
                cb(constants.STATUS_MSG.ERROR.PACKAGE_NOT_SUPPORT);
            }
        },
        function (cb) {
            if (request.body.packageType == PackageType.PACKAGE) {
                ServiceCustomer.getCustomer({
                    $or: [{
                        userType: constants.USER_TYPE.STUDENT
                    },
                        {
                            userType: constants.USER_TYPE.TEACHER
                        },
                    ]

                }, {
                    emailId: 1,
                    deviceToken:1
                }, {}, function (err, data) {

                    if (!err) {
                        for (var i = 0; i < data.length; i++) {
                            emailId.push(data[i].emailId);
                            
                        }
                        for (var i = 0; i < data.length; i++) {
                            customerId.push(data[i]._id);
                            deviceTokens.push(data[i]["deviceToken"]);
                        }
                        cb(null);
                        NotificationManager.sendAndroidPushNotification(deviceTokens,'New' + ' ' + request.body.grade + ' ' + 'topics are available at discounted prices. Check them out now!',"PACKAGE_ADVERTISEMENT",(err,data)=>{
            console.log("test");
        })
                    } else {
                        cb(err);
                    }
                })
            } else {
                cb(null);
            }

        },
        function (cb) {
            for (var i = 0; i < emailId.length; i++) {
                var emailSend = {
                    emailId: emailId[i],
                    link: constants.COMMON_VARIABLES.link,
                    grade: request.body.grade
                }
                var text;
                text = TemplatesPackage.package(emailSend);
                NotificationManager.sendEmailToUser(emailId[i], 'Purchase Packages at DISCOUNT Rates!!', text, function (err, data) {
                    if (!err) {
                        logging.consolelog('mail', err, data);
                    }

                });
            }
            cb(null);
        }
    ], function (err, data) {
        if (!err) {
            response.status(200).send(constants.STATUS_MSG.SUCCESS.PACKAGE_ADDED);
        } else {
            response.send(err);
        }
        NotificationManager.sendNotifications(customerId, 'PACKAGE_ADVERTISEMENT', {
            packageName: request.body.curriculum
        });
        

    });
}


// function addToHistory(customerId, topicId) {
//     var query = {
//         'customerId': customerId
//     };
//     ServiceFavourites.updateFavourite(query, {
//         $push: {
//             "topicHistory": {
//                 $each: [topicId],
//                 $slice: -10
//             }
//         },
//         $set: {
//             'customerId': customerId
//         }
//     }, {
//         upsert: true
//     }, function (err, data) {
//         if (err) {
//             logging.consolelog("err", err, "");
//         }
//     })
// }
var getPackage = function (request, response) {
    var openPackage = [];
    var package = [];
    var openPackageCount;
    var packageCount;
    var customisedPackage = [];
    var customisedPackageCount;
    async.auto({
        get_package:  function (cb) {
            var setOptions = {
                skip: parseInt(request.body.skip),
                limit: 10
            }
            var query = {};
            query.isDeleted = false;
            query.packageType = PackageType.PACKAGE;
            if (request.body.search) {
                setOptions.skip = 0;
                query["$or"] = [
                    {
                        "curriculum": {
                            "$regex": '.*' + request.body.search + '.*',
                            "$options": "i"
                        }
                    },
                    {
                        "grade": {
                            "$regex": '.*' + request.body.search + '.*',
                            "$options": "i"
                        }
                    },
                    {
                        "language": {
                            "$regex": '.*' + request.body.search + '.*',
                            "$options": "i"
                        }
                    }
                ]
            }
            ServicePackage.getAllPackage(query, {}, {}, setOptions, function (err, data) {
                logging.consolelog("logging", err, data);
                if (!err) {
                    for (var i = 0; i < data.length; i++) {

                        if (data[i].packageType == PackageType.PACKAGE) {

                            package.push({
                                monthlyDiscount: data[i].monthlyDiscount,
                                yearlyDiscount: data[i].yearlyDiscount,
                                curriculum: data[i].curriculum,
                                language: data[i].language,
                                grade: data[i].grade,
                                monthlyPrice: data[i].monthlyPrice,
                                yearlyPrice: data[i].yearlyPrice,
                                packageId: data[i]._id,

                                packageType: PackageType.PACKAGE,
                                typeOfDiscount: data[i].typeOfDiscount,
                                icon: data[i].icon
                            })
                        } else {

                        }
                    }
                    cb(null);
                } else {
                    cb(err);
                }
            })
        },
        get_package_count: function (cb) {
            var query = {};
            query.isDeleted = false;
            query.packageType = PackageType.PACKAGE;
            if (request.body.search) {
                query["$or"] = [
                    {
                        "curriculum": {
                            "$regex": '.*' + request.body.search + '.*',
                            "$options": "i"
                        }
                    },
                    {
                        "grade": {
                            "$regex": '.*' + request.body.search + '.*',
                            "$options": "i"
                        }
                    },
                    {
                        "language": {
                            "$regex": '.*' + request.body.search + '.*',
                            "$options": "i"
                        }
                    }
                ]
            }
            ServicePackage.getPackageCount(query, function (err, data) {
                if (!err) {
                    packageCount = data
                    cb(null);
                } else {
                    cb(err);
                }
            })

        },
        get_open_package_count:  function (cb) {
            var query = {};
            query.isDeleted = false;
            query.packageType = PackageType.OPEN_PACKAGE;
            if (request.body.search) {
                query["$or"] = [
                    {
                        "curriculum": {
                            "$regex": '.*' + request.body.search + '.*',
                            "$options": "i"
                        }
                    },
                    {
                        "grade": {
                            "$regex": '.*' + request.body.search + '.*',
                            "$options": "i"
                        }
                    },
                    {
                        "language": {
                            "$regex": '.*' + request.body.search + '.*',
                            "$options": "i"
                        }
                    }
                ]
            }
            ServicePackage.getPackageCount(query, function (err, data) {
                if (!err) {
                    openPackageCount = data
                    cb(null);
                } else {
                    cb(err);
                }
            })

        },
        get_open_package: function (cb) {
            var setOptions = {
                skip: parseInt(request.body.skip),
                limit: 10
            }
            var query = {};
            query.isDeleted = false;
            query.packageType = PackageType.OPEN_PACKAGE;
            if (request.body.search) {
                setOptions.skip = 0;
                query["$or"] = [
                    {
                        "curriculum": {
                            "$regex": '.*' + request.body.search + '.*',
                            "$options": "i"
                        }
                    },
                    {
                        "grade": {
                            "$regex": '.*' + request.body.search + '.*',
                            "$options": "i"
                        }
                    },
                    {
                        "language": {
                            "$regex": '.*' + request.body.search + '.*',
                            "$options": "i"
                        }
                    }
                ]
            }
            ServicePackage.getAllPackage(query, {}, {}, setOptions, function (err, data) {
                logging.consolelog("logging", err, data);
                if (!err) {
                    for (var i = 0; i < data.length; i++) {

                        if (data[i].packageType == PackageType.PACKAGE) {
                        } else {
                            openPackage.push({
                                monthlyDiscount: data[i].monthlyDiscount,
                                yearlyDiscount: data[i].yearlyDiscount,
                                monthlyPrice: data[i].monthlyPrice,
                                yearlyPrice: data[i].yearlyPrice,
                                noOfTopics: data[i].noOfTopics,
                                packageId: data[i]._id,
                                packageType: PackageType.OPEN_PACKAGE,
                                typeOfDiscount: data[i].typeOfDiscount,
                                icon: data[i].icon

                            })
                        }
                    }
                    cb(null);
                } else {
                    cb(err);
                }
            })
        },
        get_customised_package: function (cb) {
            var setOptions = {
                skip: parseInt(request.body.skip),
                limit: 10
            }
            var query = {};
            query.isDeleted = false;
            query.packageType = PackageType.CUSTOMISED_PACKAGE;
            if (request.body.search) {
                setOptions.skip = 0;
                query["$or"] = [
                    {
                        "curriculum": {
                            "$regex": '.*' + request.body.search + '.*',
                            "$options": "i"
                        }
                    },
                    {
                        "grade": {
                            "$regex": '.*' + request.body.search + '.*',
                            "$options": "i"
                        }
                    },
                    {
                        "language": {
                            "$regex": '.*' + request.body.search + '.*',
                            "$options": "i"
                        }
                    }
                ]
            }
            ServicePackage.getAllPackage(query, {}, {}, setOptions, function (err, data) {
                logging.consolelog("logging", err, data);
                if (!err) {
                    for (var i = 0; i < data.length; i++) {

                        if (data[i].packageType == PackageType.CUSTOMISED_PACKAGE) {
                            customisedPackage.push({
                                monthlyDiscount: data[i].monthlyDiscount,
                                yearlyDiscount: data[i].yearlyDiscount,
                                monthlyPrice: data[i].monthlyPrice,
                                yearlyPrice: data[i].yearlyPrice,
                                packageId: data[i]._id,
                                packageType: PackageType.CUSTOMISED_PACKAGE,
                                typeOfDiscount: data[i].typeOfDiscount,
                                topics: data[i].topics,
                                icon: data[i].icon,
                                packageName: data[i].packageName
                            })
                        } else {

                        }
                    }
                    cb(null);
                } else {
                    cb(err);
                }
            });
        },
        get_customised_package_count: function (cb) {
            var query = {};
            query.isDeleted = false;
            query.packageType = PackageType.CUSTOMISED_PACKAGE;
            if (request.body.search) {
                query["$or"] = [
                    {
                        "curriculum": {
                            "$regex": '.*' + request.body.search + '.*',
                            "$options": "i"
                        }
                    },
                    {
                        "grade": {
                            "$regex": '.*' + request.body.search + '.*',
                            "$options": "i"
                        }
                    },
                    {
                        "language": {
                            "$regex": '.*' + request.body.search + '.*',
                            "$options": "i"
                        }
                    }
                ]
            }
            ServicePackage.getPackageCount(query, function (err, data) {
                if (!err) {
                    customisedPackageCount = data
                    cb(null);
                } else {
                    cb(err);
                }
            })
        }
    }, function (err, data) {
        if (!err) {
            response.status(200).send({
                package: package,
                openPackageCount: openPackageCount,
                packageCount: packageCount,
                openPackage: openPackage,
                customisedPackageCount: customisedPackageCount,
                customisedPackage: customisedPackage
            })
        } else {
            response.send(err);
        }
    })
}


var editPackage = function (request, response) {
    async.series([
        function (cb) {
            var update = {
                $unset: {
                    monthlyDiscount: 1,
                    yearlyDiscount: 1,
                    monthlyPrice: 1,
                    yearlyPrice: 1,
                    typeOfDiscount: 1
                }
            }
            ServicePackage.updatePackage({
                _id: request.body.packageId
            }, update, {}, function (err, data) {
                if (!err) {
                    cb(null);
                } else {
                    cb(err);
                }
            });

        },
        function (cb) {
            logging.consolelog("logging", request.body, "edit body::1");
            var update = {};
            if (request.body.packageType == PackageType.PACKAGE) {
                if (request.body.monthlyDiscount != undefined) {
                    update.monthlyDiscount = request.body.monthlyDiscount;
                }
                if (request.body.yearlyDiscount != undefined) {
                    update.yearlyDiscount = request.body.yearlyDiscount;
                }
                if (request.body.curriculum != undefined) {
                    update.curriculum = request.body.curriculum;
                }
                if (request.body.language != undefined) {
                    update.language = request.body.language;
                }
                if (request.body.grade != undefined) {
                    update.grade = request.body.grade;
                }
                if (request.body.monthlyPrice != undefined) {
                    update.monthlyPrice = request.body.monthlyPrice;
                }
                if (request.body.yearlyPrice != undefined) {
                    update.yearlyPrice = request.body.yearlyPrice;
                }
            }
            if (request.body.packageType == PackageType.OPEN_PACKAGE) {
                if (request.body.monthlyDiscount != undefined) {
                    update.monthlyDiscount = request.body.monthlyDiscount;
                }
                if (request.body.yearlyDiscount != undefined) {
                    update.yearlyDiscount = request.body.yearlyDiscount;
                }
                if (request.body.noOfTopics != undefined) {
                    update.noOfTopics = request.body.noOfTopics;
                }
                if (request.body.monthlyPrice != undefined) {
                    update.monthlyPrice = request.body.monthlyPrice;
                }
                if (request.body.yearlyPrice != undefined) {
                    update.yearlyPrice = request.body.yearlyPrice;
                }
            }
            if (request.body.typeOfDiscount != undefined) {
                update.typeOfDiscount = request.body.typeOfDiscount;
            }
            var updateQuery = {
                $set: update
            }
            ServicePackage.updatePackage({
                _id: request.body.packageId
            }, updateQuery, {}, function (err, result) {
                logging.consolelog(err, result, "packeage edit ");
                if (!err) {
                    cb(null);
                } else {
                    cb(err);
                }
            })

        },

    ], function (err, result) {
        if (!err) {
            response.status(200).send(constants.STATUS_MSG.SUCCESS.PACKAGE_EDIT);
        } else {
            response.send(err);
        }
    })
}


var deletePackage = function (request, response) {
    async.series([
        function (cb) {
            ServicePackage.updatePackage({
                _id: request.body.packageId
            }, {
                $set: {
                    isDeleted: true
                }
            }, {}, function (err, data) {
                if (!err) {
                    cb(null);
                } else {
                    cb(err);
                }
            })
        }
    ], function (err, data) {
        if (!err) {
            response.status(200).send(constants.STATUS_MSG.SUCCESS.PACKAGE_DELETED);
        } else {
            response.send(err);
        }
    })
}


var createCustomisePackage = function (request, response) {
    var filename1 = null;
    var topicIds = [];
    var board = [];
    var grade = [];
    var subject = [];
    var chapter = [];
    var language = [];
    if (request.body.language) {
        for (var i = 0; i < request.body.language.length; i++) {
            language.push(request.body.language[i].language)
        }
    }
    if (request.body.board) {
        for (var i = 0; i < request.body.board.length; i++) {
            board.push(request.body.board[i].board)
        }
    }
    if (request.body.grade) {
        for (var i = 0; i < request.body.grade.length; i++) {
            grade.push(request.body.grade[i].grade)
        }
    }
    if (request.body.subject) {
        for (var i = 0; i < request.body.subject.length; i++) {
            subject.push(request.body.subject[i].subject)
        }

    }
    if (request.body.chapter) {
        for (var i = 0; i < request.body.chapter.length; i++) {
            chapter.push(request.body.chapter[i].chapter)
        }
    }



    logging.consolelog(request.body, "", "topic array");
    request.body.topic = request.body.selectAll ? [] : request.body.topic;
    async.series([
        function (cb) {

            if (request.body.selectAll) {
                // var projection = {
                // 	'result.topicId': 1
                //
                // }
                var query = {};
                if (language.length > 0) {
                    query.language = {
                        $in: language
                    },
                        query.isRemoved = "false";

                }
                if (board.length > 0) {

                    query.board = {
                        $in: board
                    }
                }
                if (grade.length > 0) {

                    query.grade = {
                        $in: grade
                    }
                }
                if (subject.length > 0) {

                    query.subject = {
                        $in: subject
                    }
                }
                if (chapter.length > 0) {

                    query.chapter = {
                        $in: chapter
                    }
                }
                if (!(language.length > 0)) {
                    query.board = constants.COMMON_VARIABLES.UNIVERSAL_BOARD_NAME;
                    query.isRemoved = "false";

                }

                logging.consolelog("query","", query);
                var limit = {
                    limit: request.body.limit ? request.body.limit : 5000,
                    offset: request.body.skip ? request.body.skip : 0
                }
                var search = "";
                if (request.body.search) {
                    search = ' and name like "%' + request.body.search + '%" ';
                }
                ServiceCurriculum.getCurriculumAndTopic(query, limit, search, function (err, data) {
                    if (!err) {

                        if (data.length) {
                            for (var i = 0; i < data.length; i++) {
                                logging.consolelog("check deletd topics ", data[i].name, data[i].ids.split(","))

                                request.body.topic.push({
                                    topic: data[i].name,
                                    language: data[i].topicLanguage
                                    // id: data[i].ids.split(",")
                                })
                            }
                            cb(null);
                        }else{
                            cb(null)
                        }
                    } else {
                        cb(err);
                    }
                });
            } else {
                cb(null);

            }
        },
        function (cb) {

            var counter = 0;
            async.forEach(request.body.topic, function (item, embeddedcb) {
                counter++;
                var data = {
                    name: item.topic,
                    language: item.language,
                    isDeleted:false
                };

                ServiceTopic.getTopic(data, {
                    _id: 1
                }, {
                    lean: true
                }, function (err, response) {
                    if (err) {
                        embeddedcb(null);
                    } else {
                        logging.consolelog("topic Id","", response);
                        if(response.length>0)
                        topicIds.push(response[0]._id);
                        embeddedcb(null);
                    }
                });


            }, function (err) {
                if (counter == request.body.topic.length) {
                    cb();
                }

            });

        },
        function (cb) {
            if (request.files.packgeIcon) {
                UploadManager.uploadPicToS3Bucket(request.files.packgeIcon, "profile", function (filename) {
                    if (filename == "No File") {
                        cb(null);
                    } else {
                        filename1 = AwsConfig.s3BucketCredentials.s3URL + "/profile/" + filename;
                        cb(null);
                    }
                });
            } else {
                cb(null);
            }
        },
        function (cb) {
            var data = {
                topics: topicIds,
                packageType: PackageType.CUSTOMISED_PACKAGE,
                monthlyDiscount: request.body.monthlyDiscount,
                yearlyDiscount: request.body.yearlyDiscount,
                monthlyPrice: request.body.monthlyPrice,
                yearlyPrice: request.body.yearlyPrice,
                typeOfDiscount: request.body.typeOfDiscount,
                icon: filename1,
                packageName: request.body.packageName
            }

            ServicePackage.createPackage(data, function (err, data) {
                if (!err) {
                    cb(null);
                } else {
                    cb(err);
                }
            })
        }
    ], function (err, data) {
        if (!err) {
            response.status(200).send(constants.STATUS_MSG.SUCCESS.PACKAGE_ADDED);
        } else {
            response.send(err);
        }
    })
}

module.exports = {
    createPackage: createPackage,
    getPackage: getPackage,
    editPackage: editPackage,
    deletePackage: deletePackage,
    createCustomisePackage: createCustomisePackage
}