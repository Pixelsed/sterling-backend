var async = require('async');
var ServiceCurriculum = require('../../Service/curriculum');
var constants = require('../../Library/appConstants');
var fs = require('fs');
var parseCsv = require('csv-parse');
var Path = require('path');
var Templates = require('../../Templates/superAdminRegister');
var UploadManager = require('../../Library/uploadManager');

var ServiceTopicCurriculumMapper = require('../../Service/topicCurriculumMapping');

var manageCSVData = function (data, callback) {
    var CSVData = data;
    var arrangedData = [];
    async.series([
        function (cb) {
            var DataObject = {};
            var constants = [];
            if (CSVData) {
                var taskInSubcat = [];
                for (var key in CSVData) {
                    (function (key) {
                        taskInSubcat.push((function (key) {
                            return function (cb) {
//TODO
//logging.consolelog("item:::::::::>>>>",key,CSVData[key],CSVData[key].length)
                                if (key == 0) {
//logging.consolelog("skip : ","",key);
                                    for (var i = 0; i < CSVData[key].length; i++) {
                                        constants.push(CSVData[key][i]);
                                        if (i == CSVData[key].length - 1) {
                                            if (constants.length > 9) {
                                                var error = {};
                                                error.statusCode = 400;
                                                error.customMessage = "There are extra field in CSV please match the format";
                                                cb(error);
                                            } else {
                                                cb()
                                            }
                                        }
                                    }
                                } else {

                                    for (var i = 0; i < CSVData[key].length; i++) {
                                        DataObject[constants[i]] = CSVData[key][i]
                                        if (i == CSVData[key].length - 1) {
                                            arrangedData.push(DataObject);
                                            DataObject = {};
                                            cb()
                                        }
                                    }
                                }
                            }
                        })(key))
                    }(key));
                }
                async.parallel(taskInSubcat, function (err, result) {
                    logging.consolelog("arrangedData :::::::::::: ", "", arrangedData)
                    if (err) cb(err)
                    else {
                        cb(null);
                    }
                });
            }
        },
        function (cb) {
            logging.consolelog("CSV Data", "", arrangedData);
            for (var i = 0; i < arrangedData.length; i++) {
                var Row = i + 1;
                var error = {};
                logging.consolelog("test bug", i, arrangedData.length)
                if (arrangedData[i].board == "") {
                    error.statusCode = 400;
                    error.customMessage = "Board Cannot Be Empty at Row:" + Row;
                    error.type = 'CURRICULUM_ERROR_CSV'
                    cb(error);
                }
                if (arrangedData[i].subject == "") {
                    error.statusCode = 400;
                    error.customMessage = "Subject Field Cannot Be Empty at Row:" + Row
                    error.type = 'CURRICULUM_ERROR_CSV'
                    cb(error);
                }
                if (arrangedData[i].language == "") {
                    error.statusCode = 400;
                    error.customMessage = "Language Field Cannot Be Empty at Row:" + Row
                    error.type = 'CURRICULUM_ERROR_CSV'
                    cb(error);
                }
                if (arrangedData[i].chapter == "") {
                    error.statusCode = 400;
                    error.customMessage = "Chapter Field Cannot Be Empty at Row:" + Row
                    error.type = 'CURRICULUM_ERROR_CSV'
                    cb(error);
                }
                if (arrangedData[i].grade == "") {
                    error.statusCode = 400;
                    error.customMessage = "Grade Field Cannot Be Empty at Row:" + Row
                    error.type = 'CURRICULUM_ERROR_CSV'
                    cb(error);
                }
                if (i == arrangedData.length - 1) {
                    cb()
                }
            }
// cb(null)
        },
        function (cb) {
            /*
             Save data in the database
             */
            var counter = 0;
            async.forEach(arrangedData, function (item, embeddedcb) {
                counter++
                var data = {
                    board: item.board.charAt(0).toUpperCase() + item.board.slice(1),
                    grade: item.grade.charAt(0).toUpperCase() + item.grade.slice(1),
                    chapter: item.chapter.charAt(0).toUpperCase() + item.chapter.slice(1),
                    subject: item.subject.charAt(0).toUpperCase() + item.subject.slice(1),
                    language: item.language.charAt(0).toUpperCase() + item.language.slice(1),
                    isRemoved: false
                }
                ServiceCurriculum.getCurriculum(data, {
                    _id: 1
                }, {}, function (err, res) {
                    if (res.length > 0) {
                        embeddedcb(null);
                    } else {
                        ServiceCurriculum.createCurriculum(data, function (err, response) {

                            if (err) {
                                logging.consolelog("err,response", err, response);
                                embeddedcb(null);
                            } else {

                                embeddedcb(null);
                            }
                        });
                    }
                })


            }, function (err) {
                if (counter == arrangedData.length) {
                    cb();
                }

            });
        }
    ], function (err, response) {
        if (err) callback(err);
        else callback(null);
    });
}


var curriculumUploadViaCSV = function (request, response) {
    var curriculum = [];
    async.series([
        function (cb) {
            /*
             Read CSV file and Save in uploads for temporary time.
             */

            var csvFile = request.files.UploadCsv;

            if (csvFile && csvFile.originalFilename) {
                var path = Path.resolve("..") + "/uploads/" + "file.csv";
                UploadManager.saveCSVFile(csvFile.path, path, function (err, result) {
                    if (err) {
                        logging.consolelog("save csv file", err, "");
                        cb(constants.STATUS_MSG.ERROR.IMP_ERROR)
                    } else {
                        logging.consolelog("save csv file", err, result);
                        var file = path;

                        fs.readFile(file, function (err, data) { // Read the contents of the file
                            if (err) {
                                logging.consolelog("read csv file", err, "");
                                cb(constants.STATUS_MSG.ERROR.IMP_ERROR);
                            } else {
                                parseCsv(data.toString(), {
                                    comment: '#'
                                }, function (err, data) {

                                    if (err) {
                                        cb(err);
                                    } else {
                                        if (data != undefined && data != null) {
                                            /*
                                             function call(manageCSVData) to save data in the database
                                             */
                                            manageCSVData(data, function (err, response) {
                                                if (err) {
                                                    logging.consolelog("manage CSV Data", err, "");
                                                    cb(err);
                                                } else {

                                                    curriculum = response;

                                                    cb()
                                                }
                                            })
                                        } else {
                                            cb();
                                        }

                                    }
                                });
                            }
                        });
                    }
                })
            } else {
                cb();
            }
        }
    ], function (err, data) {

        if (!err) response.status(200).send(constants.STATUS_MSG.SUCCESS.CURRICULUM_ADDED);
        else {
            response.send(err);
        }
    });
}


var curriculumUploadViaManually = function (request, response) {

    async.series([
        function (cb) {
            /*
             Validate all parameters from frontend
             */
            if (request.body.board && request.body.grade && request.body.chapter && request.body.subject && request.body.language) {
                cb(null);
            } else {
                cb(constants.STATUS_MSG.ERROR.VALIDATION_FAIL);
            }
        },
        function (cb) {

            var data = {

                board: request.body.board.charAt(0).toUpperCase() + request.body.board.slice(1),
                grade: request.body.grade.charAt(0).toUpperCase() + request.body.grade.slice(1),
                chapter: request.body.chapter.charAt(0).toUpperCase() + request.body.chapter.slice(1),
                subject: request.body.subject.charAt(0).toUpperCase() + request.body.subject.slice(1),
                language: request.body.language.charAt(0).toUpperCase() + request.body.language.slice(1),
                isRemoved: false
            }

            ServiceCurriculum.getCurriculum(data, {
                _id: 1
            }, {}, function (err, res) {
                logging.consolelog("existing", err, res);
                if (res.length > 0) {
                    /*
                     If curriculum values exists then throw error,
                     */
                    cb(constants.STATUS_MSG.ERROR.CURRICULUM_ERROR_EXIST);
                } else {
                    /*
                     If new grade then make entry in grade_seq table,else no entry in grade_seq table
                     */
                    ServiceCurriculum.checkIfGradeExistInSorting(data, function (existing) {
                        if (existing == 1) {
                            logging.consolelog("existing", "", existing);
                            cb(constants.STATUS_MSG.ERROR.CURRICULUM_ERROR_EXIST);
                        } else {
                            /*
                             Combination of CGCS is new then make entry in curriculum table,
                             */
                            ServiceCurriculum.createCurriculum(data, function (err, response) {
                                if (err) {
                                    logging.consolelog("createCurriculum", err, response);
                                    cb(constants.STATUS_MSG.ERROR.CURRICULUM_ERROR_EXIST);
                                } else {
                                    cb(null);
                                }
                            });
                        }
                    })
                }
            })

        }
    ], function (err, data) {

        if (!err) response.status(200).send(constants.STATUS_MSG.SUCCESS.CURRICULUM_ADDED);
        else {
            response.send(err);
        }
    });
}


// var GetCurriculumData = function (data, language, callbackRoute) {
//     var gradeCount = 0;
//     var subjectCount = 0;
//     var chapterCount = 0;
//     var topicCount = 0;
//     var languageAvailable = [];
//     async.series([
//
//             function (cb) {
//                 ServiceCurriculum.getUniqueCurriculum({
//                     board: data.boards,
//                 }, 'language', function (err, data) {
//                     if (!err) {
//                         if (data.length > 0) {
//                             for (var i = 0; i < data.length; i++) {
//                                 languageAvailable.push({
//                                     language: data[i]
//                                 });
//                             }
//                         }
//                         cb(null);
//                     } else {
//                         cb(err);
//                     }
//                 })
//
//             },
//             function (cb) {
//                 var query = {};
//                 if (language) {
//                     query.board = data.boards,
//                         query.language = language
//                 } else {
//                     query.board = data.boards
//                 }
//                 logging.consolelog("query", "", query);
//                 ServiceCurriculum.getUniqueCurriculum(query, 'grade', function (err, data) {
//                     if (!err) {
//                         if (data.length > 0) {
//                             gradeCount = data.length;
//                         }
//                         cb(null);
//                     } else {
//                         cb(err);
//                     }
//                 })
//             },
//             function (cb) {
//                 var query = {};
//                 if (language) {
//                     query.board = data.boards,
//                         query.language = language
//                 } else {
//                     query.board = data.boards
//                 }
//                 ServiceCurriculum.getUniqueCurriculum(query, 'subject', function (err, data) {
//                     if (!err) {
//                         if (data.length > 0) {
//                             subjectCount = data.length;
//                         }
//                         cb(null);
//                     } else {
//                         cb(err);
//                     }
//                 })
//             },
//             function (cb) {
//                 var query = {};
//                 if (language) {
//                     query.board = data.boards,
//                         query.language = language
//                 } else {
//                     query.board = data.boards
//                 }
//                 ServiceCurriculum.getUniqueCurriculum(query, 'chapter', function (err, data) {
//                     if (!err) {
//                         if (data.length > 0) {
//                             chapterCount = data.length;
//                         }
//                         cb(null);
//                     } else {
//                         cb(err);
//                     }
//                 })
//             },
//
//         ],
//         function (error) {
//             if (error) {
//                 callbackRoute(error);
//             } else {
//                 callbackRoute(null, {
//
//                     gradeCount: gradeCount,
//                     subjectCount: subjectCount,
//                     chapterCount: chapterCount,
//                     topicCount: topicCount,
//                     languageAvailable: languageAvailable
//                 });
//             }
//         }
//     );
// };

var curriculumListing = function (request, response) {
    var final = [];
    var languageList = {};
    var languagesData = [];

    async.series([
        function (cb) {
            /*
             Get Count of subjects chapters and grades in all curriculums
             */
            var query = {};
            if (request.body.language) {
                query.language = request.body.language.language;
                query.isRemoved = false;
            } else {
                query.isRemoved = false;
            }
            var search = "";
            if (request.body.search) {
                search = ' and board like "%' + request.body.search + '%" ';
            }
            ServiceCurriculum.getCurriculumListingCount(query, search, function (error, results) {
                if (error) {
                    logging.consolelog("getCurriculumListingCount", error, "");
                    cb(null);
                } else {
                    if (results) {
                        results.forEach(function (result) {
                            var languageForCurriculum = [];
                            var languages = result.language.split(",");
                            for (var i in languages) {
                                languageForCurriculum.push({
                                    "language": languages[i]
                                });
                            }

                            final.push({
                                board: result.board,
                                language: languageForCurriculum,
                                gradeCount: result.gradeCount,
                                subjectCount: result.subjectCount,
                                chapterCount: result.chapterCount,
                                topicCount: result.topicCount
                            })
                        })
                    }
                    cb(null);
                }
            });

        },
        function (cb) {
            var query = {
                'isRemoved': false
            };
            var search = "";
            if (request.body.search) {
                search = ' and board like "%' + request.body.search + '%" ';
            }
            ServiceCurriculum.getCurriculumListingCount(query, search, function (error, results) {
                if (error) {
                    logging.consolelog("getCurriculumListingCount", error, "");
                    cb(null);
                } else {
                    if (results) {
                        results.forEach(function (result) {
                            var languageForCurriculum = [];
                            var languages = result.language.split(",");
                            for (var i in languages) {
                                languageList[languages[i]] = {
                                    "language": languages[i]
                                };
                            }
                        })
                    }
                    cb(null);
                }
            });

        }

    ], function (err, data) {
        for (var key in languageList) {
            languagesData.push(languageList[key]);
        }

        if (!err) response.status(200).send({
            statusCode: 200,
            data: final,
            languages: languagesData
        });
        else {
            response.send(err);
        }
    });
}


var copyExistingCurriculumByLanguage = function (request, response) {
    var existingBoardData = [];
    async.series([
        function (cb) {
            /* Get Existing curriculum data for cloning*/
            ServiceCurriculum.getCurriculum({
                board: request.body.existingBoard,
                language: request.body.existingBoardLanguage
            }, {}, {
                lean: true
            }, function (err, data) {
                if (!err) {
                    existingBoardData = data;
                    cb(null);
                } else {
                    logging.consolelog("getCurriculum", err, data);
                    cb(err);
                }
            })
        },
        function (cb) {
            /*
             Cloning of data for universal and other curriculums are different.
             For universal topics will not cloned only curriclums values get cloned,
             For remaining curricvulum topics will also get cloned.
             */
            logging.consolelog("------check condition:: cgcs module file", request.body.cloneTopic, request.body.newBoard, request.body.cloneTopic == false, request.body.newBoard != constants.COMMON_VARIABLES.UNIVERSAL_BOARD_NAME);
            if (request.body.cloneTopic == false || request.body.newBoard == constants.COMMON_VARIABLES.UNIVERSAL_BOARD_NAME) {


                async.forEach(existingBoardData, function (item, callback) {
                    var data = {
                        existingBoardId: item["_id"],
                        newBoardName: request.body.newBoard,
                        newBoardLanguage: request.body.newBoardLanguage
                    }
                    ServiceCurriculum.cloneCurriculumByLanguage(data, function () {
                        callback(null);
                    });
                }, function (err) {
                    if (err) {
                        cb(err);
                    } else {
                        cb(null);
                    }
                });
            } else {
                async.forEach(existingBoardData, function (item, callback) {
                    var data = {
                        existingBoardId: item["_id"],
                        newBoardName: request.body.newBoard,
                        newBoardLanguage: request.body.newBoardLanguage
                    }
                    ServiceCurriculum.cloneCurriculumByLanguageByTopic(data, function () {
                        callback(null);
                    });
                }, function (err) {
                    if (err) {
                        cb(err);
                    } else {
                        cb(null);
                    }
                });

            }

        }
    ], function (err, data) {

        if (!err) response.status(200).send(constants.STATUS_MSG.SUCCESS.CURRICULUM_ADDED);
        else {
            response.send(err);
        }
    });
}


var GetCurriculumGradeWise = function (grade, language, board, callbackRoute) {
    var subjects = [];
    async.series([

            function (cb) {
                ServiceCurriculum.getUniqueCurriculum({
                    board: board,
                    language: language,
                    grade: grade,
                    isRemoved: false
                }, 'subject', function (err, data) {
                    if (!err) {
                        if (data.length > 0) {
                            for (var i = 0; i < data.length; i++) {
                                subjects.push(data[i].subject);
                            }
                        }
                        cb(null);
                    } else {
                        cb(err);
                    }
                })
            }
        ],
        function (error) {
            if (error) {
                callbackRoute(error);
            } else {
                callbackRoute(null, {
                    statusCode: 200,
                    subjects: subjects
                });
            }
        }
    );
};
var curriculumGradeListing = function (request, response) {
    var grades = [];
    var final = [];
    async.series([
        function (cb) {
            ServiceCurriculum.getSortedData({
                board: request.body.board,
                language: request.body.language,
                isRemoved: false
            }, 'grade', function (err, data) {
                if (!err) {
                    if (data.length > 0) {
                        for (var i = 0; i < data.length; i++) {
                          if(data[i].grade_name!=null){
                            grades.push(data[i].grade_name);
                          }  
                            
                        }
                    }
                    cb(null);
                } else {
                    cb(err);
                }
            })

        },
        function (cb) {
            logging.consolelog("data2::", "", grades);
            if (grades.length > 0) {
                async.each(grades, function (item, callback) {
                    GetCurriculumGradeWise(item, request.body.language, request.body.board, function (error, result) {
                        if (error) {
                            callback(error);
                        } else {
                            final.push({
                                board: request.body.board,
                                language: request.body.language,
                                grade: item,
                                subjects: result.subjects,
                            })
                            callback();
                        }
                    });
                }, function (err) {
                    if (err) {
                        cb(err);
                    } else {
                        cb(null);
                    }
                });
            } else {
                cb(null);
            }

        }
    ], function (err, data) {

        if (!err) response.status(200).send({
            statusCode: 200,
            data: final
        });
        else {
            response.send(err);
        }
    });
}

var addChapterManually = function (request, response) {
    async.series([
        function (cb) {
            logging.consolelog("request", "", request.body);
            var data = {
                board: request.body.board.charAt(0).toUpperCase() + request.body.board.slice(1),
                grade: request.body.grade.charAt(0).toUpperCase() + request.body.grade.slice(1),
                chapter: request.body.chapter.charAt(0).toUpperCase() + request.body.chapter.slice(1),
                subject: request.body.subject.charAt(0).toUpperCase() + request.body.subject.slice(1),
                language: request.body.language.charAt(0).toUpperCase() + request.body.language.slice(1),
                isRemoved: false
            }
            ServiceCurriculum.getCurriculum(data, {
                _id: 1
            }, {}, function (err, res) {
                if (res.length > 0) {
                    cb(null);
                } else {
                    ServiceCurriculum.checkIfGradeExistInSorting(data, function (existing) {
                        if (existing == 1) {
                            cb(constants.STATUS_MSG.ERROR.CURRICULUM_ERROR_EXIST);
                        } else {
                            ServiceCurriculum.createCurriculum(data, function (err, response) {
                                if (err) {
                                    cb(err);
                                } else {
                                    cb(null);
                                }
                            });
                        }
                    })
                }
            })

        }
    ], function (err, data) {

        if (!err) response.status(200).send(constants.STATUS_MSG.SUCCESS.CHAPTER_ADDED);
        else {
            response.send(err);
        }
    });
}


var chapterDetailsForSubject = function (request, response) {
    var details = [];
    var query = {
        board: request.body.board,
        language: request.body.language,
        grade: request.body.grade,
        subject: request.body.subject,
        isRemoved: false
    }

    ServiceCurriculum.getUniqueCurriculum(query, 'chapter', function (err, data) {
        if (err) {
            response.send(err);
        } else {
            for (var i = 0; i < data.length; i++) {
                if (data[i].chapter == null) {

                } else {
                    details.push(data[i]);
                }
            }
            response.status(200).send({
                statusCode: 200,
                data: details

            });
        }

    })
}


var deleteChapterOfSubject = function (request, response) {

    var query = {
        board: request.body.board,
        language: request.body.language,
        grade: request.body.grade,
        subject: request.body.subject,
        chapter: request.body.chapter
    }

    ServiceCurriculum.disableCurriculum(query, function (err, data) {
        console.log("---> delete chapter of subject", err,data);
        if (!err) {
            response.status(200).send(constants.STATUS_MSG.SUCCESS.CHAPTER_DELETED);
        } else {
            response.send(err);
        }
    });
}


var deleteSubjectOfGrade = function (request, response) {

    var query = {
        board: request.body.board,
        language: request.body.language,
        grade: request.body.grade,
        subject: request.body.subject

    }
    ServiceCurriculum.disableCurriculum(query, function (err, data) {
        logging.consolelog("error>>", err, data);
        if (!err) {
            response.status(200).send(constants.STATUS_MSG.SUCCESS.SUBJECT_DELETED);
        } else {
            response.send(err);
        }
    });
}

var deleteCurriculum = function (request, response) {

    var query = {
        board: request.body.board,
        language: request.body.language

    }
    ServiceCurriculum.updateCurriculum(query, {
        $set: {
            isRemoved: true
        }
    }, function (err, data) {
        if (!err) {
            response.status(200).send(constants.STATUS_MSG.SUCCESS.CURRICULUM_DELETED);
        } else {
            response.send(err);
        }
    });
}

var deleteGradeOfCurriculum = function (request, response) {

    var query = {
        board: request.body.board,
        language: request.body.language,
        grade: request.body.grade

    }
    ServiceCurriculum.disableCurriculum(query, function (err, data) {
        if (!err) {
            response.status(200).send(constants.STATUS_MSG.SUCCESS.GRADE_DELETED);
        } else {
            response.send(err);
        }
    });
}

var addGrade = function (request, response) {
    async.auto({
        addGrade: function (cb) {
            var dataToSave = {
                board: request.body.board.charAt(0).toUpperCase() + request.body.board.slice(1),
                grade: request.body.grade.charAt(0).toUpperCase() + request.body.grade.slice(1),
                language: request.body.language.charAt(0).toUpperCase() + request.body.language.slice(1),
                isRemoved: false
            }
            ServiceCurriculum.getCurriculum(dataToSave, {
                _id: 1
            }, {}, function (err, res) {

                if (res.length > 0) {
                    cb(null);
                } else {
                    logging.consolelog('entering>>>>>>>>>>>>>>>', "", "")
                    ServiceCurriculum.checkIfGradeExistInSorting(dataToSave, function (existing) {
                        if (existing == 1) {
                            cb(constants.STATUS_MSG.ERROR.CURRICULUM_ERROR_EXIST);
                        } else {
                            ServiceCurriculum.createCurriculum(dataToSave, function (err, response) {
                                if (!err) {
                                    cb(null);
                                } else {
                                    cb(err);
                                }
                            });
                        }
                    })
                }
            })
        }

    }, function (err, data) {

        if (!err) response.status(200).send(constants.STATUS_MSG.SUCCESS.GRADE_ADDED);
        else {
            response.send(err);
        }
    });
}

var addSubject = function (request, response) {
    async.auto({
        addSubject: function (cb) {
            var dataToSave = {
                board: request.body.board.charAt(0).toUpperCase() + request.body.board.slice(1),
                grade: request.body.grade.charAt(0).toUpperCase() + request.body.grade.slice(1),
                subject: request.body.subject.charAt(0).toUpperCase() + request.body.subject.slice(1),
                language: request.body.language.charAt(0).toUpperCase() + request.body.language.slice(1),
                isRemoved: false
            }
            ServiceCurriculum.getCurriculum(dataToSave, {
                _id: 1
            }, {}, function (err, res) {
                if (res.length > 0) {
                    cb(null);
                } else {
                    ServiceCurriculum.checkIfGradeExistInSorting(dataToSave, function (existing) {
                        if (existing == 1) {
                            cb(constants.STATUS_MSG.ERROR.CURRICULUM_ERROR_EXIST);
                        } else {
                            ServiceCurriculum.createCurriculum(dataToSave, function (err, response) {
                                if (!err) {
                                    cb(null);
                                } else {
                                    cb(err);
                                }
                            });
                        }
                    })
                }
            })
        }

    }, function (err, data) {

        if (!err) response.status(200).send(constants.STATUS_MSG.SUCCESS.GRADE_ADDED);
        else {
            response.send(err);
        }
    });
}


var editGrade = function (request, response) {

    var query = {
        board: request.body.board,
        language: request.body.language,
        grade: request.body.grade
    }
    ServiceCurriculum.updateCurriculum(query, {
        $set: {
            grade: request.body.newGradeValue
        }
    }, function (err, data) {
        if (!err) {
            response.status(200).send(constants.STATUS_MSG.SUCCESS.GRADE_VALUE_UPDATED);
        } else {
            response.send(err);
        }
    });
}
var editSubject = function (request, response) {
    var query = {
        board: request.body.board,
        language: request.body.language,
        grade: request.body.grade,
        subject: request.body.subject
    }

    logging.consolelog("request", "", request.body);
    ServiceCurriculum.updateCurriculum(query, {
        $set: {
            subject: request.body.newSubjectValue
        }
    }, function (err, data) {
        logging.consolelog("edit subject", err, data);
        if (!err) {
            response.status(200).send(constants.STATUS_MSG.SUCCESS.SUBJECT_VALUE_UPDATED);
        } else {
            response.send(err);
        }
    });
}

var editChapter = function (request, response) {
    logging.consolelog("request", "", request.body);
    var query = {
        board: request.body.board,
        language: request.body.language,
        grade: request.body.grade,
        subject: request.body.subject,
        chapter: request.body.chapter
    }

    ServiceCurriculum.updateCurriculum(query, {
        $set: {
            chapter: request.body.newChapterValue
        }
    }, function (err, data) {
        logging.consolelog("logging", err, data);
        if (!err) {
            response.status(200).send(constants.STATUS_MSG.SUCCESS.CHAPTER_VALUE_UPDATED);
        } else {
            response.send(err);
        }
    });
}


var dragAndDropList = function (request, response) {
    var language = [];
    var board = [];
    var count;

    var topicListObj = {};
    var topicList = [];
    if (request.body.language) {
        for (var i = 0; i < request.body.language.length; i++) {
            language.push(request.body.language[i].language)
        }
    }
    if (request.body.board) {
        for (var i = 0; i < request.body.board.length; i++) {
            board.push(request.body.board[i].board)
        }
    }
    async.series([
        function (cb) {
            var projection = {
                'result.topicId': 1

            }
            var query = {};
            if (language.length > 0) {
                query.language = {
                    $in: language
                },
                    query.isRemoved = "false";
                query.removed = false;

            }
            if (board.length > 0) {

                query.board = {
                    $in: board
                }
            }
            var limit = {
                limit: 10000,
                offset: request.body.skip ? request.body.skip : 0
            }
            ServiceCurriculum.getCurriculumAndTopicList(query, limit, {}, function (err, data) {
                logging.consolelog("::data sql::", err, data);
                if (!err) {
                    if (data.length) {
                        for (var i = 0; i < data.length; i++) {
                            topicListObj[data[i].name] = {
                                language: data[i].topicLanguage,
                                topicName: data[i].name,
                                topicId: data[i]["topicId"],
                                board: data[i].board,
                                grade: data[i].grade,
                                chapter: data[i].chapter,
                                subject: data[i].subject,
                                curriculumId: data[i].curriculumId,
                                sequenceOrder: data[i].sequenceOrder
                            }
                        }
                        for (var i in topicListObj) {
                            topicList.push(topicListObj[i]);
                        }
                        ServiceCurriculum.getCurriculumAndTopicCount(query, limit, {}, function (err, data) {
                            if (!err) {
                                logging.consolelog("data", "", data[0]);
                                if (data.length && data[0].cnt) {
                                    count = data[0].cnt;
                                }
                                cb(null);
                            } else {
                                cb(err);
                            }
                        });
                    } else {
                        cb(null)
                    }
                } else {
                    cb(err);
                }
            });

        }
    ], function (err, data) {
        if (!err) {
            response.status(200).send({
                topicList: topicList,
                count: count,
                statusCode: 200
            })
        }
    })
}


var dragAndDrop = function (request, response) {
    async.series([
        function (cb) {
            var obj = {
                PrevElementSeqNo: request.body.PrevElementSeqNo,
                curriculumId: request.body.curriculumId,
                topicId: request.body.topicId
            }

            ServiceTopicCurriculumMapper.dragAndDropSequence(obj, function (err, data) {
                if (!err) {
                    cb(null);
                } else {
                    cb(err);
                }
            })
        }
    ], function (err, data) {
        if (!err) {
            response.status(200).send(constants.STATUS_MSG.SUCCESS.DEFAULT);
        } else {
            response.send(err);
        }
    })
}


var exportCSV = function (request, response) {
    ServiceCurriculum.exportCSV(function (err, data) {
        if (!err) {
            response.status(200).send({
                statusCode: 200,
                data: data
            })
        } else {
            response.send(err);
        }
    })
}

var dragAndDropGrade = function (request, response) {

//prev grade value
//itself grade value,
//board and language
    async.auto({
        drag_grades: function (cb) {
            var obj = {
                prevGradeSeq: request.body.prevGradeSeq,
                gradeName: request.body.gradeName
            }

            ServiceCurriculum.dragAndDropGrade(obj, function (err, data) {
                if (!err) {
                    cb(null);
                } else {
                    cb(err);
                }
            })
        }
    }, function (err, data) {
        if (!err) {
            response.status(200).send(constants.STATUS_MSG.SUCCESS.DEFAULT);
        } else {
            response.send(err);
        }
    })
}

//grade listing for sorting
var gradeListForSorting = function (request, response) {
    var grades = [];
    async.auto({
        sorted_gradeList:  function (cb) {
            var query = {
                board: request.body.board,
                language: request.body.language,
                isRemoved: false
            }

            ServiceCurriculum.getSortedData(query, 'grade', function (err, data) {
                logging.consolelog("%%%%%%%%sorted data%%%%%%%%%%", err, data);
                if (!err) {
                    for (var i = 0; i < data.length; i++) {
                        grades.push(data[i]);
                    }
                    cb(null);
                } else {
                    cb(err);
                }
            })
        }
    }, function (err, data) {
        if (!err) {
            response.status(200).send({
                statusCode: 200,
                grades: grades
            })
        } else {
            response.send(err);
        }
    })

}


module.exports = {

    curriculumUploadViaCSV: curriculumUploadViaCSV,
    curriculumUploadViaManually: curriculumUploadViaManually,
    curriculumListing: curriculumListing,
    copyExistingCurriculumByLanguage: copyExistingCurriculumByLanguage,
    curriculumGradeListing: curriculumGradeListing,
    addChapterManually: addChapterManually,
    chapterDetailsForSubject: chapterDetailsForSubject,
    deleteChapterOfSubject: deleteChapterOfSubject,
    deleteSubjectOfGrade: deleteSubjectOfGrade,
    deleteGradeOfCurriculum: deleteGradeOfCurriculum,
    deleteCurriculum: deleteCurriculum,
    editGrade: editGrade,
    editSubject: editSubject,
    editChapter: editChapter,
    addGrade: addGrade,
    addSubject: addSubject,
    dragAndDropList: dragAndDropList,
    dragAndDrop: dragAndDrop,
    exportCSV: exportCSV,
    dragAndDropGrade: dragAndDropGrade,
    gradeListForSorting: gradeListForSorting

};