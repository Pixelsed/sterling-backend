var async = require('async');
var ServiceOfflineManagement = require('../../Service/offlineManagement');
var constants = require('../../Library/appConstants');





var addNewKey= function(request,response){
    console.log("======> request.body", JSON.stringify(request.body));
var dataToSend= {};

    async.auto({

        "generate_key":(cb)=>{
            var dataToSave={
                licensekey:Date.now(),
                validDays:request.body.validDays,
                emailId:request.body.emailId,
                phoneNumber:request.body.phoneNumber,
                macId:"false"
            }
            ServiceOfflineManagement.createkey(dataToSave, function(error,dbData){

                if(!error){
                    dataToSend= dbData;
                    cb(null);
                }else{
                    cb(error);
                }

            })
        }
    }, (err,data)=>{
        if(!err){
            response.status(200).send({data:dataToSend});
        }else{
            response.send(err);
        }

    })
}


var getKeys= function(request,response){
var dataToSend=[];
    async.auto({

        "get_keys":(cb)=>{
            ServiceOfflineManagement.getKey({},{},{lean:true},(error,result)=>{
                if(!error){
                    dataToSend=result;
                    cb(null);
                }else{
                    cb(error);
                }
            })
        }
    },(err, data)=>{

        if(!err){
            response.status(200).send({data:dataToSend});
        }else{
            response.send(err);
        }
    })
}



var offlineKey= function(request,response){
var dataToSend={};
async.auto({

    "offline_keys":(cb)=>{


        ServiceOfflineManagement.getKey({licensekey:request.body.licenseKey},{},{},(error,dbResult)=>{

            if(!error&& dbResult.length>0){
             if(request.body.macId){
              if(dbResult[0]["macId"]== request.body.macId){
                dataToSend={
                    days:dbResult[0].validDays,
                    macId:request.body.macId,
                    licenseKey:request.body.licenseKey
                }
                cb(null);

              }  else if(dbResult[0]["macId"]=="false"){
                    //update key in the db
                    ServiceOfflineManagement.updateKey({licensekey:request.body.licenseKey},{$set:{macId:request.body.macId}},{},(err,db)=>{

                    })
                    dataToSend={
                        days:dbResult[0].validDays,
                        macId:request.body.macId,
                        licenseKey:request.body.licenseKey
                    }
                    cb(null);

                }

              
             }  
            }else{
                cb({
                    statusCode: 400,
                    message: 'License Key not found',
                    type: 'LICENSE_NOT_FOUND'
                })
            }
        })
    },


},(error,data)=>{
    if(!error){
        response.status(200).send({data:dataToSend});
    }else{
        response.send(error);
    }
})



}


module.exports={
    getKeys:getKeys,
    addNewKey:addNewKey,
    offlineKey:offlineKey

}