var async = require('async');
var ServiceSuperAdmin = require('../../Service/superAdmin');
var constants = require('../../Library/appConstants');
var fs = require('fs');
var Templates = require('../../Templates/superAdminRegister');
var TemplatesFeedback = require('../../Templates/feedbackMessage');
var TemplatesForget = require('../../Templates/forgetPassword');
var UploadManager = require('../../Library/uploadManager');
var NotificationManager = require('../../Library/notificationManager');
var TokenManager = require('../../Library/tokenManager');
var AwsConfig = Config.get('awsConfig');
var UniversalFunction = require('../../Library/universalFuctions');
var ServiceMarketingDays= require("../../Service/marketingDays");
/*
 Super Admin Register

 1. Validating Parameters
 2/3. Validating Unique Parameters
 4. Upload Profile Picture of Super-Admin
 5. Save Super Admin Data(Use MD5 for encryption of passwords)
 6. Send Password to Admin via SES service of AWS

 */

var superAdminRegister = function (request, response) {
	logging.consolelog("request","",request.body);
	var data2;
	var uniqueCode = UniversalFunction.generateRandomStringAndNumbers();
	request.body.password = UniversalFunction.encryptData(uniqueCode);
	var filename1;
	var ip = request.headers['x-forwarded-for'] || request.connection.remoteAddress;
	async.series([
        function (cb) {
			if (request.body.emailId && request.body.mobileNo && request.body.state && request.body.country && request.body.address) {
				cb(null);
			} else {
				cb(constants.STATUS_MSG.ERROR.VALIDATION_FAIL);
			}

        },
        function (cb) {
			ServiceSuperAdmin.getSuperAdmin({
				emailId: request.body.emailId
			}, {}, {
				lean: true
			}, function (err, data) {

				if (err) {
					cb(err)
				}
				if (!err) {
					if (data.length > 0) {
						cb(constants.STATUS_MSG.ERROR.ALREADY_REGISTERED_EMAIL);
					} else {
						cb(null);
					}


				}
			})
        },
        function (cb) {
			ServiceSuperAdmin.getSuperAdmin({
				mobileNo: request.body.mobileNo
			}, {}, {
				lean: true
			}, function (err, data) {

				if (err) {
					cb(err)
				}
				if (!err) {
					if (data.length > 0) {

						cb(constants.STATUS_MSG.ERROR.ALREADY_REGISTERED_MOBILENO);
					} else {
						cb(null);
					}


				}
			})
        },
        function (cb) {

			UploadManager.uploadPicToS3Bucket(request.files.profilePic, "profile", function (filename) {
				if (filename == "No File") {
					cb(null);
				} else {
					filename1 = AwsConfig.s3BucketCredentials.s3URL + "/profile/" + filename;
					cb(null);
				}
			});
        },
        function (cb) {

			var data = {
				emailId: request.body.emailId,
				password: request.body.password,
				accessRights: request.body.accessRights,
				mobileNo: request.body.mobileNo,
				address: request.body.address,
				state: request.body.state,
				country: request.body.country,
				name: request.body.name,
				profilePicUrl: filename1
			}
			ServiceSuperAdmin.createSuperAdmin(data, function (err, data) {
				data2 = data;
				if (!err) {
					cb(null);
				} else {
					cb(err);
				}
			})

        },
        function (cb) {

            var emailSend = {
                emailId: request.body.emailId,
                password: uniqueCode,
                FAQ: constants.COMMON_VARIABLES.link,
                name: request.body.name
            }
            var text;
            text = Templates.adminRegister(emailSend);
            request.body.subject = request.body.subject?request.body.subject:"WELCOME";
			NotificationManager.sendEmailToUser(request.body.emailId, request.body.subject, text, function (err, data) {
				logging.consolelog(err, data, "send email to user");
				if (!err) {
					logging.consolelog('mail', err, data);
				}
				cb(null);
			});



		}
    ], function (err, data1) {
		if (!err) {
			response.status(200).send({
				data: data2,
				statusCode: 200
			})
		} else {
			response.send(err);
		}
	});
}

/*
 Super Admin Login

 1. Validating Parameters(change password to encrypted password using MD5)
 2. Validating Email Id to check super Admin exists or not
 5. Set Access Token of admin at every login using json token.

 */

var superAdminLogin = function (request, response) {
	logging.consolelog("request",request.headers, request.body);
	var data2;
	var str = request.body.emailId;
	var str1 = request.body.password;
	request.body.password = UniversalFunction.encryptData(request.body.password)
	var customerData;
	var accessToken;
	var ip = request.headers['x-forwarded-for']?request.headers['x-forwarded-for']:request.headers['host'];
	async.series([
        function (cb) {
			if (request.body.emailId && request.body.password) {
				cb(null);
			} else {
				cb(constants.STATUS_MSG.ERROR.VALIDATION_FAIL);
			}

        },
        function (cb) {
			var query = {
				emailId: request.body.emailId
			}
			ServiceSuperAdmin.getSuperAdmin(query, {}, {
				lean: true
			}, function (err, data) {
				if (err) {
					cb(err)
				}
				if (!err) {
					if (data.length > 0) {
						  if (data[0].block == true) {
							cb(constants.STATUS_MSG.ERROR.ADMIN_BLOCK);
						}else if (data[0].password == request.body.password) {
							customerData = data[0];
							cb(null);
						}else {
							cb(constants.STATUS_MSG.ERROR.PASSWORD_MISMATCH);
						}

					} else {
						cb(constants.STATUS_MSG.ERROR.USER_NOT_FOUND);
					}


				}
			})
        },
         function (cb) {
			//Set Access Token

			if (customerData) {
				var tokenData = {
					id: customerData._id,
					type: constants.USER_TYPE.SUPER_ADMIN,
					ip: ip,
					userType: constants.USER_TYPE.SUPER_ADMIN,
					loginType:constants.DATABASE.LOGIN_TYPE.WEBAPP
				};
				TokenManager.setToken(tokenData, function (err, output) {
					if (err) {
						cb(err);
					} else {
						accessToken = output && output.accessToken || null;
						cb();
					}
				})
			} else {
				cb(constants.STATUS_MSG.ERROR.IMP_ERROR)
			}
         },
    ], function (err, data1) {
		if (!err) {
            delete customerData['accessToken'];
			response.status(200).send({
				statusCode: 200,
				data: customerData,
				accessToken: accessToken
			})
		} else {
			response.send(err);
		}
	});
}


var superAdminForgetPassword = function (request, response) {
	var customerData;
	var uniqueCode;
	async.series([
        function (cb) {
			var query = {
				emailId: request.body.emailId
			}
			ServiceSuperAdmin.getSuperAdmin(query, {}, {
				lean: true
			}, function (err, data) {
				if (!err) {
					if (data.length > 0) {
						customerData = data[0];
						cb(null);
					} else {
						cb(constants.STATUS_MSG.ERROR.USER_NOT_FOUND);
					}
				} else {
					cb(err);
				}
			})
        },
		 function (cb) {
			uniqueCode = Math.floor(Math.random() * 900000) + 100000;
			var emailSend = {
				emailId: request.body.emailId,
				code: uniqueCode,
				name: customerData.name
			}
			var text;
			text = TemplatesForget.forgetPassword(emailSend);
			NotificationManager.sendEmailToUser(request.body.emailId, 'Sterling Pixels Account OTP', text, function (err, data) {
				if (!err) {
					logging.consolelog('mail', err, data);
				}
				cb(null);
			});



		},
		function (cb) {
			var data = {
				otp: uniqueCode,
				mobileNo: customerData.countryCode?customerData.countryCode:0 + customerData.mobileNo
			}
			NotificationManager.sendSMSToUser(data, 'FORGET_PASSWORD', function (err, data) {
				if (!err) {
					logging.consolelog('sms', err, data);
				}
				cb(null);
			});


		},

        function (cb) {
			ServiceSuperAdmin.updateSuperAdmin({
				emailId: request.body.emailId
			}, {
				$set: {
					code: uniqueCode
				}
			}, {
				lean: true
			}, function (err, data) {
				if (!err) {
					cb(null);
				} else {
					cb(err);
				}
			})

        }

    ], function (err, data1) {
		if (!err) {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.SENT_CODE);
		} else {
			response.send(err);
		}
	});
}


var superAdminCheckOTP = function (request, response) {
	var customerData;
	async.series([
        function (cb) {
			var query = {
				emailId: request.body.emailId,
				code: request.body.code
			}
			ServiceSuperAdmin.getSuperAdmin(query, {}, {
				lean: true
			}, function (err, data) {
				if (!err) {
					if (data.length > 0) {
						customerData = data[0];
						cb(null);
					} else {
						cb(constants.STATUS_MSG.ERROR.INVALID_CODE);
					}
				} else {
					cb(err);
				}
			})
        }
    ], function (err, data1) {
		if (!err) {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.DEFAULT);
		} else {
			response.send(err);
		}
	});
}


var superAdminresetPassword = function (request, response) {
	var customerData;
	async.series([
        function (cb) {
			var query = {
				emailId: request.body.emailId,
				code: request.body.code
			}
			var update = {
				$set: {
					password:UniversalFunction.encryptData(request.body.password)
				},
				$unset: {
					code: ""
				}
			}
			ServiceSuperAdmin.updateSuperAdmin(query, update, {
				lean: true
			}, function (err, data) {
				if (!err) {
					cb(null);
					// if (data.length > 0) {
					//     customerData = data[0];
					//     cb(null);
					// } else {
					//     cb(constants.STATUS_MSG.ERROR.INVALID_CODE);
					// }
				} else {
					cb(err);
				}
			})
        }
    ], function (err, data1) {
		if (!err) {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.PASSWORD_RESET);
		} else {
			response.send(err);
		}
	});
}



var changePassword = function (request, response) {

	var oldPassword = UniversalFunction.encryptData(request.body.oldPassword);
	var newPassword = UniversalFunction.encryptData(request.body.newPassword);
	async.series([
            function (callback) {
				var query = {
					_id: request.userData.id
				};
				var projection = {
					password: 1
				};
				var options = {
					lean: true
				};
				ServiceSuperAdmin.getSuperAdmin(query, projection, options, function (err, data) {
					if (err) {
						callback(err);
					} else {
						var customerData = data && data[0] || null;
						logging.consolelog("customerData-------->>>" ,"", JSON.stringify(customerData))
						if (customerData == null) {
							callback(constants.STATUS_MSG.ERROR.NOT_FOUND);
						} else {
							if (data[0].password == oldPassword && data[0].password != newPassword) {
								callback(null);
							} else if (data[0].password != oldPassword) {
								callback(constants.STATUS_MSG.ERROR.WRONG_PASSWORD)
							} else if (data[0].password == newPassword) {
								callback(constants.STATUS_MSG.ERROR.NOT_UPDATE_PASSWORD);
							}
						}
					}
				});
            },
            function (callback) {
				var dataToUpdate = {
					$set: {
						'password': newPassword
					}
				};
				var condition = {
					_id: request.userData.id
				};
				ServiceSuperAdmin.updateSuperAdmin(condition, dataToUpdate, {}, function (err, user) {
					logging.consolelog("customerData-------->>>", "",JSON.stringify(user));
					if (err) {
						callback(err);
					} else {
						if (!user || user.length == 0) {
							callback(constants.STATUS_MSG.ERROR.NOT_FOUND);
						} else {
							callback(null);
						}
					}
				});
            }
        ],
		function (error, result) {
			if (error) {
				response.send(error);
			} else {
				response.status(200).send(constants.STATUS_MSG.SUCCESS.DEFAULT);
			}
		});
};


var getList = function (request, response) {
	var details = [];
	async.series([
		function (cb) {
			ServiceSuperAdmin.getSuperAdmin({}, {}, {}, function (err, data) {
				if (!err) {
					if (data.length > 0) {
						details = data;
					}
					cb(null);
				} else {
					cb(err);
				}
			})
		}
	], function (err, data) {
		if (!err) {
			response.status(200).send({
				statusCode: 200,
				details: details
			})
		} else {
			response.send(err);
		}
	})
}


var blockAdmin = function (request, response) {
	var userDetails=request.userData;
	async.series([
        function (cb) {
			var query = {
				_id: request.body.adminId
			}
			ServiceSuperAdmin.updateSuperAdmin(query, {
				$set: {
					block: true
				}
			}, {
				lean: true
			}, function (err, data) {
				logging.consolelog(err, data, "edit values")
				if (!err) {
					cb(null);
				} else {
					cb(err);
				}
			})
        }
    ], function (err, result) {
		if (!err) {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.CUSTOMER_BLOCKED);
		} else {
			response.send(err);
		}
	})
}


var unblockAdmin = function (request, response) {
	var userDetails= request.userData;
	async.series([
        function (cb) {
			var query = {
				_id: request.body.adminId
			}
			ServiceSuperAdmin.updateSuperAdmin(query, {
				$set: {
					block: false
				}
			}, {
				lean: true
			}, function (err, data) {
				logging.consolelog(err, data, "edit values")
				if (!err) {
					cb(null);
				} else {
					cb(err);
				}
			})
        }
    ], function (err, result) {
		if (!err) {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.CUSTOMER_UNBLOCKED);
		} else {
			response.send(err);
		}
	})
}


var editAdmin = function (request, response) {
	var userDetails=request.userData;
	async.series([
		function (cb) {
			var update = {};
			if (request.body.mobileNo) {
				update.mobileNo = request.body.mobileNo;
			}
			if (request.body.name) {
				update.name = request.body.name;
			}
			if (request.body.accessRights) {
				update.accessRights = request.body.accessRights;
			}
			if (request.body.state) {
				update.state = request.body.state;
			}
			if (request.body.country) {
				update.country = request.body.country;
			}
			if (request.body.address) {
				update.address = request.body.address;
			}

			var query = {
				_id: request.body.adminId
			}

			ServiceSuperAdmin.updateSuperAdmin(query, update, {
				lean: true
			}, function (err, data) {
				logging.consolelog(err, data, "edit values")
				if (!err) {
					cb(null);
				} else {
					cb(err);
				}
			})
		}
	], function (err, data) {
		if (!err) {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.EDIT_SUCCESSFULLY);
		} else {
			response.send(err);
		}
	})
}


var getDetails = function (request, response) {
	var details;
	async.series([
		function (cb) {
			ServiceSuperAdmin.getSuperAdmin({
				_id: request.body.adminId
			}, {}, {
				lean: true
			}, function (err, data) {
				if (!err) {
					if (data.length > 0) {
						details = data[0];
					}
					cb(null);
				} else {
					cb(err);
				}
			})
		}
	], function (err, data) {
		if (!err) {
			response.status(200).send({
				statusCode: 200,
				details: details

			})
		} else {
			response.send(err);
		}

	})
}


var feedbackMessage = function (request, response) {

	async.series([
		function (cb) {
			var emailSend = {
				body: request.body.html
			}
			var text;
			text = TemplatesFeedback.feedback(emailSend);
			NotificationManager.sendEmailToUser(request.body.emailId, request.body.subject, text, function (err, data) {
				if (!err) {
					logging.consolelog('mail', err, data);
				}
				cb(null);
			});



		}
	], function (err, data) {
		if (!err) {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.DEFAULT);
		} else {
			response.send(err);
		}
	})
}


var updateDays= function(request,response){
	async.series([
		(cb)=>{
         ServiceMarketingDays.updateDays({},{$set:{days:request.body.days}},{multi:true},(err,data)=>{
         	if(!err){
         		cb(null);
         	}else{
         		cb(err);
         	}
         })

		}],(err,data)=>{
			if (!err) {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.DEFAULT);
		} else {
			response.send(err);
		}
		});
}

var getDays= function(request,response){
     console.log("ServiceMarketingDays ::");
	ServiceMarketingDays.getDays({},{days:1},{},(err,data)=>{
    if(!err){
    	  response.status(200).send({"data":data, statusCode:200});
    }else{
    	response.send(err);
    }


	});
}
module.exports = {

	///On Boarding of Super Admin////////
	superAdminRegister: superAdminRegister,
	superAdminLogin: superAdminLogin,
	superAdminForgetPassword: superAdminForgetPassword,
	superAdminCheckOTP: superAdminCheckOTP,
	superAdminresetPassword: superAdminresetPassword,
	changePassword: changePassword,
	getList: getList,
	blockAdmin: blockAdmin,
	unblockAdmin: unblockAdmin,
	editAdmin: editAdmin,
	getDetails: getDetails,
	feedbackMessage: feedbackMessage,
	updateDays:updateDays,
	getDays:getDays

}