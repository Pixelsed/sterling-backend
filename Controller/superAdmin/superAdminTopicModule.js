var async = require('async');
var ServiceCurriculum = require('../../Service/curriculum');
var ServiceTopic = require('../../Service/topic');
var ServiceTopicCategory = require('../../Service/topicCategory');
var ServiceTopicBinding = require('../../Service/topicBinding');
var ServiceTopicSubCategory = require('../../Service/topicSubCategory');
var ServiceTemporaryContent = require('../../Service/temporaryContentStorage');
var ServiceAssesment = require('../../Service/assessment');
var ServiceFeedback = require('../../Service/feedback');
var ServiceTopicCurriculumMapper = require('../../Service/topicCurriculumMapping');
var constants = require('../../Library/appConstants');
var fs = require('fs');
var Templates = require('../../Templates/superAdminRegister');
var UploadManager = require('../../Library/uploadManager');
var NotificationManager = require('../../Library/notificationManager');
var AwsConfig = Config.get('awsConfig');
var _ = require('underscore');
var ServiceCustomer = require('../../Service/customer');
var ServiceTempAssignData = require("../../Service/tempAgentAssignData");
var ServiceCustomerBuyPackage= require("../../Service/customerBuyPackages");

var addTopicManually = function (request, response) {
	var filename1 = null;
	var topicId = null;
	var universalId;
	var curriculumId = [];
	var categoryId = null;
	var test = null;
	var alreadyExists = 0;
	var notexists = false;
	var exists = false;
	var customerId = [];
	request.body.price = request.body.price ? request.body.price : 0
	logging.consolelog("request", request.body, request.files);
	async.series([
            function (cb) {
				var query = {
					name: request.body.name,
				}
				ServiceTopic.getTopic(query, {}, {
					lean: true
				}, function (err, data) {
					logging.consolelog("err,data::2", err, data);

					if (!err) {
						if (data.length > 0) {
							for (var i in data) {
								if (request.body.language == data[i]["language"]) {

									if (data[i]["isDeleted"] == false) {
										alreadyExists = 1;

									} else {
										notexists = true;
									}
								}
								if (request.body.language != data[i]["language"] && data[i]["isDeleted"] == false) {
									exists = true;
									test = data[0]._id;
									filename1 = data[0].icon;
								}

							}


						} else {
							notexists = true;
						}
						if (alreadyExists == 1) {
							cb(constants.STATUS_MSG.ERROR.TOPIC_ALREADY_EXIST);
						} else {
							cb(null);
						}

					} else {
						cb(err);
					}
				})


            },
            function (cb) {
				if (exists == true) {
					logging.consolelog(test, "", "topicId")
					var query = {
						topicId: test,
						isUniversal: true
					}
					ServiceCurriculum.getCurriculumByTopic(query, {
						curriculumId: 1
					}, {}, function (err, data) {
						if (data.length>0 &&data[0].board == request.body.board && data[0].grade == request.body.grade && data[0].subject == request.body.subject && data[0].chapter == request.body.chapter) {
							cb(null);
						} else {
							cb(constants.STATUS_MSG.ERROR.UNIVERSAL_LINK_ERROR);
						}
					})


				} else if (notexists == true) {
					if (request.files) {
						UploadManager.uploadPicToS3Bucket(request.files.iconImage, "profile", function (filename) {

							if (filename == "No File") {

								cb(null);
							} else {
								filename1 = AwsConfig.s3BucketCredentials.s3URL + '/profile/' + filename;
								cb(null);
							}

						});
					} else {
						cb(null);
					}

				}
            },
            function (cb) {
				logging.consolelog("logging", "", JSON.stringify(request.body));

				var dataToSave = {
					name: request.body.name,
					icon: filename1,
					authorName: request.body.authorName,
					language: request.body.language,
					description: request.body.description,
					addedDate: Date.now(),
					isFree: request.body.isFree ? request.body.isFree : 0,
					price: request.body.price,
					averageRating: 0,
					gtinCode: request.body.gtinCode

				}
				ServiceTopic.createTopic(dataToSave, function (err, data) {
					logging.consolelog('error>>>>>5', "", err)
					if (!err) {
						topicId = data._id;
						cb(null);
					} else {
						cb(err);
					}
				});
            },
            function (cb) {
				var query = {
					board: constants.COMMON_VARIABLES.UNIVERSAL_BOARD_NAME,
					grade: request.body.grade,
					subject: request.body.subject,
					chapter: request.body.chapter,
					language: request.body.language,
					isRemoved: false
				}
				ServiceCurriculum.getCurriculum(query, {
					_id: 1
				}, {}, function (err, data) {
					logging.consolelog("logging", "", JSON.stringify(data));
					if (!err) {
						if (data.length > 0) {
							universalId = data[0]["_id"];
						}
						cb(null);
					} else {
						cb(err);
					}

				})


            },
            function (cb) {
				var dataToSave = {
					curriculumId: universalId,
					topicId: topicId,
					isUniversal: true,
					removed: false
				}

				ServiceTopicCurriculumMapper.createCurriculumMapping(dataToSave, function (err, data) {
					logging.consolelog('error>>>>>4', "", err)
					if (!err) {
						cb(null);
					} else {
						cb(err);
					}
				});


            },
            function (cb) {
				if (request.body.addToCurriculum) {
					var counter = 0;
					async.forEach(request.body.addToCurriculum, function (item, embeddedcb) {
						counter++
						var query = {
							board: item.board,
							grade: item.grade,
							subject: item.subject,
							chapter: item.chapter,
							language: item.language

						}

						ServiceCurriculum.getCurriculum(query, {
							_id: 1
						}, {}, function (err, response) {
							if (err) {
								embeddedcb(null);
							} else {
								curriculumId.push(response[0]._id);
								embeddedcb(null);
							}
						});
					}, function (err) {
						if (counter == request.body.addToCurriculum.length) {
							cb();
						}
					});

				} else {
					cb(null);
				}

            },
            function (cb) {
				if (curriculumId) {

					if (curriculumId.length > 0) {
						for (var i = 0; i < curriculumId.length; i++) {
							(function (curriculum) {


								var dataToSave = {
									curriculumId: curriculum,
									topicId: topicId,
									universalCurriculumId: universalId,
									removed: false
								}
								ServiceTopicCurriculumMapper.createCurriculumMapping(dataToSave, function (err, data) {
									logging.consolelog('error>>>>>3', "", err)
									if (!err) {

									} else {

									}
								});

							})(curriculumId[i])

						}
						cb(null);

					} else {
						cb(null);
					}
				} else {
					cb(null);
				}


            },
            function (cb) {

				var dataToSave = {
					name: "Video",
					icon: Config.get('awsConfig.s3BucketCredentials.s3URL') + "/profile/pixelsEd-qmnKF-video-icon.png",
					price: 0,
					isDeleted: false,
					language: request.body.language,
					isPublish: false,
					isFree: false
				}

				ServiceTopicCategory.createTopicCategory(dataToSave, function (err, data) {
					logging.consolelog('error>>>>>2', "", err)
					if (!err) {
						categoryId = data._id
						cb(null);
					} else {
						cb(err);
					}
				})
            },
            function (cb) {
				var data = {
					topicId: topicId,
					categoryId: categoryId,
					categoryLanguage: request.body.language

				}
				ServiceTopicBinding.createTopicBinding(data, function (err, data) {
					logging.consolelog('error>>>>>1', "", err)
					if (!err) {
						cb(null);
					} else {
						cb(err);
					}
				})
            },
            function (cb) {
				var deviceTokens = [];
				ServiceCustomer.getCustomer({
					$or: [{
							userType: constants.USER_TYPE.STUDENT
                    },
						{
							userType: constants.USER_TYPE.TEACHER
                        },
                    ]

				}, {
					_id: 1,
					deviceToken: 1

				}, {}, function (err, data) {

					if (!err) {

						for (var i = 0; i < data.length; i++) {
							customerId.push(data[i]._id);
							deviceTokens.push(data[i]["deviceToken"]);
						}

						cb(null);
						if (request.body.isFree) {
							NotificationManager.sendAndroidPushNotification(deviceTokens, "New free topics are available. Check them out now!", "FREE_TOPIC_ADVERTISEMENT", (error, dataa) => {})
						}
					} else {
						cb(err);
					}
				})


			},
			
			function(cb){
				var query={
					language: request.body.language,
					subject: request.body.subject,
					grade: request.body.grade,
					chapter: request.body.chapter
				}


				ServiceTempAssignData.getSuperAdmin(query, {},{},(error, dbData)=>{
					console.log("all params",error, JSON.stringify(dbData));
					if(!error){
						var insertData=[];
						if(dbData.length>0){
                           
							for(var i=0; i< dbData.length; i++){
								 
								insertData.push({

									category: dbData[i]["category"],
									content:dbData[i]["content"],
									agentId:dbData[i]["agentId"],
									startDate:dbData[i]["startDate"],
									endDate: dbData[i]["endDate"],
									language: dbData[i]["language"],
									board:dbData[i]["board"],
									grade:dbData[i]["grade"],
									subject:dbData[i]["subject"],
									chapter:dbData[i]["chapter"],
									topicId:topicId.toString(),
									transactionId: dbData[i]["transactionId"]

								})

							}
						}
						if(insertData.length>0){
							ServiceTempAssignData.insertData(insertData,(err, data)=>{
								if(!err){
									cb(null);
								}else{
									cb(err); 
								}
	
							})
						}else{
							cb(null);
						}

						

					}else{
						cb(null);
					}
				})
			},
			function(cb){


				var query={
					language: request.body.language,
					subject: request.body.subject,
					grade: request.body.grade,
					chapter: []
				}


				ServiceTempAssignData.getSuperAdmin(query, {},{},(error, dbData)=>{
					console.log("chapter blank",error, JSON.stringify(dbData));
					if(!error){

						var insertData=[];
						if(dbData.length>0){
                           
							for(var i=0; i< dbData.length; i++){
								 
								insertData.push({

									category: dbData[i]["category"],
									content:dbData[i]["content"],
									agentId:dbData[i]["agentId"],
									startDate:dbData[i]["startDate"],
									endDate: dbData[i]["endDate"],
									language: dbData[i]["language"],
									board:dbData[i]["board"],
									grade:dbData[i]["grade"],
									subject:dbData[i]["subject"],
									chapter:dbData[i]["chapter"],
									topicId:topicId.toString(),
									transactionId: dbData[i]["transactionId"]

								})

							}
						}
						if(insertData.length>0){
							ServiceTempAssignData.insertData(insertData,(err, data)=>{
								if(!err){
									cb(null);
								}else{
									cb(err); 
								}
	
							})
						}else{
							cb(null);
						}


					}else{
						cb(null);
					}
				})
			},
			function(cb){

				var query={
					language: request.body.language,
					subject: [],
					grade: request.body.grade,
					chapter: []
				}


				ServiceTempAssignData.getSuperAdmin(query, {},{},(error, dbData)=>{
					console.log("chapter and subject",error, JSON.stringify(dbData));
					if(!error){
						var insertData=[];
						if(dbData.length>0){
                           
							for(var i=0; i< dbData.length; i++){
								 
								insertData.push({

									category: dbData[i]["category"],
									content:dbData[i]["content"],
									agentId:dbData[i]["agentId"],
									startDate:dbData[i]["startDate"],
									endDate: dbData[i]["endDate"],
									language: dbData[i]["language"],
									board:dbData[i]["board"],
									grade:dbData[i]["grade"],
									subject:dbData[i]["subject"],
									chapter:dbData[i]["chapter"],
									topicId:topicId.toString(),
									transactionId: dbData[i]["transactionId"]

								})

							}
						}
						if(insertData.length>0){
							ServiceTempAssignData.insertData(insertData,(err, data)=>{
								if(!err){
									cb(null);
								}else{
									cb(err); 
								}
	
							})
						}else{
							cb(null);
						}


					}else{
						cb(null);
					}
				})




			},
			function(cb){
				
								var query={
									language: request.body.language,
									subject: [],
									grade: [],
									chapter: []
								}
				
				
								ServiceTempAssignData.getSuperAdmin(query, {},{},(error, dbData)=>{
									console.log("chapter,subject and grade blank",error, JSON.stringify(dbData));
									if(!error){
				
										var insertData=[];
										if(dbData.length>0){
										   
											for(var i=0; i< dbData.length; i++){
												 
												insertData.push({
				
													category: dbData[i]["category"],
													content:dbData[i]["content"],
													agentId:dbData[i]["agentId"],
													startDate:dbData[i]["startDate"],
													endDate: dbData[i]["endDate"],
													language: dbData[i]["language"],
													board:dbData[i]["board"],
													grade:dbData[i]["grade"],
													subject:dbData[i]["subject"],
													chapter:dbData[i]["chapter"],
													topicId:topicId.toString(),
													transactionId: dbData[i]["transactionId"]
				
												})
				
											}
										}
										if(insertData.length>0){
											ServiceTempAssignData.insertData(insertData,(err, data)=>{
												if(!err){
													cb(null);
												}else{
													cb(err); 
												}
					
											})
										}else{
											cb(null);
										}
				
				
									}else{
										cb(null);
									}
								})
				
				
				
				
							}


        ],
		function (err, data) {

			if (!err) response.status(200).send(constants.STATUS_MSG.SUCCESS.TOPIC_ADDED);
			else {
				response.send(err);
			}
			if (request.body.isFree) {
				NotificationManager.sendNotifications(customerId, 'FREE_TOPIC_ADVERTISEMENT', {});
			}
		});
}

var uploadContent = function (request, response) {
	logging.consolelog('request>>>>>>>>>>>>>>>>>>>>>>>>', "", request.body)
	var jobId = null;
	var contentIcon = null;
	var contentId = null;
	var filename1 = null;
	var outputKey = null;
	if (request.body.isFree) {
		request.body.price = 0;
	}
	logging.consolelog('aws>>>>>>>', "", AwsConfig.s3BucketCredentials)
  var srtFile= "";
	async.waterfall([
        function (cb) {
			if (request.files.icon) {
				UploadManager.uploadPicToS3Bucket(request.files.icon, AwsConfig.s3BucketCredentials.folder.content, function (filename) {
					logging.consolelog("upload_content_icon", "", filename);
					if (filename == "No File") {
						cb(null);
					} else {
						contentIcon = AwsConfig.s3BucketCredentials.s3URL + "/" + AwsConfig.s3BucketCredentials.folder.content + "/" + filename;
						cb(null);
					}
				});
			} else {
				cb(null);
			}

		},
		
		function(cb){
			var path= request.body.typeOfContent;
			console.log("======> path=====>",path);
			if(path=='video'&& request.files.subtitle){

				UploadManager.uploadPicToS3Bucket(request.files.subtitle,AwsConfig.s3BucketCredentials.folder.content,function(filename){

					if(filename== "No File"){
						cb(null);

					}else{
						srtFile= AwsConfig.s3BucketCredentials.s3URL + "/" + AwsConfig.s3BucketCredentials.folder.content + "/" + filename;
						cb(null);
					}
				})
			}else{
				cb(null);
			}

		},
        function (cb) {
			var path = request.body.typeOfContent;
			logging.consolelog(path, "", "~~~~~~~~~~path~~~~~~~~");
			switch (path) {
			case 'video':

				UploadManager.uploadPicToS3Bucket(request.files.content, AwsConfig.s3BucketCredentials.videosFolder, function (filename) {
					logging.consolelog("filename::", "", filename);
					if (filename == "No File") {
						cb(null);
					} else {
						UploadManager.videoUpload(filename, function (filename) {
							logging.consolelog("~~~~~~~video content url~~~~~~~`~", "", filename);
							if (filename == "No File") {
								cb(constants.STATUS_MSG.ERROR.UPLOAD_ERROR);
							} else {
								jobId = filename.jobIdKey;
								outputKey = filename.outputKey;
								cb(null);
							}
						});

					}
				});
				break;

			case 'image':
				logging.consolelog("enter into image", "", AwsConfig.s3BucketCredentials.folder.content);
				UploadManager.uploadPicToS3Bucket(request.files.content, AwsConfig.s3BucketCredentials.folder.content, function (filename) {
					if (filename != "No File") {

						filename1 = filename;
						filename1 = AwsConfig.s3BucketCredentials.s3URL + "/" + AwsConfig.s3BucketCredentials.folder.content + "/" + filename;
						cb(null);
					}

				});
				break;

			case 'link':
				break;

			case 'epub':
				logging.consolelog("enter into epub", "", "");
				UploadManager.uploadPicToS3Bucket(request.files.content, AwsConfig.s3BucketCredentials.folder.content, function (filename) {
					if (filename != "No File") {

						filename1 = filename;
						filename1 = AwsConfig.s3BucketCredentials.s3URL + "/" + AwsConfig.s3BucketCredentials.folder.content + "/" + filename;
						cb(null);
					}
				});
				break;


			case 'audio':
				UploadManager.uploadPicToS3Bucket(request.files.content, AwsConfig.s3BucketCredentials.videosFolder, function (filename) {
					logging.consolelog("filename::", "", filename);
					if (filename == "No File") {
						cb(null);
					} else {
						logging.consolelog("enter into video", "", "");
						UploadManager.audioUpload(filename, function (filename) {
							logging.consolelog("~~~~~~~video content url~~~~~~~`~", "", filename);
							if (filename == "No File") {
								cb(constants.STATUS_MSG.ERROR.UPLOAD_ERROR);
							} else {
								jobId = filename.jobIdKey;
								outputKey = filename.outputKey;
								cb(null);
							}
						});

					}
				});
				break;

			case 'zip':
				logging.consolelog("zip::", "", request.files);
				UploadManager.unzipUpload(request.files.content, function (filename) {
					logging.consolelog(filename, "", "result of uploading a zip file");
					//					filename = filename.location;

					if (filename) {
						filename1 = AwsConfig.s3BucketCredentials.s3URL + "/" + filename + "index.html";
						//						jobId =
						// filename1 = AwsConfig.s3BucketCredentials.s3URL +"/profile/"+ filename;
						cb(null);
					}
				});
				break;

			case 'text':
				UploadManager.uploadPicToS3Bucket(request.files.content, AwsConfig.s3BucketCredentials.folder.content, function (filename) {
					if (filename != "No File") {

						filename1 = filename;
						filename1 = AwsConfig.s3BucketCredentials.s3URL + "/" + AwsConfig.s3BucketCredentials.folder.content + "/" + filename;
						cb(null);
					}
				});
				break;
			case 'pdf':
				UploadManager.uploadPicToS3Bucket(request.files.content, AwsConfig.s3BucketCredentials.folder.content, function (filename) {
					if (filename != "No File") {

						filename1 = filename;
						filename1 = AwsConfig.s3BucketCredentials.s3URL + "/" + AwsConfig.s3BucketCredentials.folder.content + "/" + filename;
						cb(null);
					}
				});
				break;
			default:
				cb(constants.STATUS_MSG.ERROR.NOT_SUPPORTED);

			}


        },
        function (cb) {
			logging.consolelog("filename1", filename1, request.body.isFree);
			var dataToSave = {
				title: request.body.title,
				price: request.body.price,
				language: request.body.language,
				contentUrl: filename1,
				icon: contentIcon,
				description: request.body.description,
				isFree: request.body.isFree,
				typeOfContent: request.body.typeOfContent
			}

			if (request.body.typeOfContent == "video") {
				dataToSave.isVideo = true;
				dataToSave.typeOfVideo = request.body.typeOfVideo;
				dataToSave.jobId = jobId;
				dataToSave.outputKey = outputKey;
				dataToSave.srtFile= srtFile;
			}
			if (request.body.typeOfContent == "audio") {
				dataToSave.isVideo = false;
				dataToSave.jobId = jobId;
				dataToSave.outputKey = outputKey;
			}
			if (request.body.typeOfContent == "zip") {
				dataToSave.isVideo = true;
				//				dataToSave.jobId = jobId;
				dataToSave.typeOfVideo = "Interactive Video";
			}

			logging.consolelog("query", "", dataToSave);
			ServiceTemporaryContent.createTemporaryContentStorage(dataToSave, function (err, data) {

				logging.consolelog(err, data, "saving error");
				if (!err) {
					contentId = data._id;
					cb(null);
				} else {
					cb(err);
				}
			})

        }
    ], function (err, data) {

		if (!err) {
			response.status(200).send({
				contentId: contentId,
				statusCode: 200
			});
		} else {
			response.send(err);
		}
	});
}


var addContentToTopicInSubCategory = function (request, callback) {

	var isCategoryExists = false;
	var isSubCategoryExists = false;
	var categoryId = null;
	var subCategoryId = null;
	var categoryIcon = null;
	var subCategoryIcon = null;
	var content = null;
	var dataToSave = {};
	async.auto({
		icon: function (cb) {
			ServiceTopicCategory.getTopicCategory({
				name: request.body.categoryName
			}, {
				icon: 1
			}, {}, function (err, data) {
				if (!err) {
					if (data.length > 0) {
						categoryIcon = data[0].icon;

					}
					cb(null);
				} else {
					cb(null);
				}
			})

		},
		suncat_icon: function (cb) {
			ServiceTopicSubCategory.getTopicSubCategory({
				name: request.body.subCategoryName
			}, {
				icon: 1
			}, {}, function (err, data) {
				if (!err) {
					if (data.length > 0) {
						subCategoryIcon = data[0].icon;

					}
					cb(null);
				} else {
					cb(null);
				}
			})

		},
		topic_binding_category: function (cb) {
			var query = {
				topicId: request.body.topicId,
				categoryLanguage: request.body.categoryLanguage
			}

			var populate = [{
				path: 'categoryId',
				match: {},
				select: 'name icon language isDeleted',
				options: {
					lean: true
				}
            }];

			var projection = {
				categoryId: 1
			}
			ServiceTopicBinding.getAllTopicBinding(query, projection, populate, {}, {}, function (err, data) {
				logging.consolelog("topic_binding", err, data);
				if (!err) {
					for (var i = 0; i < data.length; i++) {

						if (data[i].categoryId.name == request.body.categoryName && data[i].categoryId.language == request.body.categoryLanguage && data[i].categoryId.isDeleted == false) {
							isCategoryExists = true;
							categoryId = data[i].categoryId._id;
							break;
						} else {
							continue;
						}
					}
					cb(null);
				} else {
					cb(err);
				}
			})

		},
		topic_binding_subCategory: function (cb) {
			var query = {
				topicId: request.body.topicId,
				categoryLanguage: request.body.categoryLanguage,
				subCategoryLanguage: request.body.subCategoryLanguage
			}
			var path = 'categoryId subCategoryId';
			var select = 'name icon language isDeleted name icon language isDeleted';
			var populate = {
				path: path,
				match: {},
				select: select,
				options: {
					lean: true
				}
			};


			var projection = {
				categoryId: 1,
				subCategoryId: 1
			}

			ServiceTopicBinding.getAllTopicBinding(query, projection, populate, {}, {}, function (err, data) {
				logging.consolelog("topic_binding", err, data);
				if (!err) {
					for (var i = 0; i < data.length; i++) {

						if (data[i].subCategoryId && data[i].categoryId && data[i].categoryId.name == request.body.categoryName && data[i].categoryId.language == request.body.categoryLanguage && data[i].categoryId.isDeleted == false &&
							data[i].subCategoryId.name == request.body.subCategoryName && data[i].subCategoryId.isDeleted == false) {
							isSubCategoryExists = true;
							subCategoryId = data[i].subCategoryId._id;
							break;
						} else {
							continue;
						}
					}
					cb(null);
				} else {
					cb(err);
				}
			})

		},
		category_data: ['topic_binding_category', 'topic_binding_subCategory', function (cb) {
			logging.consolelog("category_data", isCategoryExists, request.body.categoryId);
			if (isCategoryExists) {


				ServiceTopicCategory.getTopicCategory({
					_id: categoryId
				}, {
					name: 1,
					icon: 1
				}, {
					lean: true
				}, function (err, data) {
					if (!err) {
						categoryIcon = data[0].icon;
						cb(null)
					}
				});
			} else if (request.body.categoryId) {
				ServiceTopicCategory.getTopicCategory({
					_id: request.body.categoryId
				}, {
					name: 1,
					icon: 1
				}, {
					lean: true
				}, function (err, data) {
					if (!err) {
						categoryIcon = data[0].icon;
						cb(null)
					}
				})
			} else {
				cb(null)
			};
        }],
		sub_category_data: ['topic_binding_category', 'topic_binding_subCategory', function (cb) {
			logging.consolelog("sub_category_data", isSubCategoryExists, request.body.subCategoryId);
			if (isSubCategoryExists) {


				ServiceTopicSubCategory.getTopicSubCategory({
					_id: subCategoryId
				}, {
					name: 1,
					icon: 1
				}, {
					lean: true
				}, function (err, data) {
					if (!err) {
						subCategoryIcon = data[0].icon;
						cb(null)
					}
				});
			} else if (request.body.subCategoryId) {
				ServiceTopicSubCategory.getTopicSubCategory({
					_id: request.body.subCategoryId
				}, {
					name: 1,
					icon: 1
				}, {
					lean: true
				}, function (err, data) {
					if (!err) {
						subCategoryIcon = data[0].icon;
						cb(null)
					}
				})
			} else {
				cb(null);
			}


        }],
		content_data: function (cb) {
			var query = {
				_id: request.body.contentId
			}
			ServiceTemporaryContent.getTemporaryContentStorage(query, {}, {}, function (err, data) {
				logging.consolelog("content id", "", request.body.contentId)
				if (!err) {
					if (data.length > 0) {
						content = data[0];
						cb(null);
					} else {
						cb(constants.STATUS_MSG.ERROR.INVALID_JOBID);
					}


				} else {
					cb(err);
				}
			})


		},

		upload_category_icons: ['topic_binding_category', function (cb) {
			if (isCategoryExists || request.body.categoryId) {
				cb(null);
			} else {
				if (request.files) {
					UploadManager.uploadPicToS3Bucket(request.files.categoryIcon, "profile", function (filename) {
						if (filename == "No File") {
							cb(null);
						} else {
							categoryIcon = AwsConfig.s3BucketCredentials.s3URL + "/profile/" + filename;
							cb(null);
						}
					});
				} else {
					cb(null);
				}

			}
        }],
		upload_subcategory_icons: ['topic_binding_subCategory', function (cb) {
			logging.consolelog("icons::", isSubCategoryExists, request.body.subCategoryId)
			if (isSubCategoryExists || request.body.subCategoryId) {
				cb(null);
			} else {
				if (request.files) {
					UploadManager.uploadPicToS3Bucket(request.files.subCategoryIcon, "profile", function (filename) {
						if (filename == "No File") {
							cb(null);
						} else {
							subCategoryIcon = AwsConfig.s3BucketCredentials.s3URL + "/profile/" + filename;
							cb(null);
						}
					});
				} else {
					cb(null);
				}

			}


        }],

		add_content_to_topic: ['upload_category_icons', 'category_data', 'upload_subcategory_icons', function (cb) {
			var dataToSave = {};
			logging.consolelog("logging", isCategoryExists, isSubCategoryExists);
			if (isCategoryExists && isSubCategoryExists) {

				var query = {
					_id: subCategoryId
				}

				dataToSave['title'] = content.title;
				dataToSave['price'] = content.price;
				dataToSave['language'] = content.language;
				dataToSave['isVideo'] = content.isVideo;
				dataToSave['typeOfVideo'] = content.typeOfVideo;
				dataToSave['contentUrl'] = content.contentUrl;
				dataToSave['icon'] = content.icon;
				dataToSave['description'] = content.description;
				dataToSave['ifFree'] = content.isFree;
				dataToSave['isActive'] = true;
				dataToSave['typeOfContent'] = content.typeOfContent;
				var update = {
					$push: {
						content: dataToSave
					}
				}
				ServiceTopicSubCategory.updateTopicSubCategory(query, update, {}, function (err, data) {
					if (!err) {
						cb(null);
					} else {
						cb(err);
					}
				})
			} else if (isCategoryExists && !isSubCategoryExists) {
				dataToSave.name = request.body.subCategoryName;
				dataToSave.icon = subCategoryIcon;
				// dataToSave.price = request.body.categoryPrice;
				dataToSave.language = request.body.subCategoryLanguage;
				dataToSave.isFree = request.body.isFree;
				ServiceTopicSubCategory.createTopicSubCategory(dataToSave, function (err, data) {
					if (!err) {
						subCategoryId = data._id;
						cb(null);
					} else {
						cb(err);
					}
				})

			} else if (!isCategoryExists && !isSubCategoryExists) {
				logging.consolelog("logging", subCategoryIcon, categoryIcon);
				dataToSave.name = request.body.subCategoryName;
				dataToSave.icon = subCategoryIcon;
				dataToSave.isFree = request.body.isFree;
				// dataToSave.price = request.body.categoryPrice;
				dataToSave.language = request.body.subCategoryLanguage;
				ServiceTopicSubCategory.createTopicSubCategory(dataToSave, function (err, data) {
					if (!err) {
						subCategoryId = data._id;

					}
				});
				var data = {};
				data.name = request.body.categoryName;
				data.icon = categoryIcon;
				data.isFree = request.body.isFree;
				data.price = request.body.categoryPrice;
				data.language = request.body.categoryLanguage;
				ServiceTopicCategory.createTopicCategory(data, function (err, data) {
					if (!err) {
						categoryId = data._id;
						cb(null);
					} else {
						cb(err);
					}
				});


			}
        }],
		add_dataToNewSubCategory: [
            'add_content_to_topic',
            function (cb) {
				if (isCategoryExists && isSubCategoryExists) {
					cb(null);
				} else {

					logging.consolelog("", subCategoryId, "please input subcategory Id");
					var query = {
						_id: subCategoryId
					}

					dataToSave['title'] = content.title;
					dataToSave['price'] = content.price;
					dataToSave['language'] = content.language;
					dataToSave['isVideo'] = content.isVideo;
					dataToSave['typeOfVideo'] = content.typeOfVideo;
					dataToSave['contentUrl'] = content.contentUrl;
					dataToSave['icon'] = content.icon;
					dataToSave['description'] = content.description;
					dataToSave['ifFree'] = content.isFree;
					dataToSave['isActive'] = true;
					dataToSave['typeOfContent'] = content.typeOfContent;
					var update = {
						$push: {
							content: dataToSave
						}
					}
					ServiceTopicSubCategory.updateTopicSubCategory(query, update, {}, function (err, data) {
						if (!err) {
							cb(null);
						} else {
							cb(err);
						}
					});


				}

            }
        ],
		topic_linking: ["add_dataToNewSubCategory", function (cb) {
				if (isCategoryExists && isSubCategoryExists) {
					cb(null);
				} else {
					var dataToSave = {
						topicId: request.body.topicId,
						categoryId: categoryId,
						categoryLanguage: request.body.categoryLanguage,
						subCategoryId: subCategoryId,
						subCategoryLanguage: request.body.subCategoryLanguage
					}

					ServiceTopicBinding.createTopicBinding(dataToSave, function (err, data) {
						if (!err) {
							cb(null);
						} else {
							cb(err);
						}
					})
				}


        }

        ],
		delete_content: ['topic_linking', function (cb) {

			ServiceTemporaryContent.deleteTemporaryContentStorage({
				_id: request.body.contentId
			}, function (err, data) {
				if (!err) {
					cb(null);
				} else {
					cb(err);
				}
			})
		}],
		"assign_agent_temp_tables": ["add_dataToNewSubCategory", (cb) => {
			
						console.log("request.body.topicId in assign_agent_temp_tables ", request.body.topicId);
			
						ServiceTempAssignData.getSuperAdmin({
							topicId: request.body.topicId
						}, {}, {
							lean: true
						}, (err, data) => {
							console.log("err,data::: ServiceTempAssignData", err, data);
							if (!err) {
								if (data.length > 0) {
			
									var count = 0;
									async.forEach(data, (item, embeddedcb) => {
			
										console.log(item, "=====> item");
										count++;
										var content = [];
										console.log(item.category, request.body.categoryName, "====> categoiry name");
			
										if (request.body.categoryName == "Video") {
											if (item.content.indexOf(content.typeOfVideo) == -1) {
												embeddedcb(null);
			
											} else {
												ServiceTopicCategory.getTopicCategory({
													_id: categoryId
												}, {
													content: 1
												}, {
													lean: true
												}, (errors, dataCategory) => {
													if (!errors) {
														if (dataCategory.length > 0) {
															if (dataCategory[0].content && dataCategory[0].content.length > 0) {
																var lastIndex = dataCategory[0].content.length - 1;
																var contentType = dataCategory[0].content[lastIndex]._id;
																content.push({
																	categoryId: categoryId,
																	topicId: parseInt(request.body.topicId),
																	startDate: item["startDate"],
																	endDate: item["endDate"],
																	isActive: true,
																	contentType: contentType
																})
																var query = {
																	transactionId: item.transactionId
																}
																ServiceCustomerBuyPackage.getTransaction(query, {
																	content: 1,
																	customerId: 1
																}, {}, function (err, dbdata) {
																	if (!err) {
																		dbdata[0].content = dbdata[0].content.concat(content);
																		var update = {
																			$set: {
																				'content': dbdata[0].content
																			}
																		}
																		ServiceCustomerBuyPackage.updateTransaction(query, update, {}, function (err, updateData) {
																			embeddedcb(null);
																		})
																	} else {
																		embeddedcb(err);
																	}
																})
			
			
			
			
															}
														}
													} else {
														embeddedcb(null);
													}
			
												})
			
											}
			
										} else {
											if (item.category.indexOf(request.body.categoryName) == -1) {
												embeddedcb(null);
											} else {
												content.push({
													categoryId: categoryId,
													topicId: parseInt(request.body.topicId),
													startDate: item["startDate"],
													endDate: item["endDate"],
													isActive: true
												})
			
			
												var query = {
													transactionId: item.transactionId
												}
												ServiceCustomerBuyPackage.getTransaction(query, {
													content: 1,
													customerId: 1
												}, {}, function (err, dbdata) {
													if (!err) {
														dbdata[0].content = dbdata[0].content.concat(content);
														var update = {
															$set: {
																'content': dbdata[0].content
															}
														}
														ServiceCustomerBuyPackage.updateTransaction(query, update, {}, function (err, updateData) {
															embeddedcb(null);
														})
													} else {
														embeddedcb(err);
													}
												})
			
			
											}
										}
			
			
			
			
									}, (err) => {
			
										if (!err) {
											if (count == data.length) {
												cb(null);
											}
										} else {
											cb(null);
										}
									})
								} else {
									cb(null);
								}
							} else {
								cb(err);
							}
						});
			
			
			
					}]
	}, function (err, data) {

		if (!err) callback(null, {
			categoryId: categoryId,
			subCategoryId: subCategoryId
		});
		else {
			callback(err);
		}
	});
}


var addContentToTopicInCategory = function (request, callback) {
	var content = null;
	var isCategoryExists = false;
	var categoryId = null;
	var categoryIcon = null;
	var bindingExists = false;
	var typeOfVideo= "";
	async.auto({
		icon: function (cb) {
			ServiceTopicCategory.getTopicCategory({
				name: request.body.categoryName
			}, {
				icon: 1
			}, {}, function (err, data) {
				if (!err) {
					if (data.length > 0) {
						categoryIcon = data[0].icon;

					}
					cb(null);
				} else {
					cb(null);
				}
			})

		},
		check_category_direct_data: function (cb) {
			var query = {
				topicId: request.body.topicId,
				categoryLanguage: request.body.categoryLanguage,
				subCategoryId: {
					$exists: false
				}
			}
			var populate = [{
				path: 'categoryId subCategoryId',
				match: {},
				select: 'name icon language isDeleted',
				options: {
					lean: true
				}
            }];

			var projection = {
				categoryId: 1,
				subCategoryId: 1
			}
			ServiceTopicBinding.getAllTopicBinding(query, projection, populate, {}, {}, function (err, data) {
				if (!err) {
					for (var i = 0; i < data.length; i++) {

						if (data[i].categoryId.name == request.body.categoryName && data[i].categoryId.language == request.body.categoryLanguage && data[i].categoryId.isDeleted == false) {
							bindingExists = true;
							categoryId = data[i].categoryId._id;
							break;
						} else {
							continue;
						}
					}
					cb(null);


				} else {
					cb(null);

				}

			});


		},
		topic_binding: function (cb) {
			logging.consolelog("1::topic_binding", "", "");
			var query = {
				topicId: request.body.topicId,
				categoryLanguage: request.body.categoryLanguage
			}

			var populate = [{
				path: 'categoryId subCategoryId',
				match: {},
				select: 'name icon language isDeleted name icon language',
				options: {
					lean: true
				}
            }];

			var projection = {
				categoryId: 1,
				subCategoryId: 1
			}
			ServiceTopicBinding.getAllTopicBinding(query, projection, populate, {}, {}, function (err, data) {
				logging.consolelog("topic_binding", err, data);
				if (!err) {

					for (var i = 0; i < data.length; i++) {

						if (data[i].categoryId.name == request.body.categoryName && data[i].categoryId.language == request.body.categoryLanguage && data[i].categoryId.isDeleted == false) {
							isCategoryExists = true;
							categoryId = data[i].categoryId._id;
							break;
						} else {
							continue;
						}
					}
					cb(null);
				} else {
					cb(err);
				}
			})

		},
		category_data: ['topic_binding', 'check_category_direct_data', function (cb) {
			logging.consolelog("2::category_data", "", "");
			logging.consolelog("category_data", isCategoryExists, request.body.categoryId);
			if (isCategoryExists) {


				ServiceTopicCategory.getTopicCategory({
					_id: categoryId
				}, {
					name: 1,
					icon: 1
				}, {
					lean: true
				}, function (err, data) {
					if (!err) {
						categoryIcon = data[0].icon;
						cb(null)
					}
				});
			} else if (request.body.categoryId) {
				ServiceTopicCategory.getTopicCategory({
					_id: request.body.categoryId
				}, {
					name: 1,
					icon: 1
				}, {
					lean: true
				}, function (err, data) {
					if (!err) {
						categoryIcon = data[0].icon;
						cb(null)
					}
				})
			} else {
				cb(null)
			};
        }],
		content_data: function (cb) {
			logging.consolelog("3::content_data", "", "");
			var query = {
				_id: request.body.contentId
			}
			ServiceTemporaryContent.getTemporaryContentStorage(query, {}, {}, function (err, data) {
				if (!err) {
					if (data.length > 0) {
						content = data[0];

						cb(null);
					} else {
						cb(constants.STATUS_MSG.ERROR.INVALID_JOBID);
					}

				} else {
					cb(err);
				}
			})


		},

		upload_category_icons: ['topic_binding', 'check_category_direct_data', function (cb) {
			//			logging.consolelog("4::upload_category_icons", request.files.categoryIcon);
			if (isCategoryExists || request.body.categoryId) {
				logging.consolelog("why coming here", "", "");
				cb(null);
			} else {
				if (request.files) {
					UploadManager.uploadPicToS3Bucket(request.files.categoryIcon, "profile", function (filename) {
						logging.consolelog("filename::", "", filename);
						if (filename == "No File") {
							cb(null);
						} else {
							categoryIcon = AwsConfig.s3BucketCredentials.s3URL + '/profile/' + filename;
							logging.consolelog("categoryIcon::", "", categoryIcon);
							cb(null);
						}
					});
				} else {
					cb(null);
				}

			}
        }],

		add_content_to_topic: ['upload_category_icons', 'category_data', "content_data", 'check_category_direct_data', function (cb) {
			logging.consolelog("5::add_content_to_topic", "", "");
			var dataToSave = {};
			if (isCategoryExists) {

				var query = {
					_id: categoryId
				}

				dataToSave['title'] = content.title;
				dataToSave['price'] = content.price;
				dataToSave['language'] = content.language;
				dataToSave['isVideo'] = content.isVideo;
				dataToSave['typeOfVideo'] = content.typeOfVideo;
				dataToSave['contentUrl'] = content.contentUrl;
				dataToSave['icon'] = content.icon;
				dataToSave['description'] = content.description;
				dataToSave['ifFree'] = content.isFree;
				dataToSave['isActive'] = true;
				dataToSave['typeOfContent'] = content.typeOfContent;
				dataToSave['srtFile']= content.srtFile;
				var update = {
					$push: {
						content: dataToSave
					}
				}
				ServiceTopicCategory.updateTopicCategory(query, update, {}, function (err, data) {
					if (!err) {
						cb(null);
					} else {
						cb(err);
					}
				})
			} else {
				dataToSave.name = request.body.categoryName;
				dataToSave.icon = categoryIcon;
				dataToSave.price = request.body.categoryPrice;
				dataToSave.language = request.body.categoryLanguage;
				dataToSave.isFree = request.body.isFree;

				ServiceTopicCategory.createTopicCategory(dataToSave, function (err, data) {
					if (!err) {
						categoryId = data._id;
						cb(null);
					} else {
						cb(err);
					}
				})

			}
        }],
		add_dataToNewCategory: [
            'add_content_to_topic',
            function (cb) {
				logging.consolelog("6::add_dataToNewCategory", "", content);
				typeOfVideo=content.typeOfVideo;
				if (isCategoryExists) {
					cb(null);
				} else {
					var query = {
						_id: categoryId
					}
					var dataToSave = {};
					dataToSave['title'] = content.title;
					dataToSave['price'] = content.price;
					dataToSave['language'] = content.language;
					dataToSave['isVideo'] = content.isVideo;
					dataToSave['typeOfVideo'] = content.typeOfVideo;
					dataToSave['contentUrl'] = content.contentUrl;
					dataToSave['icon'] = content.icon;
					dataToSave['description'] = content.description;
					dataToSave['ifFree'] = content.isFree;
					dataToSave['isActive'] = true;
					dataToSave['typeOfContent'] = content.typeOfContent;
					dataToSave['srtFile']= content.srtFile;
					var update = {
						$push: {
							content: dataToSave
						}
					}
					ServiceTopicCategory.updateTopicCategory(query, update, {}, function (err, data) {
						if (!err) {
							cb(null);
						} else {
							cb(err);
						}
					});


				}

            }
        ],
		"assign_agent_temp_tables": ["add_dataToNewCategory", (cb) => {

			console.log("request.body.topicId in assign_agent_temp_tables ", request.body.topicId);

			ServiceTempAssignData.getSuperAdmin({
				topicId: request.body.topicId
			}, {}, {
				lean: true
			}, (err, data) => {
				console.log("err,data::: ServiceTempAssignData", err, data);
				if (!err) {
					if (data.length > 0) {

						var count = 0;
						async.forEach(data, (item, embeddedcb) => {

							console.log(item, "=====> item", request.body.categoryName);
							count++;
							var content = [];
							console.log(item.category, request.body.categoryName, request.body.categoryName == "Video","====> categoiry name");

							if (request.body.categoryName == "Video") {
								console.log(item.content, typeOfVideo,item.content.indexOf(content.typeOfVideo), "===> type of video");
								if (item.content.indexOf(typeOfVideo) == -1) {
									embeddedcb(null);

								} else {
									ServiceTopicCategory.getTopicCategory({
										_id: categoryId
									}, {
										content: 1
									}, {
										lean: true
									}, (errors, dataCategory) => {
										console.log(errors, JSON.stringify(dataCategory), "=====> video category");
										if (!errors) {
											if (dataCategory.length > 0) {
												if (dataCategory[0].content && dataCategory[0].content.length > 0) {
													var lastIndex = dataCategory[0].content.length - 1;
													var contentType = dataCategory[0].content[lastIndex]._id;
													content.push({
														categoryId: categoryId,
														topicId: parseInt(request.body.topicId),
														startDate: item["startDate"],
														endDate: item["endDate"],
														isActive: true,
														contentType: contentType
													})
													var query = {
														transactionId: item.transactionId
													}
													ServiceCustomerBuyPackage.getTransaction(query, {
														content: 1,
														customerId: 1
													}, {}, function (err, dbdata) {
														if (!err) {
															dbdata[0].content = dbdata[0].content.concat(content);
															var update = {
																$set: {
																	'content': dbdata[0].content
																}
															}
															ServiceCustomerBuyPackage.updateTransaction(query, update, {}, function (err, updateData) {
																embeddedcb(null);
															})
														} else {
															embeddedcb(err);
														}
													})




												}
											}
										} else {
											embeddedcb(null);
										}

									})

								}

							} else {
								if (item.category.indexOf(request.body.categoryName) == -1) {
									embeddedcb(null);
								} else {
									content.push({
										categoryId: categoryId,
										topicId: parseInt(request.body.topicId),
										startDate: item["startDate"],
										endDate: item["endDate"],
										isActive: true
									})


									var query = {
										transactionId: item.transactionId
									}
									ServiceCustomerBuyPackage.getTransaction(query, {
										content: 1,
										customerId: 1
									}, {}, function (err, dbdata) {
										if (!err) {
											dbdata[0].content = dbdata[0].content.concat(content);
											var update = {
												$set: {
													'content': dbdata[0].content
												}
											}
											ServiceCustomerBuyPackage.updateTransaction(query, update, {}, function (err, updateData) {
												embeddedcb(null);
											})
										} else {
											embeddedcb(err);
										}
									})


								}
							}




						}, (err) => {

							if (!err) {
								if (count == data.length) {
									cb(null);
								}
							} else {
								cb(null);
							}
						})
					} else {
						cb(null);
					}
				} else {
					cb(err);
				}
			});



        }],
		topic_linking: ["add_dataToNewCategory", function (cb) {
				if (isCategoryExists && bindingExists == true) {
					cb(null);
				} else {
					var dataToSave = {
						topicId: request.body.topicId,
						categoryId: categoryId,
						categoryLanguage: request.body.categoryLanguage
					}

					ServiceTopicBinding.createTopicBinding(dataToSave, function (err, data) {
						if (!err) {
							cb(null);
						} else {
							cb(err);
						}
					})
				}


        }

        ],
		delete_content: ['topic_linking', function (cb) {

			ServiceTemporaryContent.deleteTemporaryContentStorage({
				_id: request.body.contentId
			}, function (err, data) {
				if (!err) {
					cb(null);
				} else {
					cb(err);
				}
			})
        }]
	}, function (err, data) {

		if (!err) callback(null, {
			categoryId: categoryId
		});
		else {
			callback(err);
		}
	});

}


var readContentStatus = function (request, response) {
	var jobId = null;
	var outputKey = null;
	async.series([
        function (cb) {
			ServiceTemporaryContent.getTemporaryContentStorage({
				_id: request.body.contentId
			}, {}, {}, function (err, data) {
				if (!err) {
					if (data.length > 0) {
						if (data[0].isVideo == true) {
							jobId = data[0].jobId;
							outputKey = data[0].outputKey;
							cb(null);
						} else if (data[0].typeOfContent == "audio") {
							jobId = data[0].jobId;
							outputKey = data[0].outputKey;
							cb(null);

						} else cb(null)
					} else cb(null);

				}


			})

        },
        function (cb) {
			if (jobId) {
				UploadManager.readJobStatus(jobId, function (data) {
					logging.consolelog("read job status::", "", data);
					if (data) {
						if (data.Job.Output.Status == 'Complete') {
							var contentUrl = AwsConfig.s3BucketCredentials.s3URL + '/' + AwsConfig.s3BucketCredentials.videosFolder + '/' + outputKey;
							logging.consolelog("content URL in read content status file", "", contentUrl);
							ServiceTemporaryContent.updateTemporaryContentStorage({
								_id: request.body.contentId
							}, {
								$set: {
									contentUrl: contentUrl
								}
							}, {}, function (err, data) {
								logging.consolelog(err, data, "update conetnt");
							})
							cb(null);
						} else if (data.Job.Output.Status == 'Error') {
							cb(constants.STATUS_MSG.ERROR.CONTENT_UPLOAD_ERROR);
						} else {
							cb(constants.STATUS_MSG.ERROR.WAIT_CONTENT_UPLOADING);
						}

					} else {
						cb(err);
					}
				})
			} else {
				cb(null);
			}

        }
    ], function (err, data) {

		if (!err) response.status(200).send(constants.STATUS_MSG.SUCCESS.CONTENT_ADDED);
		else {
			response.send(err);
		}
	});
}


var addContentToTopic = function (request, response) {
	var final = {}
	async.series([
        function (cb) {
			logging.consolelog(request.body, "", "Body data");
			if (request.body.categoryName && request.body.subCategoryName) {
				try {
					addContentToTopicInSubCategory(request, function (err, data) {
						if (!err) {
							final.categoryId = data.categoryId;
							final.subCategoryId = data.subCategoryId;
							cb(null);
						} else {
							cb(err);
						}

					})
				} catch (e) {
					cb(null)
				}


			} else if (request.body.categoryName) {
				addContentToTopicInCategory(request, function (err, data) {
					logging.consolelog(err, data, "linking of data in category");
					if (!err) {
						final.categoryId = data.categoryId;
						final.subCategoryId = data.subCategoryId;
						cb(null);
					} else {
						cb(err);
					}

				});
			}
        }
    ], function (err, data) {

		if (!err) {
			response.status(200).send({
				final: final,
				statusCode: 200
			});
		} else {
			response.send(err);
		}
	});
}


var getCategorySubCategory = function (request, response) {
	var categoryList = [];
	var subCategoryList = [];
	var language = [];
	async.series([
        function (cb) {
			ServiceTopicCategory.getUniqueTopicCategory({}, 'name', function (err, data) {
				logging.consolelog("err,data1", err, data);
				if (!err) {
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							categoryList.push({
								category: data[i]
							});
						}
						cb(null);
					} else {
						cb(null);
					}
				} else {
					cb(err);
				}
			});


        },
        function (cb) {
			ServiceTopicSubCategory.getUniqueTopicSubCategory({}, 'name', function (err, data) {
				logging.consolelog("err,data2", err, data);
				if (!err) {
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							subCategoryList.push({
								subCategory: data[i]
							});
						}
						cb(null);
					} else {
						cb(null);
					}
				} else {
					cb(err);
				}
			});
        },
        function (cb) {
			ServiceTopic.getUniqueTopics({
				isDeleted: false
			}, 'language', function (err, data) {
				if (!err) {
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							language.push({
								language: data[i]
							});
						}
						cb(null);
					} else {
						cb(null);
					}
				} else {
					cb(err);
				}
			})


        }
    ], function (err, data) {

		if (!err) response.status(200).send({
			categoryList: categoryList,
			subCategoryList: subCategoryList,
			languageList: language,
			statusCode: 200

		});
		else {
			response.send(err);
		}
	});
}


var getBoardForLanguage = function (request, response) {
	var list = [];
	var topicListObj = {};
	var topicList = [];
	var language = [];
	var languageList = [];
	var boardList = [];
	var gradeList = [];
	var subjectList = [];
	var chapterList = [];
	var count = null;
	var board = [];
	var grade = [];
	var subject = [];
	var chapter = [];

	if (request.body.language) {
		for (var i = 0; i < request.body.language.length; i++) {
			language.push(request.body.language[i].language)
		}
	}
	if (request.body.board) {
		for (var i = 0; i < request.body.board.length; i++) {
			board.push(request.body.board[i].board)
		}
	}
	if (request.body.grade) {
		for (var i = 0; i < request.body.grade.length; i++) {
			grade.push(request.body.grade[i].grade)
		}
	}
	if (request.body.subject) {
		for (var i = 0; i < request.body.subject.length; i++) {
			subject.push(request.body.subject[i].subject)
		}

	}
	if (request.body.chapter) {
		for (var i = 0; i < request.body.chapter.length; i++) {
			chapter.push(request.body.chapter[i].chapter)
		}
	}
	var curriculumId = [];
	var topicId = [];
	async.auto({
		board_list: function (cb) {
			var query = {};
			if (language.length > 0) {
				query.language = {
						$in: language
					},
					query.isRemoved = "false";
				ServiceCurriculum.getUniqueCurriculum(query, 'board', function (err, data) {
					if (!err) {
						for (var i = 0; i < data.length; i++) {
							boardList.push({
								board: data[i].board
							});
						}

						cb(null);
					} else {
						cb(err);

					}
				})
			} else {
				cb(null);
			}
		},
		grade_list: function (cb) {
			var query = {};
			if (language.length > 0 && board.length > 0) {
				query.language = {
					$in: language
				}
				query.isRemoved = "false";
				query.board = {
					$in: board
				}
				ServiceCurriculum.getUniqueCurriculum(query, 'grade', function (err, data) {
					if (!err) {
						for (var i = 0; i < data.length; i++) {
							gradeList.push({
								grade: data[i].grade
							});
						}

						cb(null);
					} else {
						cb(err);

					}
				})
			} else {
				cb(null);
			}
		},
		subject_list: function (cb) {
			var query = {};
			if (language.length > 0 && board.length > 0 && grade.length > 0) {
				query.language = {
					$in: language
				}
				query.isRemoved = "false";
				query.board = {
					$in: board
				}
				query.grade = {
					$in: grade
				}
				ServiceCurriculum.getUniqueCurriculum(query, 'subject', function (err, data) {
					if (!err) {
						for (var i = 0; i < data.length; i++) {
							subjectList.push({
								subject: data[i].subject
							});
						}

						cb(null);
					} else {
						cb(err);

					}
				})
			} else {
				cb(null);
			}
		},
		chapter_list: function (cb) {
			var query = {};
			if (language.length > 0 && board.length > 0 && grade.length > 0 && subject.length > 0) {
				query.language = {
					$in: language
				}
				query.isRemoved = "false";
				query.board = {
					$in: board
				}
				query.grade = {
					$in: grade
				}
				query.subject = {
					$in: subject
				}
				ServiceCurriculum.getUniqueCurriculum(query, 'chapter', function (err, data) {
					if (!err) {
						for (var i = 0; i < data.length; i++) {
							chapterList.push({
								chapter: data[i].chapter
							});
						}

						cb(null);
					} else {
						cb(err);

					}
				})
			} else {
				cb(null);
			}
		},
		language_list: function (cb) {
			ServiceCurriculum.getUniqueCurriculum({
				isRemoved: false
			}, 'language', function (err, data) {
				if (!err) {
					logging.consolelog("language data::", err, JSON.stringify(data));
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							languageList.push({
								language: data[i].language
							});
						}
						cb(null);
					} else {
						cb(constants.STATUS_MSG.ERROR.EMPTY_CURRICULUM);
					}

				} else {
					cb(err);
				}
			})
		},
		curriculum_list: function (cb) {
			var projection = {
				'result.topicId': 1

			}
			var query = {};
			if (language.length > 0) {
				query.language = {
						$in: language
					}
					//					query.isRemoved = "false";

			}
			if (board.length > 0) {

				query.board = {
					$in: board
				}
			}
			if (grade.length > 0) {

				query.grade = {
					$in: grade
				}
			}
			if (subject.length > 0) {

				query.subject = {
					$in: subject
				}
			}
			if (chapter.length > 0) {

				query.chapter = {
					$in: chapter
				}
			}
			if (!(language.length > 0)) {
				query.board = constants.COMMON_VARIABLES.UNIVERSAL_BOARD_NAME;
				//	query.isRemoved = "false";

			}


			logging.consolelog("query", "", query);
			var limit = {
				limit: request.body.limit ? request.body.limit : 10,
				offset: request.body.skip ? request.body.skip : 0
			}
			var search = "";
			if (request.body.search) {
				search = ' and name like "%' + request.body.search + '%" ';
			}
			ServiceCurriculum.getCurriculumAndTopic(query, limit, search, function (err, data) {
				if (!err) {
					logging.consolelog('logging', '', data)
					if (data.length) {
						for (var i = 0; i < data.length; i++) {
							topicListObj[data[i].name] = {
								language: data[i].topicLanguage.split(","),
								topicName: data[i].name,
								topicId: data[i]["topicId"],
								board: data[i].board,
								grade: data[i].grade,
								chapter: data[i].chapter,
								subject: data[i].subject
							}
						}
						for (var i in topicListObj) {
							topicList.push(topicListObj[i]);
						}
						ServiceCurriculum.getCurriculumAndTopicCount(query, limit, search, function (err, data1) {
							if (!err) {
								logging.consolelog("logging1", "", data1.length);
								if (data1.length && data1[0].cnt) {
									count = data1[0].cnt;
								}
								// count= data1.length;
								cb(null);
							} else {
								cb(err);
							}
						});
					} else {
						cb(null)
					}
				} else {
					cb(err);
				}
			});
		}
	}, function (err, data) {

		if (!err) {
			response.status(200).send({
				statusCode: 200,
				boardList: boardList,
				gradeList: gradeList,
				chapterList: chapterList,
				subjectList: subjectList,
				topicList: topicList,
				languageList: languageList,
				count: count
			})
		} else {
			response.send(err)
		}
	})
}


var editTopic = function (request, response) {
	var update = {};
	var curriculumLinked = [];
	var topicDetails = {};
	var curriculumId = [];
	var topicName = null;
	var topicIds = [];
	var customerId = [];
	logging.consolelog("request.body--editTopic", "", request.body);
	async.auto({
		icon_update: function (cb) {
			if (request.files) {
				if (request.files.hasOwnProperty("iconImage")) {
					UploadManager.uploadPicToS3Bucket(request.files.iconImage, "profile", function (filename) {
						if (filename != "No File") {
							update.icon = AwsConfig.s3BucketCredentials.s3URL + "/profile/" + filename;
							cb(null);
						} else {
							cb(null);
						}
					});
				} else {
					cb(null);
				}
			} else {
				cb(null);
			}
		},
		get_topic: function (cb) {
			var query = {
				_id: request.body.topicId
			}
			ServiceTopic.getTopic(query, {
				name: 1
			}, {}, function (err, data) {
				if (!err) {
					if (data.length > 0) {
						topicName = data[0].name;
						cb(null);
					} else {
						cb(constants.STATUS_MSG.ERROR.INVALID_JOBID);
					}
				} else {
					cb(err);
				}
			});
		},
		get_topicIds: ['get_topic', function (cb) {
			ServiceTopic.getTopic({
				name: topicName
			}, {
				_id: 1,
				language: 1
			}, {}, function (err, data) {
				if (!err) {
					for (var i = 0; i < data.length; i++) {
						topicIds.push({
							id: data[i]._id,
							language: data[i].language
						});
					}
					cb(null);
				} else {
					cb(err);
				}
			})
        }],
		update_global_parameters: ['get_topic', function (cb) {
			var updates = {};
			if (request.body.price) {

				updates.price = request.body.price;
			}
			if (request.body.name) {

				updates.name = request.body.name;
			}
			logging.consolelog(updates, "", "update name wise");
			if ('price' in updates || 'name' in updates) {
				ServiceTopic.updateTopic({
					name: topicName
				}, updates, {
					multi: true
				}, function (err, data) {
					logging.consolelog("update", err, data);
					cb(null);
				});
			} else {
				cb(null);
			}

        }],
		topic_edit: ['icon_update', 'update_global_parameters', function (cb) {

			var query = {
				_id: request.body.topicId
			}
			if (request.body.authorName) {
				update.authorName = request.body.authorName;
			}
			if (request.body.description) {
				update.description = request.body.description;
			}
			if (request.body.gtinCode) {
				update.gtinCode = request.body.gtinCode;
			}

			update.isFree = request.body.isFree;

			if ('authorName' in update || 'description' in update || 'gtinCode' in update || 'isFree' in update) {
				logging.consolelog(query, update, "update id wise");
				ServiceTopic.updateTopic(query, update, {}, function (err, data) {
					logging.consolelog("update values", err, data);
					if (!err) {
						cb(null);
					} else {
						cb(err);
					}
				});
			} else {
				cb(null);
			}

        }],
		addLinking: function (cb) {
			logging.consolelog("topic_edit::4th", "", "")
			if (request.body.addToCurriculum) {
				var counter = 0;
				async.forEach(request.body.addToCurriculum, function (item, embeddedcb) {
					counter++
					var query = {
						board: item.board,
						grade: item.grade,
						subject: item.subject,
						chapter: item.chapter,
						language: item.language,
						isRemoved: false

					}

					ServiceCurriculum.getCurriculum(query, {
						_id: 1
					}, {}, function (err, response) {
						logging.consolelog("Err,response", err, response);
						if (err) {
							embeddedcb(null);
						} else {
							curriculumId.push({
								_id: response[0]._id,
								isUniversal: item.board == constants.COMMON_VARIABLES.UNIVERSAL_BOARD_NAME ? true : false
							});
							embeddedcb(null);
						}
					});
				}, function (err) {
					if (counter == request.body.addToCurriculum.length) {
						cb();
					}
				});

			} else {
				cb(null);
			}

		},
		check_universal_mapping: ['get_topicIds', function (cb) {
			logging.consolelog("topicEdit 6th", "", "");

			if (request.body.subject) {
				if (topicIds.length > 0) {
					for (var i = 0; i < topicIds.length; i++) {
						(function (topic) {

							var dataToSave = {
								board: constants.COMMON_VARIABLES.UNIVERSAL_BOARD_NAME,
								language: topic["language"],
								subject: request.body.subject,
								chapter: request.body.chapter,
								grade: request.body.grade,
								isRemoved: false
							}
							ServiceCurriculum.getCurriculum(dataToSave, {
								_id: 1
							}, {}, function (err, data) {
								logging.consolelog(err, data, "show data");
								if (!err) {
									if (data.length > 0) {

										var query = {
											topicId: topic["id"],
											isUniversal: true
										}
										var update = {
											curriculumId: data[0]._id
										}
										logging.consolelog("", query, update);
										ServiceTopicCurriculumMapper.updateTopicCurriculumMapping(query, update, function (err, data) {
											logging.consolelog("update curriculum data", err, data);

										})
									}

								}
							})
						})(topicIds[i]);


					}
					cb(null);


				} else {
					cb(null);
				}
			} else {
				cb(null);
			}
        }],
		mapper: ['addLinking', function (cb) {
			logging.consolelog("topic_edit::5th", "", "");
			if (curriculumId) {

				if (curriculumId.length > 0) {
					for (var i = 0; i < curriculumId.length; i++) {
						(function (curriculum) {
							var dataToSave = {
								curriculumId: curriculum["_id"],
								topicId: request.body.topicId,
								isUniversal: curriculum["isUniversal"],
								removed: false
							}
							logging.consolelog("topic_edit::5th", "", dataToSave);
							ServiceTopicCurriculumMapper.createCurriculumMapping(dataToSave, function (err, data) {

								if (!err) {
									var query = {
										curriculumId: curriculum["_id"],
										topicId: request.body.topicId,
										isUniversal: curriculum["isUniversal"]
									}
									var update = {

										removed: false

									}

									ServiceTopicCurriculumMapper.updateTopicCurriculumMapping(query, update, function (error, response) {

									})
								} else {
									var query = {
										curriculumId: curriculum["_id"],
										topicId: request.body.topicId,
										isUniversal: curriculum["isUniversal"]
									}
									var update = {

										removed: false

									}

									ServiceTopicCurriculumMapper.updateTopicCurriculumMapping(query, update, function (error, response) {

									})

								}
							});
						})(curriculumId[i]);
					}
					cb(null);

				} else {
					cb(null);
				}
			} else {
				cb(null);
			}
        }],
		get_updated_topic: ['addLinking', 'topic_edit', 'icon_update', 'update_global_parameters', function (cb) {
			logging.consolelog("topic_edit::6th", "", "")
			var query = {
				topicId: request.body.topicId
			}
			ServiceCurriculum.getCurriculumByTopic(query, {}, {}, function (err, data) {

				if (!err) {

					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							if (data[i].board == constants.COMMON_VARIABLES.UNIVERSAL_BOARD_NAME) {

							} else {
								curriculumLinked.push({
									board: data[i].board,
									language: data[i].language,
									grade: data[i].grade,
									subject: data[i].subject,
									chapter: data[i].chapter
								});
							}


						}
						cb(null);
					} else {
						cb(null);
					}

				} else {
					cb(err);
				}
			})
        }],
		topic_detail: ['get_updated_topic', function (cb) {
			logging.consolelog("topic_detail::7th", "", "")
			var query = {
				_id: request.body.topicId
			}
			var projection = {
				name: 1,
				icon: 1,
				authorName: 1,
				language: 1,
				description: 1,
				gtinCode: 1
			}
			ServiceTopic.getTopic(query, projection, {}, function (err, data) {
				if (!err) {
					if (data.length > 0) {
						topicDetails.name = data[0].name;
						topicDetails.icon = data[0].icon;
						topicDetails.authorName = data[0].authorName;
						topicDetails.language = data[0].language;
						topicDetails.description = data[0].description;
						topicDetails.gtinCode = data[0].gtinCode;
						topicDetails.price = data[0].price;
						cb(null);
					} else {
						cb(null);
					}


				} else {
					cb(err);
				}
			});
        }],
		get_customer_list: function (cb) {
			ServiceCustomer.getCustomer({
				$or: [{
						userType: constants.USER_TYPE.STUDENT
                },
					{
						userType: constants.USER_TYPE.TEACHER
                    },
                ]

			}, {
				_id: 1
			}, {}, function (err, data) {

				if (!err) {

					for (var i = 0; i < data.length; i++) {
						customerId.push(data[i]._id);
					}
					cb(null);
				} else {
					cb(err);
				}
			})
		},
		notifications: ['get_customer_list', function (cb) {
			if (request.body.isFree) {
				NotificationManager.sendNotifications(customerId, 'FREE_TOPIC_ADVERTISEMENT', {});
				cb(null);
			} else {
				cb(null);
			}
        }]
	}, function (err, data) {
		if (!err) {
			response.status(200).send({
				statusCode: 200,
				topicDetails: topicDetails,
				curriculumLinked: curriculumLinked
			})
		} else {
			response.send(err);
		}
	})
}


var topicDetails = function (request, response) {
	var curriculumLinked = [];
	var topicDetails = {};
	var contentLinking = [];
	var universalLinked = [];
	var topicLanguage = [];
	var subjectList = [];
	var gradeList = [];
	var chapterList = [];
	var languageList = [];
	var boardList = [];
	async.series([
            function (cb) {
				var query = {};
				query["topic.language"] = request.body.language;
				query["topic.name"] = request.body.topic;
				query["topic.isDeleted"] = false;
				ServiceTopicCurriculumMapper.getTopicAndCurriculumDetails(query, function (err, data) {
					logging.consolelog("first", err, data);
					if (!err) {
						if (data.length > 0) {
							data.forEach(function (details) {
								if (details.isUniversal) {
									universalLinked.push({
										board: details.board,
										language: details.language,
										grade: details.grade,
										subject: details.subject,
										chapter: details.chapter
									});
								} else {
									curriculumLinked.push({
										board: details.board,
										language: details.language,
										grade: details.grade,
										subject: details.subject,
										chapter: details.chapter
									});
								}
								topicDetails.name = details.name;
								topicDetails.icon = details.icon;
								topicDetails.isFree = details.isFree;
								topicDetails.authorName = details.authorName;
								topicDetails.language = request.body.language;
								topicDetails.description = details.description;
								topicDetails.gtinCode = details.gtinCode;
								topicDetails.price = details.price;
								request.body.topicId = details.topicId;
							});
							logging.consolelog("logging", "", JSON.stringify(universalLinked));
							logging.consolelog("logging", "", JSON.stringify(curriculumLinked));
							logging.consolelog("logging", "", JSON.stringify(topicDetails));
							cb(null);
						} else {
							cb(null);
						}

					} else {
						cb(err);
					}
				});
            },
            function (cb) {
				var query = {
					name: request.body.topic,
					isDeleted: false
				}
				ServiceTopic.getTopic(query, {}, {}, function (err, data) {

					if (!err) {
						for (var i = 0; i < data.length; i++) {
							topicLanguage.push({
								language: data[i].language
							});
						}
						cb(null);
					} else {
						cb(err);
					}
				});


            },
            function (cb) {
				try {
					var query = {
						topicId: request.body.topicId
					}

					var path = 'categoryId subCategoryId';
					var select = 'name price icon language isPublish content isDeleted isFree name icon language content isDeleted isFree isPublish'
					var populate = {
						path: path,
						match: {
							isDeleted: false
						},
						select: select,
						options: {
							lean: true
						}
					};
					var projection = {
						categoryId: 1,
						subCategoryId: 1
					}
					ServiceTopicBinding.topicBindingPopulate(query, projection, populate, {}, {}, function (err, data) {
						logging.consolelog("err,data::topic Binding::", err, data);
						if (!err) {
							var i = 0;
							var length = data.length;
							var topic = {};
							for (i = 0; i < length; i++) {
								if (data[i].categoryId) {
									if (!topic[data[i].categoryId._id]) {
										topic[data[i].categoryId._id] = {
											direct: [],
											subCategory: [],
											categoryName: data[i].categoryId.name,
											categoryPrice: data[i].categoryId.price,
											categoryIcon: data[i].categoryId.icon,
											categoryLanguage: data[i].categoryId.language,
											categoryFree: data[i].categoryId.isFree,
											categoryIsPublish: data[i].categoryId.isPublish
										};
									}
									if (!data[i].subCategoryId) {
										for (var j = 0; j < data[i].categoryId.content.length; j++) {

											if (data[i].categoryId.content[j].isDeleted == false) {
												var obj = {
													content: data[i].categoryId.content[j]
												};
												topic[data[i].categoryId._id].direct.push(obj);

											}
										}

									}
									if (data[i].subCategoryId) {
										logging.consolelog("subcategory content", "", data[i].subCategoryId);
										var obj = {
											subCategoryId: data[i].subCategoryId._id,
											subCategoryName: data[i].subCategoryId.name,
											subCategoryIcon: data[i].subCategoryId.icon,
											subCategoryLanguage: data[i].subCategoryId.language,
											subCategoryContent: []
										};
										var sub_cat_content = [];
										for (var j = 0; j < data[i].subCategoryId.content.length; j++) {

											if (data[i].subCategoryId.content[j].isDeleted == false) {

												sub_cat_content.push(data[i].subCategoryId.content[j]);
											}
										}
										obj.subCategoryContent = sub_cat_content;
										topic[data[i].categoryId._id].subCategory.push(obj);
									}
								}
							}

							contentLinking = topic;
							cb(null);

						} else {
							cb(err);
						}
					})
				} catch (e) {
					cb(null);
				}

            },
            function (cb) {

				ServiceCurriculum.getAllFiltersForCurriculum({
					isRemoved: false
				}, function (err, data) {
					if (!err) {
						if (data.length > 0) {
							data[0]["languages"].split(",").forEach(function (language) {
								languageList.push({
									language: language
								});
							});
							data[0]["subjects"].split(",").forEach(function (subject) {
								subjectList.push({
									subject: subject
								});
							});
							data[0]["grades"].split(",").forEach(function (grade) {
								gradeList.push({
									grade: grade
								});
							});
							data[0]["chapters"].split(",").forEach(function (chapter) {
								chapterList.push({
									chapter: chapter
								});
							});
							data[0]["boards"].split(",").forEach(function (board) {
								boardList.push({
									board: board
								});
							});
							cb(null);
						} else {
							cb(constants.STATUS_MSG.ERROR.EMPTY_CURRICULUM);
						}

					} else {
						cb(err);
					}
				})
            }
        ],
		function (err, data) {
			if (!err) {
				response.status(200).send({
					statusCode: 200,
					curriculumLinked: curriculumLinked,
					topicDetails: topicDetails,
					contentLinking: contentLinking,
					universalLinked: universalLinked,
					topicId: request.body.topicId,
					languageList: languageList,
					subjectList: subjectList,
					chapterList: chapterList,
					gradeList: gradeList,
					boardList: boardList,
					topicLanguage: topicLanguage


				})
			}
		});
}


var deleteTopic = function (request, response) {
		var query = {
			name: request.body.topic,
			language: request.body.language

		}
		ServiceTopic.updateTopic(query, {
			isDeleted: true
		}, {
			multi: true
		}, function (error, data) {
			if (!error) {
				response.status(200).send(constants.STATUS_MSG.SUCCESS.TOPIC_DELETE);
			} else {
				response.send(error);
			}
		});
	}
	//
	// var topicLinking = function (topicId, curriculumArray, callback) {
	//
	//     async.series([
	//
	//         function (cb) {
	//             if (curriculumArray) {
	//                 if (curriculumArray.length > 0) {
	//                     logging.consolelog("logging", "", topicId, curriculumArray);
	//                     for (var i = 0; i < curriculumArray.length; i++) {
	//                         setTimeout(function (i) {
	//                             var dataToSave = {
	//                                 curriculumId: curriculumArray[i],
	//                                 topicId: topicId.topicId,
	//                                 universalCurriculumId: topicId.universal,
	//                                 removed: false
	//                             }
	//                             logging.consolelog("set time out", curriculumArray[i], topicId);
	//                             ServiceTopicCurriculumMapper.createCurriculumMapping(dataToSave, function (err, data) {
	//
	//                                 if (!err) {
	//                                 } else {
	//
	//                                 }
	//
	//                             })
	//                         }, 100, i);
	//
	//                     }
	//                     cb(null);
	//
	//                 }
	//             } else {
	//                 cb(null);
	//             }
	//
	//
	//         }
	//     ], function (err, result) {
	//         if (err) callback(err);
	//         else {
	//
	//             callback(null);
	//         }
	//     })
	// }


var topicLinkingWithCGCS = function (request, response) {
	var topicId = [];
	var curriculumId = [];
	var linked = [];
	async.auto({
		topic_id: function (cb) {
			var counter = 0;
			async.forEach(request.body.topic, function (item, embeddedcb) {
				counter++
				var query = {
					name: item.topic,
					language: item.language,
					isDeleted: false
				}

				ServiceTopic.getTopic(query, {
					_id: 1
				}, {}, function (err, response) {

					if (err) {

						embeddedcb(null);
					} else {
						topicId.push(response[0]._id);
						embeddedcb(null);
					}
				});


			}, function (err) {
				if (counter == request.body.topic.length) {
					cb();
				}

			});
		},

		curriculum_id: function (cb) {

			var counter = 0;
			async.forEach(request.body.curriculum, function (item, embeddedcb) {
				counter++
				var query = {
					board: item.board,
					grade: item.grade,
					subject: item.subject,
					chapter: item.chapter,
					language: item.language,
					isRemoved: false

				}
				logging.consolelog(query, "query data", "");
				ServiceCurriculum.getCurriculum(query, {
					_id: 1
				}, {}, function (err, response) {
					logging.consolelog(err, response, "%%%%%%%%% topic link %%%%%")
					if (err) {
						embeddedcb(null);
					} else {
						curriculumId.push({
							_id: response[0]._id,
							isUniversal: item.board == constants.COMMON_VARIABLES.UNIVERSAL_BOARD_NAME ? true : false
						});
						embeddedcb(null);
					}
				});
			}, function (err) {
				if (counter == request.body.curriculum.length) {
					cb();
				}
			});
		},
		topic_linking: ['curriculum_id', 'topic_id', function (cb) {
			//            logging.consolelog(linked, curriculumId, "third function");
			if (topicId.length > 0 && curriculumId.length > 0) {
				async.forEach(topicId, function (item, callback) {
					logging.consolelog(item, curriculumId, "");
					for (var i in curriculumId) {
						(function (curriculum) {
							var dataToSave = {
								curriculumId: curriculum["_id"],
								topicId: item,
								isUniversal: curriculum["isUniversal"],
								removed: false
							}
							logging.consolelog("set time out", curriculum, topicId);
							ServiceTopicCurriculumMapper.createCurriculumMapping(dataToSave, function (err, data) {

								if (!err) {
									callback(null);
								} else {
									callback(null);
								}

							})
						})(curriculumId[i])
					}

				}, function (err) {
					if (!err) {
						cb(null);
					} else {
						cb(null);
					}
				});
			} else {
				cb(null);
			}
        }]

	}, function (err, data) {

		if (!err) {

			response.status(200).send(constants.STATUS_MSG.SUCCESS.TOPIC_LINKED)
		} else {

			response.send(err);
		}
	})
}

var deleteContentOfTopic = function (request, response) {
	async.series([
        function (cb) {
			if (request.body.subCategoryId) {

				var query = {
					_id: request.body.subCategoryId,
					content: {
						$elemMatch: {
							_id: request.body.contentId
						}
					}
				}
				var update = {
					$set: {
						'content.$.isDeleted': true,
					}
				}
				ServiceTopicSubCategory.updateTopicSubCategory(query, update, {}, function (err, data) {
					if (!err) {
						cb(null);
					} else {
						cb(err);
					}
				});
			} else {
				var query = {
					_id: request.body.categoryId,
					content: {
						$elemMatch: {
							_id: request.body.contentId
						}
					}
				}
				var update = {
					$set: {
						'content.$.isDeleted': true,
					}
				}
				ServiceTopicCategory.updateTopicCategory(query, update, {}, function (err, data) {
					if (!err) {
						cb(null);
					} else {
						cb(err);
					}
				});
			}

        }

    ], function (err, data) {
		if (!err) {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.CONTENT_DELETED);
		} else {
			response.send(err);
		}
	});
}


var uploadAssessment = function (request, response) {
	logging.consolelog('>>>>>>>>>>>>>>>>>>>>>>>', request.body, request.files)
	var flag = false;
	var Id = null;
	var flag1 = false;
	var filename1 = null;
	async.series([
        function (cb) {
			logging.consolelog("zip::", "", "");


			if (request.body.typeOfContent == 'zip') {
				UploadManager.unzipUpload(request.files.assessmentZip, function (filename) {
					logging.consolelog(filename, "", "result of uploading a zip file");
					//				filename = filename.location;

					if (filename) {
						var zip = (request.files.assessmentZip.originalFilename).split(".")
						logging.consolelog(zip, "zip name", zip[0]);
						filename1 = Config.get('awsConfig.s3BucketCredentials.s3URL') + "/" + filename + "index.html";
						logging.consolelog("filename", "", filename1);
						cb(null);
					}
				});
			}
			if (request.body.typeOfContent == 'image') {
				UploadManager.uploadPicToS3Bucket(request.files.assessmentZip, "profile", function (filename) {
					if (filename != "No File") {

						filename1 = filename;
						filename1 = AwsConfig.s3BucketCredentials.s3URL + "/profile/" + filename;
						cb(null);
					}

				});
			}


        },

        function (cb) {
			logging.consolelog("enter1", "", request.body.topicId);
			ServiceAssesment.getAssessment({
				topicId: request.body.topicId
			}, {}, {
				lean: true
			}, function (err, data) {
				logging.consolelog("err,data", "", err, data);
				if (!err) {
					if (data.length > 0) {
						flag = true;
						cb(null);
					} else {
						flag = false;
						cb(null)
					}

				} else {
					cb(err);

				}
			});
        },
        function (cb) {
			logging.consolelog(flag, "", "flag value");
			var dataToSave = {};
			dataToSave.addedDate = new Date();
			dataToSave.assesmentUrl = filename1;
			dataToSave.isDeleted = false;
			dataToSave.name = request.body.name;
			dataToSave.language = request.body.language;
			dataToSave.price = request.body.price;
			dataToSave.isFree = request.body.isFree ? request.body.isFree : false;
			logging.consolelog("bye", "", "");
			if (flag) {
				logging.consolelog("third function::", "", flag)
				ServiceAssesment.updateAssessment({
					topicId: request.body.topicId
				}, {
					$push: {
						assesment: dataToSave
					}
				}, {}, function (err, data) {
					if (!err) {
						cb(null);
					} else {
						cb(err);
					}
				})

			} else {
				var dataToSave = {};
				dataToSave.topicId = request.body.topicId;

				logging.consolelog("logging", "", dataToSave);
				ServiceAssesment.createAssessment(dataToSave, function (err, data) {
					logging.consolelog("logging", err, data);
					if (!err) {
						Id = data._id;
						flag1 = true;
						cb(null);
					} else {
						cb(err);

					}
				})
			}
        },
        function (cb) {
			var dataToSave = {};
			dataToSave.addedDate = new Date();
			dataToSave.assesmentUrl = filename1;
			dataToSave.isDeleted = false;
			dataToSave.name = request.body.name;
			dataToSave.language = request.body.language;
			dataToSave.price = request.body.price;
			dataToSave.isFree = request.body.isFree ? request.body.isFree : false;
			if (flag1) {
				logging.consolelog("fourth function", flag, Id);
				ServiceAssesment.updateAssessment({
					_id: Id
				}, {
					$push: {
						assesment: dataToSave
					}
				}, {}, function (err, data) {
					logging.consolelog("logging", err, data);
					if (!err) {
						cb(null);
					} else {
						cb(err);
					}
				})
			} else {
				cb(null);
			}
        }
    ], function (err, data) {
		if (!err) {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.ZIP_UPLOADED);
		} else {
			response.send(err);
		}
	})
}


var getCategoryContent = function (request, response) {
	var contentLinking = [];
	async.series([
            function (cb) {
				ServiceTopicCategory.getTopicCategory({
					_id: request.body.categoryId,
					isDeleted: false
				}, {}, {}, function (err, data) {
					if (!err) {
						if (data.length > 0) {
							logging.consolelog("logging", err, data);
							var i = 0;
							var x = data[0].content.length;
							for (var i = 0; i < x; i++) {
								if (data[0].content[i].isDeleted == false) {
									contentLinking.push({

										content: data[0].content[i]


									})
								}

							}
						}

						cb(null);
					} else {
						cb(err);
					}
				})
            }
        ],
		function (err, data) {
			if (!err) {
				response.status(200).send({
					content: contentLinking,
					statusCode: 200
				})
			}
		})
}


var getSubCategoryContent = function (request, response) {
	var contentLinking = [];
	async.series([
            function (cb) {
				var query = {
					categoryId: request.body.categoryId
				}

				var path = 'subCategoryId';
				var select = 'name icon language content isDeleted'
				var populate = {
					path: path,
					match: {
						isDeleted: false
					},
					select: select,
					options: {
						lean: true
					}
				};
				var projection = {
					subCategoryId: 1
				}
				ServiceTopicBinding.topicBindingPopulate(query, projection, populate, {}, {}, function (err, data) {
					logging.consolelog("err,data::topic Binding::", err, data);
					if (!err) {
						var i = 0;
						var length = data.length;
						var topic = {};

						for (i = 0; i < length; i++) {
							if (data[i].subCategoryId != undefined) {
								if (data[i].subCategoryId.isDeleted == false) {
									contentLinking.push({
											subCategoryName: data[i].subCategoryId.name,
											subCategoryPrice: data[i].subCategoryId.price,
											subCategoryLanguage: data[i].subCategoryId.language,
											subCategoryIcon: data[i].subCategoryId.icon,
											subCategoryId: data[i].subCategoryId._id,
											subCategoryContent: []

										})
										// logging.consolelog(data.subCategoryId,"~~~~~~hi~~~~~~~");
									for (var j = 0; j < data[i].subCategoryId.content.length; j++) {
										if (data[i].subCategoryId.content[j].isDeleted == false) {
											var a = contentLinking.length - 1
											contentLinking[a].subCategoryContent.push(data[i].subCategoryId.content[j]);


										}
									}
								}


							}


						}
						cb(null);
					} else {
						cb(err);
					}


				});

            }
        ],
		function (err, data) {
			if (!err) {
				response.status(200).send({
					content: contentLinking,
					statusCode: 200
				})
			}
		})
}


var getContent = function (request, response) {
	var contentLinking = [];
	async.series([
            function (cb) {
				ServiceTopicSubCategory.getTopicSubCategory({
					_id: request.body.subCategoryId,
					isDeleted: false
				}, {}, {}, function (err, data) {
					if (!err) {
						logging.consolelog("logging", err, data);
						if (data.length > 0) {
							var i = 0;
							var x = data[0].content.length;
							for (var i = 0; i < x; i++) {
								if (data[0].content[i].isDeleted == false) {
									contentLinking.push({

										content: data[0].content[i]


									})
								}

							}
						}

						cb(null);
					} else {
						cb(err);
					}
				})
            }
        ],
		function (err, data) {
			if (!err) {
				response.status(200).send({
					content: contentLinking,
					statusCode: 200
				})
			} else {
				response.send(err);
			}
		})
}


var getAssessment = function (request, response) {
	var final = [];
	async.series([
        function (cb) {
			var query = {
				topicId: request.body.topicId
			}
			ServiceAssesment.getAssessment(query, {}, {}, function (err, data) {
				if (!err) {
					if (data.length > 0) {
						if (data[0].assesment) {
							for (var i = 0; i < data[0].assesment.length; i++) {
								if (data[0].assesment[i].isDeleted == false) {
									final.push({
										assessmentId: data[0].assesment[i]._id,
										addedDate: data[0].assesment[i].addedDate,
										assesmentUrl: data[0].assesment[i].assesmentUrl,
										name: data[0].assesment[i].name,
										price: data[0].assesment[i].price,
										language: data[0].assesment[i].language,
									})
								}
							}
						}
					}
					cb(null);
				} else {
					cb(err);
				}
			})


        }
    ], function (err, data) {
		if (!err) {
			response.status(200).send({
				final: final,
				statusCode: 200
			})
		} else {
			response.send(err);
		}
	})
}

var editContent = function (request, response) {
	//handle video editing
	var subtitlesfilename1 = null;
	logging.consolelog("editContent", "", request.body);
	var contentUrl = null;
	var subtitles="";
	async.series([
        function (cb) {
			//get content if content id
			if (request.body.newContentId) {
				ServiceTemporaryContent.getTemporaryContentStorage({
					'_id': request.body.newContentId
				}, {
					contentUrl: 1
				}, {
					lean: true
				}, function (err, cont) {
					if (err) {
						cb(err)
					} else {
						// logging.consolelog('content test>>>>>>>>',err,cont)
						if (cont && cont.length)
							contentUrl = cont[0].contentUrl;
						cb(null)
					}
				})
			} else {
				cb(null)
			}
		},
		function(cb){
			if(request.files){
				if(request.files.subtitle){
					console.log("====> subtitle")
					UploadManager.uploadPicToS3Bucket(request.files.subtitle, AwsConfig.s3BucketCredentials.folder.content, function (filename) {
						if (filename != "No File") {

							// filename1 = filename;
							subtitles = AwsConfig.s3BucketCredentials.s3URL + AwsConfig.s3BucketCredentials.folder.content + filename;
							cb(null);
						} else {
							cb(constants.STATUS_MSG.ERROR.UPLOAD_ERROR);
						}
					});

				}else{
					cb(null);
				}
			}else{
				cb(null);
			}

		},
        function (cb) {

			if (request.files) {

			
				if (request.files.icon) {

					UploadManager.uploadPicToS3Bucket(request.files.icon, "profile", function (filename) {
						if (filename != "No File") {

							filename1 = filename;
							filename1 = AwsConfig.s3BucketCredentials.s3URL + '/profile/' + filename;
							cb(null);
						} else {
							cb(constants.STATUS_MSG.ERROR.UPLOAD_ERROR);
						}
					});

				} else {
					cb(null);
				}
			} else {
				cb(null);
			}
        },
        function (cb) {

			if (request.body.categoryId) {
				var query = {
					content: {
						$elemMatch: {
							_id: request.body.contentId
						}
					},
					_id: request.body.categoryId

				}
				var update = {};
				logging.consolelog(request.body.title, request.body.price, request.body.description)
				if (request.files && request.files.icon) {
					update.icon = filename1;
				}

				if(request.files && request.files.subtitle){
					update.srtFile= subtitles;
				}
				if (contentUrl)
					update.contentUrl = contentUrl;
				if (request.body.typeOfVideo)
					update.typeOfVideo = request.body.typeOfVideo;
				if (request.body.title != undefined) {

					update.title = request.body.title;
				}
				if (request.body.price != undefined) {
					update.price = request.body.price;
				}

				if (request.body.description != undefined) {
					update.description = request.body.description;
				}
				if (request.body.ifFree != undefined) {
					update.ifFree = request.body.ifFree;
				}
				var updateQuery = {
					$set: {
						'content.$.title': update.title,
						'content.$.price': update.price,
						'content.$.description': update.description,
						// 'content.$.icon': update.icon,
						'content.$.ifFree': update.ifFree
					}

				}
				if (update.contentUrl)
					updateQuery['$set']['content.$.contentUrl'] = update.contentUrl;
				if (update.icon)
					updateQuery['$set']['content.$.icon'] = update.icon;
				if (update.typeOfVideo)
					updateQuery['$set']['content.$.typeOfVideo'] = update.typeOfVideo;
					if(update.srtFile)
					updateQuery['$set']['content.$.srtFile'] = update.srtFile;
				logging.consolelog('>>>>>>>>>>>>>>>>>>>>>>>>>>', query, updateQuery)
				ServiceTopicCategory.update(query, updateQuery, {
					upsert: false,
					safe: true
				}, function (err, data) {
					logging.consolelog("logging", err, data);
					if (!err) {
						cb(null);
					} else {
						cb(err);
					}
				})
			} else if (request.body.subCategoryId) {
				var query = {
					content: {
						$elemMatch: {
							_id: request.body.contentId
						}
					},
					_id: request.body.subCategoryId

				}
				var update = {};
				if (request.body.typeOfVideo)
					update.typeOfVideo = request.body.typeOfVideo;
				if (contentUrl)
					update.contentUrl = contentUrl;
				if (request.body.title != undefined) {
					update.title = request.body.title;
				}
				if (request.body.price != undefined) {
					update.price = request.body.price;
				}
				if (request.body.ifFree != undefined) {
					update.ifFree = request.body.ifFree;
				}
				if (request.files) {
					update.icon = filename1;
				}
				if (request.body.description != undefined) {
					update.description = request.body.description;
				}
				if(request.files && request.files.subtitle){
					update.srtFile= subtitles;
				}
				var updateQuery = {
					$set: {
						'content.$.title': update.title,
						'content.$.price': update.price,
						'content.$.description': update.description,
						// 'content.$.icon': update.icon,
						'content.$.ifFree': update.ifFree
					}
				}
				if (update.contentUrl)
					updateQuery['$set']['content.$.contentUrl'] = update.contentUrl;
				if (update.icon)
					updateQuery['$set']['content.$.icon'] = update.icon;
				if (update.typeOfVideo)
					updateQuery['$set']['content.$.typeOfVideo'] = update.typeOfVideo;
					if(update.srtFile)
					updateQuery['$set']['content.$.srtFile'] = update.srtFile;
				logging.consolelog('update query>>>>>>>>>>>>>>>>>', query, updateQuery)
				ServiceTopicSubCategory.update(query, updateQuery, {
					upsert: false,
					safe: true
				}, function (err, data) {
					if (!err) {
						cb(null);
					} else {
						cb(err);
					}
				})

			} else {
				cb(constants.STATUS_MSG.ERROR.INVALID_JOBID);
			}
        },
        function (cb) {
			//delete temp content
			if (contentUrl) {
				ServiceTemporaryContent.deleteTemporaryContentStorage({
					_id: request.body.newContentId
				}, function (err, result) {
					if (err) {
						logging.consolelog('deleted --- temp content', err, result)
						cb(null)
					} else {
						cb(null)
					}
				})
			} else {
				cb(null)
			}
        }
    ], function (err, result) {
		if (!err) {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.DEFAULT)
		} else {
			response.send(err);
		}
	})

}


var feedback = function (request, response) {
	var comments = [];
	var topicId;
	async.auto({
		get_topicId: function (cb) {

			var query = {
				name: request.body.topic,
				language: request.body.language,
				isDeleted: false
			}
			logging.consolelog("logging", request.body.topic, request.body.language)
			ServiceTopic.getTopic(query, {
				_id: 1
			}, {}, function (err, data) {
				logging.consolelog("logging", err, data)
				if (!err) {
					if (data.length > 0) {
						topicId = data[0]._id
					}
					cb(null);
				} else {
					cb(err);
				}
			})

		},

		get_comments: ['get_topicId', function (cb) {
			logging.consolelog("logging", "", topicId)
			var query = {
				topicId: topicId
			}
			var projection = {
				userType: 1,
				ratingStars: 1,
				comments: 1,
				customerId: 1,
				addedDate: 1,
				isActive: 1
			}

			var path = 'customerId';
			var select = 'emailId userType mobileNo profilePicUrl lastName firstName';
			var populate = {
				path: path,
				match: {},
				select: select,
				options: {
					lean: true
				}
			};
			ServiceFeedback.getFeedback(query, projection, {}, populate, function (err, data) {
				if (!err) {
					logging.consolelog("Commnts", "", JSON.stringify(data));
					if (data.length > 0) {
						for (var i in data) {
							var comment = {};
							comment.userType = data[i].userType;
							comment.feedbackId = data[i]._id,
								comment.isActive = data[i].isActive,
								comment.ratingStars = data[i].ratingStars;
							comment.comments = data[i].comments;
							comment.customerId = data[i].customerId;
							comment.mobileNo = data[i].mobileNo;
							comment.firstName = data[i].firstName;
							comment.lastName = data[i].lastName;
							comment.profilePicUrl = data[i].profilePicUrl;
							comment.emailId = data[i].emailId;
							comment.userType = data[i].userType;
							comment.addedDate = data[i].addedDate;
							comments.push(comment);
						}
						cb(null);
					} else {
						cb(null);
					}
				} else {
					cb(err);
				}
			});
        }]
	}, function (err, data) {
		if (!err) {
			response.status(200).send({
				comments: comments,
				statusCode: 200
			})
		} else {
			response.send(err);
		}
	})
}


var blockFeedback = function (request, response) {
	async.auto({
		block_feedback: function (cb) {

			var query = {
				_id: request.body.feedbackId
			}

			ServiceFeedback.updateFeedback(query, {
				$set: {
					isActive: false
				}
			}, {}, function (err, data) {

				if (!err) {

					cb(null);
				} else {
					cb(err);
				}
			})
		}


	}, function (err, data) {
		if (!err) {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.FEEDBACK_BLOCK);
		} else {
			response.send(err);
		}
	})
}

var unblockFeedback = function (request, response) {
	async.auto({

		block_feedback: function (cb) {

			var query = {
				_id: request.body.feedbackId
			}

			ServiceFeedback.updateFeedback(query, {
				$set: {
					isActive: true
				}
			}, {}, function (err, data) {

				if (!err) {

					cb(null);
				} else {
					cb(err);
				}
			})
		}


	}, function (err, data) {
		if (!err) {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.FEEDBACK_UNBLOCK);
		} else {
			response.send(err);
		}
	})
}


var editCategory = function (request, response) {
	var filename1 = null;
	async.auto({
		icon_update: function (cb) {
			if (request.files) {
				if (request.files.icon) {
					UploadManager.uploadPicToS3Bucket(request.files.icon, "profile", function (filename) {
						if (filename != "No File") {

							filename1 = filename;
							filename1 = AwsConfig.s3BucketCredentials.s3URL + '/profile/' + filename;
							cb(null);
						} else {
							cb(constants.STATUS_MSG.ERROR.UPLOAD_ERROR);
						}
					});

				}
			} else {
				cb(null);
			}
		},
		edit_category: ['icon_update', function (cb) {
			var update = {}
			if (request.body.name != undefined) {
				update.name = request.body.name;
			}
			if (request.body.price != undefined) {
				update.price = request.body.price;
			}
			if (request.files) {
				update.icon = filename1;
			}
			if (request.body.isFree != undefined) {
				update.isFree = request.body.isFree;
			}
			ServiceTopicCategory.update({
				_id: request.body.categoryId
			}, update, {}, function (err, data) {
				if (!err) {
					cb(null);
				} else {
					cb(err);
				}
			})
        }],
		notification: function (cb) {
			//			NotificationManager.sendNotifications(userId, 'FREE_TOPIC_ADVERTISEMENT', {});
			cb(null);
		}
	}, function (err, data) {
		if (!err) {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.EDIT_SUCCESSFULLY);
		} else {
			response.send(err);
		}

	})
}

var editSubCategory = function (request, response) {
	var filename1 = null;
	async.auto({
		icon_update: function (cb) {
			if (request.files) {
				if (request.files.icon) {
					UploadManager.uploadPicToS3Bucket(request.files.icon, "profile", function (filename) {
						if (filename != "No File") {

							filename1 = filename;
							filename1 = AwsConfig.s3BucketCredentials.s3URL + '/profile/' + filename;
							cb(null);
						} else {
							cb(constants.STATUS_MSG.ERROR.UPLOAD_ERROR);
						}
					});

				}
			} else {
				cb(null);
			}
		},
		edit_category: ['icon_update', function (cb) {
			var update = {}
			if (request.body.name != undefined) {
				update.name = request.body.name;
			}
			if (request.body.price != undefined) {
				update.price = request.body.price;
			}
			if (request.files) {
				update.icon = filename1;
			}
			ServiceTopicSubCategory.update({
				_id: request.body.subCategoryId
			}, update, {}, function (err, data) {
				if (!err) {
					cb(null);
				} else {
					cb(err);
				}
			})
        }]
	}, function (err, data) {
		if (!err) {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.EDIT_SUCCESSFULLY);
		} else {
			response.send(err);
		}
	})
}


var deleteCategory = function (request, response) {
	async.auto({
		delete_category: function (cb) {
			ServiceTopicCategory.update({
				_id: request.body.categoryId
			}, {
				$set: {
					isDeleted: true
				}
			}, {}, function (err, data) {
				if (!err) {
					cb(null);
				} else {
					cb(err);
				}
			})
		}
	}, function (err, data) {
		if (!err) {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.DEFAULT);
		} else {
			response.send(err);
		}
	})
}
var deleteSubCategory = function (request, response) {
	async.auto({
		delete_category: function (cb) {
			ServiceTopicSubCategory.update({
				_id: request.body.subCategoryId
			}, {
				$set: {
					isDeleted: true
				}
			}, {}, function (err, data) {

				console.log("====> delte sub category",err,JSON.stringify(data));
				if (!err) {
					cb(null);
				} else {
					cb(err);
				}
			})
		}
	}, function (err, data) {
		if (!err) {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.DEFAULT);
		} else {
			response.send(err);
		}
	})
}


var cloneTopic = function (request, response) {
	var topicId = null;
	var curriculum = [];
	var curriculumId = null;
	var topicDetails = null;
	var categoryId = null;
	async.auto({
		cloneTopic: function (cb) {
			var query = {
				name: request.body.topic
			}
			ServiceTopic.getTopic(query, {
				_id: 1
			}, {}, function (err, data) {
				if (!err) {
					for (var i = 0; i < data.length; i++) {

						if (data[i].language == request.body.language && data[i].isDeleted == false) {
							cb(constants.STATUS_MSG.ERROR.TOPIC_ALREADY_EXIST);
						} else {
							topicId = data[0]._id;
							topicDetails = data[0];
						}

					}
					cb(null);

				} else {
					cb(err);
				}
			});
		},
		get_universal_mapper: ['cloneTopic', function (cb) {
			var query = {
				topicId: topicId,
				isUniversal: true,

			}
			ServiceCurriculum.getCurriculumByTopic(query, {
				curriculumId: 1
			}, {}, function (err, data) {
				if (!err) {
					if (data.length > 0) {
						curriculum.push({
							board: data[0].board,
							language: request.body.language,
							chapter: data[0].chapter,
							subject: data[0].subject,
							grade: data[0].grade

						})
					}
					cb(null);
				} else {
					cb(err);
				}
			})
        }],
		get_curriculum_id: ['get_universal_mapper', function (cb) {
			var query = {
				board: curriculum[0].board,
				language: curriculum[0].language,
				chapter: curriculum[0].chapter,
				subject: curriculum[0].subject,
				grade: curriculum[0].grade,
				isRemoved: false
			}

			ServiceCurriculum.getCurriculum(query, {
				_id: 1
			}, {}, function (err, data) {
				if (!err) {
					if (data.length > 0) {
						curriculumId = data[0]._id;
						cb(null);
					} else {
						ServiceCurriculum.createCurriculum(query, function (er, da) {
							logging.consolelog(err, da, "###############")
							if (!er) {
								curriculumId = da.insertId;
								cb(null);
							} else {
								cb(er);
							}
						})
					}

				} else {
					cb(err);
				}
			})
        }],
		save_topic: ['get_curriculum_id', function (cb) {

			var dataToSave = {
				name: topicDetails.name,
				icon: topicDetails.icon,
				authorName: topicDetails.authorName,
				language: request.body.language,
				description: topicDetails.description,
				addedDate: Date.now(),
				isFree: false,
				price: topicDetails.price,
				averageRating: 0,
				gtinCode: topicDetails.gtinCode

			}

			ServiceTopic.createTopic(dataToSave, function (err, data) {
				if (!err) {
					topicId = data._id;
					cb(null);
				} else {
					cb(err);
				}
			})
        }],
		save_mapper: ['save_topic', function (cb) {
			var dataToSave = {
				curriculumId: curriculumId,
				topicId: topicId,
				isUniversal: true,
				removed: false
			}

			ServiceTopicCurriculumMapper.createCurriculumMapping(dataToSave, function (err, data) {

				if (!err) {
					cb(null);
				} else {
					cb(err);
				}
			});
        }],
		category: ['save_topic', function (cb) {

			var dataToSave = {
				name: "Video",
				icon: Config.get('awsConfig.s3BucketCredentials.s3URL') + "/profile/pixelsEd-qmnKF-video-icon.png",
				price: 0,
				isDeleted: false,
				language: request.body.language,
				isPublish: true,
				isFree: false
			}

			ServiceTopicCategory.createTopicCategory(dataToSave, function (err, data) {
				if (!err) {
					categoryId = data._id
					cb(null);
				} else {
					cb(err);
				}
			})
        }],
		topic_binding: ['category', function (cb) {
			var data = {
				topicId: topicId,
				categoryId: categoryId,
				categoryLanguage: request.body.language

			}
			ServiceTopicBinding.createTopicBinding(data, function (err, data) {
				if (!err) {
					cb(null);
				} else {
					cb(err);
				}
			})
        }]
	}, function (err, data) {
		if (!err) {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.CLONED_SUCCESSFULLY);
		} else {
			response.send(err);
		}
	})

}


var deleteLinking = function (request, response) {
	logging.consolelog("enter test", "", "");
	var curriculumId = [];
	async.auto({
		get_curriculumId: function (cb) {
			var query = {
				board: request.body.board,
				language: request.body.language,
				chapter: request.body.chapter,
				subject: request.body.subject,
				grade: request.body.grade,

			}

			ServiceCurriculum.getCurriculum(query, {}, {}, function (err, data) {
				if (!err) {
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							curriculumId.push(data[i]._id);
						}
					}
					cb(null);
				} else {
					cb(err);
				}
			})
		},
		delete_linking: ['get_curriculumId', function (cb) {
			var query = {
				topicId: request.body.topicId,
				curriculumId: {
					$in: curriculumId
				}
			}
			var update = {
				removed: true
			}
			logging.consolelog(query, update, "delete linking query and update~~~~~~~>");
			ServiceTopicCurriculumMapper.updateTopicCurriculumMapping(query, update, function (err, data) {
				logging.consolelog("update curriculum data", err, data);
				if (!err) {
					cb(null);
				} else {
					cb(err);
				}
			})
        }]
	}, function (err, data) {
		if (!err) {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.LINKING_DELETE);
		} else {
			response.send(err);
		}

	})
}


var deleteAssessment = function (request, response) {
	async.auto({
		delete_assessment: function (cb) {
			var query = {
				topicId: request.body.topicId,
				assesment: {
					$elemMatch: {
						_id: request.body.assessmentId
					}
				}
			}
			var update = {
				$set: {
					'assesment.$.isDeleted': true,
				}
			}

			ServiceAssesment.updateAssessment(query, update, {}, function (err, data) {
				if (!err) {
					cb(null);
				} else {
					cb(err);
				}
			});
		}
	}, function (err, data) {
		if (!err) {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.DEFAULT);
		} else {
			response.send(err);
		}
	});
}


var getDataBasedOnContentId = function (request, response) {
	var contentUrl = null;
	async.series([
        function (cb) {
			var query = {
				content: {
					$elemMatch: {
						_id: request.body.contentId
					}
				}
			}
			ServiceTopicCategory.getTopicCategory(query, {
				"content.$": 1
			}, {}, function (err, data) {
				if (!err) {
					if (data.length > 0) {
						if (data[0].content) {
							for (var i = 0; i < data[0].content.length; i++) {
								if (request.body.contentId.toString() == data[0].content[i]._id.toString()) {
									contentUrl = data[0].content[i].contentUrl;
								}

							}
						}

					}

					cb(null);
				} else {
					cb(err);
				}
			})

        }
    ], function (err, data) {
		if (!err) {
			response.status(200).send({
				contentUrl: contentUrl
			})
		} else {
			response.send(err);
		}
	})
}

function checkTopicForAssessment(topicId, assessmentType, language, callback) {
	var query = {
		_id: topicId,
		language: language
	};
	ServiceTopic.getTopic(query, {}, {}, function (err, data) {
		if (err) {
			callback(0)
		} else {
			if (data && data.length) {
				//check in mongo
				var mongoQuery = {
					assessmentType: assessmentType,
					topicId: topicId,
					language: language,
					isDeleted: false
				}
				ServiceAssesment.getAssessment(mongoQuery, {}, {}, function (err, data) {
					logging.consolelog('exist in mongo>>>>>>>', err, data)
					if (err) {
						callback(2)
					} else {
						if (data && data.length) {
							callback(1)
						} else {
							callback(2)
						}
					}
				})
			} else {
				callback(0)
			}
		}
	});
};

var addAssessment = function (request, response) {
	var questions = request.body.questions ? request.body.questions : "";
	var assessmentType = request.body.assessmentType ? request.body.assessmentType : "";
	var multipleChoices = request.body.multipleChoices ? request.body.multipleChoices : [];
	var answers = request.body.answers ? request.body.answers : "";
	var topicId = request.body.topicId ? request.body.topicId : "";
	var name = request.body.name ? request.body.name : "";
	var language = request.body.language ? request.body.language : "";
	var price = request.body.price ? request.body.price : 0;
	var isFree = request.body.isFree ? request.body.isFree : false

	async.auto({
		checkTopic: function (cb) {
			//mysql get topicId
			checkTopicForAssessment(topicId, assessmentType, language, function (exists) {
				logging.consolelog('exists>>>', "", exists)
				if (exists == 0) { //does not exist in mysql
					//throw error
					cb(constants.STATUS_MSG.ERROR.TOPIC_NOT_FOUND, exists)
				} else if (exists == 2) { //does not exist in mongo
					//create in mongo
					cb(null, exists)
				} else {
					//update assessment
					cb(null, exists)
				}
			})
		},
		createAssessment: ['checkTopic', function (cb, data) {
			logging.consolelog('data???', "", data)
			if (data.checkTopic == 2) {
				var dataToSave = {
					assessmentType: assessmentType,
					topicId: topicId,
					assessmentContent: [
						{
							questions: questions,
							multipleChoices: multipleChoices,
							answers: answers
                        }
                    ],
					language: language,
					isFree: isFree,
					price: price,
					name: name
				}
				ServiceAssesment.createAssessment(dataToSave, function (err, resp) {
					logging.consolelog('err in creating', "", err)
					if (err) {
						cb(err, null)
					} else {
						cb(null, null)
					}
				})
			} else {
				cb(null, null)
			}
        }],
		updateAssessment: ['checkTopic', function (cb, data) {
			logging.consolelog('data???', "", data)
			if (data.checkTopic == 1) {
				var criteria = {
					topicId: topicId,
					assessmentType: assessmentType
				};
				var dataToUpdate = {
					$push: {
						assessmentContent: {
							questions: questions,
							multipleChoices: multipleChoices,
							answers: answers
						}
					}
				};
				var options = {};
				ServiceAssesment.updateAssessment(criteria, dataToUpdate, options, function (err, resp) {
					if (err) {
						cb(err, null)
					} else {
						cb(null, null)
					}
				})
			} else {
				cb(null, null)
			}
        }]
	}, function (err, result) {
		if (err) {
			response.status(400).send(constants.STATUS_MSG.ERROR.TOPIC_NOT_FOUND)
		} else {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.DEFAULT)
		}
	})
};

var getNewAssessments = function (request, response) {
	var topicId = request.body.topicId ? request.body.topicId : "";
	var assessmentType = request.body.assessmentType ? request.body.assessmentType : "";
	var language = request.body.language ? request.body.language : ""
	var mongoQuery = {
		topicId: topicId,
		assessmentType: assessmentType,
		language: language,
		isDeleted: false
	}

	ServiceAssesment.getAssessment(mongoQuery, {}, {}, function (error, data) {
		logging.consolelog('exist in mongo>>>>>>>', error, data)
		if (error) {
			response.status(500).send(constants.STATUS_MSG.ERROR.DEFAULT)
		} else {
			if (data && data.length) {
				response.status(200).send(data)
			} else {
				response.status(404).send(constants.STATUS_MSG.ERROR.TOPIC_NOT_FOUND)
			}
		}
	})
}



var editUniversalCat = function (request, response) {

	ServiceTopicCategory.update({
		name: request.body.categoryName
	}, {
		$set: {
			name: request.body.updatedCategoryName
		}
	}, {
		multi: true
	}, (err, data) => {

		if (!err) {
			response.status(200).send(data)
		} else {
			response.status(404).send(constants.STATUS_MSG.ERROR.TOPIC_NOT_FOUND)
		}
	});

}

var editUniversalSubCat = function (request, response) {
	ServiceTopicSubCategory.update({
		name: request.body.subCatName
	}, {
		$set: {
			name: request.body.updatedSubCatName
		}
	}, {
		multi: true
	}, (err, data) => {

		if (!err) {
			response.status(200).send(data)
		} else {
			response.status(404).send(constants.STATUS_MSG.ERROR.TOPIC_NOT_FOUND)
		}
	});
}

module.exports = {

	////Topic Module///////
	addTopicManually: addTopicManually,
	addContentToTopic: addContentToTopic,
	uploadContent: uploadContent,
	readContentStatus: readContentStatus,
	addContentToTopicInCategory: addContentToTopicInCategory,
	getCategorySubCategory: getCategorySubCategory,
	getBoardForLanguage: getBoardForLanguage,
	editTopic: editTopic,
	topicDetails: topicDetails,
	deleteTopic: deleteTopic,
	addContentToTopicInSubCategory: addContentToTopicInSubCategory,
	topicLinkingWithCGCS: topicLinkingWithCGCS,
	deleteContentOfTopic: deleteContentOfTopic,
	uploadAssessment: uploadAssessment,
	getCategoryContent: getCategoryContent,
	getSubCategoryContent: getSubCategoryContent,
	getContent: getContent,
	getAssessment: getAssessment,
	editContent: editContent,
	feedback: feedback,
	blockFeedback: blockFeedback,
	unblockFeedback: unblockFeedback,
	editCategory: editCategory,
	editSubCategory: editSubCategory,
	deleteCategory: deleteCategory,
	deleteSubCategory: deleteSubCategory,
	cloneTopic: cloneTopic,
	deleteLinking: deleteLinking,
	deleteAssessment: deleteAssessment,
	getDataBasedOnContentId: getDataBasedOnContentId,
	addAssessment: addAssessment,
	getNewAssessments: getNewAssessments,
	editUniversalCat: editUniversalCat,
	editUniversalSubCat: editUniversalSubCat
}