var async = require('async');
var ServiceCurriculum = require('../../Service/curriculum');
var ServiceCustomer = require('../../Service/customer');
var ServiceTopic = require('../../Service/topic');
var ServiceTopicCategory = require('../../Service/topicCategory');
var ServiceChildMapper = require('../../Service/childParentMapper');
var ServiceContent = require('../../Service/manageContent');
var constants = require('../../Library/appConstants');
var ServiceTopicSubCategory = require('../../Service/topicSubCategory');
var fs = require('fs');
var Templates = require('../../Templates/superAdminRegister');
var UploadManager = require('../../Library/uploadManager');
var AwsConfig = Config.get('awsConfig');
var _ = require('underscore');
var moment = require('moment');
var ServiceCustomerBuyPackage = require('../../Service/customerBuyPackages');

var ServiceAnalysisData = require('../../Service/analysisData');


var uploadContent = function (request, response) {
    var filename1 = null;
    async.auto({
        upload_file: function (cb) {
            if (request.files) {
                UploadManager.uploadPicToS3Bucket(request.files.icon, "profile", function (filename) {

                    if (filename == "No File") {

                        cb(null);
                    } else {
                        filename1 = AwsConfig.s3BucketCredentials.s3URL + '/profile/' + filename;
                        cb(null);
                    }

                });
            } else {
                cb(null);
            }
        },
        upload_content: ['upload_file', function (cb) {

            var query = {
                userType: request.body.userType,
                contentType: request.body.contentType
            }
            var dataToSet = {
                $addToSet: {},
                $set: {}
            };
            if (request.files) {
                if (request.files.icon) {
                    dataToSet["$addToSet"]["contentURL"] = filename1;
                }

            }
            if (request.body.contentText) {
                dataToSet["$set"]["contentText"] = request.body.contentText;

            }
            if (request.body.userType) {
                dataToSet["$set"]["userType"] = request.body.userType;
            }
            if (request.body.contentType) {
                dataToSet["$set"]["contentType"] = request.body.contentType;
            }
            logging.consolelog("query", dataToSet, query);
            if (_.isEmpty(dataToSet["$addToSet"])) {
                delete dataToSet["$addToSet"]
            } else {

            }
            ServiceContent.updateContent(query, dataToSet, {
                upsert: true
            }, function (err, data) {
                logging.consolelog(err, data, "update");
                if (!err) {
                    cb(null);
                } else {
                    cb(err);
                }
            })
        }]
    }, function (err, data) {
        if (!err) {
            response.status(200).send(constants.STATUS_MSG.SUCCESS.DEFAULT);
        } else {
            response.send(err);
        }
    })
}


var dashboard = function (request, response) {
    var topicCount = 0;
    var curriculumCount = 0;
    var languageCount = 0;
    var contentCount = 0;
    var startDateTime = moment(new Date()).utcOffset(request.body.timeOffset).format('YYYY-MM-DD HH:mm:ss')
    var studentMonthlyArray = [
        {
            month: "jan",
            count: 0
        },
        {
            month: "feb",
            count: 0
        },
        {
            month: "mar",
            count: 0
        },
        {
            month: "apr",
            count: 0
        },
        {
            month: "may",
            count: 0
        },
        {
            month: "june",
            count: 0
        },
        {
            month: "july",
            count: 0
        },
        {
            month: "aug",
            count: 0
        },
        {
            month: "sep",
            count: 0
        },
        {
            month: "oct",
            count: 0
        },
        {
            month: "nov",
            count: 0
        },
        {
            month: "dec",
            count: 0
        }
    ];

    var teacherMonthlyArray = [
        {
            month: "jan",
            count: 0
        },
        {
            month: "feb",
            count: 0
        },
        {
            month: "mar",
            count: 0
        },
        {
            month: "apr",
            count: 0
        },
        {
            month: "may",
            count: 0
        },
        {
            month: "june",
            count: 0
        },
        {
            month: "july",
            count: 0
        },
        {
            month: "aug",
            count: 0
        },
        {
            month: "sep",
            count: 0
        },
        {
            month: "oct",
            count: 0
        },
        {
            month: "nov",
            count: 0
        },
        {
            month: "dec",
            count: 0
        }
    ]

    var agentMonthlyArray = [
        {
            month: "jan",
            count: 0
        },
        {
            month: "feb",
            count: 0
        },
        {
            month: "mar",
            count: 0
        },
        {
            month: "apr",
            count: 0
        },
        {
            month: "may",
            count: 0
        },
        {
            month: "june",
            count: 0
        },
        {
            month: "july",
            count: 0
        },
        {
            month: "aug",
            count: 0
        },
        {
            month: "sep",
            count: 0
        },
        {
            month: "oct",
            count: 0
        },
        {
            month: "nov",
            count: 0
        },
        {
            month: "dec",
            count: 0
        }
    ]
    var countryData = [];
    var completedBookings = 0;
    var cancelledBookings = 0;
    var revenue = 0;
    var purchasedTopics = 0;
    var purchasedPackage = 0;
    var purchasedCustomPackage = 0;
    var puchasedContent = 0;
    var engangedTime = {
        teacher: 0,
        student: 0,
        sub_agent: 0,
        school: 0
    }


    var customers = {
        student: 0,
        teacher: 0,
        sub_agent: 0,
        school: 0,
        agent: 0
    }
    var topicViews = {
        student: 0,
        teacher: 0,
        sub_agent: 0,
        school: 0,
        agent: 0
    }
    async.auto({

        topicCount: function (cb) {
            var query = {
                isDeleted: false
            }
            ServiceTopic.getUniqueTopics(query, 'name', function (err, data) {
                if (!err) {
                    if (data.length > 0) {
                        topicCount = data.length;
                    }
                    cb(null);
                } else {
                    cb(err);
                }

            });
        },
        curriculumCount:function (cb) {
            var query = {
                isRemoved: false
            }
            ServiceCurriculum.getUniqueCurriculum(query, 'board', function (err, data) {
                if (!err) {
                    if (data.length > 0) {
                        curriculumCount = data.length;
                    }
                    cb(null);
                } else {
                    cb(err);
                }
            });
        },
        languageCount:function (cb) {
            var query = {
                isRemoved: false
            }
            ServiceCurriculum.getUniqueCurriculum(query, 'language', function (err, data) {
                if (!err) {
                    if (data.length > 0) {
                        languageCount = data.length;
                    }
                    cb(null);
                } else {
                    cb(err);
                }
            });
        },
        categoryContent:  function (cb) {
            var query = {
                isDeleted: false,
                'content.isDeleted': false
            }
            ServiceTopicCategory.getTopicCategory(query, {
                content: 1
            }, {
                lean: true
            }, function (err, data) {
                if (!err) {
                    if (data.length > 0) {
                        for (var i = 0; i < data.length; i++) {
                            contentCount += data[i].content.length;
                        }

                    }
                    cb(null);
                } else {
                    cb(err);
                }
            });
        },
        subCategoryContent: function (cb) {
            var query = {
                isDeleted: false,
                'content.isDeleted': false
            }
            ServiceTopicSubCategory.getTopicSubCategory(query, {
                content: 1
            }, {
                lean: true
            }, function (err, data) {
                if (!err) {
                    if (data.length > 0) {
                        for (var i = 0; i < data.length; i++) {
                            contentCount += data[i].content.length;
                        }

                    }
                    cb(null);
                } else {
                    cb(err);
                }
            });
        },
        monthly_student_count:  function (cb) {
            var match = {
                userType: 'STUDENT'
            };
            var group = {
                _id: {
                    month: "$month",
                    year: "$year"
                },
                count: {
                    $sum: 1
                }
            };
            var project = {
                _id: 1,
                count: 1,
                month: {
                    $month: "$registrationDate"
                },
                year: {
                    $year: "$registrationDate"
                }
            };
            var sort = {
                registrationDate: 1
            };
            var currentYear = (new Date(startDateTime)).getFullYear();
            var matchMonthQuery = {
                "_id.year": currentYear
            };

            ServiceCustomer.getCustomerAggregateCount(match, group, sort, project, matchMonthQuery, function (err, response) {
                if (!err) {
                    var responseLen = response.length;
                    for (var i = 0; i < responseLen; i++) {
                        var index = response[i]._id.month - 1;
                        studentMonthlyArray[index].count = response[i].count;
                    }
                    cb(null);
                } else {
                    cb(err);

                }
            });


        },
        monthly_teacher_count: function (cb) {
            var match = {
                userType: 'TEACHER'
            };
            var group = {
                _id: {
                    month: "$month",
                    year: "$year"
                },
                count: {
                    $sum: 1
                }
            };
            var project = {
                _id: 1,
                count: 1,
                month: {
                    $month: "$registrationDate"
                },
                year: {
                    $year: "$registrationDate"
                }
            };
            var sort = {
                registrationDate: 1
            };
            var currentYear = (new Date(startDateTime)).getFullYear();
            var matchMonthQuery = {
                "_id.year": currentYear
            };

            ServiceCustomer.getCustomerAggregateCount(match, group, sort, project, matchMonthQuery, function (err, response) {
                if (!err) {
                    var responseLen = response.length;
                    for (var i = 0; i < responseLen; i++) {
                        var index = response[i]._id.month - 1;
                        teacherMonthlyArray[index].count = response[i].count;
                    }
                    cb(null);
                } else {
                    cb(err);

                }
            });

        },

        monthly_agent_count: function (cb) {
            var match = {
                userType: constants.USER_TYPE.AGENT
            };
            var group = {
                _id: {
                    month: "$month",
                    year: "$year"
                },
                count: {
                    $sum: 1
                }
            };
            var project = {
                _id: 1,
                count: 1,
                month: {
                    $month: "$registrationDate"
                },
                year: {
                    $year: "$registrationDate"
                }
            };
            var sort = {
                registrationDate: 1
            };
            var currentYear = (new Date(startDateTime)).getFullYear();
            var matchMonthQuery = {
                "_id.year": currentYear
            };

            ServiceCustomer.getCustomerAggregateCount(match, group, sort, project, matchMonthQuery, function (err, response) {
                if (!err) {
                    var responseLen = response.length;
                    for (var i = 0; i < responseLen; i++) {
                        var index = response[i]._id.month - 1;
                        agentMonthlyArray[index].count = response[i].count;
                    }
                    cb(null);
                } else {
                    cb(err);

                }
            });
        },
        country_wise_data: function (cb) {
            var group1 = {
                "$group": {
                    _id: {
                        "country": "$country",
                        "userType": "$userType"
                    },
                    "userCount": {
                        "$sum": 1
                    }
                }
            }

            var group2 = {
                "$group": {
                    "_id": "$_id.country",
                    "userTypes": {
                        "$push": {
                            "userType": "$_id.userType",
                            "count": "$userCount"
                        },
                    },
                    "count": {
                        "$sum": "$userCount"
                    }
                }
            }

            ServiceCustomer.countryAggregateData(group1, group2, function (err, data) {

                if (!err) {
                    for (var i = 0; i < data.length; i++) {

                        countryData.push(data[i]);

                    }


                    //					for(var key in data[0]){
                    //						for (var j=0;j<key.userTypes.length;j++){
                    //							if(key.userTypes[j].userType=='STUDENT'
                    //						}
                    //					}
                    logging.consolelog("-------->country aggregate data::", "", JSON.stringify(data));
                    cb(null);
                } else {
                    cb(err);
                }
            })


        },
        purchased_data: function (cb) {

            ServiceCustomerBuyPackage.getTransaction({}, {
                paymentStatus: 1,
                payments: 1,
                customPackage: 1,
                topic: 1,
                package: 1,
                content: 1
            }, {}, function (err, data) {
                if (!err) {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].paymentStatus == 'COMPLETED') {
                            completedBookings = completedBookings + 1;
                            revenue = revenue + data[i].payments.totalPrice;

                            purchasedTopics = purchasedTopics + data[i].topic.length;

                            purchasedPackage = purchasedPackage + data[i].package.length

                            purchasedCustomPackage = purchasedCustomPackage + data[i].customPackage.length
                            if (data[i].content) {
                                puchasedContent = puchasedContent + data[i].content.length;
                            }

                        }
                        if (data[i].paymentStatus == 'CANCELLED') {
                            cancelledBookings = cancelledBookings + 1;
                        }

                    }
                    cb(null);

                } else {
                    cb(err);
                }
            });
        },
        engangedTime: function (cb) {
            ServiceAnalysisData.groupByUserEngagedTime({}, {}, {}, function (err, data) {
                if(data.length){
                    for(var i = 0; i< data.length;i++){
                        if(data[i]._id == constants.USER_TYPE.STUDENT){
                            engangedTime.student = data[i].engagedTime
                            topicViews.student = data[i].count
                        }
                        if(data[i]._id == constants.USER_TYPE.TEACHER){
                            engangedTime.teacher = data[i].engagedTime
                            topicViews.teacher = data[i].count
                        }
                        if(data[i]._id == constants.USER_TYPE.SUB_AGENT){
                            engangedTime.sub_agent = data[i].engagedTime
                            topicViews.sub_agent = data[i].count
                        }
                        if(data[i]._id == constants.USER_TYPE.SCHOOL){
                            engangedTime.school = data[i].engagedTime
                            topicViews.school = data[i].count
                        }
                        if(data[i]._id == constants.USER_TYPE.AGENT){
                            engangedTime.agent = data[i].engagedTime
                            topicViews.agent = data[i].count
                        }
                    }
                    cb(null)
                }else{
                    cb(err)
                }
            })
        },
        usersCount: function (cb) {
            ServiceCustomer.customersCount({},{},{},function(err,data){
                if(!err && data.length){
                     for (var i = 0; i < data.length; i++){
                         if(data[i]._id == constants.USER_TYPE.STUDENT){
                                customers.student = data[i].count
                            }
                            if(data[i]._id == constants.USER_TYPE.TEACHER){
                                customers.teacher = data[i].count
                            }
                            if(data[i]._id == constants.USER_TYPE.SUB_AGENT){
                                customers.sub_agent = data[i].count
                            }
                            if(data[i]._id == constants.USER_TYPE.SCHOOL){
                                customers.school = data[i].count
                            }
                            if(data[i]._id == constants.USER_TYPE.AGENT){
                                customers.agent = data[i].count
                            }
                     }
                    cb(null)
                }else{
                    cb(err)
                }
            })
        },
    }, function (err, data) {
        if (!err) {
            response.status(200).send({
                statusCode: 200,
                countryData: countryData,
                agentMonthlyArray: agentMonthlyArray,
                studentMonthlyArray: studentMonthlyArray,
                teacherMonthlyArray: teacherMonthlyArray,
                contentCount: contentCount,
                languageCount: languageCount,
                curriculumCount: curriculumCount,
                topicCount: topicCount,
                completedBookings: completedBookings,
                cancelledBookings: cancelledBookings,
                revenue: revenue,
                puchasedContent: puchasedContent,
                purchasedCustomPackage: purchasedCustomPackage,
                purchasedPackage: purchasedPackage,
                purchasedTopics: purchasedTopics,
                engangedTime: engangedTime,
                customers: customers,
                topicViews: topicViews

            })
        } else {

            response.send(err);
        }
    })

}


var activeUsersData = function (request, response) {
    var resultData = [];
    var agentMonthlyArray = [
        {
            month: "jan",
            count: 0
        },
        {
            month: "feb",
            count: 0
        },
        {
            month: "mar",
            count: 0
        },
        {
            month: "apr",
            count: 0
        },
        {
            month: "may",
            count: 0
        },
        {
            month: "june",
            count: 0
        },
        {
            month: "july",
            count: 0
        },
        {
            month: "aug",
            count: 0
        },
        {
            month: "sep",
            count: 0
        },
        {
            month: "oct",
            count: 0
        },
        {
            month: "nov",
            count: 0
        },
        {
            month: "dec",
            count: 0
        }
    ]
    var teacherMonthlyArray = [
        {
            month: "jan",
            count: 0
        },
        {
            month: "feb",
            count: 0
        },
        {
            month: "mar",
            count: 0
        },
        {
            month: "apr",
            count: 0
        },
        {
            month: "may",
            count: 0
        },
        {
            month: "june",
            count: 0
        },
        {
            month: "july",
            count: 0
        },
        {
            month: "aug",
            count: 0
        },
        {
            month: "sep",
            count: 0
        },
        {
            month: "oct",
            count: 0
        },
        {
            month: "nov",
            count: 0
        },
        {
            month: "dec",
            count: 0
        }
    ]
    var studentMonthlyArray = [
        {
            month: "jan",
            count: 0
        },
        {
            month: "feb",
            count: 0
        },
        {
            month: "mar",
            count: 0
        },
        {
            month: "apr",
            count: 0
        },
        {
            month: "may",
            count: 0
        },
        {
            month: "june",
            count: 0
        },
        {
            month: "july",
            count: 0
        },
        {
            month: "aug",
            count: 0
        },
        {
            month: "sep",
            count: 0
        },
        {
            month: "oct",
            count: 0
        },
        {
            month: "nov",
            count: 0
        },
        {
            month: "dec",
            count: 0
        }
    ]
    var agentYearlyData = [];
    var teacherYearlyData = [];
    var studentYearlyData = [];
    var studentTodayCount = 0;
    var agentTodayCount = 0;
    var teacherTodayCount = 0;
    var startDateTime = moment(new Date()).utcOffset(request.body.timeOffset).format('YYYY-MM-DD HH:mm:ss');
    var endDateTime = new Date(startDateTime);
    endDateTime.setHours(23);
    endDateTime.setMinutes(59);
    endDateTime.setSeconds(59);
    startDateTime = new Date(startDateTime);
    startDateTime.setHours(0);
    startDateTime.setMinutes(0);
    startDateTime.setSeconds(0);
    startDateTime = startDateTime;
    startDateTime = moment(new Date(startDateTime)).utcOffset(-request.body.timeOffset).format('YYYY-MM-DD HH:mm:ss');
    startDateTime = new Date(startDateTime).toISOString();
    endDateTime = moment(new Date(endDateTime)).utcOffset(-request.body.timeOffset).format('YYYY-MM-DD HH:mm:ss');
    endDateTime = new Date(endDateTime).toISOString();
    async.auto({
        agent_monthlyAndYearly_count: function (cb) {

            if (request.body.type == "MONTHLY" || request.body.type == "YEARLY") {
                var match = {
                    userType: constants.USER_TYPE.AGENT
                };
                var group = {
                    _id: {},
                    count: {
                        $sum: 1
                    }
                };

                if (request.body.type == 'MONTHLY') {
                    group["_id"]["month"] = "$month";
                    group["_id"]["year"] = "$year";
                }
                if (request.body.type == 'YEARLY') {
                    group["_id"]["year"] = "$year";
                }


                var project = {
                    _id: 1,
                    count: 1,
                    month: {
                        $month: "$loginTime"
                    },
                    year: {
                        $year: "$loginTime"
                    }
                };
                var sort = {
                    loginTime: 1
                };
                var currentYear = (new Date()).getFullYear();

                if (request.body.type == 'MONTHLY') {
                    var matchMonthQuery = {
                        "_id.year": currentYear
                    };
                }
                if (request.body.type == "YEARLY") {
                    var matchMonthQuery = {};
                }

                ServiceCustomer.getCustomerAggregateCount(match, group, sort, project, matchMonthQuery, function (err, response) {
                    if (!err) {
                        var responseLen = response.length;
                        if (request.body.type == 'MONTHLY') {
                            for (var i = 0; i < responseLen; i++) {
                                var index = response[i]._id.month - 1;
                                agentMonthlyArray[index].count = response[i].count;
                            }
                        } else {
                            agentYearlyData = response;
                        }

                        cb(null);
                    } else {
                        cb(err);

                    }
                });
            } else {
                cb(null);
            }

        },
        teacher_monthlyAndYearly_count: function (cb) {
            if (request.body.type == "MONTHLY" || request.body.type == "YEARLY") {
                var match = {
                    userType: 'TEACHER'
                };
                var group = {
                    _id: {},
                    count: {
                        $sum: 1
                    }
                };

                if (request.body.type == 'MONTHLY') {
                    group["_id"]["month"] = "$month";
                    group["_id"]["year"] = "$year";
                }
                if (request.body.type == 'YEARLY') {
                    group["_id"]["year"] = "$year";
                }


                var project = {
                    _id: 1,
                    count: 1,
                    month: {
                        $month: "$loginTime"
                    },
                    year: {
                        $year: "$loginTime"
                    }
                };
                var sort = {
                    loginTime: 1
                };
                var currentYear = (new Date()).getFullYear();

                if (request.body.type == 'MONTHLY') {
                    var matchMonthQuery = {
                        "_id.year": currentYear
                    };
                }
                if (request.body.type == "YEARLY") {
                    var matchMonthQuery = {};
                }

                ServiceCustomer.getCustomerAggregateCount(match, group, sort, project, matchMonthQuery, function (err, response) {
                    if (!err) {
                        var responseLen = response.length;
                        if (request.body.type == 'MONTHLY') {
                            for (var i = 0; i < responseLen; i++) {
                                var index = response[i]._id.month - 1;
                                teacherMonthlyArray[index].count = response[i].count;
                            }
                        } else {
                            teacherYearlyData = response;
                        }

                        cb(null);
                    } else {
                        cb(err);

                    }
                });
            } else {
                cb(null);
            }
        },
        student_monthlyAndYearly_count: function (cb) {
            if (request.body.type == "MONTHLY" || request.body.type == "YEARLY") {
                var match = {
                    userType: 'STUDENT'
                };
                var group = {
                    _id: {},
                    count: {
                        $sum: 1
                    }
                };

                if (request.body.type == 'MONTHLY') {
                    group["_id"]["month"] = "$month";
                    group["_id"]["year"] = "$year";
                }
                if (request.body.type == 'YEARLY') {
                    group["_id"]["year"] = "$year";
                }


                var project = {
                    _id: 1,
                    count: 1,
                    month: {
                        $month: "$loginTime"
                    },
                    year: {
                        $year: "$loginTime"
                    }
                };
                var sort = {
                    loginTime: 1
                };
                var currentYear = (new Date()).getFullYear();

                if (request.body.type == 'MONTHLY') {
                    var matchMonthQuery = {
                        "_id.year": currentYear
                    };
                }
                if (request.body.type == "YEARLY") {
                    var matchMonthQuery = {};
                }

                ServiceCustomer.getCustomerAggregateCount(match, group, sort, project, matchMonthQuery, function (err, response) {
                    if (!err) {
                        var responseLen = response.length;
                        if (request.body.type == 'MONTHLY') {
                            for (var i = 0; i < responseLen; i++) {
                                var index = response[i]._id.month - 1;
                                studentMonthlyArray[index].count = response[i].count;
                            }
                        } else {
                            studentYearlyData = response;
                        }

                        cb(null);
                    } else {
                        cb(err);

                    }
                });
            } else {
                cb(null);
            }
        },
        agent_today_data: function (cb) {
            if (request.body.type == "TODAY") {
                var query = {
                    loginTime: {
                        $gte: startDateTime,
                        $lte: endDateTime
                    },
                    userType: constants.USER_TYPE.AGENT
                };
                ServiceCustomer.getCustomerCount(query, function (err, count) {
                    if (err) {
                        cb(err)
                    } else {
                        agentTodayCount = count;
                        cb(null);
                    }
                });
            } else {
                cb(null);
            }
        },
        student_today_data:function (cb) {
            if (request.body.type == "TODAY") {
                var query = {
                    loginTime: {
                        $gte: startDateTime,
                        $lte: endDateTime
                    },
                    userType: 'STUDENT'
                };
                ServiceCustomer.getCustomerCount(query, function (err, count) {
                    if (err) {
                        cb(err)
                    } else {
                        studentTodayCount = count;
                        cb(null);
                    }
                });
            } else {
                cb(null);
            }
        },
        teacher_today_data: function (cb) {
            if (request.body.type == "TODAY") {
                var query = {
                    loginTime: {
                        $gte: startDateTime,
                        $lte: endDateTime
                    },
                    userType: 'TEACHER'
                };
                ServiceCustomer.getCustomerCount(query, function (err, count) {
                    if (err) {
                        cb(err)
                    } else {
                        teacherTodayCount = count;
                        cb(null);
                    }
                });
            } else {
                cb(null);
            }
        }
    }, function (err, data) {
        if (!err) {
            response.status(200).send({
                statusCode: 200,
                teacherTodayCount: teacherTodayCount,
                studentTodayCount: studentTodayCount,
                agentTodayCount: agentTodayCount,
                studentYearlyData: studentYearlyData,
                agentYearlyData: agentYearlyData,
                teacherYearlyData: teacherYearlyData,
                agentMonthlyArray: agentMonthlyArray,
                teacherMonthlyArray: teacherMonthlyArray,
                studentMonthlyArray: studentMonthlyArray

            })
        } else {

            response.send(err);
        }
    })

}


function findChildOfParent(customerId, temp, callback) {
    logging.consolelog("hello, findChildOfParent", "", customerId);
    var childCustomers = []
    var query = {
        parentCustomerId: {
            $in: customerId
        },
        $or: [{
            childCustomerType: 'SUB_AGENT'
        }, {
            childCustomerType: 'SCHOOL'
        }],
    }
    ServiceChildMapper.getChildParentMapper(query, {
        childCustomerId: 1
    }, {
        lean: true
    }, function (err, data) {
        if (!err) {
            logging.consolelog("hello", data, temp);
            if (data.length > 0) {

                for (var i = 0; i < data.length; i++) {
                    temp.push({
                        parent: data[i].parentCustomerId,
                        child: data[i].childCustomerId
                    });
                    childCustomers.push(data[i].childCustomerId);
                }
                findChildOfParent(childCustomers, temp, callback);
            } else {
                callback(null, temp);
            }

        } else {
            callback(err);
        }
    });

}


var paymentReconcillation = function (request, response) {
    var final = {
        customers: [],
        agent: []
    }
    async.series([
        function (cb) {

            var query = {
                paymentStatus: 'COMPLETED'
            }
            var path = 'customerId';
            var select = 'firstName lastName emailId mobileNo addressValue country registrationDate'
            var populate = {
                path: path,
                match: {},
                select: select,
                options: {
                    lean: true
                }
            };

            if (request.body.userType) {
                populate["match"]["userType"] = {
                    $in: request.body.userType
                };
            }
            if (request.body.search) {
                populate['match']['$or'] = [
                    {
                        "emailId": {
                            "$regex": '.*' + request.body.search + '.*',
                            "$options": "i"
                        }
                    },
                    {
                        "firstName": {
                            "$regex": '.*' + request.body.search + '.*',
                            "$options": "i"
                        }
                    },
                    {
                        "lastName": {
                            "$regex": '.*' + request.body.search + '.*',
                            "$options": "i"
                        }
                    },
                    {
                        "mobileNo": {
                            "$regex": '.*' + request.body.search + '.*',
                            "$options": "i"
                        }
                    },
                    {
                        "country": {
                            "$regex": '.*' + request.body.search + '.*',
                            "$options": "i"
                        }

                    }
                ]

            }
            if (request.body.toDate && request.body.fromDate) {
                query.bookingDate = {
                    $gte: request.body.toDate,
                    $lte: request.body.fromDate
                }

            }
            var options = {};
            var projection = {
                customerId: 1,
                transactionId: 1,
                buyDate: 1,
                packageName: 1,
                bookingStatus: 1
            }
            logging.consolelog(query, projection, populate)
            ServiceCustomerBuyPackage.getPackages(query, projection, populate, options, function (err, data) {
                logging.consolelog('err', err, "")
                // logging.consolelog("logging",err, JSON.stringify(data));
                if (!err) {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].customerId != null) {
                            if (data[i].customerId.userType == 'STUDENT' || data[i].customerId.userType == 'TEACHER') {
                                final.customers.push({
                                    bookingId: data[i]._id,
                                    customerId: data[i].customerId._id,
                                    name: data[i].customerId.firstName + ' ' + data[i].customerId.lastName,
                                    country: data[i].customerId.country,
                                    userType: data[i].customerId.userType,
                                    registrationDate: data[i].customerId.registrationDate,
                                    emailId: data[i].customerId.emailId,
                                    mobileNo: data[i].customerId.mobileNo,
                                    location: data[i].customerId.addressValue,
                                    block: data[i].customerId.block,
                                    transactionId: data[i].transactionId,
                                    packageName: data[i].packageName,
                                    bookingDate: data[i].buyDate,
                                    bookingStatus: data[i].bookingStatus
                                })
                            } else {
                                final.customers.push({
                                    bookingId: data[i]._id,
                                    customerId: data[i].customerId._id,
                                    name: data[i].customerId.firstName + ' ' + data[i].customerId.lastName,
                                    country: data[i].customerId.country,
                                    userType: data[i].customerId.userType,
                                    registrationDate: data[i].customerId.registrationDate,
                                    emailId: data[i].customerId.emailId,
                                    mobileNo: data[i].customerId.mobileNo,
                                    location: data[i].customerId.addressValue,
                                    block: data[i].customerId.block,
                                    transactionId: data[i].transactionId,
                                    packageName: data[i].packageName,
                                    bookingDate: data[i].buyDate,
                                    bookingStatus: data[i].bookingStatus
                                });
                            }

                        }


                    }
                    cb(null)
                } else {
                    cb(err);
                }
            })
        }
    ], function (err, data) {
        if (!err) {
            response.status(200).send({
                final: final,
                statusCode: 200
            })
        } else {
            response.send(err);
        }
    })
}
var testZipFile = function (req,res) {
    // body...
    console.log('req',req.files)
    UploadManager.unzipUpload(req.files.content,function (data) {
        // body...
        console.log('data>>>',data)
        res.send(data)
    })

}
module.exports = {
    uploadContent: uploadContent,
    dashboard: dashboard,
    activeUsersData: activeUsersData,
    paymentReconcillation: paymentReconcillation,
    testZipFile:testZipFile

}