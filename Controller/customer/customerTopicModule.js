var https = require('https');
var async = require('async');
var ServiceSuperAdmin = require('../../Service/superAdmin');
var constants = require('../../Library/appConstants');
var NotificationManager = require('../../Library/notificationManager');
var ccav = require('../../Library/ccavutil.js');
var ServiceCustomer = require("../../Service/customer");
var ServiceTopic = require('../../Service/topic');
var ServiceFeedback = require('../../Service/feedback');
var ServiceTopicBinding = require('../../Service/topicBinding');
var ServiceTopicCategory = require('../../Service/topicCategory');
var ServiceTopicSubCategory = require('../../Service/topicSubCategory');
var ServiceAssesment = require('../../Service/assessment');
var ServiceTopicCurriculumMapper = require('../../Service/topicCurriculumMapping');
var ServiceCustomerCart = require('../../Service/customerCart');
var ServiceFavourites = require('../../Service/favouriteTopic');
var ServiceNotes = require('../../Service/topicNotes');
var ServicePackage = require('../../Service/package');
var ServiceCustomerBuyPackages = require('../../Service/customerBuyPackages');
var ServiceCurriculum = require('../../Service/curriculum');
var qs = require('querystring');
var ServiceChildMapper = require('../../Service/childParentMapper');
var _ = require('underscore');
var ServiceAssignDataToCustomer = require('../../Service/assignDataToCustomer');
var ServiceAnalysisData = require('../../Service/analysisData');
var ccAvenueConfig = Config.get('ccAvenueConfig');
var Templates = require('../../Templates/invoice');
var universalTopic = function (item, request, callback) {
	var query = {
		topic: {
			$elemMatch: {
				"isDeleted": false,
				"topic": item.Id
			}
		},
		'isRemoved': false
	}
	query.board = constants.COMMON_VARIABLES.UNIVERSAL_BOARD_NAME;

	if (request.body.board) {
		query.board = request.body.board
	}
	if (request.body.language) {
		query.language = request.body.language
	}
	if (request.body.grade) {
		query.grade = request.body.grade
	}
	if (request.body.subject) {
		query.subject = request.body.subject
	}
	if (request.body.chapter) {
		query.chapter = request.body.chapter
	}
	ServiceCurriculum.getCurriculum(query, {
		board: 1,
		subject: 1,
		language: 1,
		grade: 1,
		chapter: 1
	}, {}, function (err, data) {
		if (data.length > 0) {
			callback(null, {

				board: data[0].board,
				subject: data[0].subject,
				language: data[0].language,
				grade: data[0].grade,
				chapter: data[0].chapter
			})
		} else {
			callback(null);
		}
	});


}



var topicData = function (data,categorylist,contentlist, callbackRoute) {
	var mergedObject = {};
	logging.consolelog("topic DATA", "", data);
	async.forEach(data, function (item, callback) {
		categoryList(item, categorylist,contentlist, true,function (err, result) {
			if (!err) {
				if ((result && result.contentName.length) ||(result && result.categoryName) ) {
					console.log("=====>catgory list",categorylist, result.categoryName ,"======>content list", contentlist, result.contentName ,result.contentName.indexOf(contentlist),result.categoryName.indexOf(categorylist));
					if((contentlist &&contentlist.length>0)|| (categorylist&&categorylist.length>0)){
						if(((_.intersection(result.contentName,contentlist)).length>0)|| (_.intersection(result.categoryName,categorylist).length>0)){
							mergedObject[item] = {
								contentType: result.contentName ? result.contentName: [],
								topicId: item,
								language: result.language,
								categoryName: result.categoryName ? result.categoryName : []
							}
						}
					}else{
						mergedObject[item] = {
							contentType: result.contentName ? result.contentName: [],
							topicId: item,
							language: result.language,
							categoryName: result.categoryName ? result.categoryName : []
						}
					}
				
					
				}
				callback(null);
			} else {
				callback(err);
			}
		});
	}, function (err) {
		if (!err) {
			callbackRoute(null, {
				mergedObject: mergedObject
			});
		} else {

			callbackRoute(err);
		}
	});
}

var getCustomerCart = function (customerId, cb) {
	var topicCart = [];
	var query = {
		'customerId': customerId
	};
	ServiceCustomerCart.getCart(query, {
		topicCart: 1
	}, {
		lean: true
	}, function (err, data) {
		logging.consolelog("logging", "", data);
		if (err) {
			cb(err);
		} else {
			if (data && data.length && data[0].topicCart) {
				for (var i = 0; i < data[0].topicCart.length; i++) {
					topicCart.push(data[0].topicCart[i]);
				}
			}
			cb(null, topicCart);
		}
	});
}


var categoryList = function (item, categoryList, contentList, categoryCheck, callback) {
	logging.consolelog("verify categories","","")
	var counter = 0;
	var categoryName = [];
	var contentName = [];
	async.forEach(item, function (item, embeddedcb) {
		counter++

		var query = {
			topicId: item
		};
		var path = 'categoryId';
		var select = 'name content isDeleted'
		var populate = {
			path: path,
			match: {},
			select: select,
			options: {
				lean: true
			}
		};
		var options = {

		};
		var projection = {
			categoryId: 1
		}

		ServiceTopicBinding.topicBindingPopulate(query, projection, populate, {}, {}, function (err, data) {
			logging.consolelog(err, data, "category Dtaa");
			if (!err) {
				for (var i = 0; i < data.length; i++) {
					if (data[i].categoryId.name == 'Video' && data[i].categoryId.isDeleted == false) {
						for (var j = 0; j < data[i].categoryId.content.length; j++) {
							if (data[i].categoryId.content[j].isDeleted == false && data[i].categoryId.content[j].isVideo == true) {
								if (categoryCheck == true) {
									// if (contentList.indexOf(data[i].categoryId.content[j].typeOfVideo) != -1) {
									// 	contentName.push(data[i].categoryId.content[j].typeOfVideo);
									// }
									contentName.push(data[i].categoryId.content[j].typeOfVideo);
								} else {
									contentName.push(data[i].categoryId.content[j].typeOfVideo);
								}

							}
						}
					} else if (data[i].categoryId.isDeleted == false) {
						if (categoryCheck == true) {
							// if (categoryList.indexOf(data[i].categoryId.name) != -1) {
							// 	categoryName.push(data[i].categoryId.name);
							// }

							categoryName.push(data[i].categoryId.name);
						} else {
							categoryName.push(data[i].categoryId.name);
						}

					}

				}
				contentName = _.unique(contentName);
				categoryName = _.unique(categoryName);
				embeddedcb(null);
			} else {
				embeddedcb(err);
			}
		});

	}, function (err) {
		if (counter == item.length) {

			callback(null, {
				contentName: contentName,
				categoryName: categoryName
			});
		}

	});
}


// var filterTopic= function(item,categorylist,contentList,callback){
// 	logging.consolelog("verify categories","","")
// 	var counter = 0;
// 	var categoryName = [];
// 	var contentName = [];
// 	var pass= false;


// 		var query = {
// 			topicId: item.topicId
// 		};
// 		var path = 'categoryId';
// 		var select = 'name content isDeleted'
// 		var populate = {
// 			path: path,
// 			match: {},
// 			select: select,
// 			options: {
// 				lean: true
// 			}
// 		};
// 		var options = {

// 		};
// 		var projection = {
// 			categoryId: 1
// 		}

// 		ServiceTopicBinding.topicBindingPopulate(query, projection, populate, {}, {}, function (err, data) {
// 			logging.consolelog(err, data, "category Dtaa");
// 			if (!err) {
// 				for (var i = 0; i < data.length; i++) {
// 					if (data[i].categoryId.name == 'Video' && data[i].categoryId.isDeleted == false) {
// 						for (var j = 0; j < data[i].categoryId.content.length; j++) {
// 							if (data[i].categoryId.content[j].isDeleted == false && data[i].categoryId.content[j].isVideo == true) {
								
//                                  if(data[i].categoryId.content[j]["typeOfVideo"].indexOf(contentlist)){
// 									pass= true;
// 								 }
// 							}
// 						}
// 					} else if (data[i].categoryId.isDeleted == false) {
// 						if(data[i].categoryId.name.indexOf(categorylist)){
// 							pass= true;
// 						}

// 					}

// 				}
			
// 				callback(null);
// 			} else {
// 				callback(err);
// 			}
// 		});

// 	}



var topicLibrary = function (request, response) {
	logging.consolelog("Fetching topic library!", "", "");
	var topics = [];
	var final = {
		data: {}
	};
	var search = "";
	var topicHistory = [];
	var topicIds = [];
	var purchasedTopics = [];
	var topicListObj = {};
	//var topic = [];
	var offsetCount = 0;
	var limitCount = 10;
	var userType = request.userData.userType;
	var cartTopics = [];
	async.series([
            function (cb) {
				//check user type:: if not agent find favorite topics

				if (userType != constants.USER_TYPE.AGENT) {
					logging.consolelog("----->customer should enter", "", userType != constants.USER_TYPE.AGENT);
					try {
						getFavourite(request.userData.id, function (data, history) {
							topicIds = topicIds.concat(data);
							if (request.params.type == "history") {
								topicHistory = topicHistory.concat(history);
							}
							logging.consolelog("logging", "", topicIds);
							cb(null);
						})
					} catch (e) {
						logging.consolelog("---->Some error occured while fetching fav for topicLibrary!", "", e);
						cb(null);
					}
				} else {
					cb(null);
				}
            },
            function (cb) {
				if (userType != constants.USER_TYPE.AGENT) {
					// logging.consolelog("----->customer should enter", "", userType != constants.USER_TYPE.AGENT);
					try {
						getCustomerCart(request.userData.id, function (data, cartData) {
							cartTopics = cartTopics.concat(cartData);
							// logging.consolelog("logging", "", topicIds);
							cb(null);
						})
					} catch (e) {
						// logging.consolelog("---->Some error occured while fetching fav for topicLibrary!", "", e);
						cb(null);
					}
				} else {
					cb(null);
				}

            },
            function (cb) {
				//check user type:: if not agent find purchased topics
				if (userType != constants.USER_TYPE.AGENT) {
					logging.consolelog("------->customer should enter", "", "");
					getCustomerPurchases(request.userData.id, function (data) {
						console.log(JSON.stringify(data), "purchased Data")
						for (var i = 0; i < data.length; i++) {
							purchasedTopics.push(parseInt(data[i]));
						}
						cb(null);
					})
				} else {
					cb(null);
				}
            },
            function (cb) {
				//check user type:: get all topics
				if (userType != constants.USER_TYPE.AGENT) {
					logging.consolelog("------->customer should enter", "", "");
					var query = {
						'board': constants.COMMON_VARIABLES.UNIVERSAL_BOARD_NAME,
						'isDeleted': false,
						// 'isPublish': true
					}
					var sort = {
						sortBy: "sequenceOrder"
					};
					prepareFilter('board', request.body.board, query);
					prepareFilter('curriculum.language', request.body.language, query);
					prepareFilter('grade', request.body.grade, query);
					prepareFilter('subject', request.body.subject, query);
					prepareFilter('chapter', request.body.chapter, query);

					if (request.body.offset) {
						offsetCount = request.body.offset;
					}
					if (request.body.limit) {
						limitCount = request.body.limit;
					}
					if (request.params.type) {
						if (request.params.type == "new") {
							sort.sortBy = "addedDate"
						} else if (request.params.type == "best") {
							sort.sortBy = "averageRating"
						} else if (request.params.type == "free") {
							logging.consolelog('in free>>>>>>>>>>>>>>>>>>>>>', "", "")
								// query.price = "0";
							query.isFree = "1"
						} else if (request.params.type == "favourite") {
							if (!topicIds.length) {
								cb(null);
								return;
							}
							query.topicId = {
								$in: topicIds
							}
						} else if (request.params.type == "history") {
							if (!topicHistory.length) {
								cb(null);
								return;
							}
							query.topicId = {
								$in: topicHistory
							}
							sort = {};
						} else if (request.params.type == "topicPackage") {
							if (!request.body.topicPackage.length) {
								cb(null);
								return;
							}
							query.topicId = {
								$in: request.body.topicPackage
							}
						}
					}

					if (request.body.search) {
						// search = " and match(name, authorName, description, gtinCode, topic.language) against " +
						//     "('" + request.body.search + "*' IN BOOLEAN MODE)";
						search = " and (match(name, authorName, description, gtinCode, topic.language) against " +
							"('+" + '"' + request.body.search + '"' + "' IN BOOLEAN MODE) OR chapter like '%" +
							request.body.search + "%' OR subject like '%" + request.body.search + "%')";
					}

					var limit = {
						limit: limitCount,
						offset: offsetCount
					}
					final["count"] = 0;
					final["offset"] = offsetCount;
					final["limit"] = limitCount;
					logging.consolelog(query, "-------->query print", "");
					ServiceTopic.getTopicsByCurriculum(query, sort, limit, search, function (err, data) {
						var curriculumsIds = [];
						if (!err) {
							if (data.length > 0) {
								data.forEach(function (curriculum) {
									if (curriculum && curriculum.name) {
										topics.push({
											Id: curriculum["_id"],
											name: curriculum.name,
											icon: curriculum.icon,
											authorName: curriculum.authorName,
											averageRating: curriculum.averageRating,
											price: curriculum.price,
											description: curriculum.description,
											board: curriculum.board,
											subject: curriculum.subject,
											language: curriculum.boardLanguage,
											topicLanguage: curriculum.topicLanguage,
											grade: curriculum.grade,
											chapter: curriculum.chapter,
											addedDate: curriculum.addedDate,
											isFree: curriculum.isFree,
											isPurchased: purchasedTopics.indexOf(parseInt(curriculum["_id"])) != -1 ? true : false,
											isFavourite: topicIds.indexOf(curriculum["_id"] + "") != -1 ? true : false,
											isCart: cartTopics.indexOf(parseInt(curriculum["_id"])) != -1 ? true : false
										});
									}
								});

								ServiceTopic.getTopicsByCurriculumCount(query, {}, {}, search, function (err, countData) {
									logging.consolelog(err, countData, "check count");
									if (!err) {
										final["count"] = countData ? countData[0]["cnt"] : 0;
									}
									cb(null);
								});
							} else {
								cb(null);
							}
						} else {
							logging.consolelog(err, data, "-------->TOPIC_NOT_FOUND");
							cb(constants.STATUS_MSG.ERROR.TOPIC_NOT_FOUND);
						}
					});

				} else {
					//get topics with language aggregation in case of agent
					var projection = {
						'result.topicId': 1

					}
					var query = {
						// 'isRemoved': false
					}

					prepareFilter('board', request.body.board, query);
					prepareFilter('language', request.body.language, query);
					prepareFilter('grade', request.body.grade, query);
					prepareFilter('subject', request.body.subject, query);
					prepareFilter('chapter', request.body.chapter, query);
					logging.consolelog("query", "", query);
					var limit = {
						limit: request.body.limit ? request.body.limit : 10,
						offset: request.body.offset ? request.body.offset : 0
					}
					var search = "";
					if (request.body.search) {
						// search = ' and name like "%' + request.body.search + '%" ';
						search = " and match(name, authorName, description, gtinCode, topic.language) against " +
							"('+" + '"' + request.body.search + '"' + "' IN BOOLEAN MODE)";
					}

					ServiceCurriculum.getCurriculumAndTopic(query, limit, search, function (err, data) {
						logging.consolelog(err, data, "------->curriculum data");
						if (!err) {
							if (data.length) {
								for (var i = 0; i < data.length; i++) {
									topicListObj[data[i].name] = {
										language: data[i].topicLanguage.split(","),
										topicIds: data[i].ids.split(","),
										rating: Math.max(data[i].rating.split(",")),
										topicName: data[i].name,
										icon: data[i].icon,
										topicId: data[i]["topicId"],
										board: data[i].board,
										grade: data[i].grade,
										chapter: data[i].chapter,
										subject: data[i].subject
									}
								}
								for (var i in topicListObj) {
									topics.push(topicListObj[i]);
								}
								logging.consolelog(query.board, "", "value of query board");
								if (query.board) {

								} else {
									query.board = constants.COMMON_VARIABLES.UNIVERSAL_BOARD_NAME
								}
								//								prepareFilter('board', request.body.board, query);

								ServiceCurriculum.getCurriculumAndTopicCount(query, limit, search, function (err, countData) {
									logging.consolelog(err, data, "count data");
									if (!err) {
										if (countData.length && countData[0].cnt) {
											final["count"] = countData ? countData[0]["cnt"] : 0;
										}
										cb(null);
									} else {
										cb(err);
									}
								});
							} else {
								cb(null)
							}
						} else {
							logging.consolelog(err, data, "-------->TOPIC_NOT_FOUND");
							cb(err);
						}
					});

				}
            },
            function (cb) {
				//topic data with categories fetch from db from mongoose
				if (userType == constants.USER_TYPE.AGENT) {
					var data = [];
					logging.consolelog(topics, "", "topics:@:@@@@@@@")
					if (topics.length > 0) {
						async.forEach(topics, function (item, callback) {
							topicData(item.topicIds,request.body.category, request.body.videoType, function (err, result) {
								if (!err) {
									if(Object.keys(result.mergedObject).length>0){
										data.push({
											name: item.topicName,
											mergedObject: result.mergedObject,
											icon: item.icon,
											averageRating: item.rating,
											board: item.board,
											subject: item.subject,
											grade: item.grade,
											chapter: item.chapter
										})
									}
									
									callback(null);
								} else {
									logging.consolelog(err, result, "-------->TOPIC_DATA");
									callback(err);
								}
							});
						}, function (err) {
							if (!err) {
								final["data"]["topicLibrary"] = data;
								cb(null);
							} else {
								logging.consolelog(err, "", "-------->TOPIC_DATA_ERR");
								cb(err);
							}
						});
					} else {
						cb(null);
					}

				} else {
					final["data"]["topicLibrary"] = topics;
					cb(null);
				}
			},
			// function(cb){


			// 	if(userType==constants.USER_TYPE.AGENT){

			// 		var data = [];
			// 		logging.consolelog(topics, "", "topics:@:@@@@@@@")
			// 		if (topics.length > 0) {
			// 			async.forEach(final["data"]["topicLibrary"], function (item, callback) {
			// 				filterTopic(item, request.body.categoryList, request.body.contentList,function (err, result) {
			// 					if (!err) {
			// 						data.push({
			// 							name: item.topicName,
			// 							mergedObject: result.mergedObject,
			// 							icon: item.icon,
			// 							averageRating: item.rating,
			// 							board: item.board,
			// 							subject: item.subject,
			// 							grade: item.grade,
			// 							chapter: item.chapter
			// 						})
			// 						callback(null);
			// 					} else {
			// 						logging.consolelog(err, result, "-------->TOPIC_DATA");
			// 						callback(err);
			// 					}
			// 				});
			// 			}, function (err) {
			// 				if (!err) {
			// 					final["data"]["topicLibrary"] = data;
			// 					cb(null);
			// 				} else {
			// 					logging.consolelog(err, "", "-------->TOPIC_DATA_ERR");
			// 					cb(err);
			// 				}
			// 			});
			// 		} else {
			// 			cb(null);
			// 		}


			// 	}else{
			// 		cb(null);
			// 	}





			// },
            function (cb) {
				var contentName = [];
				var categoryName = [];
				if (userType == constants.USER_TYPE.AGENT) {

					ServiceTopicCategory.getTopicCategory({
						isPublish: true,
						isDeleted: false
					}, {}, {}, function (err, data) {
						if (!err) {
							for (var i = 0; i < data.length; i++) {
								if (data[i].name == 'Video' && data[i].isDeleted == false) {
									for (var j = 0; j < data[i].content.length; j++) {
										if (data[i].content[j].isDeleted == false && data[i].content[j].isVideo == true && data[i].content[j].isPublish == true) {
											contentName.push(data[i].content[j].typeOfVideo)
										}
									}
								} else if (data[i].isDeleted == false) {
									categoryName.push(data[i].name);
								}

							}

							final.contentName = _.uniq(contentName);
							final.categoryName = _.uniq(categoryName);
							cb(null);
						} else {
							logging.consolelog(err, "", "-------->TOPIC_CATEGORY_ERR");
							cb(err);
						}
					})
				} else {
					cb(null);
				}
            }],
		function (err, data) {
			if (!err) {
				final.statusCode = 200;
				response.status(200).send(final);
			} else {
				response.send(err);
			}
		})
}
var prepareFilter = function (key, data, query) {
	if (data) {
		try {
			data = JSON.parse(data);
		} catch (e) {}
		if (!Array.isArray(data)) {
			data = [data]
		}
		if (data && data.length) {
			query[key] = {
				$in: data
			}
		}
	}
}


var createComment = function (request, response) {

	async.series([
            function (cb) {
				var dataToSave = {
					topicId: request.body.topicId,
					userType: request.body.userType,
					ratingStars: request.body.ratingStars,
					comments: request.body.comments,
					isActive: true,
					customerId: request.userData.id
				};
				logging.consolelog('sending objectTosave', "", dataToSave);
				ServiceFeedback.createFeedback(dataToSave, function (err, data) {
					logging.consolelog("logging", err, data);
					if (!err) {
						cb(null);
					} else {
						cb(err);
					}
				})
            },
            function (cb) {
				var criteria = {
					topicId: request.body.topicId.toString()
				};
				logging.consolelog('type of topicid', "", typeof criteria.topicId);
				ServiceFeedback.getAvgRating(criteria, {}, {}, {}, function (err, data) {
					if (err) {
						cb(err)
					} else {
						cb(null)
					}
				})
            }
        ],
		function (err, data) {
			if (!err) {
				response.status(200).send(constants.STATUS_MSG.SUCCESS.COMMENT_ADDED);
			} else {
				response.send(err);
			}
		})
}

function getAssignedTopics(customerId, topicId, callback) {
	logging.consolelog("logging", customerId, topicId)
	var content = [];
	var topic = [];
	async.series([
        function (cb) {
			var query = {
				assignedTo: customerId,
				$or: [{
						'content.topicId': topicId
                },
					{
						'topic': topicId
                    }]
			}

			logging.consolelog(query, "", "assigned data query")
			ServiceAssignDataToCustomer.getAssignDataToCustomer(query, {
				'content': 1,
				'topic': 1
			}, {
				lean: true
			}, function (err, data) {
				if (!err) {
					for (var i in data) {
						if (data[i] && data.length > 0 && data[i].content && data[i].content.length > 0) {
							for (var j = 0; j < data[i].content.length; j++) {
								if (data[i].content[j].isActive == true || data[i].content[j].isActive == undefined) {
									if (data[i].content[j].topicId == topicId) {
										content.push({
											categoryId: data[i].content[j].categoryId,
											contentType: data[i].content[j].contentType,
											topicId: data[i].content[j].topicId,
											startDate: data[i].content[j].startDate,
											endDate: data[i].content[j].endDate
										})

									}
								}

							}
						}

					}
					cb(null);

				} else {
					cb(err);
				}
			})


        }], function (err, data) {
		logging.consolelog(err, data, "assigned topics");
		if (!err) {
			callback(null, {
				content: content,
				topic: topic
			});
		} else {
			callback(err);
		}
	});

}


function getAgentPurchases(customerId, callback) {
	var topics = [];
	var packages = {};
	var packageDetails = [];
	var content = [];
	async.series([
        function (cb) {
			var query = {
				'customerId': {
					$in: customerId
				},
				'paymentStatus': constants.DATABASE.STATUS.COMPLETED
			};
			var path = 'content.categoryId';
			var select = 'name icon language content';
			var populate = {
				path: path,
				match: {},
				select: select,
				options: {
					lean: true
				}
			};

			ServiceCustomerBuyPackages.getPackages(query, {
				topic: 1,
				package: 1,
				packageName: 1,
				buyDate: 1,
				customerId: 1,
				content: 1,
				embeddedLink: 1
			}, populate, {}, function (err, data) {
				logging.consolelog(err, data, "data::purchase topic data")
				if (err) {
					cb(err);
				} else {
					if (data.length) {
						for (var i in data) {
							if (data[i] && data.length > 0 && data[i].content && data[i].content.length > 0) {
								for (var j = 0; j < data[i].content.length; j++) {

									content.push({
										categoryId: data[i].content[j].categoryId,
										contentType: data[i].content[j].contentType,
										topicId: data[i].content[j].topicId,
										startDate: data[i].content[j].startDate,
										endDate: data[i].content[j].endDate,
										embeddedLink: data[i].embeddedLink
									})


								}
							}
						}
					}
					cb(null);
				}
			})
        }], function () {
		callback(null, {
			content: content
		});
	});

}


function reverseArr(input) {
    var ret = new Array;
    for(var i = input.length-1; i >= 0; i--) {
        ret.push(input[i]);
    }
    return ret;
}

var topicDetails = function (request, response) {
	var curriculumLinked = [];
	var topicDetails = {};
	var videoObj={};
	var contentLinking = {};
	var universalLinked = [];
	var purchasedTopics = [];
	var comments = [];
	var topicIds = [];
	var assignedContentTopics = [];
	var assignedTopics = [];
	var languages = {};
	var topicId;
	var assessments = [];
	var userId = request.userData.id,
		userType = request.userData.userType;
	var superParentCustomerId = [];
	var agentPurchasedTopics = [];
	var isDomain = false;
	var demo = false;
	async.series([
        function (cb) {
			// front end will pass topic name and language// get topic Id
			ServiceTopic.getTopic({
				name: request.body.topicName,
				language: request.body.topicLanguage,
				isDeleted: false
			}, {
				_id: 1
			}, {
				lean: true
			}, function (err, data) {
				logging.consolelog(err, data, request.body)
				if (!err) {
					if (data.length > 0) {
						topicId = data[0]._id;
					}
					cb(null);
				} else {
					cb(err);
				}
			});
        },
         function (cb) {
			var query = {
				'customerId': userId,
				'topics.topicId': topicId.toString()
			};

			ServiceNotes.getNote(query, {
				topics: 1,
				customerId: 1
			}, {}, function (err, data) {
				logging.consolelog("logging", "", data);
				if (err) {
					cb(err);
				} else {
					if (data && data[0] && data[0].topics && data[0].topics)
						topicDetails.notes = data[0].topics;
					cb(null);
				}
			})
            },
        function (cb) {
			ServiceCustomer.getCustomer({
				_id: userId
			}, {
				emailId: 1,
				registrationDate: 1
			}, {
				lean: true
			}, function (err, data) {
				if (!err) {
					if (data.length > 0) {
						var email = data[0].emailId;
						var date = Date.now();
						var oneDay = 24 * 60 * 60 * 1000;
						var firstDate = new Date(date);
						var registeredDate = data[0].registrationDate;
						var secondDate = new Date(registeredDate);
						var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
						if (diffDays <= 7) {
							demo = true;

						} else {
							demo = false;
						}

						//get domain of customer
						var domain = email.substring(email.lastIndexOf("@") + 1);
						var query = {
							userType: constants.USER_TYPE.AGENT,
							domainName: domain
						}
						ServiceCustomer.getCustomer(query, {
							_id: 1
						}, {}, function (err, data) {
							if (!err) {
								if (data.length > 0) {
									for (var i = 0; i < data.length; i++) {
										superParentCustomerId.push(data[i]._id);
										isDomain = true;
									}

								}
								cb(null);
							} else {
								cb(err);
							}
						})
					}
				} else {
					cb(err);
				}
			})
        },
        function (cb) {
			//fetch favorite topic
			try {
				getFavourite(request.body.customerId, function (data) {
					topicIds = topicIds.concat(data);
					logging.consolelog("logging", "", topicIds);
					cb(null);
				})
			} catch (e) {
				logging.consolelog("logging", e, "");
				cb(null);
			}
        },
        function (cb) {
			//fetch purchased topic
			if (userType != constants.USER_TYPE.AGENT) {
				getCustomerPurchases(request.body.customerId, function (data) {
					console.log(JSON.stringify(data), "purchased Data")
					for (var i = 0; i < data.length; i++) {
						purchasedTopics.push(parseInt(data[i]));
					}
					if (demo == true) {
						purchasedTopics.push(parseInt(topicId));
					}
					// purchasedTopics = purchasedTopics.concat(data);
					cb(null);
				})
			} else {
				cb(null);
			}

        },

        function (cb) {
			//fetch assigned topics
			getAssignedTopics(request.body.customerId, topicId, function (err, data) {
				assignedTopics = assignedTopics.concat(data.topic);
				assignedContentTopics = assignedContentTopics.concat(data.content);

				cb(null);
			})
        },
        function (cb) {
			if (userType == constants.USER_TYPE.AGENT) {

				ServiceChildMapper.getChildParentMapper({
					childCustomerId: userId
				}, {
					superParentCustomerId: 1
				}, {
					lean: true
				}, function (err, data) {
					if (!err) {
						if (data.length > 0) {
							superParentCustomerId.push(data[0].superParentCustomerId);
						}
						cb(null);
					} else {
						cb(err);
					}
				});
			} else {
				cb(null);
			}

        },
        function (cb) {
			if (isDomain == true || userType == constants.USER_TYPE.AGENT) {
				getAgentPurchases(superParentCustomerId, function (err, data) {
					agentPurchasedTopics = agentPurchasedTopics.concat(data.content);
					cb(null);
				})

			} else {
				cb(null);
			}


        },
        function (cb) {
			logging.consolelog("universal_linked", "", "");
			var query = {
				topicId: topicId,
				board: constants.COMMON_VARIABLES.UNIVERSAL_BOARD_NAME
			}
			ServiceTopic.getTopicsByCurriculum(query, {}, {}, "", function (err, data) {
				if (!err) {
					if (data.length > 0) {
						data.forEach(function (curriculum) {
							universalLinked.push({
								board: curriculum.board,
								language: curriculum.boardLanguage,
								grade: curriculum.grade,
								subject: curriculum.subject,
								chapter: curriculum.chapter
							});
							topicDetails.id = curriculum["_id"];
							topicDetails.name = curriculum.name;
							topicDetails.icon = curriculum.icon;
							topicDetails.authorName = curriculum.authorName;
							topicDetails.language = curriculum.topicLanguage;
							topicDetails.description = curriculum.description;
							topicDetails.gtinCode = curriculum.gtinCode;
							topicDetails.price = curriculum.price;
							topicDetails.isFavourite = topicIds.indexOf(curriculum["_id"] + "") != -1 ? true : false;
						});
						cb(null);
					} else {
						cb(null);
					}
				} else {
					cb(err);
				}
			});
        },
        function (cb) {

			var query = {
				name: topicDetails.name,
				isDeleted: false
			};
			ServiceTopic.getTopic(query, {}, {}, function (err, data) {
				if (data && data.length) {
					for (var i in data) {
						languages[data[i]["language"]] = data[i]["_id"];
					}
				}
				cb(null);
			});
        },
        function (cb) {

			var query = {
				topicId: topicId,
				isActive: true
			}
			var projection = {
				userType: 1,
				ratingStars: 1,
				comments: 1,
				customerId: 1,
				addedDate: 1
			}

			var path = 'customerId';
			var select = 'emailId userType mobileNo profilePicUrl lastName firstName';
			var populate = {
				path: path,
				match: {},
				select: select,
				options: {
					lean: true
				}
			};
			ServiceFeedback.getFeedback(query, projection, {}, populate, function (err, data) {
				if (!err) {
					logging.consolelog("Commnts", "", JSON.stringify(data));
					if (data.length > 0) {
						for (var i in data) {
							var comment = {};
							comment.userType = data[i].userType;
							comment.ratingStars = data[i].ratingStars;
							comment.comments = data[i].comments;
							comment.customerId = data[i].customerId;
							comment.mobileNo = data[i].mobileNo;
							comment.firstName = data[i].firstName;
							comment.lastName = data[i].lastName;
							comment.profilePicUrl = data[i].profilePicUrl;
							comment.emailId = data[i].emailId;
							comment.userType = data[i].userType;
							comment.addedDate = data[i].addedDate;
							comments.push(comment);
						}
						cb(null);
					} else {
						cb(null);
					}
				} else {
					cb(err);
				}
			});
        },
        function (cb) {

			var query = {
				topicId: topicId
			}

			var select = 'name price icon language isPublish content isDeleted isFree name icon language content isDeleted isFree isPublish'
			var populate = {
				path: 'categoryId',
				match: {
					// isPublish: true,
					isDeleted: false
				},
				select: select,
				options: {
					lean: true
				}
			};
			var projection = {
				categoryId: 1
			};
			ServiceTopicBinding.topicBindingPopulate(query, projection, populate, {}, {
				lean: true
			}, function (err, data) {
				if (!err) {
					var i = 0;
					var length = data.length;
					var categories = [];
					var embeddedLinks = [];
					logging.consolelog(assignedContentTopics, "", "print purchased categories")
					for (var i = 0; i < assignedContentTopics.length; i++) {
						categories.push(assignedContentTopics[i].categoryId.toString());
					}
					for (var i = 0; i < assignedContentTopics.length; i++) {
						if (assignedContentTopics[i].embeddedLink) {
							embeddedLinks.push(assignedContentTopics[i].contentType);
						}

					}
					for (var i = 0; i < agentPurchasedTopics.length; i++) {
						categories.push(agentPurchasedTopics[i].categoryId._id.toString());
					}
					logging.consolelog("logging", purchasedTopics, purchasedTopics.indexOf(topicId) != -1 ? true : false)
					console.log("puchased topics values", JSON.stringify(purchasedTopics), JSON.stringify(assignedTopics), JSON.stringify(agentPurchasedTopics));
					var purchased = (purchasedTopics.indexOf(topicId) != -1 ? true : false) ||
						(assignedTopics.indexOf(topicId.toString()) != -1 ? true : false) ||
						(agentPurchasedTopics.indexOf(topicId.toString()) != -1 ? true : false);

					var topic = {};
					var videoHasContent = false;
					var reverseData= data;
					// reverseData = reverseArr(reverseData);
					

				
					console.log(reverseData, "=======> reverse array")
					for (i = 0; i < length; i++) {
 
						logging.consolelog('track error', '>>>>>>>>>>>>', reverseData[i])

						if (reverseData && reverseData.length && reverseData[i].categoryId && reverseData[i].categoryId.name && reverseData[i].categoryId.isPublish && contentLinking[reverseData[i].categoryId.name]) {
							continue;
						} else if (reverseData && reverseData.length && (reverseData[i].categoryId && reverseData[i].categoryId.isPublish && reverseData[i].categoryId.name)) {
							logging.consolelog('preparing content linking>>>>>>>>>>>>>', reverseData[i].categoryId, reverseData[i].subCategoryId)
							if(reverseData[i].categoryId.name== "Video"){
								videoObj[reverseData[i].categoryId.name]={
									name: reverseData[i].categoryId.name,
									_id: reverseData[i].categoryId._id,
									icon: reverseData[i].categoryId.icon,
									isFree: reverseData[i].categoryId.isFree,
									purchasedCategories: categories,
									isPurchased: demo ? demo : (purchased ? purchased : (categories.indexOf(reverseData[i].categoryId["_id"].toString()) == -1 ? false : true)),
									embeddedLink: embeddedLinks
								}
							}else{
								contentLinking[reverseData[i].categoryId.name] = {
									name: reverseData[i].categoryId.name,
									_id: reverseData[i].categoryId._id,
									icon: reverseData[i].categoryId.icon,
									isFree: reverseData[i].categoryId.isFree,
									purchasedCategories: categories,
									isPurchased: demo ? demo : (purchased ? purchased : (categories.indexOf(reverseData[i].categoryId["_id"].toString()) == -1 ? false : true)),
									embeddedLink: embeddedLinks
								}
							}
							
							logging.consolelog('>>>>>>>>>>>>>>>>>>>>>>>', reverseData[i].categoryId.content.length, reverseData[i].categoryId.name)
							logging.consolelog('KKKKKKKKKKKKKKKKKK', !(reverseData[i].categoryId.content.length != 0 && reverseData[i].categoryId.name === 'Video'), '')
							if ((reverseData[i].categoryId.content.length != 0 && reverseData[i].categoryId.name === 'Video'))
								videoHasContent = true;
						}
					}


					logging.consolelog('contentLinking before>>>>>>>>', videoHasContent, contentLinking)
					if (!videoHasContent)
						delete contentLinking['Video']
					logging.consolelog('contentLinking after>>>>>>>>', videoHasContent, contentLinking)
					 contentLinking=Object.assign(videoObj,contentLinking)
					cb(null);
				} else {
					cb(err);
				}
			})
        },
        function (cb) {
			let query = {
				topicId: topicId,
				'assesment.isDeleted': false
			}
			let projection = {
				'assesment': 1,
				isFree: 1,
				price: 1,
				language: 1,
				name: 1,
				addedDate: 1
			}
			ServiceAssesment.getAssessment(query, projection, {}, function (err, data) {
				if (!err) {
					if (data.length > 0) {
						if (data[0].assesment) {
							for (var i = 0; i < data[0].assesment.length; i++) {
								if (data[0].assesment[i].isDeleted == false) {
									assessments.push({
										_id: data[0].assesment[i]._id,
										addedDate: data[0].assesment[i].addedDate,
										assesmentUrl: data[0].assesment[i].assesmentUrl,
										name: data[0].assesment[i].name,
										price: data[0].assesment[i].price,
										language: data[0].assesment[i].language,
									})
								}

							}

						}
					}
					cb(null);
				} else {
					cb(err);
				}
			})
        }], function (err, data) {
		if (!err) {
			response.status(200).send({
				statusCode: 200,
				topicDetails: topicDetails,
				contentLinking: contentLinking,
				videoObj:videoObj,
				universalLinked: universalLinked,
				topicId: topicId,
				comments: comments,
				isPurchased: purchasedTopics.indexOf(topicId + "") != -1 ? true : false,
				languages: languages,
				assessments: assessments
			})
		} else {
			response.send(err);
		}
		addToHistory(request.body.customerId, topicId);
	})

}

function addToHistory(customerId, topicId) {
	var query = {
		'customerId': customerId
	};
	ServiceFavourites.updateFavourite(query, {
		$push: {
			"topicHistory": {
				$each: [topicId],
				$slice: -10
			}
		},
		$set: {
			'customerId': customerId
		}
	}, {
		upsert: true
	}, function (err, data) {
		if (err) {
			logging.consolelog("logging", "", err);
		}
	})
}
var getAssessments = function (request, response) {
	var assessments = [];
	async.series([
            function (cb) {
				var query = {
					topicId: request.body.topicId
				}
				ServiceAssesment.getAssessment(query, {}, {}, function (err, data) {
					if (!err) {
						if (data.length > 0) {
							if (data[0].assesment) {
								for (var i = 0; i < data[0].assesment.length; i++) {
									assessments.push({
										_id: data[0].assesment[i]._id,
										addedDate: data[0].assesment[i].addedDate,
										assesmentUrl: data[0].assesment[i].assesmentUrl,
										name: data[0].assesment[i].name,
										price: data[0].assesment[i].price,
										language: data[0].assesment[i].language,
									})
								}

							}
						}
						cb(null);
					} else {
						cb(err);
					}
				})
            }],
		function (err, data) {
			if (!err) {
				response.status(200).send({
					statusCode: 200,
					assessments: assessments
				})
			}
		});
}

// var downloadContent = function (request, response) {
//     var chunks = "";

//     TokenManager.verifyToken(request.headers.authorization, function (err, token) {
//         logging.consolelog(err, token, "token authorization");
//         if (!err) {
//             var secCheck = request.headers['host']?request.headers['host']:request.headers['x-forwarded-for'];             logging.consolelog('check ip>>>>',err,token.userData.ip === secCheck);             if (token.userData.ip === secCheck) {
//                 userType = token.userData.userType;

//                 https.get(request.body.url, function (res) {
//                     logging.consolelog('statusCode:', "", res.statusCode);
//                     logging.consolelog('headers:', "", res.headers);

//                     res.on('data', function (d) {
//                         chunks += d;

//                     });
//                     res.on('end', function (d) {
//                         response.status(200).send(chunks);

//                     });

//                 }).on('error', function (e) {
//                     console.error(e);
//                 });
//             } else {
//                 response.send(constants.STATUS_MSG.ERROR.INVALID_TOKEN);
//             }

//         } else {
//             response.send(constants.STATUS_MSG.ERROR.INVALID_TOKEN);
//         }
//     });
// };

function getFilters(request, response) {

	var filters = ["subject", "grade", "language", "board", "chapter"];
	var final = {
		subjectList: [],
		gradeList: [],
		languageList: [],
		boardList: [],
		chapterList: []
	};
	var conditional = false;
	if (request.body.filter.curriculum.length || request.body.filter.grade.length || request.body.filter.subject.length || request.body.filter.language.length)
		conditional = true;
	async.auto({
		board_list: function (cb) {
			// if (conditional) {
			logging.consolelog('board_list>>>>>>', "", "")
			ServiceCurriculum.getUniqueTable('board', function (err, data) {
					if (!err) {
						for (var i = 0; i < data.length; i++) {

							if(data[i].board!=null){
								final.boardList.push({
									board: data[i].board
								});
							}
							
						}

						cb(false, null);
					} else {
						cb(err, null);

					}
				})
				// } else {
				// 	cb(false, null)
				// }
		},
		language_list: ["board_list", function (cb) {
			// if (conditional) {
			logging.consolelog('language_list>>>>>>', "", "")
			ServiceCurriculum.getConditionalFilterData(request.body.filter, 'language', function (err, data) {
					if (!err) {
						logging.consolelog("language data::", err, JSON.stringify(data));
						if (data.length > 0) {
							for (var i = 0; i < data.length; i++) {
								if(data[i].language!=null){
									final.languageList.push({
										language: data[i].language
									});
								}
								
							}
							cb(false, null);
						} else {
							cb(false, null);
						}

					} else {
						cb(err, null);
					}
				})
				// } else {
				// 	cb(false, null)
				// }

        }],
		grade_list: ["language_list", function (cb) {
			// if (conditional) {
			logging.consolelog('grade_list>>>>>>', "", "")
			ServiceCurriculum.getConditionalFilterData(request.body.filter, 'grade', function (err, data) {
					if (!err) {
						for (var i = 0; i < data.length; i++) {

							if(data[i]["grade"]!=null){
								final.gradeList.push({
									grade: data[i].grade
								});
							}
							
						}

						cb(false, null);
					} else {
						cb(err, null);
					}
				})
				// } else {
				// 	cb(false, null)
				// }
        }],
		subject_list: ["grade_list", function (cb) {
			// if (conditional) {
			logging.consolelog('subject_list>>>>>>>', "", "");
			ServiceCurriculum.getConditionalFilterData(request.body.filter, 'subject', function (err, data) {
					if (!err) {
						for (var i = 0; i < data.length; i++) {
							if(data[i]["subject"]!=null){
								final.subjectList.push({
									subject: data[i].subject
								});
							}
							
						}
						cb(false, null);
					} else {
						cb(err, null);

					}
				})
				// } else {
				// 	cb(false, null)
				// }
        }],

		chapter_list: ["subject_list", function (cb) {
			// if (conditional) {
			logging.consolelog('chapter_list>>>>>>', "", "")
			ServiceCurriculum.getConditionalFilterData(request.body.filter, 'chapter', function (err, data) {
					if (!err) {
						logging.consolelog('chapter list', "", data)
						for (var i = 0; i < data.length; i++) {

							if(data[i]["chapter"]!=null){
								final.chapterList.push({
									chapter: data[i].chapter
								});
							}
							
						}
						cb(false, null);
					} else {
						cb(err, null);
					}
				})
				// } else {
				// 	cb(false, null)
				// }
        }]

	}, function (err, result) {
		if (err) {
			response.send(constants.STATUS_MSG.ERROR.EMPTY_CURRICULUM);
		} else {
			response.status(200).send(final);
		}
	})
}


function getAllCurriculumDetails(param, query, cb) {
	var list = [];
	ServiceCurriculum.getUniqueCurriculum(query, param, function (err, data) {
		if (data && data.length) {
			for (var i = 0; i < data.length; i++) {
				var obj = {};
				obj[param] = data[i];
				list.push(obj);
			}
		}
		cb(list);
	});
}


var getSubCategoryContent = function (request, response) {
	var content = [];
	async.auto({
		get_content: function (cb) {
			ServiceTopicSubCategory.getTopicSubCategory({
				_id: request.body.subCategoryId
			}, {}, {}, function (err, data) {
				if (!err) {
					logging.consolelog("logging", err, data);
					for (var i = 0; i < data[0].content.length; i++) {
						if (data[0].content[i].isDeleted == false) {
							logging.consolelog(data[0].content[i].ifFree, "", "ifFree or not");

							content.push({
								content: data[0].content[i]
							});
						}
					}
					cb(null);
				} else {
					cb(err);
				}
			})
		}
	}, function (err, data) {
		if (!err) {
			response.status(200).send({
				statusCode: 200,
				content: content
			})
		} else {
			response.send(err);
		}
	})
}


var addCollection = function (item, callback) {
	logging.consolelog('item>>>', "", item)
	var topicIds = [];
	var final = [];
	async.auto({


		get_category: function (cb) {

			var query = {
				topicId: item.topicId

			}
			var path = 'categoryId';
			var select = 'name content isDeleted'
			var populate = {
				path: path,
				match: {},
				select: select,
				options: {
					lean: true
				}
			};
			var options = {};
			var projection = {
				categoryId: 1,
				topicId: 1
			}
			ServiceTopicBinding.getAllTopicBinding(query, projection, populate, {}, {}, function (err, data) {
				if (!err) {
					logging.consolelog(err, data, "topic binding result");
					for (var i = 0; i < data.length; i++) {
						logging.consolelog(data[i].categoryId.name, "", "topic binding result");
						if (data[i].categoryId.name == 'Video') {
							logging.consolelog(data[i].categoryId.name, "", "Video result");
							for (var j = 0; j < data[i].categoryId.content.length; j++) {

								if (item.contentType && item.contentType.indexOf(data[i].categoryId.content[j].typeOfVideo) != -1) {
									final.push({
										topicId: Number(data[i].topicId),
										contentType: data[i].categoryId.content[j]._id,
										categoryId: data[i].categoryId._id
									});
								} else {

									if (item.contentType && item.contentType.toString() == data[i].categoryId.content[j]._id.toString()) {
										final.push({
											topicId: Number(data[i].topicId),
											contentType: data[i].categoryId.content[j]._id,
											categoryId: data[i].categoryId._id
										});
									}
								}
							}

						} else {
							if (item.categoryName && item.categoryName.length > 0) {
								if (item.categoryName.indexOf(data[i].categoryId.name) == -1) {

								} else {
									final.push({
										topicId: data[i].topicId,
										categoryId: data[i].categoryId._id
									});


								}
							} else {

								if (item.categoryId && (item.categoryId.toString() == data[i].categoryId._id.toString())) {
									final.push({
										topicId: Number(data[i].topicId),
										categoryId: data[i].categoryId._id
									});

								}
							}


						}

					}
					cb(null);
				} else {
					cb(err);
				}
			})
		}


	}, function (err, data) {
		if (!err) {
			callback(null, final);
		} else {
			callback(err);
		}
	})
}


//input
//key : value
//key- topicId
//value {categoryName:[],contentType:[],topicId:"234"}
var addToCart = function (request, response) {
	var repeat = false;
	var mergedObject = {};
	var mergerdArray = [];
	var final = [];
	var userType = request.userData.userType;
	var customerId = request.userData.id;
	var topicCart = [];
	var array = [];
	//	logging.consolelog("topicArray is an object::",topicArray);
	for (var key in request.body.topicArray) {
		array.push(request.body.topicArray[key]);
	}
	logging.consolelog("topicArray is array", "", array, request.body.topicCart);
	async.series([
            function (cb) {
				if (userType != constants.USER_TYPE.AGENT) {
					var query = {
						'customerId': customerId
					};
					var dataToSet = {
						$addToSet: {},
						$set: {
							'customerId': customerId
						}
					};
					if (request.body.packageCart && request.body.packageCart.length > 0) {
						dataToSet["$addToSet"]["packageCart"] = {
							$each: request.body.packageCart
						};
					}
					if (request.body.topicCart && request.body.topicCart.length > 0) {
						dataToSet["$addToSet"]["topicCart"] = {
							$each: request.body.topicCart
						};
					}
					if (request.body.customPackageCart && request.body.customPackageCart.length > 0) {
						dataToSet["$addToSet"]["customPackageCart"] = {
							$each: request.body.customPackageCart
						};
					}
					logging.consolelog('query mongo', query, dataToSet)
					ServiceCustomerCart.updateCart(query, dataToSet, {
						upsert: true
					}, function (err, data) {
						if (data.nModified == 0 && !data.upserted) {
							repeat = true;
						}
						if (err) {
							cb(err);
						} else {
							cb(null);
						}
					})
				} else {
					cb(null);
				}

            },
            function (cb) {
				if (userType == constants.USER_TYPE.AGENT) {

					async.forEach(array, function (item, callback) {
						addCollection(item, function (err, result) {
							logging.consolelog(JSON.stringify(result), "", "add collection result")
							if (!err) {
								final = final.concat(result);

								callback(null);
							} else {
								callback(err);
							}
						});
					}, function (err) {
						if (!err) {

							cb(null);
						} else {

							cb(err);
						}
					});


				} else {
					cb(null);
				}
            },
            function (cb) {
				if (userType == constants.USER_TYPE.AGENT) {
					var query = {
						'customerId': customerId
					};
					var dataToSet = {
						$addToSet: {},
						$set: {
							'customerId': customerId
						}
					};
					if (array && array.length > 0) {
						dataToSet["$addToSet"]["content"] = {
							$each: final
						}
					}

					ServiceCustomerCart.updateCart(query, dataToSet, {
						upsert: true
					}, function (err, data) {
						if (data.nModified == 0 && !data.upserted) {
							repeat = true;
						}
						if (err) {
							cb(err);
						} else {
							cb(null);
						}
					})
				} else {
					cb(null);
				}


            }

        ],
		function (err, data) {
			if (!err) {
				response.status(200).send(repeat ? constants.STATUS_MSG.SUCCESS.ADDED_CART_DUP : constants.STATUS_MSG.SUCCESS.ADDED_CART);
			} else {
				response.send(err);
			}
		})
}

var getCart2 = function (request, response) {
	var topicIds = [];
	var topicDetails = [];
	var packageDetails = [];
	async.auto({
		customer_cart: function (cb) {
			var populate = [{
				path: 'packageCart',
				match: {},
				select: 'icon curriculum grade language',
				options: {
					lean: true
				}
            }];
			logging.consolelog("populate", "", populate);

			ServiceCustomerCart.cartPopulate({
				customerId: request.body.customerId
			}, {
				topicCart: 1,
				packageCart: 1
			}, populate, {}, {}, function (err, data) {
				// logging.consolelog(err, data[0].packageCart[0], "populate Data");
				if (!err) {
					logging.consolelog("logging", "", data);
					if (data && data.length && data[0].topicCart) {
						for (var i = 0; i < data[0].topicCart.length; i++) {
							topicIds.push(data[0].topicCart[i]);
						}
					}
					if (data && data.length && data[0].packageCart) {
						for (var i = 0; i < data[0].packageCart.length; i++) {
							packageDetails.push({
								_id: data[0].packageCart[i]._id,
								curriculum: data[0].packageCart[i].curriculum,
								grade: data[0].packageCart[i].grade,
								language: data[0].packageCart[i].language,
								icon: data[0].packageCart[i].icon,
								subjects: 10,
								topics: 100,
								chapter: 20,
								price: 100
							})

						}
					}
					cb(null);

				} else {
					cb(err);
				}


			})
		},
		fetch_topic: ['customer_cart', function (cb) {
			if (topicIds.length) {
				var query = {
					_id: {
						$in: topicIds
					}
				};
				ServiceTopic.getTopic(query, {}, {}, function (err, data) {
					logging.consolelog("logging", "", data);
					if (data) {
						for (var i = 0; i < data.length; i++) {
							topicDetails.push({
								_id: data[i]["_id"],
								name: data[i].name,
								rating: data[i].averageRating,
								icon: data[i].icon,
								price: data[i].price
							})

						}
					}
					cb(null);
				})
			} else {
				cb(null);
			}
        }]
	}, function (err, data) {
		if (!err) {
			response.status(200).send({
				statusCode: 200,
				packageDetails: packageDetails,
				topicDetails: topicDetails
			})
		} else {
			response.send(err);
		}
	})
}

var removeCart = function (request, response) {
	var userType = request.userData.userType;
	var customerId = request.userData.id;
	async.auto({
			removeCart: function (cb) {
				var update = {}
				var query = {
					customerId: customerId
				}
				if (userType != constants.USER_TYPE.AGENT && request.params.type == 'TOPIC') {
					update = {
						$pull: {
							topicCart: parseInt(request.body.removeId)
						}
					}
				} else if (userType != constants.USER_TYPE.AGENT && request.params.type == 'PACKAGE') {
					update = {
						$pull: {
							packageCart: request.body.removeId
						}
					}
				} else if (userType != constants.USER_TYPE.AGENT && request.params.type == 'CUSTOM_PACKAGE') {
					update = {
						$pull: {
							customPackageCart: request.body.removeId
						}
					}
				} else if (userType == constants.USER_TYPE.AGENT && request.params.type == 'TOPIC' && request.body.removeId.length > 0) {
					var query = {
						'customerId': customerId,
						'content.topicId': {
							$in: request.body.removeId
						}
					}
					update = {
						$pull: {
							topicCart: {
								$in: request.body.removeId
							}
						}
					}
					update = {
						$pull: {
							'content': {
								'topicId': {
									$in: request.body.removeId
								}
							}
						}
					}
				}

				//	logging.consolelog("update", update, query);


				console.log("======> updtae cart ", JSON.stringify(query), JSON.stringify(update));
				ServiceCustomerCart.updateCart(query, update, {
					multi: true
				}, function (err, data) {
					logging.consolelog(err, data, "pull data1");
					if (!err) {
						cb(null);
					} else {
						cb(err);
					}
				})
			},
			remove: ['removeCart', function (cb) {
					logging.consolelog("logging", "", request.body.removeId)
					if (userType == constants.USER_TYPE.AGENT && request.params.type == 'TOPIC' && request.body.removeId.length > 0) {
						var query = {
							'customerId': customerId,

						}
						var update = {
							$pull: {
								topicCart: {
									$in: request.body.removeId
								}
							}
						}

					}

					console.log("=======> upodate cart",  JSON.stringify(query), JSON.stringify(update))

					ServiceCustomerCart.updateCart(query, update, {
						multi: true
					}, function (err, data) {
						logging.consolelog(err, data, "pull data2");
						if (!err) {
							cb(null);
						} else {
							cb(err);
						}
					})
            },
            ]
		},
		function (err, data) {
			if (!err) {
				response.status(200).send(constants.STATUS_MSG.SUCCESS.REMOVED_CART);
			} else {
				response.send(err);
			}
		})
}

var cartNumber = function (request, response) {
	var items = 0;
	var topics = 0;
	var packages = 0;
	var customPackages = 0;
	var topicIds = [];
	var content=[];
	async.series([
        function (cb) {
			ServiceCustomerCart.getCart({
				customerId: request.body.customerId
			}, {}, {}, function (err, data) {
				if (!err) {
					logging.consolelog("logging", "", data);
					if (data && data.length > 0) {
						if (data[0].topicCart) {
							topicIds = topicIds.concat(data[0].topicCart);
						}

					 if(data[0].content && data[0].content.length>0){
						 for(var i=0; i< data[0]["content"].length; i++){
							 content.push(data[0]["content"][i]["topicId"]);

						 }
						 if(content.length>0){
							var uniqueContent= _.unique(content);
							topics=uniqueContent.length;
						 }
					 }
						packages = data[0].packageCart ? data[0].packageCart.length : 0;
						customPackages = data[0].customPackageCart ? data[0].customPackageCart.length : 0;
					}
					cb(null);
				} else {
					cb(err);
				}
			})
        },
        function (cb) {
			if (topicIds.length) {
				ServiceTopic.getValidTopics(topicIds, function (err, data) {
					if (data && data.length) {
						topics = data.length;
					}
					cb(null);
				});
			} else {
				cb(null);
			}
        }
    ], function (err, data) {
		if (!err) {
			items = topics + packages + customPackages;
			response.status(200).send({
				items: items,
				topics: topics,
				packages: packages,
				customPackages: customPackages,
				statusCode: 200
			})
		} else {
			response.send(err);
		}
	})
}

var checkout = function (request, response) {
	var topicDetails = [];
	var customPackageDetails = [];
	var customPackageTopics = [];
	var packageDetails = [];
	var packageDetailsObj = {};
	var packageDetailsForDB = [];
	var transactionId = "txn" + Date.now();
	var topicPrice = 0;
	var topicDiscountPrice = 0;
	var topicSubtotalPrice = 0;
	var packagePrice = 0;
	var totalPrice = 0;
	var totalCount = 0;
	var topics = {};
	var formbody = '';
	var redirectUrl = '';
	var superAdminIds = [];
	var topicType = request.body.topicType;
	var url = '';
	var objToSave = {
		payments: {
			subTotalPrice: 0,
			discountPrice: 0,
			totalPrice: 0
		},
		topicPayments: {
			subTotalPrice: 0,
			discountPrice: 0,
			totalPrice: 0
		},
		packagePayments: {
			subTotalPrice: 0,
			discountPrice: 0,
			totalPrice: 0
		},
		customPackagePayments: {
			subTotalPrice: 0,
			discountPrice: 0,
			totalPrice: 0
		},
		topicOfferApplied: {
			offerName: "",
			offerAmount: 0,
			noOfTopics: 0
		}
	};
	var packages = {};
	var userType = request.userData.userType;
	var customerId = request.userData.id;
	var content = [];
	async.auto({
			fetch_cart: function (cb) {
				var path = 'packageCart customPackageCart';
				var select = 'curriculum grade language monthlyPrice yearlyPrice monthlyDiscount yearlyDiscount typeOfDiscount packageName topics icon'
				var populate = {
					path: path,
					match: {
						isDeleted: false
					},
					select: select,
					options: {
						lean: true
					}
				};
				ServiceCustomerCart.cartPopulate({
					customerId: customerId
				}, {
					topicCart: 1,
					packageCart: 1,
					customPackageCart: 1
				}, populate, {}, {
					lean: true
				}, function (err, data) {
					// logging.consolelog(err, data[0].packageCart[0], "populate Data");
					if (!err) {
						logging.consolelog("logging", "", JSON.stringify(data));
						if (data && data.length && data[0].packageCart) {
							for (var i = 0; i < data[0].packageCart.length; i++) {
								logging.consolelog("logging", "", data[0].packageCart.length);
								logging.consolelog("logging", "", i);
								var obj = {
									_id: data[0].packageCart[i]._id,
									curriculum: data[0].packageCart[i].curriculum,
									grade: data[0].packageCart[i].grade,
									language: data[0].packageCart[i].language,
									icon: data[0].packageCart[i].icon
								};
								if (data[0].packageCart[i].monthlyPrice) {
									obj.offerType = "monthlyPrice";
									obj.offerAmount = parseFloat(data[0].packageCart[i].monthlyPrice);
									obj.duration = 1;
								} else if (data[0].packageCart[i].yearlyPrice) {
									obj.offerType = "yearlyPrice";
									obj.offerAmount = parseFloat(data[0].packageCart[i].yearlyPrice);
									obj.duration = 12;
								} else if (data[0].packageCart[i].monthlyDiscount) {
									obj.offerType = "monthlyDiscount";
									obj.offerAmount = parseFloat(data[0].packageCart[i].monthlyDiscount);
									obj.duration = 1;
									obj.typeOfDiscount = data[0].packageCart[i].typeOfDiscount;
								} else if (data[0].packageCart[i].yearlyDiscount) {
									obj.offerType = "yearlyDiscount";
									obj.offerAmount = parseFloat(data[0].packageCart[i].yearlyDiscount);
									obj.duration = 12;
									obj.typeOfDiscount = data[0].packageCart[i].typeOfDiscount;
								}
								//packageDetails.push(obj);
								packageDetailsObj[data[0].packageCart[i].curriculum + "||" + data[0].packageCart[i].grade + "||" + data[0].packageCart[i].language + "||" + data[0].packageCart[i]._id] = obj;
								packageDetailsForDB.push({
									board: data[0].packageCart[i].curriculum,
									grade: data[0].packageCart[i].grade,
									"curriculum.language": data[0].packageCart[i].language
								});
							}
							//fetchPackageDetails(packageDetailsObj, packageDetailsForDB);
						}
						logging.consolelog("logging", "", packageDetails);
						if (data && data.length && data[0].topicCart) {
							for (var i = 0; i < data[0].topicCart.length; i++) {
								topicDetails.push(data[0].topicCart[i]);
							}
						}
						if (data && data.length && data[0].customPackageCart) {
							for (var i = 0; i < data[0].customPackageCart.length; i++) {
								customPackageDetails.push({
									monthlyDiscount: data[0].customPackageCart[i].monthlyDiscount,
									yearlyDiscount: data[0].customPackageCart[i].yearlyDiscount,
									monthlyPrice: data[0].customPackageCart[i].monthlyPrice,
									yearlyPrice: data[0].customPackageCart[i].yearlyPrice,
									packageId: data[0].customPackageCart[i]._id,
									packageName: data[0].customPackageCart[i].packageName,
									packageType: constants.DATABASE.PACKAGE_TYPE.CUSTOMISED_PACKAGE,
									typeOfDiscount: data[0].customPackageCart[i].typeOfDiscount,
									price: 0,
									topicCount: data[0].customPackageCart[i].topics.length,
									topics: data[0].customPackageCart[i].topics,
									icon: data[0].customPackageCart[i].icon
								})
								customPackageTopics = customPackageTopics.concat(data[0].customPackageCart[i].topics);
							}
						}
						cb(null);

					} else {
						cb(err);
					}
				})
			},
			get_custom_package: ['fetch_cart', function (cb) {
				if (userType != constants.USER_TYPE.AGENT) {

					if (customPackageTopics.length) {
						request.body.topics = customPackageTopics;
						getTopics(request, function (data) {
							var lib = data.data.topicLibrary;
							for (var i in lib) {
								topics[lib[i]["Id"]] = lib[i];
							}
							for (var i = 0; i < customPackageDetails.length; i++) {
								var validTopics = [];
								for (var j = 0; j < customPackageDetails[i].topics.length; j++) {

									if (topics[customPackageDetails[i].topics[j]]) {
										customPackageDetails[i].price += parseFloat(topics[customPackageDetails[i].topics[j]]["price"]);
										validTopics.push(topics[customPackageDetails[i].topics[j]]["Id"]);
									}
								}
								customPackageDetails[i]["topics"] = validTopics;
								customPackageDetails[i]["topicCount"] = validTopics.length;
								if (customPackageDetails[i].monthlyPrice) {
									customPackageDetails[i]["grossPrice"] = parseFloat(customPackageDetails[i].price);
									customPackageDetails[i]["price"] = parseFloat(customPackageDetails[i].monthlyPrice);
									customPackageDetails[i]["discount"] = customPackageDetails[i]["grossPrice"] - parseFloat(customPackageDetails[i].monthlyPrice);
									customPackageDetails[i]["duration"] = 1;
									customPackageDetails[i]["offerType"] = "monthlyPrice";
									customPackageDetails[i]["offerAmount"] = parseFloat(customPackageDetails[i].monthlyPrice);
								} else if (customPackageDetails[i].yearlyPrice) {
									customPackageDetails[i]["grossPrice"] = customPackageDetails[i].price;
									customPackageDetails[i]["price"] = parseFloat(customPackageDetails[i].yearlyPrice);
									customPackageDetails[i]["discount"] = customPackageDetails[i]["grossPrice"] - parseFloat(customPackageDetails[i].yearlyPrice);
									customPackageDetails[i]["duration"] = 12;
									customPackageDetails[i]["offerType"] = "yearlyPrice";
									customPackageDetails[i]["offerAmount"] = parseFloat(customPackageDetails[i].yearlyPrice);
								} else if (customPackageDetails[i].monthlyDiscount) {
									customPackageDetails[i]["offerType"] = "monthlyDiscount";
									var price = parseFloat(customPackageDetails[i].price);
									var grossPrice = parseFloat(customPackageDetails[i].price);
									var discount = parseFloat(customPackageDetails[i].monthlyDiscount);
									var type = customPackageDetails[i].typeOfDiscount;
									if (type == "%") {
										customPackageDetails[i]["offerType"] += "(%)";
										price = price - ((discount * price) / 100);
									} else {
										price = price - discount;
									}

									customPackageDetails[i]["price"] = price;
									customPackageDetails[i]["grossPrice"] = grossPrice;
									customPackageDetails[i]["discount"] = grossPrice - price;
									customPackageDetails[i]["duration"] = 1;
									customPackageDetails[i]["offerAmount"] = customPackageDetails[i].monthlyDiscount;
								} else if (customPackageDetails[i].yearlyDiscount) {
									customPackageDetails[i]["offerType"] = "yearlyDiscount";
									var price = customPackageDetails[i].price;
									var grossPrice = customPackageDetails[i].price;
									var discount = customPackageDetails[i].yearlyDiscount;
									var type = customPackageDetails[i].typeOfDiscount;
									if (type == "%") {
										customPackageDetails[i]["offerType"] += "(%)";
										price = price - ((discount * price) / 100);
									} else {
										price = price - discount;
									}
									customPackageDetails[i]["price"] = price;
									customPackageDetails[i]["grossPrice"] = grossPrice;
									customPackageDetails[i]["discount"] = grossPrice - price;
									customPackageDetails[i]["duration"] = 12;
									customPackageDetails[i]["offerAmount"] = parseFloat(customPackageDetails[i].yearlyDiscount);
								}
							}
							cb(null);
						});
					} else {
						cb(null);
					}
				} else {
					cb(null);
				}

            }],
			fetch_topics: ['fetch_cart', function (cb) {

				var topic = [];
				if (topicDetails.length) {
					var topicQuery = {
						"_id": {
							"$in": topicDetails
						},
						"isDeleted": false,
						"isPublish": true
					}
					var topicValidity = request.body.topicType == "YEARLY" ? 12 : 1;
					var date = new Date();
					var endDate = new Date(date.setMonth(date.getMonth() + parseFloat(topicValidity)));
					ServiceTopic.getTopic(topicQuery, {}, {}, function (err, data) {
						for (var i in data) {
							var topicObj = {};
							topicObj.topicId = data[i]["_id"];
							topicObj.price = parseFloat(data[i]["price"]);
							topicObj.lastSeen = new Date();
							topicObj.endDate = endDate;
							topicObj.duration = topicValidity;
							logging.consolelog("logging", "", topicObj);
							topicPrice += data[i]["price"];
							totalCount++;
							topic.push(topicObj);
						}

						objToSave.topic = topic;
						cb(null);
					});
				} else {

					objToSave.topic = topic;
					cb(null);
				}
            }],
			fetch_package_details: ['fetch_cart', function (cb) {
				if (userType != constants.USER_TYPE.AGENT) {
					if (packageDetailsForDB.length) {
						fetchPackageDetails(packageDetailsObj, packageDetailsForDB, function (data) {
							packageDetails = packageDetails.concat(data);
							cb(null);
						});
					} else {
						cb(null);
					}
				} else {
					cb(null);
				}
            }],
			fetch_open_package: ['fetch_topics', function (cb) {
				if (userType != constants.USER_TYPE.AGENT) {
					topicSubtotalPrice = topicPrice;
					if (totalCount) {
						ServicePackage.getAllPackage({
							isDeleted: false,
							packageType: constants.DATABASE.PACKAGE_TYPE.OPEN_PACKAGE,
							noOfTopics: {
								'$lte': totalCount
							}
						}, {}, {
							noOfTopics: -1
						}, {}, function (err, data) {
							if (data && data.length && topicPrice) {
								if (data[0]["monthlyPrice"] && topicType == "MONTHLY") {
									topicDiscountPrice = topicPrice - data[0]["monthlyPrice"];
									topicPrice = data[0]["monthlyPrice"];
									objToSave.topicOfferApplied.offerName = "monthlyPrice";
									objToSave.topicOfferApplied.offerAmount = data[0]["monthlyPrice"];
									objToSave.topicOfferApplied.noOfTopics = data[0]["noOfTopics"];
								} else if (data[0]["yearlyPrice"] && topicType == "YEARLY") {
									topicDiscountPrice = topicPrice - data[0]["yearlyPrice"];
									topicPrice = parseFloat(data[0]["yearlyPrice"]);
									topicSubtotalPrice = topicPrice;
									objToSave.topicOfferApplied.offerName = "yearlyPrice";
									objToSave.topicOfferApplied.offerAmount = data[0]["yearlyPrice"];
									objToSave.topicOfferApplied.noOfTopics = data[0]["noOfTopics"];
								} else if (data[0]["monthlyDiscount"] && topicType == "MONTHLY") {
									objToSave.topicOfferApplied.offerName = "monthlyDiscount";
									objToSave.topicOfferApplied.offerAmount = data[0]["monthlyDiscount"];
									objToSave.topicOfferApplied.noOfTopics = data[0]["noOfTopics"];
									var grossPrice = topicPrice;
									var price = topicPrice;
									var discount = data[0]["monthlyDiscount"];
									var type = data[0].typeOfDiscount;
									logging.consolelog(grossPrice, price, discount);
									if (type == "%" && discount) {
										objToSave.topicOfferApplied.offerName += "(%)";
										price = price - ((discount * price) / 100);
									} else {
										price = price - discount;
									}
									topicDiscountPrice = grossPrice - price;
									topicPrice = price;
									topicSubtotalPrice = grossPrice;
								} else if (data[0]["yearlyDiscount"] && topicType == "YEARLY") {
									objToSave.topicOfferApplied.offerName = "yearlyDiscount";
									objToSave.topicOfferApplied.offerAmount = data[0]["yearlyDiscount"];
									objToSave.topicOfferApplied.noOfTopics = data[0]["noOfTopics"];
									var grossPrice = topicPrice;
									var price = topicPrice;
									var discount = data[0]["yearlyDiscount"];
									var type = data[0].typeOfDiscount;
									logging.consolelog(grossPrice, price, discount);
									if (type == "%" && discount) {
										objToSave.topicOfferApplied.offerName += "(%)";
										price = price - ((discount * price) / 100);
									} else {
										price = price - discount;
									}
									topicDiscountPrice = grossPrice - price;
									topicPrice = price;
									topicSubtotalPrice = grossPrice;
								}
							} else if (topicPrice) {
								if (topicType == "YEARLY") {
									topicPrice = topicPrice;
									topicSubtotalPrice = topicSubtotalPrice;
								}
							}
							cb(null);
						});
					} else {
						cb(null);
					}

				} else {
					cb(null);
				}
            }],
			temp_data: ['fetch_open_package', 'fetch_package_details', 'get_custom_package', function (cb) {
				if (userType == constants.USER_TYPE.AGENT) {
					ServiceCustomerCart.getCart({
						customerId: customerId
					}, {
						content: 1,
						topicArray: 1
					}, {
						lean: true
					}, function (err, data) {
						if (!err) {
							if (data.length > 0) {
								content = data[0].content;
							}


							cb(null);
						} else {
							cb(err);
						}
					});
				} else {
					cb(null);
				}
            }],
			fetch_details: ['fetch_open_package', 'fetch_package_details', 'get_custom_package', 'temp_data', function (cb) {
				if (userType != constants.USER_TYPE.AGENT) {
					var package = [];
					for (var i in packageDetails) {
						var packageObj = {};
						var discountObj = {};
						var date = new Date();
						var endDate = new Date(date.setMonth(date.getMonth() + parseFloat(packageDetails[i].duration ? packageDetails[i].duration : 1)));
						packageObj.packageId = packageDetails[i]["packageId"];
						discountObj.offerName = packageDetails[i].offerType;
						discountObj.offerAmount = parseFloat(packageDetails[i].offerAmount);
						packageObj.discount = discountObj;
						packageObj.price = parseFloat(packageDetails[i].grossPrice);
						packageObj.finalPrice = parseFloat(packageDetails[i].price);
						packageObj.endDate = endDate;
						packageObj.duration = packageDetails[i].duration;
						packageObj.type = packageDetails[i].duration == 12 ? "YEARLY" : "MONTHLY";
						packageObj.lastSeen = new Date();
						objToSave.payments.totalPrice += parseFloat(packageDetails[i].price);
						objToSave.payments.subTotalPrice += parseFloat(packageDetails[i].grossPrice);
						objToSave.payments.discountPrice += parseFloat(packageDetails[i].discount);
						objToSave.packagePayments.totalPrice += parseFloat(packageDetails[i].price);
						objToSave.packagePayments.subTotalPrice += parseFloat(packageDetails[i].grossPrice);
						objToSave.packagePayments.discountPrice += parseFloat(packageDetails[i].discount);
						logging.consolelog("logging", "", packageObj);
						package.push(packageObj);
					}
					var customPackage = [];
					for (var i in customPackageDetails) {
						var packageObj = {};
						var discountObj = {};
						var date = new Date();
						var endDate = new Date(date.setMonth(date.getMonth() + parseFloat(customPackageDetails[i].duration ? customPackageDetails[i].duration : 1)));
						packageObj.packageId = customPackageDetails[i]["packageId"];
						discountObj.offerName = customPackageDetails[i].offerType;
						discountObj.offerAmount = parseFloat(customPackageDetails[i].offerAmount);
						packageObj.discount = discountObj;
						packageObj.price = parseFloat(customPackageDetails[i].grossPrice);
						packageObj.finalPrice = parseFloat(customPackageDetails[i].price);
						packageObj.endDate = endDate;
						packageObj.duration = customPackageDetails[i].duration;
						packageObj.type = customPackageDetails[i].duration == 12 ? "YEARLY" : "MONTHLY";
						packageObj.lastSeen = new Date();
						packageObj.icon = customPackageDetails[i].icon;
						packageObj.topics = customPackageDetails[i].topics;
						packageObj.packageName = customPackageDetails[i].packageName;
						objToSave.payments.totalPrice += parseFloat(customPackageDetails[i].price);
						objToSave.payments.subTotalPrice += parseFloat(customPackageDetails[i].grossPrice);
						objToSave.payments.discountPrice += parseFloat(customPackageDetails[i].discount);
						objToSave.customPackagePayments.totalPrice += parseFloat(customPackageDetails[i].price);
						objToSave.customPackagePayments.subTotalPrice += parseFloat(customPackageDetails[i].grossPrice);
						objToSave.customPackagePayments.discountPrice += parseFloat(customPackageDetails[i].discount);
						logging.consolelog("logging", "", packageObj);
						customPackage.push(packageObj);
					}
					logging.consolelog("logging", "", package);
					objToSave.package = package;
					objToSave.customPackage = customPackage;

					objToSave.customerId = customerId;
					objToSave.paymentStatus = constants.DATABASE.STATUS.PENDING;
					objToSave.transactionId = transactionId;
					objToSave.packageName = request.body.packageName ? request.body.packageName : 'Package Created on' + ' ' + new Date();
					objToSave.paymentCardId = "0847";
					objToSave.buyDate = new Date();
					if (topicPrice) {
						objToSave.topicPayments.subTotalPrice = parseFloat(topicSubtotalPrice);
						objToSave.topicPayments.discountPrice = parseFloat(topicDiscountPrice);
						objToSave.topicPayments.totalPrice = parseFloat(topicPrice);
						objToSave.payments.totalPrice += parseFloat(topicPrice);
						objToSave.payments.subTotalPrice += parseFloat(topicSubtotalPrice);
						objToSave.payments.discountPrice += parseFloat(topicDiscountPrice);
					}
					ServiceCustomerBuyPackages.createTransaction(objToSave, function (err, data) {
						if (!err) {
							cb(null);
						} else {
							cb(err);
						}
					})
				} else {
					var contentData = [];
					var topicValidity = request.body.topicType == "YEARLY" ? 12 : 1;
					var date = new Date();
					var endDate = new Date(date.setMonth(date.getMonth() + parseFloat(topicValidity)));
					for (var i = 0; i < content.length; i++) {
						contentData.push({
							topicId: content[i].topicId,
							categoryId: content[i].categoryId,
							contentType: content[i].contentType,
							startDate: Date.now(),
							endDate: endDate
						})
					}
					objToSave.topicValidity = topicValidity;
					objToSave.content = contentData;
					objToSave.customerId = customerId;
					objToSave.paymentStatus = constants.DATABASE.STATUS.PENDING;
					objToSave.transactionId = transactionId;
					objToSave.packageName = request.body.packageName ? request.body.packageName : 'Package Created on' + ' ' + new Date().getDate() + '/' + new Date().getMonth() + '/' + new Date().getFullYear();
					objToSave.paymentCardId = "0847";
					objToSave.buyDate = new Date();
					objToSave.embeddedLink = request.body.embeddedLink;
					objToSave.domainName = request.body.domainName;
					objToSave.license = request.body.license ? parseInt(request.body.license) : 0;
					ServiceCustomerBuyPackages.createTransaction(objToSave, function (err, data) {
						if (!err) {
							cb(null);
						} else {
							cb(err);
						}
					})
				}

            }],

			redirect_payemnt_gateway: ["fetch_details", function (cb) {
				if (userType == constants.USER_TYPE.AGENT) {
					ServiceCustomerCart.deletePackage({
						customerId: customerId
					}, function (err, data) {
						if (!err) {
							url = "Your request have been submitted to super-admin."
							cb(null);
						} else {
							cb(err);
						}
					})


				} else {
					var workingKey = ccAvenueConfig.ccAvenueCredentials.workingKey, //'64B8D1FE13664821E0F3C48F9271493C', //Put in the 32-Bit key shared by CCAvenues.
						accessCode = ccAvenueConfig.ccAvenueCredentials.accessCode, //'AVPO67DK65AD02OPDA', //Put in the access code shared by CCAvenues.
						encRequest = '',
						merchantId = ccAvenueConfig.ccAvenueCredentials.merchantId;

					var body = "merchant_id=" + merchantId + "&order_id=" + transactionId +
						"&currency=INR&amount=" + encodeURIComponent(objToSave.payments.totalPrice) +
						"&redirect_url=" + encodeURIComponent(ccAvenueConfig.ccAvenueCredentials.transactionCompleted +
							transactionId) + "&cancel_url=" + encodeURIComponent(ccAvenueConfig.ccAvenueCredentials.transactionCompleted + transactionId) +
						"&language=EN&billing_name=gkg&billing_address=okhla&billing_city=delhi&billing_state=delhi&billing_zip=11020&billing_country=india&billing_tel=8054106049&billing_email=" +
						encodeURIComponent("gkg@sterlingpublishers.com") +
						"&delivery_name=gkg&delivery_address=okhla&delivery_city=delhi&delivery_state=delhi&delivery_zip=110020&delivery_country=india&delivery_tel=8527116211&merchant_param1=" +
						customerId + "&merchant_param2=additional&merchant_param3=additional&merchant_param4=additional&merchant_param5=additional&integration_type=iframe_normal&promo_code=&customer_identifier=" +
						customerId;
					encRequest = ccav.encrypt(body, workingKey);
					redirectUrl = ccAvenueConfig.ccAvenueCredentials.redirectUrl + merchantId + '&encRequest=' + encRequest + '&access_code=' + accessCode;
					logging.consolelog("logging", "", body);
					//logging.consolelog(encodedBody);
					//logging.consolelog(formbody);
					cb(null);
				}

            }],
			fetch_superAdmin_ids: ['redirect_payemnt_gateway', function (cb) {
				if (userType == constants.USER_TYPE.AGENT) {
					//fetch all super admin Ids
					ServiceSuperAdmin.getSuperAdmin({}, {
						_id: 1
					}, {
						lean: true
					}, function (err, data) {
						if (!err) {
							for (var i = 0; i < data.length; i++) {
								superAdminIds.push(data[i]._id);
							}
							cb(null);
						} else {
							cb(err);
						}
					})
				} else {
					cb(null);
				}
            }],
            
		},
		function (err, data) {
			console.log("====> transaction completd", redirectUrl, url);
			if (!err) {
				response.send({
					url: redirectUrl,
					agentUrl: url
				});
			} else {
				response.send(err);
			}

			if (userType == constants.USER_TYPE.AGENT) {
				NotificationManager.sendNotifications(superAdminIds, 'PURCHASE_REQUEST', {});
			}
		});
}


var getTopic = function (item, callback) {


	var query = {
		_id: item.topicId
	}
	ServiceTopic.getTopic(query, {
		name: 1,
		averageRating: 1,
		icon: 1
	}, {}, function (err, data) {

		if (!err) {
			if (data.length > 0) {
				callback(null, {
					name: data[0].name,
					rating: data[0].averageRating,
					icon: data[0].icon,
					language: data[0].language
				});
			}

		} else {
			callback(err);
		}
	});
}


var getCart = function (request, response) {
	var topicDetails = [];
	var packageDetails = [];
	var customPackageDetails = [];
	var customPackageTopics = [];
	var packageDetailsObj = {};
	var packageDetailsForDB = [];
	var topics = {};
	var transactionId = "txn" + Date.now();
	var topicPrice = 0;
	var topicDiscountPrice = 0;
	var topicSubtotalPrice = 0;
	var packagePrice = 0;
	var totalPrice = 0;
	var totalCount = 0;
	var topicType = request.body.topicType;
	var objToSave = {
		payments: {
			subTotalPrice: 0,
			discountPrice: 0,
			totalPrice: 0
		},
		topicPayments: {
			subTotalPrice: 0,
			discountPrice: 0,
			totalPrice: 0
		},
		packagePayments: {
			subTotalPrice: 0,
			discountPrice: 0,
			totalPrice: 0
		},
		customPackagePayments: {
			subTotalPrice: 0,
			discountPrice: 0,
			totalPrice: 0
		},
		topicOfferApplied: {
			offerName: "",
			offerAmount: 0,
			noOfTopics: 0
		}
	};
	var packages = {};
	var userType = request.userData.userType;
	var mergedObject = {};
	var mergerdArray = [];
	var final = [];
	var output = [];
	var customerId = request.userData.id;
	async.auto({
			fetch_cart: function (cb) {
				if (userType != constants.USER_TYPE.AGENT) {
					var path = 'packageCart customPackageCart';
					var select = 'curriculum grade language monthlyPrice yearlyPrice monthlyDiscount yearlyDiscount typeOfDiscount packageName topics icon'
					var populate = {
						path: path,
						match: {
							isDeleted: false
						},
						select: select,
						options: {
							lean: true
						}
					};
					ServiceCustomerCart.cartPopulate({
						customerId: customerId
					}, {
						topicCart: 1,
						packageCart: 1,
						customPackageCart: 1
					}, populate, {}, {
						lean: true
					}, function (err, data) {
						if (!err) {
							if (data && data.length && data[0].packageCart) {
								for (var i = 0; i < data[0].packageCart.length; i++) {
									var obj = {
										_id: data[0].packageCart[i]._id,
										curriculum: data[0].packageCart[i].curriculum,
										grade: data[0].packageCart[i].grade,
										language: data[0].packageCart[i].language,
										icon: data[0].packageCart[i].icon
									};
									if (data[0].packageCart[i].monthlyPrice) {
										obj.offerType = "monthlyPrice";
										obj.offerAmount = parseFloat(data[0].packageCart[i].monthlyPrice);
										obj.duration = 1;
									} else if (data[0].packageCart[i].yearlyPrice) {
										obj.offerType = "yearlyPrice";
										obj.offerAmount = parseFloat(data[0].packageCart[i].yearlyPrice);
										obj.duration = 12;
									} else if (data[0].packageCart[i].monthlyDiscount) {
										obj.offerType = "monthlyDiscount";
										obj.offerAmount = parseFloat(data[0].packageCart[i].monthlyDiscount);
										obj.duration = 1;
										obj.typeOfDiscount = data[0].packageCart[i].typeOfDiscount;
									} else if (data[0].packageCart[i].yearlyDiscount) {
										obj.offerType = "yearlyDiscount";
										obj.offerAmount = parseFloat(data[0].packageCart[i].yearlyDiscount);
										obj.duration = 12;
										obj.typeOfDiscount = data[0].packageCart[i].typeOfDiscount;
									}
									//packageDetails.push(obj);
									packageDetailsObj[data[0].packageCart[i].curriculum + "||" + data[0].packageCart[i].grade + "||" + data[0].packageCart[i].language + "||" + data[0].packageCart[i]._id] = obj;
									packageDetailsForDB.push({
										board: data[0].packageCart[i].curriculum,
										grade: data[0].packageCart[i].grade,
										"curriculum.language": data[0].packageCart[i].language
									});
								}
							}
							if (data && data.length && data[0].topicCart) {
								for (var i = 0; i < data[0].topicCart.length; i++) {
									topicDetails.push(data[0].topicCart[i]);
								}
							}
							if (data && data.length && data[0].customPackageCart) {
								for (var i = 0; i < data[0].customPackageCart.length; i++) {
									customPackageDetails.push({
										monthlyDiscount: parseFloat(data[0].customPackageCart[i].monthlyDiscount),
										yearlyDiscount: parseFloat(data[0].customPackageCart[i].yearlyDiscount),
										monthlyPrice: parseFloat(data[0].customPackageCart[i].monthlyPrice),
										yearlyPrice: parseFloat(data[0].customPackageCart[i].yearlyPrice),
										packageId: data[0].customPackageCart[i]._id,
										packageName: data[0].customPackageCart[i].packageName,
										packageType: constants.DATABASE.PACKAGE_TYPE.CUSTOMISED_PACKAGE,
										typeOfDiscount: data[0].customPackageCart[i].typeOfDiscount,
										price: 0,
										topicCount: data[0].customPackageCart[i].topics.length,
										topics: data[0].customPackageCart[i].topics,
										icon: data[0].customPackageCart[i].icon
									})
									customPackageTopics = customPackageTopics.concat(data[0].customPackageCart[i].topics);
								}
							}

							cb(null);

						} else {
							cb(err);
						}

					})
				} else {
					var path = 'content.categoryId';
					var select = 'name isDeleted isPublish content'
					var populate = {
						path: path,
						match: {},
						select: select,
						options: {
							lean: true
						}
					};

					ServiceCustomerCart.cartPopulate({
						customerId: customerId
					}, {
						content: 1,
						topicCart: 1
					}, populate, {}, {}, function (err, data) {
						logging.consolelog(err, JSON.stringify(data), "", "populate data");
						if (!err) {
							if (data.length > 0 && data[0].content) {
								for (var i = 0; i < data[0].content.length; i++) {
									if (data[0].content[i].contentId) {
										for (var j = 0; j < data[0].content[i].categoryId.content.length; j++) {
											if (data[0].content[i].categoryId.content[j]._id == data[0].content[i].contentId) {
												final.push({
													topicId: data[0].content[i].topicId,
													categoryId: data[0].content[i].categoryId._id,
													categoryName: data[0].content[i].categoryId.name,
													contentId: data[0].content[i].contentId,
													contentType: data[0].content[i].categoryId.content[j].typeOfVideo
												});
											}
										}

									} else {
										final.push({
											topicId: data[0].content[i].topicId,
											categoryId: data[0].content[i].categoryId._id,
											categoryName: data[0].content[i].categoryId.name
										});
									}

								}
								for (var i = 0; i < data[0].topicCart.length; i++) {

									final.push({
										topicId: data[0].topicCart[i]
									})
								}
							}

							cb(null);
						} else {
							cb(err);
						}
					});
				}
			},
			merge_objects: ['fetch_cart', function (cb) {

				if (userType == constants.USER_TYPE.AGENT) {
					logging.consolelog(userType, "final", final)
					if (final.length) {
						for (var i in final) {
							if (mergedObject.hasOwnProperty(final[i]["topicId"])) {
								if (final[i]["contentId"]) {
									if (mergedObject[final[i]["topicId"]]["contentType"].indexOf(final[i].contentType) == -1) {
										mergedObject[final[i]["topicId"]]["contentType"].push(final[i]["contentType"]);
									}

								} else {
									if (mergedObject[final[i]["topicId"]]["categoryName"].indexOf(final[i].categoryName) == -1) {
										mergedObject[final[i]["topicId"]]["categoryName"].push(final[i]["categoryName"]);
									}

								}

							} else {
								mergedObject[final[i]["topicId"]] = {
									contentType: final[i]["contentType"] ? [final[i]["contentType"]] : [],
									topicId: final[i]["topicId"],
									categoryName: final[i]["contentType"] ? [] : [final[i]["categoryName"]]
								}
							}
						}
						for (var key in mergedObject) {
							mergerdArray.push(mergedObject[key]);
						}

						logging.consolelog(mergerdArray, "", "mergedArray");
						async.forEach(mergerdArray, function (item, callback) {
							getTopic(item, function (err, result) {
								if (!err) {
									output = output.concat({
										topicId: item.topicId,
										name: result.name,
										rating: result.rating,
										language: result.language,
										icon: result.icon,
										contentType: item.contentType,
										categoryName: item.categoryName
									});
									callback(null);
								} else {
									callback(err);
								}
							});
						}, function (err) {
							if (!err) {
								var object = {};
								logging.consolelog("output array get cat array wise!!!!!!!!", "", output)
								for (var i in output) {
									logging.consolelog("logging", "", object.hasOwnProperty(output[i]["name"]));
									if (object.hasOwnProperty(output[i]["name"])) {
										object[output[i]["name"]]["rating"].push(output[i]["rating"]);
										object[output[i]["name"]]["mergedObject"].push({
											topicId: output[i]["topicId"],
											categoryName: output[i]["categoryName"],
											contentType: output[i]["contentType"],
											language: output[i]["language"]
										});

									} else {
										object[output[i]["name"]] = {
											name: output[i]["name"],
											icon: output[i]["icon"],
											rating: output[i]["rating"] ? [output[i]["rating"]] : [],
											mergedObject: [{
												topicId: output[i]["topicId"],
												categoryName: output[i]["categoryName"],
												contentType: output[i]["contentType"],
												language: output[i]["language"]
                                            }]
										}
									}
								}
								var resultArray = [];
								for (var key in object) {
									resultArray.push(object[key]);
								}
								objToSave.topic = resultArray;
								cb(null);
							} else {

								cb(err);
							}
						});
					} else {
						cb(null);
					}
				} else {
					cb(null);
				}
            }],
			get_custom_package: ['fetch_cart', function (cb) {
				if (userType != constants.USER_TYPE.AGENT) {
					if (customPackageTopics.length) {
						request.body.topics = customPackageTopics;
						getTopics(request, function (data) {
							var lib = data.data.topicLibrary;
							for (var i in lib) {
								topics[lib[i]["Id"]] = lib[i];
							}
							for (var i = 0; i < customPackageDetails.length; i++) {
								var validTopics = [];
								for (var j = 0; j < customPackageDetails[i].topics.length; j++) {

									if (topics[customPackageDetails[i].topics[j]]) {
										customPackageDetails[i].price += parseFloat(topics[customPackageDetails[i].topics[j]]["price"]);
										validTopics.push(topics[customPackageDetails[i].topics[j]]["Id"]);
									}
								}
								customPackageDetails[i]["topics"] = validTopics;
								customPackageDetails[i]["topicCount"] = validTopics.length;
								if (customPackageDetails[i].monthlyPrice) {
									customPackageDetails[i]["grossPrice"] = parseFloat(customPackageDetails[i].price);
									customPackageDetails[i]["price"] = parseFloat(customPackageDetails[i].monthlyPrice);
									customPackageDetails[i]["discount"] = parseFloat(customPackageDetails[i]["grossPrice"]) - parseFloat(customPackageDetails[i].monthlyPrice);
									customPackageDetails[i]["duration"] = 1;
									customPackageDetails[i]["offerType"] = "monthlyPrice";
									customPackageDetails[i]["offerAmount"] = parseFloat(customPackageDetails[i].monthlyPrice);
								} else if (customPackageDetails[i].yearlyPrice) {
									customPackageDetails[i]["grossPrice"] = customPackageDetails[i].price;
									customPackageDetails[i]["price"] = parseFloat(customPackageDetails[i].yearlyPrice);
									customPackageDetails[i]["discount"] = parseFloat(customPackageDetails[i]["grossPrice"]) - parseFloat(customPackageDetails[i].yearlyPrice);
									customPackageDetails[i]["duration"] = 12;
									customPackageDetails[i]["offerType"] = "yearlyPrice";
									customPackageDetails[i]["offerAmount"] = parseFloat(customPackageDetails[i].yearlyPrice);
								} else if (customPackageDetails[i].monthlyDiscount) {
									customPackageDetails[i]["offerType"] = "monthlyDiscount";
									var price = parseFloat(customPackageDetails[i].price);
									var grossPrice = parseFloat(customPackageDetails[i].price);
									var discount = parseFloat(customPackageDetails[i].monthlyDiscount);
									var type = customPackageDetails[i].typeOfDiscount;
									if (type == "%") {
										customPackageDetails[i]["offerType"] += "(%)";
										price = price - ((discount * price) / 100);
									} else {
										price = price - discount;
									}

									customPackageDetails[i]["price"] = parseFloat(price);
									customPackageDetails[i]["grossPrice"] = parseFloat(grossPrice);
									customPackageDetails[i]["discount"] = parseFloat(grossPrice - price);
									customPackageDetails[i]["duration"] = 1;
									customPackageDetails[i]["offerAmount"] = parseFloat(customPackageDetails[i].monthlyDiscount);
								} else if (customPackageDetails[i].yearlyDiscount) {
									customPackageDetails[i]["offerType"] = "yearlyDiscount";
									var price = customPackageDetails[i].price;
									var grossPrice = customPackageDetails[i].price;
									var discount = customPackageDetails[i].yearlyDiscount;
									var type = customPackageDetails[i].typeOfDiscount;
									if (type == "%") {
										customPackageDetails[i]["offerType"] += "(%)";
										price = price - ((discount * price) / 100);
									} else {
										price = price - discount;
									}
									customPackageDetails[i]["price"] = parseFloat(price);
									customPackageDetails[i]["grossPrice"] = parseFloat(grossPrice);
									customPackageDetails[i]["discount"] = parseFloat(grossPrice - price);
									customPackageDetails[i]["duration"] = 12;
									customPackageDetails[i]["offerAmount"] = parseFloat(customPackageDetails[i].yearlyDiscount);
								}
							}
							cb(null);
						});
					} else {
						cb(null);
					}

				} else {
					cb(null);
				}
            }],
			fetch_topics: ['fetch_cart', function (cb) {
				if (userType != constants.USER_TYPE.AGENT) {
					var topic = [];
					if (topicDetails.length) {
						var topicQuery = {
							"_id": {
								"$in": topicDetails
							},
							"isDeleted": false,
							"isPublish": true
						}
						var topicValidity = request.body.topicType == "YEARLY" ? 12 : 1;
						var date = new Date();
						var endDate = new Date(date.setMonth(date.getMonth() + parseInt(topicValidity)));
						ServiceTopic.getTopic(topicQuery, {}, {}, function (err, data) {
							for (var i in data) {
								var topicObj = {};
								topicObj.topicId = data[i]["_id"];
								topicObj.price = data[i]["price"];
								topicObj.lastSeen = new Date();
								topicObj.endDate = endDate;
								topicObj.duration = topicValidity;
								topicObj.name = data[i].name;
								topicObj.rating = data[i].averageRating;
								topicObj.icon = data[i].icon;
								topicPrice += parseFloat(data[i]["price"]);
								totalCount++;
								topic.push(topicObj);
							}

							objToSave.topic = topic;
							cb(null);
						});
					} else {

						objToSave.topic = topic;
						cb(null);
					}

				} else {
					cb(null);
				}
            }],
			fetch_package_details: ['fetch_cart', function (cb) {
				if (userType != constants.USER_TYPE.AGENT) {
					if (packageDetailsForDB.length) {
						fetchPackageDetails(packageDetailsObj, packageDetailsForDB, function (data) {
							packageDetails = packageDetails.concat(data);
							cb(null);
						});
					} else {
						cb(null);
					}
				} else {
					cb(null);
				}
            }],
			fetch_open_package: ['fetch_topics', function (cb) {
				if (userType != constants.USER_TYPE.AGENT) {
					topicSubtotalPrice = topicPrice;
					if (totalCount) {
						ServicePackage.getAllPackage({
							isDeleted: false,
							packageType: constants.DATABASE.PACKAGE_TYPE.OPEN_PACKAGE,
							noOfTopics: {
								'$lte': totalCount
							}
						}, {}, {
							noOfTopics: -1
						}, {}, function (err, data) {
							if (data && data.length && topicPrice) {
								if (data[0]["monthlyPrice"] && topicType == "MONTHLY") {
									topicDiscountPrice = topicPrice - data[0]["monthlyPrice"];
									topicPrice = data[0]["monthlyPrice"];
									objToSave.topicOfferApplied.offerName = "monthlyPrice";
									objToSave.topicOfferApplied.offerAmount = data[0]["monthlyPrice"];
									objToSave.topicOfferApplied.noOfTopics = data[0]["noOfTopics"];
								} else if (data[0]["yearlyPrice"] && topicType == "YEARLY") {
									topicDiscountPrice = topicPrice - data[0]["yearlyPrice"];
									topicPrice = data[0]["yearlyPrice"];
									topicSubtotalPrice = topicPrice;
									objToSave.topicOfferApplied.offerName = "yearlyPrice";
									objToSave.topicOfferApplied.offerAmount = data[0]["yearlyPrice"];
									objToSave.topicOfferApplied.noOfTopics = data[0]["noOfTopics"];
								} else if (data[0]["monthlyDiscount"] && topicType == "MONTHLY") {
									objToSave.topicOfferApplied.offerName = "monthlyDiscount";
									objToSave.topicOfferApplied.offerAmount = data[0]["monthlyDiscount"];
									objToSave.topicOfferApplied.noOfTopics = data[0]["noOfTopics"];
									var grossPrice = topicPrice;
									var price = topicPrice;
									var discount = data[0]["monthlyDiscount"];
									var type = data[0].typeOfDiscount;
									logging.consolelog(grossPrice, price, discount);
									if (type == "%" && discount) {
										objToSave.topicOfferApplied.offerName += "(%)";
										price = price - ((discount * price) / 100);
									} else {
										price = price - discount;
									}
									topicDiscountPrice = parseFloat(grossPrice - price);
									topicPrice = parseFloat(price);
									topicSubtotalPrice = parseFloat(grossPrice);
								} else if (data[0]["yearlyDiscount"] && topicType == "YEARLY") {
									objToSave.topicOfferApplied.offerName = "yearlyDiscount";
									objToSave.topicOfferApplied.offerAmount = parseFloat(data[0]["yearlyDiscount"]);
									objToSave.topicOfferApplied.noOfTopics = parseFloat(data[0]["noOfTopics"]);
									var grossPrice = parseFloat(topicPrice);
									var price = topicPrice;
									var discount = parseFloat(data[0]["yearlyDiscount"]);
									var type = data[0].typeOfDiscount;
									logging.consolelog(grossPrice, price, discount);
									if (type == "%" && discount) {
										objToSave.topicOfferApplied.offerName += "(%)";
										price = price - ((discount * price) / 100);
									} else {
										price = price - discount;
									}
									topicDiscountPrice = parseFloat(grossPrice - price);
									topicPrice = parseFloat(price);
									topicSubtotalPrice = parseFloat(grossPrice);
								}
							} else if (topicPrice) {
								if (topicType == "YEARLY") {
									topicPrice = topicPrice;
									topicSubtotalPrice = topicSubtotalPrice;
								}
							}
							cb(null);
						});
					} else {
						cb(null);
					}
				} else {
					cb(null);
				}
            }],
			fetch_details: ['fetch_open_package', 'fetch_package_details', 'get_custom_package', function (cb) {
				if (userType != constants.USER_TYPE.AGENT) {
					var package = [];
					for (var i in packageDetails) {
						var packageObj = {};
						var discountObj = {};
						var date = new Date();
						var endDate = new Date(date.setMonth(date.getMonth() + parseInt(packageDetails[i].duration ? packageDetails[i].duration : 1)));
						packageObj.packageId = packageDetails[i]["packageId"];
						discountObj.offerName = packageDetails[i].offerType;
						discountObj.offerAmount = packageDetails[i].offerAmount;
						packageObj.discount = discountObj;
						packageObj.price = packageDetails[i].grossPrice;
						packageObj.finalPrice = packageDetails[i].price;
						packageObj.endDate = endDate;
						packageObj.duration = packageDetails[i].duration;
						packageObj.curriculum = packageDetails[i].curriculum;
						packageObj.grade = packageDetails[i].grade;
						packageObj.language = packageDetails[i].language;
						packageObj.icon = packageDetails[i].icon;
						packageObj.subjectCount = packageDetails[i].subjectCount;
						packageObj.topicCount = packageDetails[i].topicCount;
						packageObj.topics = packageDetails[i].topics;
						packageObj.chapterCount = packageDetails[i].chapterCount;
						packageObj.type = packageDetails[i].duration == 12 ? "YEARLY" : "MONTHLY";
						packageObj.lastSeen = new Date();
						objToSave.payments.totalPrice += packageDetails[i].price;
						objToSave.payments.subTotalPrice += packageDetails[i].grossPrice;
						objToSave.payments.discountPrice += packageDetails[i].discount;
						objToSave.packagePayments.totalPrice += packageDetails[i].price;
						objToSave.packagePayments.subTotalPrice += packageDetails[i].grossPrice;
						objToSave.packagePayments.discountPrice += packageDetails[i].discount;
						logging.consolelog("logging", "", packageObj);
						package.push(packageObj);
					}
					var customPackage = [];
					for (var i in customPackageDetails) {
						var packageObj = {};
						var discountObj = {};
						var date = new Date();
						var endDate = new Date(date.setMonth(date.getMonth() + parseInt(customPackageDetails[i].duration ? customPackageDetails[i].duration : 1)));
						packageObj.packageId = customPackageDetails[i]["packageId"];
						discountObj.offerName = customPackageDetails[i].offerType;
						discountObj.offerAmount = parseFloat(customPackageDetails[i].offerAmount);
						packageObj.discount = discountObj;
						packageObj.price = parseFloat(customPackageDetails[i].grossPrice);
						packageObj.finalPrice = parseFloat(customPackageDetails[i].price);
						packageObj.endDate = endDate;
						packageObj.duration = customPackageDetails[i].duration;
						packageObj.type = customPackageDetails[i].duration == 12 ? "YEARLY" : "MONTHLY";
						packageObj.lastSeen = new Date();
						packageObj.icon = customPackageDetails[i].icon;
						packageObj.topics = customPackageDetails[i].topics;
						packageObj.packageName = customPackageDetails[i].packageName;
						objToSave.payments.totalPrice += parseFloat(customPackageDetails[i].price);
						objToSave.payments.subTotalPrice += parseFloat(customPackageDetails[i].grossPrice);
						objToSave.payments.discountPrice += parseFloat(customPackageDetails[i].discount);
						objToSave.customPackagePayments.totalPrice += parseFloat(customPackageDetails[i].price);
						objToSave.customPackagePayments.subTotalPrice += parseFloat(customPackageDetails[i].grossPrice);
						objToSave.customPackagePayments.discountPrice += parseFloat(customPackageDetails[i].discount);
						logging.consolelog("logging", "", packageObj);
						customPackage.push(packageObj);
					}
					logging.consolelog("logging", "", package);
					objToSave.package = package;
					objToSave.customPackage = customPackage;

					if (topicPrice) {
						objToSave.topicPayments.subTotalPrice = parseFloat(topicSubtotalPrice);
						objToSave.topicPayments.discountPrice = parseFloat(topicDiscountPrice);
						objToSave.topicPayments.totalPrice = parseFloat(topicPrice);
						objToSave.payments.totalPrice += parseFloat(topicPrice);
						objToSave.payments.subTotalPrice += parseFloat(topicSubtotalPrice);
						objToSave.payments.discountPrice += parseFloat(topicDiscountPrice);
					}
					cb(null);
				} else {
					cb(null);
				}
            }]
		},
		function (err, data) {
			if (!err) {
				response.status(200).send({
					statusCode: 200,
					data: objToSave,
					output: output
				})
			} else {
				response.send(err);
			}
		})
}

function getCustomerPurchases(customerId, callback) {
	var topics = [];
	var packages = {};
	var packageDetails = [];
	async.series([
        function (cb) {
			var query = {
				'customerId': customerId,
				'paymentStatus': constants.DATABASE.STATUS.COMPLETED
			};
			var path = 'package.packageId';
			var select = 'name icon language  price curriculum grade';
			var populate = {
				path: path,
				match: {},
				select: select,
				options: {
					lean: true
				}
			};

			ServiceCustomerBuyPackages.getPackages(query, {
				topic: 1,
				package: 1,
				packageName: 1,
				buyDate: 1,
				customerId: 1
			}, populate, {}, function (err, data) {
				logging.consolelog(err, data, "data::purchase topic data")
				if (err) {
					cb(err);
				} else {
					if (data.length) {
						for (var i in data) {
							var topicData = [];
							if (data && data.length && data[i].topic) {

								topicData = data[i].topic;
								getTopics = true;
							}
							for (var j in topicData) {
								if (new Date() < new Date(topicData[j].endDate))
									topics.push(topicData[j]["topicId"]);
							}
							var packageData = [];

							if (data && data.length && data[i].package && data[i].package.length) {
								packageData = data[i].package;
								for (var j in packageData) {
									if (new Date() < new Date(packageData[j].endDate)) {
										if (packageData[j].packageId) {
											var packageObj = {};
											packageObj.grade = packageData[j].packageId.grade;
											packageObj["curriculum.language"] = packageData[j].packageId.language;
											packageObj.board = packageData[j].packageId.curriculum;
											packageDetails.push(packageObj);
											packages[packageData[j].packageId.curriculum + "||" + packageData[j].packageId.grade + "||" + packageData[j].packageId.language + "||" + packageData[j].packageId._id] = {
												curriculum: packageData[j].packageId.curriculum,
												language: packageData[j].packageId.language,
												grade: packageData[j].packageId.grade,
												packageId: packageData[j].packageId["_id"],
												icon: packageData[j].packageId["icon"]
											}
										}
									}

								}
							}
						}
					}
					cb(null);
				}
			})
        },
        function (cb) {
			if (packageDetails.length) {
				fetchPackageDetails(packages, packageDetails, function (data) {
					if (data && data.length) {
						for (var i in data) {
							topics = topics.concat(data[i].topics);
						}
					}
					logging.consolelog("logging fetch package details ", "", JSON.stringify(topics));
					cb(null);
				});
			} else {
				cb(null);
			}
        }], function () {
		callback(topics);
	});

}

function fetchPackageDetails(packages, packageObj, callback) {
	var final = {};
	var finalPackage = [];
	ServicePackage.getPackageDetails(packageObj, function (err, data) {
		if (data && data.length) {
			for (var i in data) {
				var key = data[i].board + "||" + data[i].grade + "||" + data[i].language;
				for (var j in packages) {
					if (j.indexOf(key) != -1) {
						packages[j]["price"] = data[i].price;
						packages[j]["topicCount"] = data[i].topicCount;
						packages[j]["subjectCount"] = data[i].subjectCount;
						packages[j]["chapterCount"] = data[i].chapterCount;
						packages[j]["board"] = data[i].board;
						packages[j]["grade"] = data[i].grade;
						packages[j]["language"] = data[i].language;
						packages[j]["topics"] = data[i].topics;
					}
				}
			}
		}
		for (var key in packages) {
			if (packages[key]["topicCount"]) {
				if (packages[key].offerType == "monthlyPrice") {
					packages[key]["price"] = packages[key].offerAmount;
					packages[key]["grossPrice"] = parseInt(packages[key].price);
					packages[key]["discount"] = packages[key]["grossPrice"] - parseInt(packages[key].offerAmount);
				} else if (packages[key].offerType == "yearlyPrice") {
					packages[key]["price"] = packages[key].offerAmount;
					packages[key]["grossPrice"] = parseInt(packages[key].price);
					packages[key]["discount"] = packages[key]["grossPrice"] - parseInt(packages[key].offerAmount);
				} else if (packages[key].offerType == "monthlyDiscount") {
					var grossPrice = parseInt(packages[key].price);
					var price = parseInt(packages[key].price);
					var discount = parseInt(packages[key].offerAmount);
					var type = packages[key].typeOfDiscount;
					logging.consolelog(grossPrice, price, discount);
					if (type == "%" && discount) {
						price = price - ((discount * price) / 100);
					} else {
						price = price - discount;
					}

					packages[key]["price"] = price;
					packages[key]["grossPrice"] = grossPrice;
					packages[key]["discount"] = grossPrice - price;
				} else if (packages[key].offerType == "yearlyDiscount") {
					var grossPrice = parseInt(packages[key].price);
					var price = parseInt(packages[key].price);
					var discount = packages[key].offerAmount;
					var type = packages[key].typeOfDiscount;
					if (type == "%" && discount) {
						price = price - ((discount * price) / 100);
					} else {
						price = price - discount;
					}
					packages[key]["price"] = price;
					packages[key]["grossPrice"] = grossPrice;
					packages[key]["discount"] = grossPrice - price;
				}
				finalPackage.push({
					packageId: packages[key]["_id"],
					curriculum: packages[key].curriculum,
					language: packages[key].language,
					icon: packages[key].icon,
					price: packages[key].price,
					grossPrice: packages[key].grossPrice,
					discount: packages[key].discount,
					topicCount: packages[key].topicCount,
					chapterCount: packages[key].chapterCount,
					grade: packages[key].grade,
					subjectCount: packages[key].subjectCount,
					topics: packages[key].topics.split(","),
					offerType: packages[key].offerType,
					offerAmount: packages[key].offerAmount,
					duration: packages[key].duration,
					typeOfDiscount: packages[key].typeOfDiscount
				});
			}
		}
		callback(finalPackage);

	});

}

var transactionSuccessful = function (request, response) {
	var amount=0;
	async.series([
        function (cb) {
			ServiceCustomerCart.deletePackage({
				customerId: request.body.customerId
			}, function (err, data) {
				if (!err) {
					cb(null);
				} else {
					cb(err);
				}
			})
        },
		function (cb) {
			var query = {
				transactionId: request.body.transactionId
			};
			var dataToSet = {
				paymentStatus: constants.DATABASE.STATUS.COMPLETED
			}
			ServiceCustomerBuyPackages.updateTransaction(query, dataToSet, {}, function (err, data) {
				if (!err) {
					cb(null);
				} else {
					cb(err);
				}
			});
        },
        function(cb){
         var query = {
				transactionId: request.body.transactionId
			};
			
			ServiceCustomerBuyPackages.getTransaction(query, {payments:1}, {lean:true}, function (err, data) {
				if (!err) {
					if(data.length>0){
						amount=data[0].payments.totalPrice;
					}
					cb(null);
				} else {
					cb(err);
				}
			});

        },
        (cb)=>{

        	ServiceCustomer.getCustomer({_id:request.body.customerId},{emailId:1, firstName:1},{lean:true},(err,data)=>{

        		if(!err){
        			if(data.length>0){
        				var emailSend = {
				emailId: data[0].emailId,
				name: data[0].firstName,
				amount: amount,
				transactionId: request.body.transactionId
			}
			var text;
			text = Templates.invoice(emailSend);
			NotificationManager.sendEmailToUser(data[0].emailId, 'PixelsEd Payment Successful', text, function (err, data) {
				if (!err) {
					logging.consolelog('mail', err, data);
				}
				cb(null);
			});
        			}
        		}else{
        			cb(null);
        		}
        	})
                

            	
            }],
        function (err, data) {
		if (!err) {
			response.status(200).send({
				statusCode: 200
			})
		} else {
			response.send(err);
		}
	});
}

var transactionCompleted = function (request, response) {
	logging.consolelog("hit rcvd at transactionCompleted", "", "");
	var ccavEncResponse = '',
		ccavResponse = '',
		workingKey = 'ABF1BC5B9D06B45F769873F98650F5CA', //'64B8D1FE13664821E0F3C48F9271493C', //Put in the 32-Bit key provided by CCAvenues.
		ccavPOST = '';

	var encryption = request.body.encResp;
	ccavResponse = ccav.decrypt(encryption, workingKey);
	var ccavResponseObj = qs.parse(ccavResponse);
	if (ccavResponseObj.order_status == "Success") {
		async.series([
            function (cb) {
				ServiceCustomerCart.deletePackage({
					customerId: ccavResponseObj.merchant_param1
				}, function (err, data) {
					if (!err) {
						cb(null);
					} else {
						cb(err);
					}
				})
            },
			function (cb) {
				var query = {
					transactionId: ccavResponseObj.order_id
				};
				var dataToSet = {
					paymentStatus: constants.DATABASE.STATUS.COMPLETED
				}
				ServiceCustomerBuyPackages.updateTransaction(query, dataToSet, {}, function (err, data) {
					if (!err) {
						cb(null);
					} else {
						cb(err);
					}
				});
            }], function (err, data) {
			response.redirect(Config.get('ccAvenueConfig.sucessRedirectUrl') + 'Your transaction ID for this payment is: ' + ccavResponseObj.order_id);
		});

	} else if (ccavResponseObj.order_status == "Failure" || ccavResponseObj.order_status == "Aborted" || ccavResponseObj.order_status == "Invalid") {
		var redirectFailureUrl = Config.get('ccAvenueConfig.failRedirectUrl');
		if (ccavResponseObj.order_status == "Failure") {
			redirectFailureUrl += constants.paymentMSG.Failure + ccavResponseObj.failure_message ? ccavResponseObj.failure_message : '' + ' Please try again. Your transaction ID is: ' + ccavResponseObj.order_id;
		} else if (ccavResponseObj.order_status == "Aborted") {
			redirectFailureUrl += constants.paymentMSG.Aborted + ccavResponseObj.order_id;
		} else if (ccavResponseObj.order_status == "Invalid") {
			redirectFailureUrl += constants.paymentMSG.Invalid + ccavResponseObj.order_id;
		}
		async.series([function (cb) {
			ServiceCustomerBuyPackages.deleteTransaction({
				transactionId: ccavResponseObj.order_id
			}, function (err, data) {
				if (!err) {
					cb(null);
				} else {
					cb(err);
				}
			})
        }], function (err, data) {
			response.redirect(redirectFailureUrl);
		});
	}
};

var transactionFailed = function (request, response) {
	async.series([
        function (cb) {
			ServiceCustomerBuyPackages.deleteTransaction({
				transactionId: request.body.transactionId
			}, function (err, data) {
				if (!err) {
					cb(null);
				} else {
					cb(err);
				}
			})
        }], function (err, data) {
		if (!err) {
			response.status(200).send({
				statusCode: 200
			})
		} else {
			response.send(err);
		}
	});
}

var markFavourite = function (request, response) {
	async.series([
            function (cb) {
				var query = {
					'customerId': request.userData.id
				};
				ServiceFavourites.updateFavourite(query, {
					$addToSet: {
						"topicId": request.body.topicId
					},
					$set: {
						'customerId': request.userData.id
					}
				}, {
					upsert: true
				}, function (err, data) {
					if (err) {
						cb(err);
					} else {
						cb(null);
					}
				})
            }
        ],
		function (err, data) {
			if (!err) {
				response.status(200).send(constants.STATUS_MSG.SUCCESS.MARKED_FAV);
			} else {
				response.send(err);
			}
		})
}

var saveNote = function (request, response) {
	async.series([
            function (cb) {
				var query = {
					'customerId': request.userData.id,
					'topics.topicId': request.body.topicId.toString()
				};
				ServiceNotes.updateNote(query, {
					$set: {
						'customerId': request.userData.id,
						'topics.notes': request.body.note,
						'topics.topicId': request.body.topicId.toString()
					}
				}, {
					upsert: true
				}, function (err, data) {
					if (err) {
						cb(err);
					} else {
						cb(null);
					}
				})
            }
        ],
		function (err, data) {
			if (!err) {
				response.status(200).send(constants.STATUS_MSG.SUCCESS.SAVED_NOTE);
			} else {
				response.send(err);
			}
		})
}

var getNote = function (request, response) {
	var topics = [];
	var final = [];
	async.series([
            function (cb) {
				var query = {
					'customerId': request.userData.id,
					'topics.topicId': request.body.topicId.toString()
				};
				logging.consolelog('query', query, '');
				ServiceNotes.getNote(query, {
					topics: 1,
					customerId: 1
				}, {}, function (err, data) {
					logging.consolelog("logging", "", data);
					if (err) {
						cb(err);
					} else {
						if (data && data[0] && data[0].topics && data[0].topics)
							topics = data[0].topics;
						cb(null);
					}
				})
            }
        ],
		function (err, data) {
			if (!err) {
				response.status(200).send(topics);
			} else {
				response.send(err);
			}
		})
}

var unmarkFavourite = function (request, response) {
	async.series([
            function (cb) {
				var query = {
					'customerId': request.userData.id
				};
				ServiceFavourites.updateFavourite(query, {
					$pullAll: {
						"topicId": [request.body.topicId]
					}
				}, {}, function (err, data) {
					if (err) {
						cb(err);
					} else {
						cb(null);
					}
				})
            }
        ],
		function (err, data) {
			if (!err) {
				response.status(200).send(constants.STATUS_MSG.SUCCESS.UNMARKED_FAV);
			} else {
				response.send(err);
			}
		})
}

var getFavourite = function (customerId, callback) {
	var topics = [];
	var history = [];
	async.series([
            function (cb) {
				var query = {
					'customerId': customerId
				};
				ServiceFavourites.getFavourite(query, {
					topicId: 1,
					customerId: 1,
					topicHistory: 1
				}, {}, function (err, data) {
					logging.consolelog("logging", "", data);
					if (err) {
						cb(err);
					} else {
						if (data && data[0] && data[0].topicId && data[0].topicId.length)
							topics = topics.concat(data[0].topicId);
						if (data && data[0] && data[0].topicHistory && data[0].topicHistory.length)
							history = history.concat(data[0].topicHistory);
						cb(null);
					}
				})
            }
        ],
		function (err, data) {
			callback(topics, history);
		})
}


var getCustomerPackages = function (request, response) {
	var topics = [];
	var packages = [];
	var packageDetails = [];
	var topicPackages = [];
	var closedPackage = [];
	var customPackage = [];
	var final = [];
	var packageNames = [];
	var userId = request.userData.id,
		userType = request.userData.userType;
	var assignedPackages = [];
	var superParentCustomerId = [];
	var agentPackage = [];
	var domainWise = false;
	async.auto({
		fetch_data: function (cb) {
			var query = {
				assignedTo: userId
			};
			var path = 'assignedBy';
			var select = 'firstName lastName'
			var populate = {
				path: path,
				match: {},
				select: select,
				options: {
					lean: true
				}
			};
			var options = {
				lean: true
			};
			var projection = {
				assignedBy: 1,
				content: 1,
				topic: 1
			}

			ServiceAssignDataToCustomer.assignDataToCustomerPopulate(query, projection, populate, {}, {}, function (err, data) {
				logging.consolelog(err, data, "assigned data package populate pacakage");
				if (!err) {
					for (var i = 0; i < data.length; i++) {
						var topic = [];
						if (data[i].content) {
							for (var k = 0; k < data[i].content.length; k++) {
								if (data[i].content[k].isActive == true || data[i].content[k].isActive == undefined) {
									if (new Date() < new Date(data[i].content[k].endDate)) {
										topic.push(data[i].content[k].topicId);
									}
								}


							}
						}
						var unique = _.unique(topic);
						assignedPackages.push({
							packageName: data[i].assignedBy.firstName + '' + data[i].assignedBy.lastName,
							packageId: data[i]._id,
							topicCount: unique.length,
							type: "ASSIGNED_PACKAGE"
						})
					}
					cb(null);
				} else {
					cb(err);
				}
			})
		},
		get_customer_detail: function (cb) {
			// check domain name for domain type of agents
			ServiceCustomer.getCustomer({
				_id: userId
			}, {
				emailId: 1
			}, {
				lean: true
			}, function (err, data) {
				if (!err) {
					if (data.length > 0) {
						var email = data[0].emailId;
						//get domain of customer
						var domain = email.substring(email.lastIndexOf("@") + 1);
						logging.consolelog("print domain of customer", "", domain);
						var query = {
							userType: constants.USER_TYPE.AGENT,
							domainName: domain
						}
						ServiceCustomer.getCustomer(query, {
							_id: 1
						}, {}, function (err, data) {
							logging.consolelog(err, data, "check domain agent");
							if (!err) {
								if (data.length > 0) {
									for (var i = 0; i < data.length; i++) {
										superParentCustomerId.push(data[i]._id);
										domainWise = true;
									}

								}
								cb(null);
							} else {
								cb(err);
							}
						})
					}
				} else {
					cb(err);
				}
			})
		},
		get_packages: ['fetch_data', 'get_customer_detail', function (cb) {
			logging.consolelog("2", "", userType);

			if (userType == constants.USER_TYPE.AGENT) {
				//find super parent;
				ServiceChildMapper.getChildParentMapper({
					childCustomerId: userId
				}, {
					superParentCustomerId: 1
				}, {
					lean: true
				}, function (err, data) {
					if (!err) {
						if (data.length > 0) {
							superParentCustomerId.push(data[0].superParentCustomerId);

						}
						cb(null);
					} else {
						cb(err);
					}
				});
			} else {
				var query = {
					'customerId': userId,
					'paymentStatus': constants.DATABASE.STATUS.COMPLETED
				};
				var path = 'package.packageId';
				var select = 'name icon language  price curriculum grade';
				var populate = {
					path: path,
					match: {},
					select: select,
					options: {
						lean: true
					}
				};
				if (request.body.search) {
					populate["match"]["$text"] = {
						$search: request.body.search
					};
				}

				ServiceCustomerBuyPackages.getPackages(query, {
					topic: 1,
					package: 1,
					customPackage: 1,
					packageName: 1,
					buyDate: 1,
					customerId: 1
				}, populate, {}, function (err, data) {
					// logging.consolelog(JSON.stringify(data));
					if (err) {
						cb(err);
					} else {
						if (data.length) {
							for (var i in data) {
								var topicCount = 0;
								var topicData = [];
								if (data && data.length && data[i].topic) {
									for (var k = 0; k < data[i].topic.length; k++) {
										if (new Date() < new Date(data[i].topic[k].endDate)) {
											topicData.push(data[i].topic[k]);
										}
									}
								}
								for (var j in topicData) {
									topicCount++;
								}
								if (topicCount && data[i].packageName) {
									topicPackages.push({
										name: data[i].packageName,
										topic: topicCount,
										type: "TOPIC_PACKAGE",
										id: data[i]["_id"]
									});
								}
								var customPackageData = [];
								if (data && data.length && data[i].customPackage) {
									for (var m = 0; m < data[i].customPackage.length; m++) {
										if (new Date() < new Date(data[i].customPackage[m].endDate)) {
											customPackageData.push(data[i].customPackage[m]);
										}
									}

								}
								for (var j in customPackageData) {
									customPackage.push({
										packageId: customPackageData[j].packageId,
										packageName: customPackageData[j].packageName,
										packageType: constants.DATABASE.PACKAGE_TYPE.CUSTOMISED_PACKAGE,
										topicCount: customPackageData[j].topics.length,
										topics: customPackageData[j].topics,
										icon: customPackageData[j].icon
									})
								}
								var packageData = [];
								if (data && data.length && data[i].package && data[i].package.length) {
									for (var k = 0; k < data[i].package.length; k++) {
										if (new Date() < new Date(data[i].package[k].endDate))
											packageData.push(data[i].package[k]);
									}
								}

								for (var j in packageData) {
									if (packageData[j].packageId) {
										logging.consolelog("packageData", "", packageData[j].packageId);
										var packageObj = {};
										packageObj.grade = packageData[j].packageId.grade;
										packageObj["curriculum.language"] = packageData[j].packageId.language;
										packageObj.board = packageData[j].packageId.curriculum;
										packageDetails.push(packageObj);
										logging.consolelog("logging", "", packageObj)
										packageNames.push({
											name: packageData[j].packageId.grade + ", " + packageData[j].packageId.language + ", " + packageData[j].packageId.grade,
											id: packageData[j].packageId["_id"],
											type: "CLOSED_PACKAGE"
										});
										packages[packageData[j].packageId.curriculum + "||" + packageData[j].packageId.grade + "||" + packageData[j].packageId.language + "||" + packageData[j].packageId._id] = {
											curriculum: packageData[j].packageId.curriculum,
											language: packageData[j].packageId.language,
											grade: packageData[j].packageId.grade,
											packageId: packageData[j].packageId["_id"],
											icon: packageData[j].packageId["icon"]

										}
									}
								}

								if (data[i].packageName) {
									packageNames.push({
										name: data[i].packageName,
										id: data[i]._id,
										type: "TOPIC_PACKAGE"
									});
								}
							}
						}
					}
					cb(null);
				});
			}

        }],
		seggregate_packages: ['get_packages', function (cb) {
			logging.consolelog("3", "", userType);
			if (userType == constants.USER_TYPE.AGENT || domainWise == true) {
				var query = {
					'customerId': {
						$in: superParentCustomerId
					},
					'paymentStatus': constants.DATABASE.STATUS.COMPLETED
				};
				var path = 'package.packageId';
				var select = 'name icon language  price curriculum grade';
				var populate = {
					path: path,
					match: {},
					select: select,
					options: {
						lean: true
					}
				};
				if (request.body.search) {
					populate["match"]["$text"] = {
						$search: request.body.search
					};
				}

				ServiceCustomerBuyPackages.getPackages(query, {
					topic: 1,
					package: 1,
					customPackage: 1,
					packageName: 1,
					buyDate: 1,
					customerId: 1,
					content: 1
				}, populate, {}, function (err, data) {
					logging.consolelog("logging", "", JSON.stringify(data));
					if (err) {
						cb(err);
					} else {
						if (data.length) {
							for (var i in data) {
								var topicCount = 0;
								var topicData = [];
								if (data && data.length && data[i].topic.length > 0) {
									for (var j = 0; j < data[i].topic.length; j++) {
										if (new Date() < new Date(data[i].topic[j].endDate))
											topicData.push(data[i].topic[j].topicId);
									}
								}
								if (data && data.length && data[i].content.length > 0) {
									for (var k = 0; k < data[i].content.length; k++) {
										if (data[i].content[k].isActive == true || data[i].content[k].isActive == undefined) {
											if (new Date() < new Date(data[i].content[k].endDate))
												topicData.push(data[i].content[k].topicId);
										}

									}
								}
								topicData = _.unique(topicData);
								for (var m in topicData) {
									topicCount++;
								}
								if (topicCount && data[i].packageName) {
									agentPackage.push({
										name: data[i].packageName,
										topic: topicCount,
										type: "AGENT_PACKAGE",
										id: data[i]["_id"]
									});
								}
							}
						}
					}
					cb(null);
				});
			} else {
				cb(null);
			}
        }],
		last_function: ['seggregate_packages', function (cb) {
			if (packageDetails.length) {
				ServicePackage.getPackageDetails(packageDetails, function (err, data) {
					if (data && data.length) {
						logging.consolelog("logging", "", data);
						for (var i in data) {
							var key = data[i].board + "||" + data[i].grade + "||" + data[i].language;
							for (var j in packages) {
								if (j.indexOf(key) != -1) {
									packages[j]["price"] = data[i].price;
									packages[j]["topicCount"] = data[i].topicCount;
									packages[j]["subjectCount"] = data[i].subjectCount;
									packages[j]["chapterCount"] = data[i].chapterCount;
									packages[j]["board"] = data[i].board;
									packages[j]["grade"] = data[i].grade;
									packages[j]["language"] = data[i].language;
								}
							}
						}
						for (var key in packages) {
							closedPackage.push({
								packageId: packages[key].packageId,
								curriculum: packages[key].curriculum,
								language: packages[key].language,
								topic: packages[key].topicCount,
								chapter: packages[key].chapterCount,
								grade: packages[key].grade,
								subject: packages[key].subjectCount,
								icon: packages[key].icon
							})
						}
					}
					cb(null);
				});
			} else {
				cb(null);
			}
        }]
	}, function (err, data) {
		if (!err) {
			response.status(200).send({
				topicPackages: topicPackages,
				closedPackage: closedPackage,
				customPackage: customPackage,
				packageNames: packageNames,
				assignedPackage: assignedPackages,
				agentPackage: agentPackage
			});
		} else {
			response.send(err);
		}
	})
}

var getExpiredPackages = function (request, response) {
	var userData = request.userData;
	var topics = [];
	var packages = [];
	var packageDetails = [];
	var topicPackages = [];
	var closedPackage = [];
	var customPackage = [];
	var final = [];
	var packageNames = [];
	var assignedPackages = [];
	var superParentCustomerId = [];
	var agentPackage = [];
	var domainWise = false;

	async.auto({
		expired_assigned_data: function (cb) {
			var query = {
				assignedTo: userData.id,
				"content.endDate": {
					$lte: (new Date()).toISOString()
				}
			};
			var path = 'assignedBy';
			var select = 'firstName lastName'
			var populate = {
				path: path,
				match: {},
				select: select,
				options: {
					lean: true
				}
			};
			var options = {
				lean: true
			};
			var projection = {
				assignedBy: 1,
				"content.$": 1,
				topic: 1
			}

			ServiceAssignDataToCustomer.assignDataToCustomerPopulate(query, projection, populate, {}, {}, function (err, data) {
				logging.consolelog(err, data, "assigned data package populate pacakage");
				if (!err) {
					for (var i = 0; i < data.length; i++) {
						var topic = [];
						if (data[i].content) {
							for (var k = 0; k < data[i].content.length; k++) {
								if (data[i].content[k].isActive == true || data[i].content[k].isActive == undefined) {
									topic.push(data[i].content[k].topicId);
								}
							}
						}
						var unique = _.unique(topic);
						assignedPackages.push({
							packageName: data[i].assignedBy.firstName + '' + data[i].assignedBy.lastName,
							packageId: data[i]._id,
							topicCount: unique.length,
							type: "ASSIGNED_PACKAGE"
						})
					}
					cb(false, null);
				} else {
					cb(err, null);
				}
			})
		},
		check_domain: function (cb) {
			// check domain name for domain type of agents
			var email = userData.emailId;
			//get domain of customer
			var domain = email.substring(email.lastIndexOf("@") + 1);
			logging.consolelog("print domain of customer", "", domain);
			var query = {
				userType: constants.USER_TYPE.AGENT,
				domainName: domain
			}
			ServiceCustomer.getCustomer(query, {
				_id: 1
			}, {}, function (err, data) {
				logging.consolelog(err, data, "check domain agent");
				if (!err) {
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							superParentCustomerId.push(data[i]._id);
							domainWise = true;
						}
					}
					cb(false, null);
				} else {
					cb(err, null);
				}
			})
		},
		get_expired_packages: ['expired_assigned_data', 'check_domain', function (cb) {

			if (userData.userType == constants.USER_TYPE.AGENT) {
				//find super parent;
				ServiceChildMapper.getChildParentMapper({
					childCustomerId: userData.id
				}, {
					superParentCustomerId: 1
				}, {
					lean: true
				}, function (err, data) {
					if (!err) {
						if (data.length > 0) {
							superParentCustomerId.push(data[0].superParentCustomerId);

						}
						cb(null);
					} else {
						cb(err);
					}
				});
			} else {
				var query = {
					'customerId': userData.id,
					'paymentStatus': constants.DATABASE.STATUS.COMPLETED,
					'isDeleted': false,
					"topic.endDate": {
					$lte: (new Date()).toISOString()
				}
				};
				var path = 'package.packageId';
				var select = 'name icon language  price curriculum grade';
				var populate = {
					path: path,
					match: {},
					select: select,
					options: {
						lean: true
					}
				};
				if (request.body.search) {
					populate["match"]["$text"] = {
						$search: request.body.search
					};
				}
 				console.log(query, "===> service customer query ====> get packages");
				ServiceCustomerBuyPackages.getPackages(query, {
					topic: 1,
					package: 1,
					customPackage: 1,
					packageName: 1,
					buyDate: 1,
					customerId: 1
				}, populate, {}, function (err, data) {
					logging.consolelog("logging get packages", "", JSON.stringify(data));
					if (err) {
						cb(err);
					} else {
						if (data.length) {
							for (var i in data) {
								var topicCount = 0;
								var topicData = [];
								if (data && data.length && data[i].topic) {
									for (var k = 0; k < data[i].topic.length; k++) {
										if (new Date() > new Date(data[i].topic[k].endDate)) {
											topicData.push(data[i].topic[k]);
										}
									}
								}
								for (var j in topicData) {
									topicCount++;
								}
								if (topicCount && data[i].packageName) {
									topicPackages.push({
										name: data[i].packageName,
										topic: topicCount,
										type: "TOPIC_PACKAGE",
										id: data[i]["_id"]
									});
								}
								var customPackageData = [];
								if (data && data.length && data[i].customPackage) {
									for (var m = 0; m < data[i].customPackage.length; m++) {
										if (new Date() > new Date(data[i].customPackage[m].endDate)) {
											customPackageData.push(data[i].customPackage[m]);
										}
									}

								}
								for (var j in customPackageData) {
									customPackage.push({
										packageId: customPackageData[j].packageId,
										packageName: customPackageData[j].packageName,
										packageType: constants.DATABASE.PACKAGE_TYPE.CUSTOMISED_PACKAGE,
										topicCount: customPackageData[j].topics.length,
										topics: customPackageData[j].topics,
										icon: customPackageData[j].icon
									})
								}
								var packageData = [];
								if (data && data.length && data[i].package && data[i].package.length) {
									for (var k = 0; k < data[i].package.length; k++) {
										if (new Date() > new Date(data[i].package[k].endDate))
											packageData.push(data[i].package[k]);
									}
								}

								for (var j in packageData) {
									if (packageData[j].packageId) {
										logging.consolelog("packageData", "", packageData[j].packageId);
										var packageObj = {};
										packageObj.grade = packageData[j].packageId.grade;
										packageObj["curriculum.language"] = packageData[j].packageId.language;
										packageObj.board = packageData[j].packageId.curriculum;
										packageDetails.push(packageObj);
										logging.consolelog("logging", "", packageObj)
										packageNames.push({
											name: packageData[j].packageId.grade + ", " + packageData[j].packageId.language + ", " + packageData[j].packageId.grade,
											id: packageData[j].packageId["_id"],
											type: "CLOSED_PACKAGE"
										});
										packages[packageData[j].packageId.curriculum + "||" + packageData[j].packageId.grade + "||" + packageData[j].packageId.language + "||" + packageData[j].packageId._id] = {
											curriculum: packageData[j].packageId.curriculum,
											language: packageData[j].packageId.language,
											grade: packageData[j].packageId.grade,
											packageId: packageData[j].packageId["_id"],
											icon: packageData[j].packageId["icon"]

										}
									}
								}

								if (data[i].packageName) {
									packageNames.push({
										name: data[i].packageName,
										id: data[i]._id,
										type: "TOPIC_PACKAGE"
									});
								}
							}
						}
					}
					cb(null);
				});
			}
        }],
		seggregate_packages: ['get_expired_packages', function (cb) {
			logging.consolelog("3", "", userData.userType);
			if (userData.userType == constants.USER_TYPE.AGENT || domainWise == true) {
				var query = {
					'customerId': {
						$in: superParentCustomerId
					},
					'paymentStatus': constants.DATABASE.STATUS.COMPLETED,
					'isDeleted': false
				};
				var path = 'package.packageId';
				var select = 'name icon language  price curriculum grade';
				var populate = {
					path: path,
					match: {},
					select: select,
					options: {
						lean: true
					}
				};
				if (request.body.search) {
					populate["match"]["$text"] = {
						$search: request.body.search
					};
				}

				ServiceCustomerBuyPackages.getPackages(query, {
					topic: 1,
					package: 1,
					customPackage: 1,
					packageName: 1,
					buyDate: 1,
					customerId: 1,
					content: 1
				}, populate, {}, function (err, data) {
					logging.consolelog(JSON.stringify(data), "", "get pacakges of agent");
					if (err) {
						cb(err);
					} else {
						if (data.length) {
							for (var i in data) {
								var topicCount = 0;
								var topicData = [];
								if (data && data.length && data[i].topic.length > 0) {
									for (var j = 0; j < data[i].topic.length; j++) {
										if (new Date() > new Date(data[i].topic[j].endDate))
											topicData.push(data[i].topic[j].topicId);
									}
								}
								if (data && data.length && data[i].content.length > 0) {
									for (var k = 0; k < data[i].content.length; k++) {
										if (data[i].content[k].isActive == true || data[i].content[k].isActive == undefined) {
											if (new Date() > new Date(data[i].content[k].endDate))
												topicData.push(data[i].content[k].topicId);
										}

									}
								}
								topicData = _.unique(topicData);
								for (var m in topicData) {
									topicCount++;
								}
								if (topicCount && data[i].packageName) {
									agentPackage.push({
										name: data[i].packageName,
										topic: topicCount,
										type: "AGENT_PACKAGE",
										id: data[i]["_id"]
									});
								}
							}
						}
					}
					cb(null);
				});
			} else {
				cb(null);
			}
        }],
		last_function: ['seggregate_packages', function (cb) {
			if (packageDetails.length) {
				ServicePackage.getPackageDetails(packageDetails, function (err, data) {
					if (data && data.length) {
						logging.consolelog("logging", "", data);
						for (var i in data) {
							var key = data[i].board + "||" + data[i].grade + "||" + data[i].language;
							for (var j in packages) {
								if (j.indexOf(key) != -1) {
									packages[j]["price"] = data[i].price;
									packages[j]["topicCount"] = data[i].topicCount;
									packages[j]["subjectCount"] = data[i].subjectCount;
									packages[j]["chapterCount"] = data[i].chapterCount;
									packages[j]["board"] = data[i].board;
									packages[j]["grade"] = data[i].grade;
									packages[j]["language"] = data[i].language;
								}
							}
						}
						for (var key in packages) {
							closedPackage.push({
								packageId: packages[key].packageId,
								curriculum: packages[key].curriculum,
								language: packages[key].language,
								topic: packages[key].topicCount,
								chapter: packages[key].chapterCount,
								grade: packages[key].grade,
								subject: packages[key].subjectCount,
								icon: packages[key].icon
							})
						}
					}
					cb(null);
				});
			} else {
				cb(null);
			}
        }]

	}, function (err, data) {
		if (!err) {
			response.status(200).send({
				topicPackages: topicPackages,
				closedPackage: closedPackage,
				customPackage: customPackage,
				packageNames: packageNames,
				assignedPackage: assignedPackages,
				agentPackage: agentPackage
			});
		} else {
			response.send(err);
		}
	})
}

//var getCustomerPackages = function (request, response) {
//	var topics = [];
//	var packages = [];
//	var packageDetails = [];
//	var topicPackages = [];
//	var closedPackage = [];
//	var customPackage = [];
//	var final = [];
//	var packageNames = [];
//	async.series([
//			function (cb) {
//				var query = {
//					'customerId': request.body.customerId,
//					'paymentStatus': constants.DATABASE.STATUS.COMPLETED
//				};
//				var path = 'package.packageId';
//				var select = 'name icon language  price curriculum grade';
//				var populate = {
//					path: path,
//					match: {},
//					select: select,
//					options: {
//						lean: true
//					}
//				};
//				if (request.body.search) {
//					populate["match"]["$text"] = {
//						$search: request.body.search
//					};
//				}
//
//				ServiceCustomerBuyPackages.getPackages(query, {
//					topic: 1,
//					package: 1,
//					customPackage: 1,
//					packageName: 1,
//					buyDate: 1,
//					customerId: 1
//				}, populate, {}, function (err, data) {
//					logging.consolelog(JSON.stringify(data));
//					if (err) {
//						cb(err);
//					} else {
//						if (data.length) {
//							for (var i in data) {
//								var topicCount = 0;
//								var topicData = [];
//								if (data && data.length && data[i].topic) {
//									topicData = data[i].topic;
//								}
//								for (var j in topicData) {
//									topicCount++;
//								}
//								if (topicCount && data[i].packageName) {
//									topicPackages.push({
//										name: data[i].packageName,
//										topic: topicCount,
//										type: "TOPIC_PACKAGE",
//										id: data[i]["_id"]
//									});
//								}
//								var customPackageData = [];
//								if (data && data.length && data[i].customPackage) {
//									customPackageData = data[i].customPackage;
//								}
//								for (var j in customPackageData) {
//									customPackage.push({
//										packageId: customPackageData[j].packageId,
//										packageName: customPackageData[j].packageName,
//										packageType: constants.DATABASE.PACKAGE_TYPE.CUSTOMISED_PACKAGE,
//										topicCount: customPackageData[j].topics.length,
//										topics: customPackageData[j].topics,
//										icon: customPackageData[j].icon
//									})
//								}
//								var packageData = [];
//								if (data && data.length && data[i].package && data[i].package.length) {
//									packageData = data[i].package;
//								}
//								for (var j in packageData) {
//									if (packageData[j].packageId) {
//										logging.consolelog("packageData", packageData[j].packageId);
//										var packageObj = {};
//										packageObj.grade = packageData[j].packageId.grade;
//										packageObj["curriculum.language"] = packageData[j].packageId.language;
//										packageObj.board = packageData[j].packageId.curriculum;
//										packageDetails.push(packageObj);
//										logging.consolelog(packageObj)
//										packageNames.push({
//											name: packageData[j].packageId.grade + ", " + packageData[j].packageId.language + ", " + packageData[j].packageId.grade,
//											id: packageData[j].packageId["_id"],
//											type: "CLOSED_PACKAGE"
//										});
//										packages[packageData[j].packageId.curriculum + "||" + packageData[j].packageId.grade + "||" + packageData[j].packageId.language + "||" + packageData[j].packageId._id] = {
//											curriculum: packageData[j].packageId.curriculum,
//											language: packageData[j].packageId.language,
//											grade: packageData[j].packageId.grade,
//											packageId: packageData[j].packageId["_id"],
//											icon: packageData[j].packageId["icon"]
//
//										}
//									}
//								}
//
//								if (data[i].packageName) {
//									packageNames.push({
//										name: data[i].packageName,
//										id: data[i]._id,
//										type: "TOPIC_PACKAGE"
//									});
//								}
//							}
//						}
//					}
//					cb(null);
//				})
//			},
//		function (cb) {
//				if (packageDetails.length) {
//					logging.consolelog(packageDetails);
//					logging.consolelog(packages);
//					ServicePackage.getPackageDetails(packageDetails, function (err, data) {
//						if (data && data.length) {
//							logging.consolelog(data);
//							for (var i in data) {
//								var key = data[i].board + "||" + data[i].grade + "||" + data[i].language;
//								for (var j in packages) {
//									if (j.indexOf(key) != -1) {
//										packages[j]["price"] = data[i].price;
//										packages[j]["topicCount"] = data[i].topicCount;
//										packages[j]["subjectCount"] = data[i].subjectCount;
//										packages[j]["chapterCount"] = data[i].chapterCount;
//										packages[j]["board"] = data[i].board;
//										packages[j]["grade"] = data[i].grade;
//										packages[j]["language"] = data[i].language;
//									}
//								}
//							}
//							for (var key in packages) {
//								closedPackage.push({
//									packageId: packages[key].packageId,
//									curriculum: packages[key].curriculum,
//									language: packages[key].language,
//									topic: packages[key].topicCount,
//									chapter: packages[key].chapterCount,
//									grade: packages[key].grade,
//									subject: packages[key].subjectCount,
//									icon: packages[key].icon
//								})
//							}
//						}
//						cb(null);
//					});
//				} else {
//					cb(null);
//				}
//		}],
//		function (err, data) {
//			if (!err) {
//				response.status(200).send({
//					topicPackages: topicPackages,
//					closedPackage: closedPackage,
//					customPackage: customPackage,
//					packageNames: packageNames
//				});
//			} else {
//				response.send(err);
//			}
//		})
//}

var getTopics = function (request, callback) {
	var topics = [];
	var final = {
		data: {}
	};
	var search = "";
	var topicHistory = [];
	var topicIds = [];

	var offsetCount = 0;
	var limitCount = 10;
	async.series([
            function (cb) {
				try {
					getFavourite(request.body.customerId, function (data) {
						topicIds = topicIds.concat(data);
						logging.consolelog("logging", "", topicIds);
						cb(null);
					})
				} catch (e) {
					logging.consolelog("Some error occured while fetching fav for getTopics!", e, "");
					cb(null);
				}
            },
            function (cb) {
				var query = {
					'board': constants.COMMON_VARIABLES.UNIVERSAL_BOARD_NAME,
					'isRemoved': false,
					'isDeleted': false,
					'isPublish': true
				}
				var sort = {
					sortBy: "sequenceOrder"
				};
				if (request.body.board) {
					prepareFilter('board', request.body.board, query);
				}
				if (request.body.language) {
					prepareFilter('curriculum.language', request.body.language, query);
				}
				if (request.body.grade) {
					prepareFilter('grade', request.body.grade, query);
				}
				if (request.body.subject) {
					prepareFilter('subject', request.body.subject, query);
				}
				if (request.body.chapter) {
					prepareFilter('chapter', request.body.chapter, query);
				}

				if (request.body.offset) {
					offsetCount = request.body.offset;
				}
				if (request.body.limit) {
					limitCount = request.body.limit;
				}

				if (request.body.topics) {
					query.topicId = {
						$in: request.body.topics
					}
				}

				if (request.body.search) {
					search = " and match(name, authorName, description, gtinCode, topic.language) against ('" + request.body.search + "*' IN BOOLEAN MODE)";
				}

				var limit = {
					limit: limitCount,
					offset: offsetCount
				}
				final["count"] = 0;
				final["offset"] = offsetCount;
				final["limit"] = limitCount;

				ServiceTopic.getTopicsByCurriculum(query, sort, limit, search, function (err, data) {
					var curriculumsIds = [];
					if (!err) {
						if (data.length > 0) {
							data.forEach(function (curriculum) {
								if (curriculum && curriculum.name) {
									topics.push({
										Id: curriculum["_id"],
										name: curriculum.name,
										icon: curriculum.icon,
										authorName: curriculum.authorName,
										averageRating: curriculum.averageRating,
										price: curriculum.price,
										description: curriculum.description,
										board: curriculum.board,
										subject: curriculum.subject,
										language: curriculum.boardLanguage,
										topicLanguage: curriculum.topicLanguage,
										grade: curriculum.grade,
										chapter: curriculum.chapter,
										addedDate: curriculum.addedDate,
										isFavourite: topicIds.indexOf(curriculum["_id"] + "") != -1 ? true : false
									});
								}
							});
							final["data"]["topicLibrary"] = topics;
							ServiceTopic.getTopicsByCurriculumCount(query, {}, {}, search, function (err, countData) {
								if (!err) {
									final["count"] = countData ? countData[0]["cnt"] : 0;
								}
								cb(null);
							});
						} else {
							cb(null);
						}
					} else {
						cb(constants.STATUS_MSG.ERROR.TOPIC_NOT_FOUND);
					}
				});
            }],
		function (err, data) {
			callback(final);
		})
}


var getCustomerTopicPackages = function (request, response) {
	var topic = [];
	var packages = [];
	var packageNames = [];
	var userType = request.userData.userType,
		userId = request.userData.id;
	var superParentCustomerId = [];
	var topics = [];
	var topicIds = [];
	var final = {
		data: {}
	};
	var search = "";
	var assignedTopics = [];
	var domainWise = false;
	async.auto({
		get_customer_detail: function (cb) {
			// check domain name for domain type of agents
			ServiceCustomer.getCustomer({
				_id: userId
			}, {
				emailId: 1
			}, {
				lean: true
			}, function (err, data) {
				if (!err) {
					if (data.length > 0) {
						var email = data[0].emailId;
						//get domain of customer
						var domain = email.substring(email.lastIndexOf("@") + 1);
						var query = {
							userType: constants.USER_TYPE.AGENT,
							domainName: domain
						}
						ServiceCustomer.getCustomer(query, {
							_id: 1
						}, {}, function (err, data) {
							if (!err) {
								if (data.length > 0) {
									for (var i = 0; i < data.length; i++) {
										superParentCustomerId.push(data[i]._id);
										domainWise = true;
									}

								}
								cb(null);
							} else {
								cb(err);
							}
						})
					}
				} else {
					cb(err);
				}
			})


		},
		get_superparent: ['get_customer_detail', function (cb) {

			//fetch super parent of customer who is sharing

			if (userType == constants.USER_TYPE.AGENT) {
				ServiceChildMapper.getChildParentMapper({
					childCustomerId: userId
				}, {
					superParentCustomerId: 1
				}, {
					lean: true
				}, function (err, data) {
					if (!err) {
						if (data.length > 0) {
							superParentCustomerId.push(data[0].superParentCustomerId);

						}
						cb(null);
					} else {
						cb(err);
					}
				});


			} else {
				var query = {
					'customerId': userId,
					'paymentStatus': constants.DATABASE.STATUS.COMPLETED
				};
				var path = 'package.packageId customPackage.packageId';
				var select = 'name icon language price curriculum grade topics';
				var populate = {
					path: path,
					match: {},
					select: select,
					options: {
						lean: true
					}
				};

				ServiceCustomerBuyPackages.getPackages(query, {
					topic: 1,
					package: 1,
					customPackage: 1,
					packageName: 1,
					buyDate: 1,
					customerId: 1
				}, populate, {}, function (err, data) {
					logging.consolelog("logging", "", JSON.stringify(data));
					if (err) {
						cb(err);
					} else {
						if (data.length) {
							for (var i in data) {
								var topicData = data[i].topic;
								if (topicData.length > 0) {
									for (var j in topicData) {

										if (new Date() < new Date(topicData[j].endDate))
											topic.push(topicData[j].topicId);
									}
								}

								var customPackageData = data[i].customPackage;
								for (var j in customPackageData) {
									if (customPackageData[j].packageId && customPackageData[j].packageId.topics && customPackageData[j].packageId.topics.length) {
										if (new Date() < new Date(customPackageData[j].endDate))
											topic = topic.concat(customPackageData[j].packageId.topics);
									}
								}
								var packageData = data[i].package;
								for (var j in packageData) {
									logging.consolelog("logging", "", packageData[j].packageId);
									var packageObj = {};
									packageObj.grade = packageData[j].packageId.grade;
									packageObj["curriculum.language"] = packageData[j].packageId.language;
									packageObj.board = packageData[j].packageId.curriculum;
									packages.push(packageObj);
									packageNames.push({
										name: packageData[j].packageId.grade + ", " + packageData[j].packageId.language + ", " + packageData[j].packageId.grade,
										id: packageData[j].packageId["_id"],
										type: "CLOSED_PACKAGE"
									});
								}
								if (data[i].packageName) {
									packageNames.push({
										name: data[i].packageName,
										id: data[i]._id,
										type: "TOPIC_PACKAGE"
									});
								}
							}
						}
					}

					cb(null);
				})
			}

        }],
		get_topics: ['get_superparent', function (cb) {
			if (userType == constants.USER_TYPE.AGENT || domainWise == true) {
				logging.consolelog(superParentCustomerId, "", "super parent customer Id of agent");
				var query = {
					customerId: {
						$in: superParentCustomerId
					},
					paymentStatus: constants.DATABASE.STATUS.COMPLETED
				}
				ServiceCustomerBuyPackages.getTransaction(query, {
					topic: 1,
					content: 1
				}, {}, function (err, data) {
					if (!err) {
						logging.consolelog("logging", err, data);
						for (var i = 0; i < data.length; i++) {
							// for (var j = 0; j < data[i].topic.length; j++) {
							//     if (new Date() < new Date(data[i].topic[j].endDate))
							//         topic.push(data[i].topic[j].topicId);
							// }
							for (var k = 0; k < data[i].content.length; k++) {
								if (data[i].content[k].isActive == true || data[i].content[k].isActive == undefined) {
									if (new Date() < new Date(data[i].content[k].endDate))
										topic.push(data[i].content[k].topicId);
								}

							}

						}
						cb(null);
					} else {
						cb(err);
					}
				})


			} else {

				cb(null);
			}
        }],
		middle: ['get_topics', function (cb) {
			if (packages.length > 0) {
				ServiceTopicCurriculumMapper.getTopicIdsByCurriculum(packages, function (err, data) {
					console.log("data of packages", JSON.stringify(data));
					for (var i in data) {
						topic.push(data[i].topicId);
					}

					cb(null);
				})
			} else {
				cb(null);
			}
        }],
		share_topics: ['get_topics', 'middle', function (cb) {

			var query = {
				assignedTo: userId
			}
			ServiceAssignDataToCustomer.getAssignDataToCustomer(query, {
				content: 1,
				topic: 1
			}, {
				lean: true
			}, function (err, data) {
				if (!err) {
					for (var i = 0; i < data.length; i++) {
						if (data[i].topic) {
							for (var j = 0; j < data[i].topic.length; j++) {

								if (new Date() < new Date(data[i].topic[j].endDate)) {
									topic.push(data[i].topic[j]);
									assignedTopics.push(data[i].topic[j]);
								}

							}
						}
						if (data[i].content) {
							for (var k = 0; k < data[i].content.length; k++) {
								if (data[i].content[k].isActive == true || data[i].content[k].isActive == undefined) {
									if (new Date() < new Date(data[i].content[k].endDate)) {
										topic.push(data[i].content[k].topicId);
										assignedTopics.push(data[i].content[k].topicId);
									}
								}
							}
						}
					}
					cb(null);
				} else {
					cb(err);
				}
			})
        }],
		topic_detail: ['share_topics', function (cb) {
			if ((userType == constants.USER_TYPE.AGENT || userType == constants.USER_TYPE.SUB_AGENT || userType == constants.USER_TYPE.SCHOOL || domainWise == true) && topic && topic.length > 0) {
				topic = _.unique(topic);
				logging.consolelog('topicId count>>>>>', '', topic);
				var query = {
					'board': constants.COMMON_VARIABLES.UNIVERSAL_BOARD_NAME,
					'isRemoved': false,
					'isDeleted': false,
					'isPublish': true
				}
				var sort = {
					sortBy: "sequenceOrder"
				};
				if (request.body.board) {
					prepareFilter('board', request.body.board, query);
				}
				if (request.body.language) {
					prepareFilter('curriculum.language', request.body.language, query);
				}
				if (request.body.grade) {
					prepareFilter('grade', request.body.grade, query);
				}
				if (request.body.subject) {
					prepareFilter('subject', request.body.subject, query);
				}
				if (request.body.chapter) {
					prepareFilter('chapter', request.body.chapter, query);
				}

				if (request.body.offset) {
					var offsetCount = request.body.offset;
				}
				if (request.body.limit) {
					var limitCount = request.body.limit;
				}

				if (topic && topic.length > 0) {
					query.topicId = {
						$in: topic
					}
				}

				if (request.body.search) {
					search = " and (match(name, authorName, description, gtinCode, topic.language) against " +
						"('+" + '"' + request.body.search + '"' + "' IN BOOLEAN MODE) OR chapter like '%" +
						request.body.search + "%' OR subject like '%" + request.body.search + "%')";
				}

				var limit = {
					limit: limitCount,
					offset: offsetCount
				}
				final["count"] = 0;
				final["offset"] = offsetCount;
				final["limit"] = limitCount;

				ServiceTopic.getTopicsByCurriculum(query, sort, limit, search, function (err, data) {
					if (!err) {
						if (data.length > 0) {
							data.forEach(function (curriculum) {
								if (curriculum && curriculum.name) {
									topics.push({
										Id: curriculum["_id"],
										name: curriculum.name,
										icon: curriculum.icon,
										authorName: curriculum.authorName,
										averageRating: curriculum.averageRating,
										price: curriculum.price,
										description: curriculum.description,
										board: curriculum.board,
										subject: curriculum.subject,
										language: curriculum.boardLanguage,
										topicLanguage: curriculum.topicLanguage,
										grade: curriculum.grade,
										chapter: curriculum.chapter,
										addedDate: curriculum.addedDate
									});
								}
							});
							final["data"]["topicLibrary"] = topics;
							ServiceTopic.getTopicsByCurriculumCount(query, {}, {}, search, function (err, countData) {
								if (!err) {
									final["count"] = countData ? countData[0]["cnt"] : 0;
								}
								cb(null);
							});
						} else {
							cb(null);
						}


					} else {
						cb(err);
					}
				});
			} else {

				cb(null);

			}
        }],
		get_favorite: ['topic_detail', function (cb) {

			try {
				getFavourite(request.body.customerId, function (data) {
					topicIds = topicIds.concat(data);
					logging.consolelog("logging", "", topicIds);
					cb(null);
				})
			} catch (e) {
				logging.consolelog("Some error occured while fetching fav for getTopics!", e, "");
				cb(null);
			}
        }],
		final_call: ['topic_detail', 'get_favorite', "middle", function (cb) {
			if (userType == constants.USER_TYPE.AGENT || userType == constants.USER_TYPE.SUB_AGENT || userType == constants.USER_TYPE.SCHOOL || domainWise == true) {
				cb(null);
			} else if (topic && topic.length > 0) {
				topic = _.unique(topic);
				var query = {
					'board': constants.COMMON_VARIABLES.UNIVERSAL_BOARD_NAME,
					'isRemoved': false,
					'isDeleted': false,
					'isPublish': true
				}
				var sort = {
					sortBy: "sequenceOrder"
				};
				if (request.body.board) {
					prepareFilter('board', request.body.board, query);
				}
				if (request.body.language) {
					prepareFilter('curriculum.language', request.body.language, query);
				}
				if (request.body.grade) {
					prepareFilter('grade', request.body.grade, query);
				}
				if (request.body.subject) {
					prepareFilter('subject', request.body.subject, query);
				}
				if (request.body.chapter) {
					prepareFilter('chapter', request.body.chapter, query);
				}

				if (request.body.offset) {
					var offsetCount = request.body.offset;
				}
				if (request.body.limit) {
					var limitCount = request.body.limit;
				}

				if (topic && topic.length > 0) {
					query.topicId = {
						$in: topic
					}
				}

				if (request.body.search) {
					search = " and (match(name, authorName, description, gtinCode, topic.language) against " +
						"('+" + '"' + request.body.search + '"' + "' IN BOOLEAN MODE) OR chapter like '%" +
						request.body.search + "%' OR subject like '%" + request.body.search + "%')";
				}

				var limit = {
					limit: limitCount,
					offset: offsetCount
				};
				final["count"] = 0;
				final["offset"] = offsetCount;
				final["limit"] = limitCount;
				logging.consolelog(query, search, "check query");

				ServiceTopic.getTopicsByCurriculum(query, sort, limit, search, function (err, data) {
					if (!err) {
						if (data.length > 0) {
							data.forEach(function (curriculum) {
								if (curriculum && curriculum.name) {
									topics.push({
										Id: curriculum["_id"],
										name: curriculum.name,
										icon: curriculum.icon,
										authorName: curriculum.authorName,
										averageRating: curriculum.averageRating,
										price: curriculum.price,
										description: curriculum.description,
										board: curriculum.board,
										subject: curriculum.subject,
										language: curriculum.boardLanguage,
										topicLanguage: curriculum.topicLanguage,
										grade: curriculum.grade,
										chapter: curriculum.chapter,
										addedDate: curriculum.addedDate,
										isFavourite: topicIds.indexOf(curriculum["_id"] + "") != -1 ? true : false,
										isAssigned: assignedTopics.indexOf(curriculum["_id"] + "") != -1 ? true : false
									});
								}
							});
							final["data"]["topicLibrary"] = topics;
							ServiceTopic.getTopicsByCurriculumCount(query, {}, {}, search, function (err, countData) {
								if (!err) {
									final["count"] = countData ? countData[0]["cnt"] : 0;
								}
								cb(null);
							});
						} else {
							cb(null);
						}
					} else {
						cb(constants.STATUS_MSG.ERROR.TOPIC_NOT_FOUND);
					}
				});
			} else {
				cb(null)
			}
        }]
	}, function (err, data) {
		if (!err) {
			response.status(200).send({
				topics: final,
				statusCode: 200
			})
		} else {
			response.send(err);
		}
	})
}


var getCategoryContent = function (request, response) {
	var purchasedTopics = [];
	var contentLinking = [];
	var customerId = request.userData.id;
	var assignedTopics = [];
	var assignedContentTopics = [];
	var isDomain = false;
	var superParentCustomerId = [];
	var userType = request.userData.userType;
	var agentPurchasedTopics = [];
	var demo = false;
	async.series([

        function (cb) {
			ServiceCustomer.getCustomer({
				_id: customerId
			}, {
				emailId: 1,
				registrationDate: 1
			}, {
				lean: true
			}, function (err, data) {
				if (!err) {
					if (data.length > 0) {
						var date = Date.now();
						var oneDay = 24 * 60 * 60 * 1000;
						var firstDate = new Date(date);
						var registeredDate = data[0].registrationDate;
						var secondDate = new Date(registeredDate);
						var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
						if (diffDays <= 7) {
							demo = true;

						} else {
							demo = false;
						}
						var email = data[0].emailId;
						//get domain of customer
						var domain = email.substring(email.lastIndexOf("@") + 1);
						var query = {
							userType: constants.USER_TYPE.AGENT,
							domainName: domain
						}
						ServiceCustomer.getCustomer(query, {
							_id: 1
						}, {}, function (err, data) {
							if (!err) {
								if (data.length > 0) {
									for (var i = 0; i < data.length; i++) {
										superParentCustomerId.push(data[i]._id);
										isDomain = true;
									}

								}
								cb(null);
							} else {
								cb(err);
							}
						})
					}
				} else {
					cb(err);
				}
			});
        },
		function (cb) {
			getCustomerPurchases(customerId, function (data) {
				logging.consolelog("data::output::purchased data", "", data);
				console.log(JSON.stringify(data), "purchased Data")
				for (var i = 0; i < data.length; i++) {
					purchasedTopics.push(parseInt(data[i]));
				}
				if (demo == true) {
					purchasedTopics.push(parseInt(request.body.topicId));
				}
				cb(null);
			})
        },
        function (cb) {
			logging.consolelog("data of assigned topics");
			getAssignedTopics(customerId, request.body.topicId, function (err, data) {
				logging.consolelog("data of assigned topics", "", data);
				assignedTopics = assignedTopics.concat(data.topic);
				assignedContentTopics = assignedContentTopics.concat(data.content);

				cb(null);
			})
        },
        function (cb) {
			if (userType == constants.USER_TYPE.AGENT) {

				ServiceChildMapper.getChildParentMapper({
					childCustomerId: customerId
				}, {
					superParentCustomerId: 1
				}, {
					lean: true
				}, function (err, data) {
					if (!err) {
						if (data.length > 0) {
							superParentCustomerId.push(data[0].superParentCustomerId);
						}
						cb(null);
					} else {
						cb(err);
					}
				});
			} else {
				cb(null);
			}

        },
        function (cb) {
			if (userType == constants.USER_TYPE.AGENT || isDomain == true) {
				getAgentPurchases(superParentCustomerId, function (err, data) {
					agentPurchasedTopics = agentPurchasedTopics.concat(data.content);

					cb(null);
				})

			} else {
				cb(null);
			}


        },
        function (cb) {
			var query = {
				categoryId: request.body.categoryId
			}

			var path = 'categoryId subCategoryId';
			var select = 'name price isPublish icon language content isDeleted isFree name price icon language content isDeleted content isFree'

			var populate = {
				path: path,
				match: {
					isDeleted: false
				},
				select: select,
				options: {
					lean: true
				}
			};
			var projection = {
				categoryId: 1,
				subCategoryId: 1,
				topicId: 1
			}
			ServiceTopicBinding.topicBindingPopulate(query, projection, populate, {}, {}, function (err, data) {
				logging.consolelog(err, data, "topicBindingPopulate");
				if (!err) {
					var i = 0;
					var length = data.length;
					var topic = {};

					var categories = [];
					var content = [];
					var embeddedLinks = [];

					logging.consolelog("agent Purchased topics", "", agentPurchasedTopics);
					for (var i = 0; i < agentPurchasedTopics.length; i++) {
						categories.push(agentPurchasedTopics[i].categoryId._id.toString());
						if (agentPurchasedTopics[i].contentType) {
							content.push(agentPurchasedTopics[i].contentType);
						}
						if (agentPurchasedTopics[i].embeddedLink) {
							if (agentPurchasedTopics[i].contentType)
								embeddedLinks.push(agentPurchasedTopics[i].contentType.toString());
						}

					}

					for (var i = 0; i < assignedContentTopics.length; i++) {
						categories.push(assignedContentTopics[i].categoryId.toString());
						if (assignedContentTopics[i].contentType) {
							content.push(assignedContentTopics[i].contentType);
						}
						if (assignedContentTopics[i].embeddedLink) {
							if (assignedContentTopics[i].contentType)
								embeddedLinks.push(assignedContentTopics[i].contentType.toString());
						}

					}
					for (i = 0; i < length; i++) {
						if (data[i].categoryId && !topic[data[i].categoryId._id]) {
							topic[data[i].categoryId._id] = {
								direct: [],
								subCategory: [],
							};
						}
						if (!data[i].subCategoryId && data[i].categoryId) {

							if (data[i].categoryId.isPublish == true && data[i].categoryId.isDeleted == false) {
								for (var j = 0; j < data[i].categoryId.content.length; j++) {

									logging.consolelog(purchasedTopics, purchasedTopics.indexOf(parseInt(data[i]["topicId"])) != -1, data[i]["topicId"]);
									if (data[i].categoryId.isFree == true || purchasedTopics.indexOf(parseInt(data[i]["topicId"])) != -1) {
										if(data[i].categoryId.content[j]["isDeleted"]== false){
											var obj = {
												content: data[i].categoryId.content[j],
	
											};
											obj.content.embeddedLink = embeddedLinks.indexOf(data[i].categoryId.content[j]._id.toString()) != -1 ? true : false
											topic[data[i].categoryId._id].direct.push(obj);
											logging.consolelog("subCategoryIddata", "", obj);
										}
										

									} else if (categories.indexOf(data[i].categoryId._id.toString() + "") != -1) {
										if (data[i].categoryId.name == 'Video') {
											if (content.indexOf(data[i].categoryId.content[j]._id.toString()) != -1) {
												if(data[i].categoryId.content[j]["isDeleted"]==false){
													var obj = {
														content: data[i].categoryId.content[j],
														embeddedLink: embeddedLinks.indexOf(data[i].categoryId.content[j]._id.toString()) != -1 ? true : false
													};
													obj.content.embeddedLink = embeddedLinks.indexOf(data[i].categoryId.content[j]._id.toString()) != -1 ? true : false
													topic[data[i].categoryId._id].direct.push(obj);

												}
												

											}
										} else {
											if(data[i].categoryId.content[j]["isDeleted"]==false){
												var obj = {
													content: data[i].categoryId.content[j],
													embeddedLink: embeddedLinks.indexOf(data[i].categoryId.content[j]._id.toString()) != -1 ? true : false
												};
												obj.content.embeddedLink = embeddedLinks.indexOf(data[i].categoryId.content[j]._id.toString()) != -1 ? true : false
												topic[data[i].categoryId._id].direct.push(obj);
											}
											
										}


									} else {
										if(data[i].categoryId.content[j]["isDeleted"]==false){
											var content = {};
											
																					content['_id'] = {};
																					content['_id'] = data[i].categoryId.content[j]._id;
																					content.description = {};
																					content.description = data[i].categoryId.content[j].description;
																					content.icon = {};
																					content.icon = data[i].categoryId.content[j].icon;
																					content.typeOfContent = {};
																					content.typeOfContent = data[i].categoryId.content[j].typeOfContent;
																					var obj = {
																						content: content
																					}
											
																					topic[data[i].categoryId._id].direct.push(obj);


										}
									
									}

								}
							}

						}
						if (data[i].subCategoryId && data[i].subCategoryId.isDeleted == false) {

							var obj = {
								subCategoryName: data[i].subCategoryId.name,
								subCategoryIcon: data[i].subCategoryId.icon,
								subCategoryLanguage: data[i].subCategoryId.language,
								subCategoryId: data[i].subCategoryId._id,
								subCatFree: data[i].subCategoryId.isFree

							};

							topic[data[i].categoryId._id].subCategory.push(obj);
						}
					}
					topic = _.values(topic);
					contentLinking = topic;
					cb(null);

				} else {
					cb(err);
				}
			})
        }
    ], function (err, data) {
		if (!err) {
			response.status(200).send({
				contentLinking: contentLinking,
				statusCode: 200
			})
		} else {
			response.send(err);
		}
	})
}


var addAnalysisData = function (request, response) {
	var userId = request.userData.id;
	var userType = request.userData.userType;
	async.auto({
		save_data: function (cb) {
			var data = {
				customerId: userId,
				topicId: request.body.topicId,
				engangedTime: request.body.engangedTime,
				timeStamp: new Date(),
				userType: userType
			}

			ServiceAnalysisData.createAnalysisData(data, function (err, data) {

				if (!err) {
					cb(null);
				} else {
					cb(err);
				}
			});
		}
	}, function (err, data) {
		if (!err) {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.DEFAULT);
		} else {
			response.send(err);
		}
	})
}

// BULK DATA PURCHASE FROM AGENT (ONLY AGENT HAVE THIS OPTION)
var selectAllAddToCart = function (request, response) {
	var repeat = false;
	var mergedObject = {};
	var mergerdArray = [];
	var final = [];
	var userType = null;
	var customerId = null;
	var topicCart = [];
	var array = [];
	var topics = [];
	var customerId = request.userData.id;
	var userType = request.userData.userType;
	async.series([
            function (cb) {
				logging.consolelog("customer should enter", "", "");
				var query = {
					'board': constants.COMMON_VARIABLES.UNIVERSAL_BOARD_NAME,
					'isDeleted': false,
					'isPublish': true
				}
				var sort = {
					sortBy: "sequenceOrder"
				};
				prepareFilter('board', request.body.board, query);
				prepareFilter('curriculum.language', request.body.language, query);
				prepareFilter('grade', request.body.grade, query);
				prepareFilter('subject', request.body.subject, query);
				prepareFilter('chapter', request.body.chapter, query);
				var search = "";
				if (request.body.search) {
					search = " and match(name, authorName, description, gtinCode, topic.language) against ('" + request.body.search + "*' IN BOOLEAN MODE)";
				}

				var limit = {
					limit: 5000,
					offset: 0
				}

				logging.consolelog(query, "", "query print");
				ServiceTopic.getTopicsByCurriculum(query, sort, limit, search, function (err, data) {

					if (!err) {
						if (data.length > 0) {
							data.forEach(function (curriculum) {
								if (curriculum && curriculum.name) {
									topics.push({
										topicId: curriculum["_id"],
										name: curriculum.name,
										topicLanguage: curriculum.topicLanguage,
									});
								}
							});
							cb(null);
						} else {
							cb(null);
						}
					} else {
						cb(constants.STATUS_MSG.ERROR.TOPIC_NOT_FOUND);
					}
				});


            },

            function (cb) {
				var counter = 0;
				logging.consolelog("topics for select all", "", topics)
				async.forEach(topics, function (item, callback) {
					counter++;
					var query = {
						topicId: item.topicId

					}
					var path = 'categoryId';
					var select = 'name content isDeleted'
					var populate = {
						path: path,
						match: {},
						select: select,
						options: {
							lean: true
						}
					};
					var options = {};
					var projection = {
						categoryId: 1,
						topicId: 1
					}
					ServiceTopicBinding.getAllTopicBinding(query, projection, populate, {}, {}, function (err, data) {
						if (!err) {
							for (var i = 0; i < data.length; i++) {
								if (data[i].categoryId.name == 'Video') {
									for (var j = 0; j < data[i].categoryId.content.length; j++) {
										if (request.body.contentType.indexOf(data[i].categoryId.content[j].typeOfVideo) == -1) {

										} else {
											final.push({
												topicId: data[i].topicId,
												contentType: data[i].categoryId.content[j]._id,
												categoryId: data[i].categoryId._id
											});


										}
									}

								} else {

									if (request.body.categoryName.indexOf(data[i].categoryId.name) == -1) {

									} else {
										final.push({
											topicId: data[i].topicId,
											categoryId: data[i].categoryId._id
										});


									}


								}

							}
							callback(null);
						} else {
							callback(err);
						}
					})

				}, function (err) {

					if (!err) {
						logging.consolelog("final data", request.body.categoryName, request.body.contentType)
						if (counter == topics.length) {
							cb(null);
						} else {
							cb(err);
						}
					}
				})


            },


            function (cb) {


				var query = {
					'customerId': customerId
				};
				var dataToSet = {
					$addToSet: {},
					$set: {
						'customerId': customerId
					}
				};
				if (final && final.length) {
					dataToSet["$addToSet"]["content"] = {
						$each: final
					}
				}

				ServiceCustomerCart.updateCart(query, dataToSet, {
					upsert: true
				}, function (err, data) {
					if (data.nModified == 0 && !data.upserted) {
						repeat = true;
					}
					if (err) {
						cb(err);
					} else {
						cb(null);
					}
				})


            }

        ],
		function (err, data) {
			if (!err) {
				response.status(200).send(repeat ? constants.STATUS_MSG.SUCCESS.ADDED_CART_DUP : constants.STATUS_MSG.SUCCESS.ADDED_CART);
			} else {
				response.send(err);
			}
		})
}


var renewExpirey = function (request, response) {
	// input package type, id
	var packageType = request.body.packageType;
	//packageId
	//topicId// pass in array
	//pass topic only renew or full pacakeg renew
	var userType = request.userData.userType;
	request.body.topicArray = [];
	request.body.packageCart = [];
	request.body.topicCart = [];
	request.body.customPackageCart = [];
	async.auto({
			error_handling: function (cb) {
				if (packageType == constants.DATABASE.PACKAGE_TYPE.ASSIGNED_PACKAGE) {
					//do not give renewed option to assigned package
					cb(constants.STATUS_MSG.ERROR.RENEW_FAIL);
				} else if (packageType == constants.DATABASE.PACKAGE_TYPE.AGENT_PACKAGE && userType != constants.USER_TYPE.AGENT) {
					//for domain wise case
					cb(constants.STATUS_MSG.ERROR.RENEW_FAIL);

				} else {
					cb(null);
				}
			},
			getPackageDetails: ['error_handling', function (cb) {
				if (request.body.topicId.length > 0) {
					var query = {
						_id: request.body.packageId,
						$or: [
							{
								topic: {
									$elemMatch: {
										"topicId": {
											$in: request.body.topicId
										},
									}
								}
                            },
							{
								content: {
									$elemMatch: {
										"topicId": {
											$in: request.body.topicId
										},
									}
								}
                            },
							{
								customPackage: {
									$elemMatch: {
										topics: {
											$in: request.body.topicId
										}
									}
								}
                            }
                        ]
					}
					ServiceCustomerBuyPackages.getTransaction(query, {
						content: 1,
						topic: 1,
						customPackage: 1
					}, {
						lean: true
					}, function (err, data) {
						if (!err) {
							if (data.length > 0) {
								if (data[0].content) {
									for (var i = 0; i < data[0].content.length; i++) {
										logging.consolelog(request.body.topicId, data[0].content[i].topicId, request.body.topicId.indexOf(data[0].content[i].topicId))
										if (request.body.topicId.indexOf(data[0].content[i].topicId) != -1 && (data[0].content[i].isActive == true || data[0].content[i].isActive == undefined) && userType == constants.USER_TYPE.AGENT) {
											request.body.topicArray.push({
												topicId: Number(data[0].content[i].topicId),
												categoryId: data[0].content[i].categoryId,
												contentType: data[0].content[i].contentType
											});
										}
									}
								}
								if (data[0].topic) {
									for (var j = 0; j < data[0].topic.length; j++) {
										if (request.body.topicId.indexOf(data[0].topic[j].topicId) != -1)
											request.body.topicCart.push(Number(data[0].topic[j].topicId));
									}
								}

								if (data[0].customPackage) {
									for (var k = 0; k < data[0].customPackage.length; k++) {

										if (request.body.topicId.indexOf(data[0].customPackage[k].topics) != -1) {
											request.body.topicCart.push(Number(request.body.topicId));
										}

									}
								}
								cb(null);
							} else {
								cb(constants.STATUS_MSG.ERROR.RENEW_FAIL);
							}


						} else {
							logging.consolelog("package Id error in renew function", err, "");
							cb(err);
						}


					});
				} else {
					var query = {
						_id: request.body.packageId,
					}
					ServiceCustomerBuyPackages.getTransaction(query, {
						topic: 1,
						content: 1,
						package: 1,
						customPackage: 1
					}, {
						lean: true
					}, function (err, data) {
						if (!err) {
							if (data.length > 0) {
								if (data[0].content) {
									for (var i = 0; i < data[0].content.length; i++) {
										if ((data[0].content[i].isActive == true || data[0].content[i].isActive == undefined) && userType == constants.USER_TYPE.AGENT) {
											request.body.topicArray.push({
												topicId: Number(data[0].content[i].topicId),
												categoryId: data[0].content[i].categoryId,
												contentType: data[0].content[i].contentType
											});
										}
									}
								}
								if (data[0].topic) {
									for (var j = 0; j < data[0].topic.length; j++) {

										request.body.topicCart.push(Number(data[0].topic[j].topicId));
									}
								}

								if (data[0].customPackage) {
									for (var k = 0; k < data[0].customPackage.length; k++) {

										request.body.customPackageCart.push(data[0].customPackage[k].packageId);

									}
								}

								if (data[0].package) {
									for (var l = 0; l < data[0].package.length; l++) {

										request.body.packageCart.push(data[0].package[l].packageId);

									}
								}

								cb(null);
							} else {
								cb(constants.STATUS_MSG.ERROR.RENEW_FAIL);
							}


						} else {
							logging.consolelog("package Id error in renew function", err, "");
							cb(err);
						}


					});


				}
            }],
			//		add_tocart:['getPackageDetails',function(cb){
			//			var resp={
			//				send:function(obj){
			//					response.send(obj)
			//				}
			//			}
			//			addToCart(request,resp);
			//
			//
			//
			//		}]
		},
		function (err, result) {
			if (!err) {
				addToCart(request, response);
			} else {
				response.send(err);
			}
		})
}

var deleteBooking = function (request, response) {
	// booking id
	//package type
	var packageType = request.body.packageType
	var criteria = {
		_id: request.body.id
	};
	var dataToUpdate = {
		$set: {
			isDeleted: true
		}
	};
	async.auto({
		updateBooking: function (cb) {
			if (packageType == constants.DATABASE.PACKAGE_TYPE.ASSIGNED_PACKAGE) {
				ServiceAssignDataToCustomer.updateAssignDataToCustomer(criteria, dataToUpdate, {}, function (err, data) {
					if (err) {
						cb(err, null)
					} else {
						cb(null, null)
					}
				})
			} else {
				ServiceCustomerBuyPackages.updateTransaction(criteria, dataToUpdate, {}, function (err, data) {
					if (err) {
						cb(err, null)
					} else {
						cb(null, null)
					}
				})
			}
		}
	}, function (err, result) {
		if (err) {
			response.send(err)
		} else {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.DEFAULT);
		}
	})
};

var addEditScoreBoard = function (request, response) {
	var correct = 0;
	var wrong = 0;
	var assessmentId = request.body.assessmentId;
	var assessmentContentId = request.body.assessmentContentId;
	var answers = request.body.answers; //user response
	var attempt = request.body.attempt ? request.body.attempt : 1;
	var startTime = request.body.startTime ? request.body.startTime : "";
	var endTime = request.body.endTime ? request.body.endTime : "";
	var score = 0;
	var scoreBoardId = request.body.scoreBoardId;
	var assessmentId = request.body.assessmentId;
	var status = request.body.status;

	async.auto({
		getAssessments: function (cb, data) {
			var query = {
				_id: assessmentId,
				'assessmentContent._id': assessmentContentId
			}
			ServiceAssesment.getAssessment(query, {
				'assessmentContent.$': 1
			}, {}, function (err, fetchedData) {
				if (err) {
					cb(err, null)
				} else {
					if (fetchedData) {
						cb(null, fetchedData)
					} else {
						cb(constants.STATUS_MSG.ERROR.ASSESSMENT_NOT_FOUND, null)
					}
				}
			})
		},
		evaluateAnswer: ['getAssessments', function (cb, data) {
			//check the question in assessment content and verify answers
			var check;
			if (data.getAssessments && data.getAssessments.length && data.getAssessments[0].assessmentContent.length) {
				answers.forEach(function (evl) {
					if (data.getAssessments[0].assessmentContent.answers.indexOf(evl) == -1) {
						check = -1
					} else {
						check = 1
					}
				})
				if (check)
					correct += correct;
				if (!check)
					wrong += wrong;
				cb(null, check)
			} else {
				cb(constants.STATUS_MSG.ERROR.DEFAULT, null)
			}
        }],
		calculateScore: ['evaluateAnswer', function (cb, data) {
			if (data.evaluateAnswer) {
				score += data.getAssessments[0].assessmentContent[0].weightage
				cb(null, score)
			} else {
				//handle negative marking
				score -= data.getAssessments[0].assessmentContent[0].weightage
				cb(null, score)
			}
        }],
		// checkIfScoreBoardExist:['calculateScore',function(cb,data){
		// 	var query={
		// 		assessment:assessmentId,
		// 		customer:userDetails._id,
		// 		attempt:attempt,
		// 		userType:userDetails.userType
		// 	};
		// 	ServiceAssesment.getScoreBoard(query,{},{},function(err,SB){
		// 		if(err){
		// 			cb(err,null)
		// 		}else{
		// 			if(SB){
		// 				cb(null,1)
		// 			}else{
		// 				cb(null,0)
		// 			}
		// 		}
		// 	})
		// }],
		updateScoreBoard: ['calculateScore', function (cb, data) {
			if (scoreBoardId) {
				//update SB
				var query = {
					_id: scoreBoardId
				};
				var dataToUpdate = {
					correct: [correct],
					wrong: [wrong],
					score: score,
					status: status
				}
				if (attempt)
					dataToUpdate.attempt = attempt
				if (endTime)
					dataToUpdate.endTime = endTime
				ServiceAssesment.updateScoreBoard(query, dataToUpdate, {}, function (err, USB) {
					if (err) {
						cb(err, null)
					} else {
						cb(null, null)
					}
				})
			} else {
				//create SB
				var dataToSave = {
					assessment: assessmentId,
					customer: userDetails._id,
					userType: userDetails.userType,
					attempt: attempt,
					startTime: startTime,
					status: status,
					correct: [correct],
					wrong: [wrong],
					score: score

				};
				ServiceAssesment.createScoreBoard(dataToSave, function (err, SSB) {
					if (err) {
						cb(err, null)
					} else {
						cb(null, null)
					}
				})
			}
        }]
	}, function (err, result) {
		if (err) {
			response.status(400).send(err)
		} else {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.DEFAULT)
		}
	})
}

var updateLevelOfCustomer = function (request, response) {
	var level, userCount, score, userDetails;
	var newLevel = level * userCount >= level * userCount ? level + 1 : level
	async.auto({
		authorization: function (cb) {

		},
		getAssessments: function (cb, data) {

		},


	}, function (err, result) {

	})
}



let appTopicDetails = (request, response) => {
	console.log(request.body,"app topic details");
	var details = {
		topic: {},
		assessments: []
	};
	async.auto({
		"get_topic_details": (cb) => {
			ServiceTopic.getTopic({
				_id: request.body.topicId
			}, {
				name: 1,
				language: 1,
				isFree: 1,
				icon: 1,
				averageRating: 1,
				price: 1,
				description: 1

			}, {
				lean: true
			}, function (err, data) {
				logging.consolelog(err, data, request.body)
				if (!err) {
					if (data.length > 0) {
						details["topic"]["name"] = data[0].name;
						details["topic"]["language"] = data[0].language;
						details["topic"]["isFree"] = data[0].isFree;
						details["topic"]["averageRating"] = data[0].averageRating;
						details["topic"]["price"] = data[0].price;
						details["topic"]["description"] = data[0].description;
					}
					cb(null);
				} else {
					cb(err);
				}
			});

		},
		"get_favourite": (cb) => {
			var topicIds = [];
			//fetch favorite topic
			try {
				getFavourite(request.userData.id, function (data) {
					topicIds = topicIds.concat(data);
					logging.consolelog("logging", "", topicIds);
					if (topicIds.indexOf(request.body.topicId) != -1 || topicIds.indexOf(JSON.stringify(request.body.topicId))!=-1) {
						details["topic"]["isFavourite"] = true;
					} else {
						details["topic"]["isFavourite"] = false;
					}
					cb(null);
				})
			} catch (e) {
				logging.consolelog("logging", e, "");
				cb(null);
			}


		},
		"notes_data": (cb) => {
			var query = {
				'customerId': request.userData.id,
				'topics.topicId': request.body.topicId
			};

			ServiceNotes.getNote(query, {
				topics: 1,
				customerId: 1
			}, {}, function (err, data) {
				logging.consolelog("logging", "", data);
				if (err) {
					cb(err);
				} else {
					if (data && data[0] && data[0].topics && data[0].topics)
						details["topic"]["notes"] = data[0].topics;
					cb(null);
				}
			});
		},
		"assesment_data": function (cb) {
			let query = {
				topicId: request.body.topicId,
				'assesment.isDeleted': false
			}
			let projection = {
				'assesment': 1,
				isFree: 1,
				price: 1,
				language: 1,
				name: 1,
				addedDate: 1
			}
			ServiceAssesment.getAssessment(query, projection, {}, function (err, data) {
				if (!err) {
					if (data.length > 0) {
						if (data[0].assesment) {
							for (var i = 0; i < data[0].assesment.length; i++) {
								if (data[0].assesment[i].isDeleted == false) {
									details.assessments.push({
										_id: data[0].assesment[i]._id,
										addedDate: data[0].assesment[i].addedDate,
										assesmentUrl: data[0].assesment[i].assesmentUrl,
										name: data[0].assesment[i].name,
										price: data[0].assesment[i].price,
										language: data[0].assesment[i].language,
									})
								}

							}

						}
					}
					cb(null);
				} else {
					cb(err);
				}
			})
		},

	}, (err, data) => {
		if (err) {
			response.status(400).send(err);
		} else {
			console.log("=====> deatisl", details);
			response.status(200).send({
				details: details,
				statusCode: 200
			});
		}
		addToHistory(request.userData.id, request.body.topicId);
	})
}



let appTopicCategories = (request, response) => {
	var superParentCustomerId = [];
	var purchasedTopics = [];
	var assignedTopics = [];
	var assignedContentTopics = [];
	var agentPurchasedTopics = [];
	var contentLinking = {};
	var isDomain = false;
	var contentArray = [];
	async.series([function (cb) {
			ServiceCustomer.getCustomer({
				_id: request.userData.id
			}, {
				emailId: 1
			}, {
				lean: true
			}, function (err, data) {
				if (!err) {
					if (data.length > 0) {
						var email = data[0].emailId;
						//get domain of customer
						var domain = email.substring(email.lastIndexOf("@") + 1);
						var query = {
							userType: constants.USER_TYPE.AGENT,
							domainName: domain
						}
						ServiceCustomer.getCustomer(query, {
							_id: 1
						}, {}, function (err, data) {
							if (!err) {
								if (data.length > 0) {
									for (var i = 0; i < data.length; i++) {
										superParentCustomerId.push(data[i]._id);
										isDomain = true;
									}

								}
								cb(null);
							} else {
								cb(err);
							}
						})
					}
				} else {
					cb(err);
				}
			})
        },
        function (cb) {
			//fetch purchased topic
			if (request.userData.userType != constants.USER_TYPE.AGENT) {
				getCustomerPurchases(request.userData.id, function (data) {
					console.log(JSON.stringify(data), "purchased Data")
					for (var i = 0; i < data.length; i++) {
						purchasedTopics.push(parseInt(data[i]));
					}

					cb(null);
				})
			} else {
				cb(null);
			}

        },

        function (cb) {
			//fetch assigned topics
			getAssignedTopics(request.userData.id, request.body.topicId, function (err, data) {
				assignedTopics = assignedTopics.concat(data.topic);
				assignedContentTopics = assignedContentTopics.concat(data.content);

				cb(null);
			})
        },
        function (cb) {
			if (request.userData.userType == constants.USER_TYPE.AGENT) {

				ServiceChildMapper.getChildParentMapper({
					childCustomerId: request.userData.id
				}, {
					superParentCustomerId: 1
				}, {
					lean: true
				}, function (err, data) {
					if (!err) {
						if (data.length > 0) {
							superParentCustomerId.push(data[0].superParentCustomerId);
						}
						cb(null);
					} else {
						cb(err);
					}
				});
			} else {
				cb(null);
			}

        },
        function (cb) {
			if (isDomain == true || request.userData.userType == constants.USER_TYPE.AGENT) {
				getAgentPurchases(superParentCustomerId, function (err, data) {
					agentPurchasedTopics = agentPurchasedTopics.concat(data.content);
					cb(null);
				});

			} else {
				cb(null);
			}


        },

        function (cb) {

			var query = {
				topicId: request.body.topicId
			}

			var select = 'name price icon language isPublish content isDeleted isFree name icon language content isDeleted isFree isPublish'
			var populate = {
				path: 'categoryId',
				match: {
					// isPublish: true,
					isDeleted: false
				},
				select: select,
				options: {
					lean: true
				}
			};
			var projection = {
				categoryId: 1
			};
			ServiceTopicBinding.topicBindingPopulate(query, projection, populate, {}, {
				lean: true
			}, function (err, data) {
				if (!err) {
					var i = 0;
					var length = data.length;
					var categories = [];
					var embeddedLinks = [];
					logging.consolelog(assignedContentTopics, "", "print purchased categories")
					for (var i = 0; i < assignedContentTopics.length; i++) {
						categories.push(assignedContentTopics[i].categoryId.toString());
					}
					for (var i = 0; i < assignedContentTopics.length; i++) {
						if (assignedContentTopics[i].embeddedLink) {
							embeddedLinks.push(assignedContentTopics[i].contentType);
						}

					}
					for (var i = 0; i < agentPurchasedTopics.length; i++) {
						categories.push(agentPurchasedTopics[i].categoryId._id.toString());
					}
					//					logging.consolelog("logging", purchasedTopics, purchasedTopics.indexOf(topicId) != -1 ? true : false)
					//					console.log("puchased topics values", JSON.stringify(purchasedTopics), JSON.stringify(assignedTopics), JSON.stringify(agentPurchasedTopics));
					var purchased = (purchasedTopics.indexOf(request.body.topicId) != -1 ? true : false) ||
						(assignedTopics.indexOf(request.body.topicId.toString()) != -1 ? true : false) ||
						(agentPurchasedTopics.indexOf(request.body.topicId.toString()) != -1 ? true : false);

					var topic = {};
					var videoHasContent = false;
					for (i = 0; i < length; i++) {

						logging.consolelog('track error', '>>>>>>>>>>>>', data[i])

						if (data && data.length && data[i].categoryId && data[i].categoryId.name && data[i].categoryId.isPublish && contentLinking[data[i].categoryId.name]) {
							continue;
						} else if (data && data.length && (data[i].categoryId && data[i].categoryId.isPublish && data[i].categoryId.name)) {
							logging.consolelog('preparing content linking>>>>>>>>>>>>>', data[i].categoryId, data[i].subCategoryId)
							contentLinking[data[i].categoryId.name] = {
								name: data[i].categoryId.name,
								_id: data[i].categoryId._id,
								icon: data[i].categoryId.icon,
								isFree: data[i].categoryId.isFree,
								//								purchasedCategories: categories,
								isPurchased: purchased ? purchased : (categories.indexOf(data[i].categoryId["_id"].toString()) == -1 ? false : true),
								embeddedLink: embeddedLinks
							}
							logging.consolelog('>>>>>>>>>>>>>>>>>>>>>>>', data[i].categoryId.content.length, data[i].categoryId.name)

							if ((data[i].categoryId.content.length != 0 && data[i].categoryId.name === 'Video'))
								videoHasContent = true;
						}
					}
					logging.consolelog('contentLinking before>>>>>>>>', videoHasContent, contentLinking)
					if (!videoHasContent)
						delete contentLinking['Video']
					logging.consolelog('contentLinking after>>>>>>>>', videoHasContent, contentLinking)

					for (var k in contentLinking) {
						contentArray.push(contentLinking[k]);
					}

					cb(null);
				} else {
					cb(err);
				}
			})
        }], (err, data) => {
		if (err) {
			response.status(400).send(err);
		} else {
			response.status(200).send({
				"content": contentArray,
				"statusCode": 200
			});
		}
	})
}


let appTopicAssessment = (request, repsonse) => {
	var details = {
		assessments: []
	};
	async.series([
		(cb) => {
			let query = {
				topicId: request.body.topicId,
				'assesment.isDeleted': false
			}
			let projection = {
				'assesment': 1,
				isFree: 1,
				price: 1,
				language: 1,
				name: 1,
				addedDate: 1
			}
			ServiceAssesment.getAssessment(query, projection, {}, function (err, data) {
				if (!err) {
					if (data.length > 0) {
						if (data[0].assesment) {
							for (var i = 0; i < data[0].assesment.length; i++) {
								if (data[0].assesment[i].isDeleted == false) {
									details.assessments.push({
										_id: data[0].assesment[i]._id,
										addedDate: data[0].assesment[i].addedDate,
										assesmentUrl: data[0].assesment[i].assesmentUrl,
										name: data[0].assesment[i].name,
										price: data[0].assesment[i].price,
										language: data[0].assesment[i].language,
									})
								}

							}

						}
					}
					cb(null);
				} else {
					cb(err);
				}
			})
		}
	], (err, data) => {
		if (!err) {
			response.status(200).send({
				details: details,
				statusCode: 200
			})
		} else {
			response.send(err);
		}
	})
}


let paymentHistory = (request, response) => {
	var data = [];

	async.auto({

		"get_transaction_history": (cb) => {

			ServiceCustomerBuyPackages.getTransaction({
				customerId: request.userData.id,
				paymentStatus:"COMPLETED"
			}, {
				paymentStatus: 1,
				transactionId: 1,
				payments: 1,
				topicPayments: 1,
				packageName: 1,
				customPackagePayments: 1,
				packagePayments: 1,
				topicOfferApplied: 1
			}, {
				lean: true
			}, (err, dbData) => {

				if (!err) {
					if (dbData.length > 0) {
						data = dbData;
					}
					cb(null);
				}
			})
		}
	}, (err, some) => {
		if (!err) {
			response.status(200).send({
				details: data,
				statusCode: 200
			})
		} else {
			response.send(err);
		}
	})
}

let appTopicComments = (request, response) => {

	console.log("------> topic id -------->", request.body.topicId);
	var comments = [];
	async.auto({
		"get_comments": function (cb) {

			var query = {
				topicId: request.body.topicId,
				isActive: true
			}
			var projection = {
				userType: 1,
				ratingStars: 1,
				comments: 1,
				customerId: 1,
				addedDate: 1
			}

			var path = 'customerId';
			var select = 'emailId userType mobileNo profilePicUrl lastName firstName';
			var populate = {
				path: path,
				match: {},
				select: select,
				options: {
					lean: true
				}
			};
			ServiceFeedback.getFeedback(query, projection, {}, populate, function (err, data) {
				if (!err) {
					logging.consolelog("Commnts", "", JSON.stringify(data));
					if (data.length > 0) {
						for (var i in data) {
							var comment = {};
							comment.userType = data[i].userType;
							comment.ratingStars = data[i].ratingStars;
							comment.comments = data[i].comments;
							comment.customerId = data[i].customerId;
							comment.mobileNo = data[i].mobileNo;
							comment.firstName = data[i].firstName;
							comment.lastName = data[i].lastName;
							comment.profilePicUrl = data[i].profilePicUrl;
							comment.emailId = data[i].emailId;
							comment.userType = data[i].userType;
							comment.addedDate = data[i].addedDate;
							comments.push(comment);
						}
						cb(null);
					} else {
						cb(null);
					}
				} else {
					cb(err);
				}
			});
		},
	}, (err, data) => {
		if (!err) {
			response.status(200).send({
				statusCode: 200,
				comments: comments
			})
		} else {
			response.send(err);
		}
	})
}





var learnTopicLibrary = function (request, response) {
	logging.consolelog("Fetching topic library!", "", "");
	var topics = [];
	var final = {
		data: {}
	};
	var search = "";
	var topicHistory = [];
	var topicIds = [];
	var purchasedTopics = [];
	var topicListObj = {};
	//var topic = [];
	var offsetCount = 0;
	var limitCount = 10;
	var cartTopics = [];
	async.series([

            function (cb) {
				//check user type:: get all topics

				logging.consolelog("------->customer should enter", "", "");
				var query = {
					'board': constants.COMMON_VARIABLES.UNIVERSAL_BOARD_NAME,
					'isDeleted': false,
					// 'isPublish': true
				}
				var sort = {
					sortBy: "sequenceOrder"
				};
				prepareFilter('board', request.body.board, query);
				prepareFilter('curriculum.language', request.body.language, query);
				prepareFilter('grade', request.body.grade, query);
				prepareFilter('subject', request.body.subject, query);
				prepareFilter('chapter', request.body.chapter, query);

				if (request.body.offset) {
					offsetCount = request.body.offset;
				}
				if (request.body.limit) {
					limitCount = request.body.limit;
				}

				var limit = {
					limit: limitCount,
					offset: offsetCount
				}
				final["count"] = 0;
				final["offset"] = offsetCount;
				final["limit"] = limitCount;

				logging.consolelog(query, "-------->query print", "");


				ServiceTopic.getTopicsByCurriculum(query, sort, limit, search, function (err, data) {
					var curriculumsIds = [];
					if (!err) {
						if (data.length > 0) {
							data.forEach(function (curriculum) {
								if (curriculum && curriculum.name) {
									topics.push({
										Id: curriculum["_id"],
										name: curriculum.name,
										icon: curriculum.icon,
										authorName: curriculum.authorName,
										averageRating: curriculum.averageRating,
										price: curriculum.price,
										description: curriculum.description,
										board: curriculum.board,
										subject: curriculum.subject,
										language: curriculum.boardLanguage,
										topicLanguage: curriculum.topicLanguage,
										grade: curriculum.grade,
										chapter: curriculum.chapter,
										addedDate: curriculum.addedDate,
										isFree: curriculum.isFree,
										isPurchased: purchasedTopics.indexOf(parseInt(curriculum["_id"])) != -1 ? true : false,
										isFavourite: topicIds.indexOf(curriculum["_id"] + "") != -1 ? true : false,
										isCart: cartTopics.indexOf(parseInt(curriculum["_id"])) != -1 ? true : false
									});
								}
							});

							ServiceTopic.getTopicsByCurriculumCount(query, {}, {}, search, function (err, countData) {
								logging.consolelog(err, countData, "check count");
								if (!err) {
									final["count"] = countData ? countData[0]["cnt"] : 0;
								}
								cb(null);
							});
						} else {
							cb(null);
						}
					} else {
						logging.consolelog(err, data, "-------->TOPIC_NOT_FOUND");
						cb(constants.STATUS_MSG.ERROR.TOPIC_NOT_FOUND);
					}
				});


            },
            function (cb) {

				final["data"]["topicLibrary"] = topics;
				cb(null);

						}],
		function (err, data) {
			if (!err) {
				final.statusCode = 200;
				response.status(200).send(final);
			} else {
				response.send(err);
			}
		})
}




var websiteTopicDetails = function (request, response) {
	var curriculumLinked = [];
	var topicDetails = {};
	var contentLinking = {
	
	};
	var universalLinked = [];
	var purchasedTopics = [];
	var comments = [];
	var topicIds = [];
	var assignedContentTopics = [];
	var assignedTopics = [];
	var languages = {};
	var topicId;
	var assessments = [];
	var userId = "",
		userType ="";
	var superParentCustomerId = [];
	var agentPurchasedTopics = [];
	var isDomain = false;
	var demo = false;
	async.series([
        function (cb) {
			// front end will pass topic name and language// get topic Id
			ServiceTopic.getTopic({
				name: request.body.topicName,
				language: request.body.topicLanguage,
				isDeleted: false
			}, {
				_id: 1
			}, {
				lean: true
			}, function (err, data) {
				logging.consolelog(err, data, request.body)
				if (!err) {
					if (data.length > 0) {
						topicId = data[0]._id;
					}
					cb(null);
				} else {
					cb(err);
				}
			});
        },
        function (cb) {
			logging.consolelog("universal_linked", "", "");
			var query = {
				topicId: topicId,
				board: constants.COMMON_VARIABLES.UNIVERSAL_BOARD_NAME
			}
			ServiceTopic.getTopicsByCurriculum(query, {}, {}, "", function (err, data) {
				if (!err) {
					if (data.length > 0) {
						data.forEach(function (curriculum) {
							universalLinked.push({
								board: curriculum.board,
								language: curriculum.boardLanguage,
								grade: curriculum.grade,
								subject: curriculum.subject,
								chapter: curriculum.chapter
							});
							topicDetails.id = curriculum["_id"];
							topicDetails.name = curriculum.name;
							topicDetails.icon = curriculum.icon;
							topicDetails.authorName = curriculum.authorName;
							topicDetails.language = curriculum.topicLanguage;
							topicDetails.description = curriculum.description;
							topicDetails.gtinCode = curriculum.gtinCode;
							topicDetails.price = curriculum.price;
							topicDetails.isFavourite = topicIds.indexOf(curriculum["_id"] + "") != -1 ? true : false;
						});
						cb(null);
					} else {
						cb(null);
					}
				} else {
					cb(err);
				}
			});
        },
        function (cb) {

			var query = {
				name: topicDetails.name,
				isDeleted: false
			};
			ServiceTopic.getTopic(query, {}, {}, function (err, data) {
				if (data && data.length) {
					for (var i in data) {
						languages[data[i]["language"]] = data[i]["_id"];
					}
				}
				cb(null);
			});
        },
        function (cb) {

			var query = {
				topicId: topicId,
				isActive: true
			}
			var projection = {
				userType: 1,
				ratingStars: 1,
				comments: 1,
				customerId: 1,
				addedDate: 1
			}

			var path = 'customerId';
			var select = 'emailId userType mobileNo profilePicUrl lastName firstName';
			var populate = {
				path: path,
				match: {},
				select: select,
				options: {
					lean: true
				}
			};
			ServiceFeedback.getFeedback(query, projection, {}, populate, function (err, data) {
				if (!err) {
					logging.consolelog("Commnts", "", JSON.stringify(data));
					if (data.length > 0) {
						for (var i in data) {
							var comment = {};
							comment.userType = data[i].userType;
							comment.ratingStars = data[i].ratingStars;
							comment.comments = data[i].comments;
							comment.customerId = data[i].customerId;
							comment.mobileNo = data[i].mobileNo;
							comment.firstName = data[i].firstName;
							comment.lastName = data[i].lastName;
							comment.profilePicUrl = data[i].profilePicUrl;
							comment.emailId = data[i].emailId;
							comment.userType = data[i].userType;
							comment.addedDate = data[i].addedDate;
							comments.push(comment);
						}
						cb(null);
					} else {
						cb(null);
					}
				} else {
					cb(err);
				}
			});
        },
        function (cb) {

			var query = {
				topicId: topicId
			}

			var select = 'name price icon language isPublish content isDeleted isFree name icon language content isDeleted isFree isPublish'
			var populate = {
				path: 'categoryId',
				match: {
					// isPublish: true,
					isDeleted: false
				},
				select: select,
				options: {
					lean: true
				}
			};
			var projection = {
				categoryId: 1
			};
			ServiceTopicBinding.topicBindingPopulate(query, projection, populate, {}, {
				lean: true
			}, function (err, data) {
				if (!err) {
					var i = 0;
					var length = data.length;
					var categories = [];
					var embeddedLinks = [];
					logging.consolelog(assignedContentTopics, "", "print purchased categories")
					for (var i = 0; i < assignedContentTopics.length; i++) {
						categories.push(assignedContentTopics[i].categoryId.toString());
					}
					for (var i = 0; i < assignedContentTopics.length; i++) {
						if (assignedContentTopics[i].embeddedLink) {
							embeddedLinks.push(assignedContentTopics[i].contentType);
						}

					}
					for (var i = 0; i < agentPurchasedTopics.length; i++) {
						categories.push(agentPurchasedTopics[i].categoryId._id.toString());
					}
					logging.consolelog("logging", purchasedTopics, purchasedTopics.indexOf(topicId) != -1 ? true : false)
					console.log("puchased topics values", JSON.stringify(purchasedTopics), JSON.stringify(assignedTopics), JSON.stringify(agentPurchasedTopics));
					var purchased = (purchasedTopics.indexOf(topicId) != -1 ? true : false) ||
						(assignedTopics.indexOf(topicId.toString()) != -1 ? true : false) ||
						(agentPurchasedTopics.indexOf(topicId.toString()) != -1 ? true : false);

					var topic = {};
					var videoHasContent = false;
					for (i = 0; i < length; i++) {

						logging.consolelog('track error', '>>>>>>>>>>>>', data[i])

						if (data && data.length && data[i].categoryId && data[i].categoryId.name && data[i].categoryId.isPublish && contentLinking[data[i].categoryId.name]) {
							continue;
						} else if (data && data.length && (data[i].categoryId && data[i].categoryId.isPublish && data[i].categoryId.name)) {
							logging.consolelog('preparing content linking>>>>>>>>>>>>>', data[i].categoryId, data[i].subCategoryId)
							contentLinking[data[i].categoryId.name] = {
								name: data[i].categoryId.name,
								_id: data[i].categoryId._id,
								icon: data[i].categoryId.icon,
								isFree: false,
								purchasedCategories: categories,
								isPurchased: false,
								embeddedLink: embeddedLinks
							}
							logging.consolelog('>>>>>>>>>>>>>>>>>>>>>>>', data[i].categoryId.content.length, data[i].categoryId.name)
							logging.consolelog('KKKKKKKKKKKKKKKKKK', !(data[i].categoryId.content.length != 0 && data[i].categoryId.name === 'Video'), '')
							if ((data[i].categoryId.content.length != 0 && data[i].categoryId.name === 'Video'))
								videoHasContent = true;
						}
					}
					logging.consolelog('contentLinking before>>>>>>>>', videoHasContent, contentLinking)
					if (!videoHasContent)
						delete contentLinking['Video']
					logging.consolelog('contentLinking after>>>>>>>>', videoHasContent, contentLinking)
					cb(null);
				} else {
					cb(err);
				}
			})
        },
        function (cb) {
			let query = {
				topicId: topicId,
				'assesment.isDeleted': false
			}
			let projection = {
				'assesment': 1,
				isFree: 1,
				price: 1,
				language: 1,
				name: 1,
				addedDate: 1
			}
			ServiceAssesment.getAssessment(query, projection, {}, function (err, data) {
				if (!err) {
					if (data.length > 0) {
						if (data[0].assesment) {
							for (var i = 0; i < data[0].assesment.length; i++) {
								if (data[0].assesment[i].isDeleted == false) {
									assessments.push({
										_id: data[0].assesment[i]._id,
										addedDate: data[0].assesment[i].addedDate,
										// assesmentUrl: data[0].assesment[i].assesmentUrl,
										name: data[0].assesment[i].name,
										price: data[0].assesment[i].price,
										language: data[0].assesment[i].language,
									})
								}

							}

						}
					}
					cb(null);
				} else {
					cb(err);
				}
			})
        }], function (err, data) {
		if (!err) {
			response.status(200).send({
				statusCode: 200,
				topicDetails: topicDetails,
				contentLinking: contentLinking,
				universalLinked: universalLinked,
				topicId: topicId,
				comments: comments,
				isPurchased: purchasedTopics.indexOf(topicId + "") != -1 ? true : false,
				languages: languages,
				assessments: assessments
			})
		} else {
			response.send(err);
		}
	})

}

module.exports = {
	topicLibrary: topicLibrary,
	topicDetails: topicDetails,
	createComment: createComment,
	// downloadContent: downloadContent,
	getFilters: getFilters,
	getAssessments: getAssessments,
	getCategoryContent: getCategoryContent,
	getSubCategoryContent: getSubCategoryContent,
	addToCart: addToCart,
	getCart: getCart,
	removeCart: removeCart,
	cartNumber: cartNumber,
	checkout: checkout,
	transactionFailed: transactionFailed,
	transactionCompleted: transactionCompleted,
	transactionSuccessful: transactionSuccessful,
	markFavourite: markFavourite,
	unmarkFavourite: unmarkFavourite,
	saveNote: saveNote,
	getNote: getNote,
	getCustomerPackages: getCustomerPackages,
	getCustomerTopicPackages: getCustomerTopicPackages,
	getCustomerPurchases: getCustomerPurchases,
	getTopics: getTopics,
	addAnalysisData: addAnalysisData,
	selectAllAddToCart: selectAllAddToCart,
	getExpiredPackages: getExpiredPackages,
	renewExpirey: renewExpirey,
	deleteBooking: deleteBooking,
	addEditScoreBoard: addEditScoreBoard,
	appTopicDetails: appTopicDetails,
	appTopicCategories: appTopicCategories,
	appTopicAssessment: appTopicAssessment,
	appTopicComments: appTopicComments,
	paymentHistory: paymentHistory,
	learnTopicLibrary: learnTopicLibrary,
	websiteTopicDetails:websiteTopicDetails,
	


}