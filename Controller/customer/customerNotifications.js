var async = require('async');
var constants = require('../../Library/appConstants');
var ServiceNotifications = require('../../Service/customerNotifications');

var notificationsCount = function (request, response) {
	var notificationCount;
	var userId= request.userData.id;
	async.series([
        function (cb) {
			ServiceNotifications.getNotifications({
				customerId: userId,
				isRead: false
			}, {}, {}, function (err, data) {
				if (err) cb(err)
				else {
					if (data.length > 0) {
						notificationCount = data.length;
						cb()
					} else {
						notificationCount = 0;
						cb()
					}
				}
			})
        }
    ], function (err, data) {
		if (!err) {
			response.status(200).send({
				notificationCount: notificationCount,
				statusCode: 200
			})
		} else response.send(err);
	})
}

var getAllNotifications = function (request, response) {
	var notificationData, totalNotifications, userId=request.userData.id;
	async.series([
        function (cb) {
			ServiceNotifications.getNotifications({
				customerId: userId
			}, {
				_id: 1
			}, {
				lean: true
			}, function (err, data) {
				if (err) cb(err)
				else {
					totalNotifications = data.length;
					cb(null);
				}
			})
        },
        function (cb) {
			ServiceNotifications.updateNotifications({
				customerId: userId
			}, {
				$set: {
					isRead: true
				}
			}, {
				multi: true
			}, function (err, data) {
				if (err) cb(err)
				else {
					cb(null);
				}
			})
        },
        function (cb) {
			var skip = request.body.skip ? request.body.skip : 0;
			var limit = request.body.limit ? request.body.limit : 10;
					
			ServiceNotifications.getNotifications({
				customerId: userId
			}, {}, {
				skip: parseInt(skip),
				limit: parseInt(limit)
			}, function (err, data) {
				logging.consolelog(err,data,"::notification data::");
				if (err) cb(err)
				else {
					if (data.length > 0) {
						notificationData = data;
						cb()
					} else {
						notificationData = [];
						cb()
					}
				}
			})

        }
    ], function (err, data) {
		if (!err) response.status(200).send({
			notificationData: notificationData,
			skip: request.body.skip,
			limit: request.body.limit,
			totalNotifications: totalNotifications,
			statusCode: 200

		})
		else response.send(err);
	})
}

module.exports = {
	getAllNotifications: getAllNotifications,
	notificationsCount: notificationsCount

}