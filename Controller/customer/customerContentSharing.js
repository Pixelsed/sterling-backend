var md5 = require('MD5');
var async = require('async');
var constants = require('../../Library/appConstants');
var UploadManager = require('../../Library/uploadManager');
var NotificationManager = require('../../Library/notificationManager');
var ServiceCustomer = require("../../Service/customer");
var AwsConfig = Config.get('awsConfig');
var moment = require('moment');
var ServiceChildMapper = require('../../Service/childParentMapper');
var ServiceAssignDataToCustomer = require('../../Service/assignDataToCustomer');
var Templates = require('../../Templates/subAgentRegister');
var ServiceCustomerBuyPackages = require('../../Service/customerBuyPackages');
var ServiceTopic = require('../../Service/topic');
var _ = require('underscore');
var TemplatesBlock = require('../../Templates/blockCustomer');
var ServiceTopicCategory = require('../../Service/topicCategory');
var UniversalFunctions = require('../../Library/universalFuctions');


var licenseDeduction = function (item, superParentCustomerId, userId, topic, content, callback) {
    var assignedBy = null;
    var licenseDeduction = false;
    async.series([
        function (cb) {
            logging.consolelog("licenseDeductionitem", item, topic);
            //checking if any content shared with to shared customer
            var query = {
                assignedTo: item
            }
            ServiceAssignDataToCustomer.getAssignDataToCustomer(query, {
                assignedBy: 1
            }, {
                lean: true
            }, function (err, data) {
                if (!err) {
                    if (data.length > 0) {
                        assignedBy = data[0].assignedBy;

                    }
                    cb(null);
                } else {
                    cb(err);
                }
            })
        },
        function (cb) {
            logging.consolelog(assignedBy, superParentCustomerId, "assigned By in child mapper table")
            if (assignedBy != null) {

                ServiceChildMapper.getChildParentMapper({
                    parentCustomerId: assignedBy
                }, {}, {}, function (err, data) {
                    if (!err) {
                        if (data[0].superParentCustomerId == superParentCustomerId) {
                            //no license deduction
                            licenseDeduction = false
                        } else {
                            //license deduction

                            licenseDeduction = true;
                        }
                        cb(null);
                    } else {
                        cb(err);

                    }
                })
            } else {
                //license deduction
                licenseDeduction = true;
                cb(null);
            }


        },
        function (cb) {
            //
            if (licenseDeduction) {
                var query = {
                    _id: userId,
                    'license.parentCustomerId': superParentCustomerId

                }
                logging.consolelog(query, "", "check license")
                ServiceCustomer.getCustomer(query, {
                    'license.$': 1
                }, {}, function (err, data) {
                    if (!err) {
                        logging.consolelog(err, data, "get customer")
                        if (data.length > 0) {
                            if (data[0].license[0].usedLicense == data[0].license[0].allocatedLicense) {
                                cb(constants.STATUS_MSG.ERROR.CANNOT_SHARE);
                            } else {
                                logging.consolelog("jane do", "", "");
                                cb(null);
                            }
                        } else {
                            cb(constants.STATUS_MSG.ERROR.CANNOT_SHARE)
                        }
                    } else {
                        cb(null);
                    }
                })
            } else {
                cb(null);
            }
        },
        function (cb) {
            if (licenseDeduction) {
                var conditions = {
                    _id: userId,
                    'license.parentCustomerId': superParentCustomerId

                };

                var update = {
                    '$inc': {
                        'license.$.usedLicense': 1
                    }
                };
                /*Increment License counter*/
                /*Increment License counter*/
                var options = {};

                ServiceCustomer.update(conditions, update, options, function (err, data) {
                    if (!err) {
                        cb(null);
                    } else {
                        cb(err);
                    }

                })


            } else {
                cb(null);
            }


        },
        function (cb) {
            var query = {
                assignedBy: userId,
                assignedTo: item,
                superParentCustomerId: superParentCustomerId
            }
            logging.consolelog('topic', "", topic);
            ServiceAssignDataToCustomer.getAssignDataToCustomer(query, {}, {}, function (err, data) {
                if (!err) {

                    //push topic id to content array
                    var query = {
                        assignedBy: userId,
                        assignedTo: item,
                        superParentCustomerId: superParentCustomerId,
                        'content.topicId': {
                            $in: topic
                        }
                    }
                    var update = {
                        $pull: {
                            'content': {
                                'topicId': {
                                    $in: topic
                                }
                            }
                        }
                    }
                    ServiceAssignDataToCustomer.updateAssignDataToCustomer(query, update, {
                        lean: true,
                        safe: true
                    }, function (err, data) {
                        if (!err) {
                            //  create new assignedTo data customer entry in db
                            var query = {
                                assignedBy: userId,
                                assignedTo: item,
                                superParentCustomerId: superParentCustomerId
                            };
                            var dataToSet = {
                                $addToSet: {},
                                $set: {
                                    assignedBy: userId,
                                    assignedTo: item,
                                    superParentCustomerId: superParentCustomerId
                                }
                            };
                            if (content && content.length > 0) {
                                dataToSet["$addToSet"]["content"] = {
                                    $each: content
                                }
                            }
                            if (topic && topic.length > 0) {
                                dataToSet["$addToSet"]["topic"] = {
                                    $each: topic
                                };
                            }
                            ServiceAssignDataToCustomer.updateAssignDataToCustomer(query, dataToSet, {
                                upsert: true
                            }, function (err, data) {
                                if (data.nModified == 0 && !data.upserted) {
                                    repeat = true;
                                }
                                if (err) {
                                    cb(err);
                                } else {
                                    cb(null);
                                }
                            })
                        } else {
                            cb(err);
                        }
                    })


                } else {
                    cb(err);

                }
            })

        },
    ], function (err, data) {
        if (!err) {
            callback(null);
        } else {
            callback(err);
        }
    })
}

var shareContent = function (request, response) {
    var superParentCustomerId = 0;
    var userId= request.userData.id;
    var topicId = [];
    var content = [];
    var topic = [];
    async.series([
            function (cb) {
                //fetch super parent of customer who is sharing
                ServiceChildMapper.getChildParentMapper({
                    childCustomerId: userId
                }, {
                    superParentCustomerId: 1
                }, {
                    lean: true
                }, function (err, data) {
                    if (!err) {
                        if (data.length > 0) {
                            superParentCustomerId = data[0].superParentCustomerId;
                        }
                        cb(null);
                    } else {
                        cb(err);
                    }
                });

            },
            function (cb) {
                var counter = 0;
                async.forEach(request.body.topic, function (item, embeddedcb) {
                    counter++
                    var query = {
                        name: item

                    }
                    query.language = {
                        $in: request.body.language
                    }

                    ServiceTopic.getTopic(query, {
                        _id: 1
                    }, {}, function (err, response) {

                        if (err) {

                            embeddedcb(null);
                        } else {
                            if (response.length > 0)
                                topicId.push(response[0]._id);
                            embeddedcb(null);
                        }
                    });


                }, function (err) {
                    if (counter == request.body.topic.length) {
                        cb();
                    }

                });
            },
            function (cb) {
                logging.consolelog("topic Array", "", topicId);
                var query = {
                    customerId: superParentCustomerId,
                    paymentStatus: constants.DATABASE.STATUS.COMPLETED
                }
                ServiceCustomerBuyPackages.getTransaction(query, {
                    topic: 1,
                    content: 1
                }, {}, function (err, data) {
                    if (!err) {
                        logging.consolelog("logging", err, data);
                        for (var i = 0; i < data.length; i++) {
                            for (var j = 0; j < data[i].topic.length; j++) {
                                logging.consolelog("logging", topicId.indexOf(data[i].topic[j].topicId), data[i].topic[j].topicId)
                                if (topicId.indexOf(data[i].topic[j].topicId) != -1) {
                                    topic.push(data[i].topic[j].topicId);
                                }

                            }
                            for (var k = 0; k < data[i].content.length; k++) {
                                logging.consolelog("", "", topicId.indexOf(data[i].content[k].topicId))
                                if (topicId.indexOf(data[i].content[k].topicId) != -1) {
                                    content.push({
                                        topicId: data[i].content[k].topicId,
                                        categoryId: data[i].content[k].categoryId,
                                        contentType: data[i].content[k].contentType,
                                        startDate: request.body.startDate,
                                        endDate: request.body.endDate
                                    })
                                }
                            }

                        }
                        cb(null);
                    } else {
                        cb(err);
                    }
                })
            },

            function (cb) {
                logging.consolelog(topic, content, "topic Content");
                async.forEach(request.body.customerId, function (item, callback) {
                    licenseDeduction(item, superParentCustomerId, userId, topic, content, function (err, result) {
                        if (!err) {

                            callback(null);
                        } else {
                            callback(err);
                        }
                    });
                }, function (err) {
                    if (!err) {
                        cb(null);
                    } else {

                        cb(err);
                    }
                });
            }],
        function (err, data) {
            if (!err) response.status(200).send(constants.STATUS_MSG.SUCCESS.TOPIC_SHARED);
            else response.send(err);
        })

}

//				abc(data, callback) {
//
//					async.series([
//                        function (cb) {
//							if (item.usedLicense == item.allocatedLicense) {
//								cb(err);
//							} else {
//								cb(null);
//							}
//                        },
//                        function (cb) {
//							var conditions = {
//								_id: item.parentId,
//								license: {
//									$elemMatch: {
//										"parentCustomerId": superParentCustomerId,
//										"isActive": true
//									}
//								}
//
//							};
//							var update = {
//								'license.$.usedLicense': {
//									$inc: {
//										usedLicense: 1
//									}
//								}
//							}; /*Increment License counter*/
//							var options = {};
//
//							ServiceCustomer.update(conditions, update, options, function (err, data) {
//								if (!err) {
//									cb(null);
//								} else {
//									cb(err);
//								}
//
//							})
//                        }
//                    ], function (err, data) {
//						if (!err) {
//							callback(null);
//						} else {
//							callback(err);
//						}
//					})
//				}
////
//				var revokeLicense = function (request, response) {
//						var parents = [];
//						async.series([
//
//                                    function (cb) {
//										var query = {
//											superParentCustomerId: request.body.superParentCustomerId,
//											childCustomerId: request.body.childCustomerId
//										}
//										var projection = {
//											parentCustomerId: 1
//										}
//										var options = {
//											timeStamp: 'asc'
//										}
//										var populate = [{
//											path: 'parentCustomerId',
//											match: {},
//											select: 'license',
//											options: {
//												lean: true
//											}
//                                        }];
//										ServiceChildMapper.childParentMapperPopulate(query, projection, populate, options, {}, function (err, data) {
//											if (!err) {
//												var i = 0;
//												var length = data.length;
//												for (i = 0; i < length; i++) {
//													for (var j = 0; j < data.parentCustomerId.license.length; j++) {
//														if (data.parentCustomerId.license[j].parentCustomerId == request.body.superParentCustomerId) {
//															parents.push({
//																parentId: data.parentCustomerId._id,
//																usedLicense: data.parentCustomerId.license[j].usedLicense,
//																allocatedLicense: data.parentCustomerId.license[j].allocatedLicense
//															})
//														}
//													}
//
//												}
//												cb(null);
//											} else {
//												cb(err);
//											}
//										});
//
//                                    },
//                                    function (cb) {
//										async.each(parents, function (item, callback) {
//
//
//												abc(item, function (err, data) {
//													if (err) {
//
//													} else {
//
//													}
//												})
//											})
//											//async.forEach
//											//check available license of each parent
//											//if present then increase usedLicense
//											//and vapis kro to revoking request parent
//                                    }


/**
 * Agent is adding sub-agent,school or customer under them.
 */

var addCustomer = function (request, response) {
    var userId= request.userData.id;
    var superParentCustomerId;
    var customerId;
    var name= request.userData.firstName;
    var password = UniversalFunctions.encryptData(UniversalFunctions.generateRandomStringAndNumbers());
    var userType=request.userData.userType;
    var dataToSave = {};
    var customerExists = false;
    var customerType = null;
    async.series([
            function (cb) {
                //find super parent of who is adding customer.
                ServiceChildMapper.getChildParentMapper({
                    childCustomerId: userId
                }, {
                    superParentCustomerId: 1
                }, {
                    lean: true
                }, function (err, data) {
                    if (!err) {
                        if (data.length > 0) {
                            superParentCustomerId = data[0].superParentCustomerId;
                        }
                        cb(null);
                    } else {
                        cb(err);
                    }
                });

            },
            function (cb) {
                //check customer to be added already exists in the system
                var query = {
                    $or: [{
                        emailId: request.body.emailId
                    }]
                };
                ServiceCustomer.getCustomer(query, {}, {
                    lean: true
                }, function (error, data) {
                    if (error) {
                        cb(error);
                    } else {
                        if (data && data.length > 0) {
                            if (data[0].phoneVerified == true) {
                                customerId = data[0]._id;
                                customerExists = true;
                                customerType = data[0].userType;
                                cb(null);
                            } else {
                                ServiceCustomer.deleteCustomer({
                                    _id: data[0]._id
                                }, function (err, updatedData) {
                                    if (err) cb(err)
                                    else cb(null);
                                })
                            }
                        } else {
                            cb(null);
                        }
                    }
                });
            },
            function (cb) {
                ServiceChildMapper.getChildParentMapper({
                    childCustomerId: customerId
                }, {
                    _id: 1
                }, {}, function (err, data) {
                    if (!err) {
                        if (data.length > 0) {
                            cb(constants.STATUS_MSG.ERROR.USER_ALREADY_REGISTERED);
                        } else {
                            cb(null);
                        }
                    } else {
                        cb(err);
                    }
                });


            },
            function (cb) {
                if (request.files && customerExists == false) {
                    if (request.files.profilePic) {
                        UploadManager.uploadPicToS3Bucket(request.files.profilePic, "profile", function (filename) {
                            if (filename == "No File") {
                                cb(null);
                            } else {
                                dataToSave.profilePicUrl = AwsConfig.s3BucketCredentials.s3URL + '/profile/' + filename;
                                cb(null);
                            }
                        });
                    } else {
                        cb(null);
                    }
                } else {
                    cb(null);
                }
            },
            function (cb) {
                //check license of customer who is adding new customer
                if (request.body.license) {
                    var query = {
                        _id: userId,
                        'license.parentCustomerId': superParentCustomerId
                    }
                    ServiceCustomer.getCustomer(query, {
                        'license.$': 1
                    }, {}, function (err, data) {
                        logging.consolelog(err, data, "license cut")
                        if (!err) {
                            if (data.length > 0) {
                                logging.consolelog("license details", data, data[0].usedLicense, data[0].allocatedLicensen);
                                if (data[0].license[0].usedLicense == data[0].license[0].allocatedLicense || ((data[0].license[0].allocatedLicense-data[0].license[0].usedLicense)<request.body.license)) {
                                    cb(constants.STATUS_MSG.ERROR.CANNOT_SHARE);
                                } else if(request.body.license > data[0].license[0].allocatedLicense) {
                                    cb(constants.STATUS_MSG.ERROR.LICENSE_ERROR);
                                }else{
                                    logging.consolelog("jane do", "", "");
                                    cb(null);
                                }
                            } else {
                                cb(constants.STATUS_MSG.ERROR.CANNOT_SHARE)
                            }
                        } else {
                            cb(null);
                        }
                    })
                } else {
                    cb(null);
                }
            },
            function (cb) {
                // save new customer in the system.
                if (customerExists == false) {
                    dataToSave.countryCode = request.body.countryCode;
                    dataToSave.mobileNo = request.body.mobileNo;
                    dataToSave.registrationDate = new Date().toISOString();
                    dataToSave.lastName = request.body.lastName;
                    dataToSave.firstName = request.body.firstName;
                    dataToSave.curriculum = request.body.curriculum;
                    dataToSave.grade = request.body.grade;
                    dataToSave.gender = request.body.gender;
                    dataToSave.phoneVerified = true;
                    dataToSave.password = password;
                    dataToSave.addressValue = request.body.addressValue;
                    dataToSave.state = request.body.state;
                    dataToSave.city = request.body.city;
                    dataToSave.zip = request.body.zip;
                    dataToSave.gender = request.body.gender;
                    if (request.body.longitude && request.body.latitude) {
                        dataToSave.location = {
                            "coordinates": [request.body.longitude, request.body.latitude],
                            "type": "Point"
                        }
                    } else {
                        request.body.longitude = 0;
                        request.body.latitude = 0;
                        dataToSave.location = {
                            "coordinates": [request.body.longitude, request.body.latitude],
                            "type": "Point"

                        }
                    }
                    dataToSave.emailId = request.body.emailId;
                    dataToSave.userType = request.body.userType;
                    dataToSave.country = request.body.country;
                    dataToSave.sessionTo = request.body.sessionTo;
                    dataToSave.sessionFrom = request.body.sessionFrom;
                    dataToSave.accessRights = request.body.accessRights;
                    ServiceCustomer.createCustomer(dataToSave, function (err, customerDataFromDB) {
                        logging.consolelog('hello', err, customerDataFromDB)
                        if (err) {
                            cb(err)
                        } else {
                            customerId = customerDataFromDB._id;
                            cb();
                        }
                    })
                } else {
                    cb(null);
                }

            },
            function (cb) {
                // create link between new customer and customer who is adding
                var dataToSave = {
                    'childCustomerId': customerId,
                    'childCustomerType': customerType ? customerType : request.body.userType,
                    parentCustomerId: userId,
                    parentCustomerType: userType,
                    superParentCustomerId: superParentCustomerId
                };
                ServiceChildMapper.createChildParentMapper(dataToSave, function (err, data) {
                    if (err) {
                        cb(err);
                    } else {
                        cb(null);
                    }
                })
            },


            function (cb) {

                /*Update License counter in new customer*/
                var query = {
                    _id: customerId
                }

                var update = {
                    $push: {
                        license: {
                            $each: [{
                                parentCustomerId: superParentCustomerId,
                                allocatedLicense: request.body.license ? request.body.license : 0,
                                usedLicense: 0,
                                isActive: true
                            }]
                        }
                    }
                }
                logging.consolelog("logging", query, update);
                ServiceCustomer.updateCustomer(query, update, {
                    lean: true,
                    safe: true
                }, function (err, data) {
                    logging.consolelog(err, data, "new assign license to customer");
                    if (!err) {
                        cb(null);
                    } else {
                        cb(err);
                    }
                })


            },
            function (cb) {
                /*Increment License counter*/
                if (request.body.license) {


                    var conditions = {
                        _id: userId,
                        'license.parentCustomerId': superParentCustomerId

                    };
                    var license = parseInt(request.body.license);
                    var update = {
                        '$inc': {
                            'license.$.usedLicense': license
                        }
                    };
                    var options = {};
                    ServiceCustomer.update(conditions, update, options, function (err, data) {
                        logging.consolelog(err, data, "customer update deduction of license");
                        if (!err) {
                            cb(null);
                        } else {
                            cb(err);
                        }

                    })
                } else {
                    cb(null);
                }
            },
            function (cb) {
                /* send email to customer*/
                var asignee= name ? name: "Sterling Team";
                var emailSend = {
                    emailId: request.body.emailId,
                    password: UniversalFunctions.decryptData(password),
                    name: request.body.firstName + ' ' + request.body.lastName,
                    type: request.body.userType,
                    assign_name: asignee
                }
                var text;
                text = Templates.register(emailSend);
                NotificationManager.sendEmailToUser(request.body.emailId, "Congratulations!! Login to www.pixelsed.com", text, function (err, data) {
                    if (err) {
                        cb(err);
                    } else {
                        cb(null);
                    }
                });
            },
            function (cb) {

                var variableData = {}
                NotificationManager.sendNotifications(customerId, 'ADD_CUSTOMER', variableData, function (err, data) {

                })
                cb(null);


            }],
        function (err, data) {
            if (!err) {
                response.status(200).send(constants.STATUS_MSG.SUCCESS.AGENT_ADDED);
            } else {
                response.send(err);
            }

        })
}

var addCustomerInSystem = function (item, parentCustomerId, parentCustomerType, superParentCustomerId, callback) {
    var customerId = null;
    var password = UniversalFunctions.encryptData(UniversalFunctions.generateRandomStringAndNumbers());
    async.series([
        function (cb) {
            var query = {
                $or: [{
                    emailId: item.emailId
                }]
            };
            ServiceCustomer.getCustomer(query, {}, {
                lean: true
            }, function (error, data) {
                if (error) {
                    cb(error);
                } else {
                    if (data && data.length > 0) {
                        if (data[0].phoneVerified == true) {
                            cb(constants.STATUS_MSG.ERROR.USER_ALREADY_REGISTERED)
                        } else {
                            ServiceCustomer.deleteCustomer({
                                _id: data[0]._id
                            }, function (err, updatedData) {
                                if (err) cb(err)
                                else cb(null);
                            })
                        }
                    } else {
                        cb(null);
                    }
                }
            });
        },
        function (cb) {
            var dataToSave = {};

            dataToSave.mobileNo = item.mobileNo;
            dataToSave.registrationDate = new Date().toISOString();
            dataToSave.lastName = item.lastName;
            dataToSave.firstName = item.firstName;
            dataToSave.curriculum = item.curriculum;
            dataToSave.grade = item.grade;
            dataToSave.gender = item.gender;


            dataToSave.password =password;
            dataToSave.addressValue = item.addressValue;

            dataToSave.emailId = item.emailId;
            dataToSave.userType = item.userType;
            dataToSave.phoneVerified = true;
            dataToSave.sessionTo = item.sessionTo;
            dataToSave.sessionFrom = item.sessionFrom;
            dataToSave.addressValue = item.addressValue;
            dataToSave.state = item.state;
            dataToSave.city = item.city;
            dataToSave.zip = item.zip,
                dataToSave.gender = request.body.gender;
            if (item.longitude && item.latitude) {
                dataToSave.location = {
                    "coordinates": [item.longitude, item.latitude],
                    "type": "Point"
                }
            } else {
                item.longitude = 0;
                item.latitude = 0;
                dataToSave.location = {
                    "coordinates": [item.longitude, item.latitude],
                    "type": "Point"

                }
            }
            ServiceCustomer.createCustomer(dataToSave, function (err, customerDataFromDB) {
                logging.consolelog('hello', err, customerDataFromDB)
                if (err) {
                    cb(err)
                } else {
                    customerId = customerDataFromDB._id;
                    cb();
                }
            })
        },
        function (cb) {
            var dataToSave = {
                'childCustomerId': customerId,
                'childCustomerType': item.userType,
                parentCustomerId: parentCustomerId,
                parentCustomerType: parentCustomerType,
                superParentCustomerId: superParentCustomerId
            };
            ServiceChildMapper.createChildParentMapper(dataToSave, function (err, data) {
                if (err) {
                    cb(err);
                } else {
                    cb(null);
                }
            })
        },
        function (cb) {

            /*Update License counter in new customer*/
            var query = {
                _id: customerId
            }

            var update = {
                $push: {
                    license: {
                        $each: [{
                            parentCustomerId: superParentCustomerId,
                            allocatedLicense: item.license ? item.license : 0,
                            usedLicense: 0,
                            isActive: true
                        }]
                    }
                }
            }
            logging.consolelog("logging", query, update);
            ServiceCustomer.updateCustomer(query, update, {
                lean: true,
                safe: true
            }, function (err, data) {
                logging.consolelog(err, data, "new assign license to customer");
                if (!err) {
                    cb(null);
                } else {
                    cb(err);
                }
            })


        },
        function (cb) {

            var emailSend = {
                emailId: item.emailId,
                password: UniversalFunctions.decryptData(password),
                name: item.firstName + '' + item.lastName,
                type: item.userType
            }
            var text;
            text = Templates.register(emailSend);
            NotificationManager.sendEmailToUser(item.emailId, "Congratulations!! Login to www.pixelsed.com", text, function (err, data) {

                cb(null);

            });
        }], function (err, data) {
        if (!err) {
            callback(constants.STATUS_MSG.SUCCESS.AGENT_ADDED);
        } else {
            callback(constants.STATUS_MSG.SUCCESS.AGENT_ADDED);
        }
    })
}

var manageCSVData = function (data, parentCustomerId, parentCustomerType, superParentCustomerId, callback) {
    var CSVData = data;
    var arrangedData = [];
    async.series([
        function (cb) {
            var DataObject = {};
            var constants = [];
            if (CSVData) {
                var taskInSubcat = [];
                for (var key in CSVData) {
                    (function (key) {
                        taskInSubcat.push((function (key) {
                            return function (cb) {
                                //TODO
                                //logging.consolelog("item:::::::::>>>>",key,CSVData[key],CSVData[key].length)
                                if (key == 0) {
                                    //logging.consolelog("skip : ",key);
                                    for (var i = 0; i < CSVData[key].length; i++) {
                                        constants.push(CSVData[key][i]);
                                        if (i == CSVData[key].length - 1) {
                                            if (constants.length > 9) {
                                                var error = {};
                                                error.statusCode = 400;
                                                error.customMessage = "There are extra field in CSV please match the format";
                                                cb(error);
                                            } else {
                                                cb()
                                            }
                                        }
                                    }
                                } else {

                                    for (var i = 0; i < CSVData[key].length; i++) {
                                        DataObject[constants[i]] = CSVData[key][i]
                                        if (i == CSVData[key].length - 1) {
                                            arrangedData.push(DataObject);
                                            DataObject = {};
                                            cb()
                                        }
                                    }
                                }
                            }
                        })(key))
                    }(key));
                }
                async.parallel(taskInSubcat, function (err, result) {
                    logging.consolelog("arrangedData :::::::::::: ", "", arrangedData)
                    if (err) cb(err)
                    else {
                        cb(null);
                    }
                });
            }
        },
        function (cb) {
            logging.consolelog(arrangedData, "", "CSV Data");
            for (var i = 0; i < arrangedData.length; i++) {
                var Row = i + 1;
                var error = {};
                logging.consolelog(i, arrangedData.length, "test bug")
                if (arrangedData[i].emailId == "") {
                    error.statusCode = 400;
                    error.customMessage = "emailId Cannot Be Empty at Row:" + Row;
                    error.type = 'CURRICULUM_ERROR_CSV'
                    cb(error);
                }
                if (arrangedData[i].mobileNo == "") {
                    error.statusCode = 400;
                    error.customMessage = "mobileNo Field Cannot Be Empty at Row:" + Row
                    error.type = 'CURRICULUM_ERROR_CSV'
                    cb(error);
                }
                if (arrangedData[i].addressValue == "") {
                    error.statusCode = 400;
                    error.customMessage = "addressValue Field Cannot Be Empty at Row:" + Row
                    error.type = 'CURRICULUM_ERROR_CSV'
                    cb(error);
                }
                if (arrangedData[i].curriculum == "") {
                    error.statusCode = 400;
                    error.customMessage = "Ccurriculum Field Cannot Be Empty at Row:" + Row
                    error.type = 'CURRICULUM_ERROR_CSV'
                    cb(error);
                }
                if (arrangedData[i].grade == "") {
                    error.statusCode = 400;
                    error.customMessage = "Grade Field Cannot Be Empty at Row:" + Row
                    error.type = 'CURRICULUM_ERROR_CSV'
                    cb(error);
                }
                if (arrangedData[i].userType == "") {
                    error.statusCode = 400;
                    error.customMessage = "userType Field Cannot Be Empty at Row:" + Row
                    error.type = 'CURRICULUM_ERROR_CSV'
                    cb(error);
                }
                if (arrangedData[i].gender == "") {
                    error.statusCode = 400;
                    error.customMessage = "gender Field Cannot Be Empty at Row:" + Row
                    error.type = 'CURRICULUM_ERROR_CSV'
                    cb(error);
                }
                if (i == arrangedData.length - 1) {
                    cb()
                }
            }
            // cb(null)
        },
        function (cb) {
            var query = {
                $or: [{
                    emailId: item.emailId
                }, {
                    mobileNo: item.mobileNo
                }]
            };
            ServiceCustomer.getCustomer(query, {}, {
                lean: true
            }, function (error, data) {
                if (error) {
                    cb(error);
                } else {
                    if (data && data.length > 0) {
                        if (data[0].phoneVerified == true) {
                            cb(constants.STATUS_MSG.ERROR.USER_ALREADY_REGISTERED)
                        } else {
                            ServiceCustomer.deleteCustomer({
                                _id: data[0]._id
                            }, function (err, updatedData) {
                                if (err) cb(err)
                                else cb(null);
                            })
                        }
                    } else {
                        cb(null);
                    }
                }
            });
        },
        function (cb) {
            var counter = 0;
            async.forEach(arrangedData, function (item, embeddedcb) {
                counter++

                addCustomerInSystem(item, parentCustomerId, parentCustomerType, superParentCustomerId, function (err) {
                    if (counter == arrangedData.length) {
                        cb();
                    }

                });
            });
        }
    ], function (err, response) {
        if (err) callback(err);
        else callback(null);
    });
}
var customerUploadViaCSV = function (request, response) {
    var customers = [];
    var userId=request.userData.id;
    var superParentCustomerId;
    var userType=request.userData.userType;
    async.series([
        function (cb) {
            ServiceCustomer.getCustomer({
                _id: userId
            }, {
                userType: 1
            }, {
                lean: true
            }, function (err, data) {
                if (!err) {
                    if (data.length > 0) {
                        userType = data[0].userType;
                    }
                    cb(null);
                } else {
                    cb(err);
                }
            });
        },
        function (cb) {
            ServiceChildMapper.getChildParentMapper({
                childCustomerId: userId
            }, {
                superParentCustomerId: 1
            }, {
                lean: true
            }, function (err, data) {
                if (!err) {
                    if (data.length > 0) {
                        superParentCustomerId = data[0].superParentCustomerId;
                    }
                    cb(null);
                } else {
                    cb(err);
                }
            });

        },
        function (cb) {

            var csvFile = request.files.uploadCsv;
            if (csvFile && csvFile.originalFilename) {
                var path = Path.resolve("..") + "/uploads/" + "file.csv";
                UploadManager.saveCSVFile(csvFile.path, path, function (err, result) {
                    if (err) {
                        cb(constants.STATUS_MSG.ERROR.IMP_ERROR)
                    } else {
                        var file = path;

                        fs.readFile(file, function (err, data) { // Read the contents of the file
                            if (err) {
                                cb(constants.STATUS_MSG.ERROR.IMP_ERROR);
                            } else {
                                parseCsv(data.toString(), {
                                    comment: '#'
                                }, function (err, data) {

                                    if (err) {
                                        cb(err);
                                    } else {
                                        if (data != undefined && data != null) {

                                            manageCSVData(data, userId, userType, superParentCustomerId, function (err, response) {
                                                if (err) {

                                                    cb(err);
                                                } else {

                                                    customers = response;

                                                    cb()
                                                }
                                            })
                                        } else {
                                            cb();
                                        }

                                    }
                                });
                            }
                        });
                    }
                })
            } else {
                cb();
            }
        }
    ], function (err, data) {

        if (!err) response.status(200).send(constants.STATUS_MSG.SUCCESS.AGENT_ADDED);
        else {
            response.send(err);
        }
    });
}


var prepareFilter = function (key, data, query) {
    if (data) {
        try {
            data = JSON.parse(data);
        } catch (e) {
        }
        if (!Array.isArray(data)) {
            data = [data]
        }
        if (data && data.length) {
            query[key] = {
                $in: data
            }
        }
    }
}


/*Increment License counter*/
var getPurchasedTopics = function (request, response) {
    var userType=request.userData.userType;
    var userId=request.userData.id;
    var topicIds = [];
    var topics = [];
    var languagesAvailable=[];
    var topicsObject = {};
    var package = {};
    var final = {};
    async.series([
        function (cb) {
            logging.consolelog("logging", userId, userType);
            ServiceChildMapper.getChildParentMapper({
                childCustomerId: userId
            }, {
                superParentCustomerId: 1
            }, {
                lean: true
            }, function (err, data) {
                if (!err) {
                    if (data.length > 0) {
                        superParentCustomerId = data[0].superParentCustomerId;
                    }
                    cb(null);
                } else {
                    cb(err);
                }
            });
        },
        function (cb) {
            logging.consolelog(userType, superParentCustomerId, constants.DATABASE.STATUS.COMPLETED)
            if (userType == constants.USER_TYPE.AGENT) {
                var query = {
                    'customerId': superParentCustomerId,
                    'paymentStatus': constants.DATABASE.STATUS.COMPLETED
                };

                var projection = {
                    content: 1,
                    packageName: 1
                }

                ServiceCustomerBuyPackages.getTransaction(query, projection, {}, function (err, data) {
                    if (!err) {
                        for (var i = 0; i < data.length; i++) {
                            topicsObject[data[i]._id] = {
                                topics: [],
                                packageName: data[i].packageName,
                                packageId: data[i]._id

                            }
                            logging.consolelog(data[i].content, "", "content");
                            if (data[i].content) {
                                for (var k = 0; k < data[i].content.length; k++) {
                                    logging.consolelog(data[i].content[k].isActive, data[i].content[k].isActive, "content");
                                    if (data[i].content[k].isActive == true || data[i].content[k].isActive == undefined) {
                                        if (new Date() < new Date(data[i].content[k].endDate)) {
                                            topicsObject[data[i]._id]["topics"].push(data[i].content[k].topicId);
                                            topicsObject[data[i]._id]["endDate"] = data[i].content[0].endDate;
                                        }
                                    }
                                }
                            }
                        }
                        cb(null);
                    } else {
                        cb(err);
                    }
                });
            }
            if (userType == constants.USER_TYPE.TEACHER || userType == constants.USER_TYPE.SCHOOL || userType == constants.USER_TYPE.SUB_AGENT) {
                var query = {
                    assignedTo: userId,
                    superParentCustomerId: superParentCustomerId
                }
                logging.consolelog(query, "", "check hit");
                ServiceAssignDataToCustomer.getAssignDataToCustomer(query, {
                    topic: 1,
                    content: 1
                }, {}, function (err, data) {
                    if (!err) {
                        for (var i = 0; i < data.length; i++) {
                            topicsObject[data[i]._id] = {
                                topics: [],
                                packageName: "Assigned Package",
                                packageId: data[i]._id

                            }
                            if (data[i].content) {
                                for (var k = 0; k < data[i].content.length; k++) {
                                    if (data[i].content[k].isActive == true || data[i].content[k].isActive == undefined) {
                                        if (new Date() < new Date(data[i].content[k].endDate)) {
                                            topicsObject[data[i]._id]["topics"].push(data[i].content[k].topicId);
                                            topicsObject[data[i]._id]["endDate"] = data[i].content[0].endDate;
                                            topicIds.push(data[i].content[k].topicId);
                                        }

                                    }

                                }
                            }
                        }
                        cb(null);
                    } else {
                        cb(err);
                    }
                })
            }
        },
        function (cb) {
            logging.consolelog("merged arRAT of name", "", topicsObject);
            var mergedArray = [];
            for (var key in topicsObject) {
                mergedArray.push(topicsObject[key]);
            }
            var counter = 0;
            logging.consolelog("merged arRAT of packages", "", mergedArray);
            async.forEach(mergedArray, function (item, embeddedcb) {
                counter++
                if (item.topics.length > 0) {
                    package[item.packageId] = {
                        topics: [],
                        packageName: item.packageName,
                        endDate: item.endDate,
                        packageId: item.packageId
                    }
                    var query = {
                        'board': constants.COMMON_VARIABLES.UNIVERSAL_BOARD_NAME,
                        'isRemoved': false,
                        'isDeleted': false,
                        'isPublish': true
                    }
                    var sort = {
                        sortBy: "sequenceOrder"
                    };
                    query.topicId = {
                        $in: item.topics
                    }
                    var search = "";

                    var limit = {
                        limit: 5000,
                        offset: 0
                    }
                    ServiceTopic.getTopicsByCurriculum(query, sort, limit, search, function (err, data) {
                        var curriculumsIds = [];
                        if (!err) {
                            if (data.length > 0) {
                                data.forEach(function (curriculum) {
                                    if (curriculum && curriculum.name) {
                                        package[item.packageId]["topics"].push({
                                            Id: curriculum["_id"],
                                            icon: curriculum.icon,
                                            name: curriculum.name,
                                            board: curriculum.board,
                                            subject: curriculum.subject,
                                            topicLanguage: curriculum.topicLanguage,
                                            grade: curriculum.grade,
                                            chapter: curriculum.chapter,
                                            addedDate: curriculum.addedDate
                                        });
                                    }
                                });
                                embeddedcb(null);
                            } else {
                                embeddedcb(null);
                            }
                        } else {
                            cb(constants.STATUS_MSG.ERROR.TOPIC_NOT_FOUND);
                        }
                    });
                } else {
                    embeddedcb(null);
                }
            }, function (err) {
                if (counter == mergedArray.length) {
                    cb(null);
                }
            });
        },
        function (cb) {
            var mergedArray = [];
            logging.consolelog("package data", "", package);
            for (var key in package) {
                mergedArray.push(package[key]);
            }
            var counter = 0;
            logging.consolelog("merged set", "", mergedArray);
            async.forEach(mergedArray, function (item, embeddedcb) {
                counter++;
                var mergeObject = {};
                for (var i in item.topics) {
                    if (mergeObject.hasOwnProperty(item.topics[i]["name"])) {
                        if (_.contains(mergeObject[item.topics[i]["name"]]["language"], item.topics[i]["topicLanguage"])) {
                            continue;
                        } else {
                            mergeObject[item.topics[i]["name"]]["language"].push(item.topics[i]["topicLanguage"]);
                        }
                    } else {
                        mergeObject[item.topics[i]["name"]] = {
                            name: item.topics[i]["name"],
                            icon: item.topics[i]["icon"],
                            language: item.topics[i]["topicLanguage"] ? [item.topics[i]["topicLanguage"]] : [],
                            grade: item.topics[i]["grade"],
                            subject: item.topics[i]["subject"],
                            chapter: item.topics[i]["chapter"]
                        }
                    }
                }
                final[item.packageId] = {
                    packageId: item.packageId,
                    packageName: item.packageName,
                    endDate: item.endDate,
                    topics: mergeObject
                }
                logging.consolelog(final, "", "final object");
                embeddedcb(null);
            }, function (err) {
                if (counter == mergedArray.length) {
                    cb(null);
                }
            });
        },
        function (cb) {
            //set all languages
            for (var i in final){
                logging.consolelog('i>>>>>>>',final,i)
                for(var j in final[i].topics){
                    logging.consolelog('j>>>>>>>',j,'')
                    final[i].topics[j].language.forEach(function(item){
                        if(languagesAvailable.indexOf(item)==-1){
                            languagesAvailable.push(item)
                        }
                    })
                }   
            }
            cb(null)
        }],
    function (err, data) {
        if (!err) {
            logging.consolelog("logging", "", "response data");
            response.status(200).send({
                topics: final,
                languages:languagesAvailable,
                statusCode: 200
            })
        } else {
            response.send(err);
        }
    })
}


var getGradeAndCountryList = function (request, response) {
    var grade = [];
    var country = [];
    async.auto({
        country: function (cb) {
            ServiceCustomer.getDistinctCustomer({}, 'country', function (err, data) {
                if (!err) {
                    for (var i = 0; i < data.length; i++) {
                        country.push(data[i]);
                    }
                    cb(null);
                } else {
                    cb(err);
                }

            })
        },
        grade: function (cb) {
            var grade1 = [];
            ServiceCustomer.getCustomer({}, {
                'grade': 1
            }, {}, function (err, data) {

                for (var i = 0; i < data.length; i++) {
                    if (data[i].grade) {
                        for (var j = 0; j < data[i].grade.length; j++) {
                            grade1.push(data[i].grade[j]);
                        }
                        grade = _.unique(grade1);
                    }

                }
                cb(null);
            });
        },
    }, function (err, data) {
        if (!err) {
            response.status(200).send({
                grade: grade,
                country: country,
                statusCode: 200

            })
        } else {
            response.send(err);
        }
    })
}


function findParentOfChild(customerId, type, temp, callback) {
    logging.consolelog("helllo::", customerId, type)
    if (type == constants.USER_TYPE.AGENT) {
        logging.consolelog("1st case", "", type);
        findChildOfParent1([customerId], temp, function (err, data) {
            if (!err) {
                x = data;
                logging.consolelog(x, "", "return value of x")
                callback(null, x);
            }
        });

    } else if (type != 'SCHOOL') {
        logging.consolelog("2nd case", "", type);
        var query = {
            childCustomerId: customerId
        }
        ServiceChildMapper.getChildParentMapper(query, {
            parentCustomerId: 1,
            parentCustomerType: 1
        }, {
            lean: true
        }, function (err, data) {
            if (!err) {
                if (data.length > 0) {

                    findParentOfChild(data[0].parentCustomerId, data[0].parentCustomerType, temp, callback);
                }
            } else {
                callback(err);
            }
        })
    } else {
        logging.consolelog("3rd case", "", "");
        findChildOfParent1([customerId], temp, function (err, data) {
            if (!err) {
                x = data;
                logging.consolelog(x, "", "return value of x")
                callback(null, x);
            }
        });
    }
}

function findChildOfParent(customerId, temp, callback) {
    logging.consolelog("hello, findChildOfParent", "", customerId);
    var childCustomers = []
    var query = {
        parentCustomerId: {
            $in: customerId
        }
    }
    ServiceChildMapper.getChildParentMapper(query, {
        childCustomerId: 1
    }, {
        lean: true
    }, function (err, data) {
        if (!err) {
            logging.consolelog("hello", data, temp);
            if (data.length > 0) {

                for (var i = 0; i < data.length; i++) {
                    temp.push(data[i].childCustomerId);
                    childCustomers.push(data[i].childCustomerId);
                }
                findChildOfParent(childCustomers, temp, callback);
            } else {
                callback(null, temp);
            }

        } else {
            callback(err);
        }
    });

}


function findChildOfParent1(customerId, temp, cb) {
    logging.consolelog("hello, findChildOfParent", "", customerId);
    var childCustomers = []
    var query = {
        parentCustomerId: {
            $in: customerId
        }
    }
    ServiceChildMapper.getChildParentMapper(query, {
        childCustomerId: 1
    }, {
        lean: true
    }, function (err, data) {
        if (!err) {
            logging.consolelog("hello", data, temp);
            if (data.length > 0) {

                for (var i = 0; i < data.length; i++) {
                    temp.push(data[i].childCustomerId);
                    childCustomers.push(data[i].childCustomerId);
                }
                findChildOfParent1(childCustomers, temp, cb);
            } else {
                logging.consolelog(temp, "", "temp final value")
                cb(null, temp);
            }

        } else {
            cb(err);
        }
    });

}


/*Find sub-agents and school under one agent
 This structure is following a tree structure
 Using recursion to traverse tree
 */
var getAgents = function (request, response) {
    var userId = request.userData.id;
    var parentCustomerId = null;
    var parentType = null;
    var customers = [];
    var accessRights = false;
    var x = [];
    async.series([
        function (cb) {
            /*Check access Rights of customer */
            ServiceCustomer.getCustomer({
                _id: userId
            }, {
                accessRights: 1
            }, {
                lean: true
            }, function (err, data) {
                if (!err) {
                    for (var i = 0; i < data[0].accessRights.length; i++) {
                        if (data[0].accessRights[i].key == 'SETTINGS') {
                            accessRights = data[0].accessRights[i].peer_view;
                            break;
                        } else {
                            continue;
                        }
                    }
                    cb(null);
                } else {
                    cb(err);
                }
            })

        },

        function (cb) {
            // with customerId find it super agent(main root of tree)
            ServiceChildMapper.getChildParentMapper({
                childCustomerId: userId
            }, {
                parentCustomerId: 1,
                parentCustomerType: 1
            }, {
                lean: true
            }, function (err, data) {
                if (!err) {
                    if (data.length > 0) {
                        logging.consolelog("logging", err, data);
                        if (data[0].parentCustomerId == null) {
                            logging.consolelog("hello", "", "");
                            parentCustomerId = userId;
                            parentType = constants.USER_TYPE.AGENT;
                        } else {
                            parentCustomerId = data[0].parentCustomerId;
                            parentType = data[0].parentCustomerType;
                        }

                    }
                    cb(null);
                } else {
                    cb(err);
                }
            });

        },
        function (cb) {
            // find agents/sub -agents and school with help of recursion
            var customerIds = []
            var temp = [];

            if (accessRights == false) {
                logging.consolelog(accessRights, "", "accessRights false")

                findChildOfParent([parentCustomerId], temp, function (err, data) {
                    logging.consolelog("logging", err, data);
                    if (!err) {
                        x = data;
                        cb(null);
                    }
                });
            } else {
                logging.consolelog(parentCustomerId, parentType, "accessRights true")
                findParentOfChild(parentCustomerId, parentType, temp, function (err, data) {
                    logging.consolelog(err, data, "final data");
                    if (!err) {
                        x = data;
                        cb(null);
                    }
                });

            }
        },
        function (cb) {
            //get details of child retreive after recursion
            logging.consolelog(x, "", "x data")
            var query = {
                _id: {
                    $in: x
                },
                userType: {
                    $in: [constants.USER_TYPE.TEACHER, constants.USER_TYPE.SCHOOL, constants.USER_TYPE.SUB_AGENT]
                }
            }
            var projection = {
                firstName: 1,
                lastName: 1,
                emailId: 1,
                mobileNo: 1,
                curriculum: 1,
                grade: 1,
                userType: 1,
                registrationDate: 1,
                addressValue: 1,
                accessRights: 1,
                profilePicUrl: 1,
                block: 1
            }
            ServiceCustomer.getCustomer(query, projection, {
                lean: true
            }, function (err, data) {
                if (!err) {
                    if (data.length > 0) {
                        for (var i = 0; i < data.length; i++) {
                            customers.push({
                                customerId: data[i]._id,
                                name: data[i].firstName + ' ' + data[i].lastName,
                                emailId: data[i].emailId,
                                grade: data[i].grade,
                                curriculum: data[i].curriculum,
                                addressValue: data[i].addressValue,
                                registerationDate: data[i].registrationDate,
                                userType: data[i].userType,
                                mobileNo: data[i].mobileNo,
                                accessRights: data[i].accessRights,
                                profilePic: data[i].profilePicUrl,
                                block: data[i].block
                            })
                        }
                        cb(null);
                    } else {
                        cb(null);
                    }
                }

            })
        }], function (err, data) {
        if (!err) {
            response.status(200).send({
                customers: customers,
                count: customers.length,
                statusCode: 200
            })
        } else {
            response.send(err);
        }
    })
};

var blockAgent = function (request, response) {
    var emailId;
    async.series([
        function (cb) {
            var query = {
                _id: request.body.agentId
            }
            ServiceCustomer.update(query, {
                $set: {
                    block: true
                }
            }, {}, function (err, data) {

                if (!err) {
                    cb(null);
                } else {
                    cb(err);
                }
            })
        },
        function (cb) {
            ServiceCustomer.getCustomer({
                _id: request.body.agentId
            }, {
                emailId: 1
            }, {}, function (err, data) {
                if (!err) {
                    if (data.length > 0) {
                        emailId = data[0].emailId
                    }
                    cb(null)
                } else {
                    cb(err);
                }
            })
        },
        function (cb) {

            var emailSend = {
                link: 'www.pixelsed.com',
                action: 'blocked'
            }
            var text;
            text = TemplatesBlock.action(emailSend);
            NotificationManager.sendEmailToUser(emailId, 'Blocked by Agent!!', text, function (err, data) {
                if (!err) {

                }

            });
            cb(null);
        }
    ], function (err, result) {
        if (!err) {
            response.status(200).send(constants.STATUS_MSG.SUCCESS.CUSTOMER_BLOCKED);
        } else {
            response.send(err);
        }
    })
};


var unblockAgent = function (request, response) {
    var emailId;

    async.series([
        function (cb) {
            var query = {
                _id: request.body.agentId
            }
            ServiceCustomer.update(query, {
                $set: {
                    block: false
                }
            }, {}, function (err, data) {
                logging.consolelog(err, data, "output")
                if (!err) {
                    cb(null);
                } else {
                    cb(err);
                }
            })
        },
        function (cb) {
            ServiceCustomer.getCustomer({
                _id: request.body.agentId
            }, {
                emailId: 1
            }, {}, function (err, data) {
                if (!err) {
                    if (data.length > 0) {
                        emailId = data[0].emailId
                    }
                    cb(null)
                } else {
                    cb(err);
                }
            })
        },
        function (cb) {

            var emailSend = {
                link: 'www.pixelsed.com',
                action: 'unblocked'
            }
            var text;
            text = TemplatesBlock.action(emailSend);
            NotificationManager.sendEmailToUser(emailId, 'Unblocked by Agent!!', text, function (err, data) {
                if (!err) {
                    logging.consolelog('mail', err, data);
                }

            });
            cb(null);
        }
    ], function (err, result) {
        if (!err) {
            response.status(200).send(constants.STATUS_MSG.SUCCESS.CUSTOMER_UNBLOCKED);
        } else {
            response.send(err);
        }
    })
};


var editAgent = function (request, response) {
    var superParentCustomerId;
    var update = {};
    var userId=request.userData.id;
    async.series([
        function (cb) {
            //find super parent of who is adding customer.
            ServiceChildMapper.getChildParentMapper({
                childCustomerId: userId
            }, {
                superParentCustomerId: 1
            }, {
                lean: true
            }, function (err, data) {
                if (!err) {
                    if (data.length > 0) {
                        superParentCustomerId = data[0].superParentCustomerId;
                    }
                    cb(null);
                } else {
                    cb(err);
                }
            });

        },
        function (cb) {

            if (request.files) {
                if (request.files.profilePic) {
                    UploadManager.uploadPicToS3Bucket(request.files.profilePic, "profile", function (filename) {
                        if (filename == "No File") {
                            cb(null);
                        } else {
                            update.profilePicUrl = AwsConfig.s3BucketCredentials.s3URL + '/profile/' + filename;
                            cb(null);
                        }
                    });
                } else {
                    cb(null);
                }
            } else {
                cb(null);
            }
        },
        function (cb) {
            if (request.body.mobileNo) {
                update.mobileNo = request.body.mobileNo;
            }
            if (request.body.firstName) {
                update.firstName = request.body.firstName;
            }
            if (request.body.lastName) {
                update.lastName = request.body.lastName;
            }
            if (request.body.accessRights) {
                update.accessRights = request.body.accessRights;
            }
            if (request.body.state) {
                update.state = request.body.state;
            }
            if (request.body.country) {
                update.country = request.body.country;
            }
            if (request.body.address) {
                update.address = request.body.address;
            }
            if (request.body.gender) {
                update.gender = request.body.gender;
            }
            var query = {
                _id: request.body.agentId
            }
            logging.consolelog('update query', "", update)
            ServiceCustomer.update(query, update, {
                lean: true
            }, function (err, data) {

                if (!err) {
                    cb(null);
                } else {
                    cb(err);
                }
            })
        },
        function (cb) {
            /*Increment License counter*/
            if (request.body.license) {


                var conditions = {
                    _id: userId,
                    'license.parentCustomerId': superParentCustomerId

                };
                var license = parseInt(request.body.license);
                var update = {
                    '$inc': {
                        'license.$.usedLicense': license
                    }
                };
                var options = {};
                ServiceCustomer.update(conditions, update, options, function (err, data) {
                    // logging.consolelog(err, data, "customer update deduction of license");
                    if (!err) {
                        cb(null);
                    } else {
                        cb(err);
                    }

                })
            } else {
                cb(null);
            }
        },
        function (cb) {
            /*Increment License counter*/
            if (request.body.license) {


                var conditions = {
                    _id: request.body.agentId,
                    'license.parentCustomerId': superParentCustomerId

                };
                //				var license = parseInt(request.body.license) + 1;
                var update = {

                    'license.$.allocatedLicense': request.body.license

                };
                var options = {};
                ServiceCustomer.update(conditions, update, options, function (err, data) {
                    // logging.consolelog(err, data, "customer update deduction of license");
                    if (!err) {
                        cb(null);
                    } else {
                        cb(err);
                    }

                })
            } else {
                cb(null);
            }
        },
    ], function (err, data) {
        if (!err) {
            response.status(200).send(constants.STATUS_MSG.SUCCESS.EDIT_SUCCESSFULLY);
        } else {
            response.send(err);
        }
    })
}


// SAME WORKING LIKE GET AGENTS API
var getCustomers = function (request, response) {
    var userId = request.userData.id;
    var parentCustomerId = null;
    var parentType = null;
    var customers = [];
    var accessRights = false;
    var x = [];
    async.series([
        function (cb) {
            ServiceCustomer.getCustomer({
                _id: userId
            }, {
                accessRights: 1
            }, {
                lean: true
            }, function (err, data) {
                if (!err) {
                    for (var i = 0; i < data[0].accessRights.length; i++) {
                        if (data[0].accessRights[i].key == 'MANAGE_CUSTOMERS') {
                            accessRights = data[0].accessRights[i].peer_view;
                            break;
                        } else {
                            continue;
                        }
                    }
                    cb(null);
                } else {
                    cb(err);
                }
            })

        },
        function (cb) {
            // with customerId find it super agent(main root of tree)
            ServiceChildMapper.getChildParentMapper({
                childCustomerId: userId
            }, {
                parentCustomerId: 1,
                parentCustomerType: 1
            }, {
                lean: true
            }, function (err, data) {
                if (!err) {
                    if (data.length > 0) {
                        logging.consolelog(err, data, "err,data");
                        if (data[0].parentCustomerId == null) {
                            logging.consolelog("hello", "", "");
                            parentCustomerId = userId;
                            parentType = constants.USER_TYPE.AGENT;
                        } else {
                            parentCustomerId = data[0].parentCustomerId;
                            parentType = data[0].parentCustomerType;
                        }

                    }
                    cb(null);
                } else {
                    cb(err);
                }
            });

        },
        function (cb) {

            var customerIds = [];
            var temp = [];

            if (accessRights == false) {
                findChildOfParent([parentCustomerId], temp, function (err, data) {
                    if (!err) {
                        x = data;
                        cb(null);
                    }
                });
            } else {
                findParentOfChild(parentCustomerId, parentType, temp, function (err, data) {
                    if (!err) {
                        x = data;
                        cb(null);
                    }
                });

            }

        },
        function (cb) {

            var query = {
                _id: {
                    $in: x
                }
            }
            if(request.body.search){
                query['$or'] = [
                    {
                        emailId: {
                            $regex: '.*' + request.body.search + '.*',
                            $options: "i"
                        }
                    },
                    {
                        firstName: {
                            $regex: '.*' + request.body.search + '.*',
                            $options: "i"
                        }
                    },
                    {
                        lastName: {
                            $regex: '.*' + request.body.search + '.*',
                            $options: "i"
                        }
                    },
                    {
                        mobileNo: {
                            $regex: '.*' + request.body.search + '.*',
                            $options: "i"
                        }
                    }
                ]
            }
            if (request.body.userType) {
                query.userType = {
                    $in: request.body.userType
                }
            } else {
                query.userType = {
                    $in: [constants.USER_TYPE.TEACHER, constants.USER_TYPE.STUDENT, constants.USER_TYPE.SUB_AGENT, constants.USER_TYPE.SCHOOL]
                }
            }

            if (request.body.gender) {
                query.gender = {
                    $in: request.body.gender
                }
            }

            if (request.body.country) {
                query.country = {
                    $in: request.body.country
                }
            }
            if (request.body.grade) {
                query.grade = {
                    $in: request.body.grade
                }
            }
            if (request.body.registrationDateTo && request.body.registerationDateFrom) {

                query.registrationDate = {
                    $gte: request.body.registerationDateFrom,
                    $lte: request.body.registrationDateTo

                }

            }

            var projection = {
                firstName: 1,
                lastName: 1,
                emailId: 1,
                mobileNo: 1,
                curriculum: 1,
                grade: 1,
                userType: 1,
                registrationDate: 1,
                addressValue: 1,
                accessRights: 1,
                profilePicUrl: 1,
                block: 1
            }
            ServiceCustomer.getCustomer(query, projection, {
                lean: true
            }, function (err, data) {
                if (!err) {
                    if (data.length > 0) {
                        for (var i = 0; i < data.length; i++) {
                            customers.push({
                                customerId: data[i]._id,
                                name: data[i].firstName + ' ' + data[i].lastName,
                                emailId: data[i].emailId,
                                grade: data[i].grade,
                                curriculum: data[i].curriculum,
                                addressValue: data[i].addressValue,
                                registerationDate: data[i].registrationDate,
                                userType: data[i].userType,
                                mobileNo: data[i].mobileNo,
                                accessRights: data[i].accessRights,
                                profilePic: data[i].profilePicUrl,
                                block: data[i].block
                            })
                        }
                        cb(null);
                    } else {
                        cb(null);
                    }
                }

            })
        }], function (err, data) {
        if (!err) {
            response.status(200).send({
                customers: customers,
                count: customers.length,
                statusCode: 200
            })
        } else {
            response.send(err);
        }
    })
}


var sharedTopicHistory = function (request, response) {
    var content = [];
    var final = [];
    var objToSave = {};
    async.auto({
        topicHistory: function (cb) {
            var query = {
                assignedTo: request.body.customerId
            }
            var path = 'content.categoryId';
            var select = 'name price content'

            var populate = {
                path: path,
                match: {},
                select: select,
                options: {
                    lean: true
                }
            };
            var projection = {
                content: 1
            }
            ServiceAssignDataToCustomer.assignDataToCustomerPopulate(query, projection, populate, {}, {}, function (err, data) {
                if (!err) {
                    for (var i in data) {
                        if (data[i] && data.length > 0 && data[i].content && data[i].content.length > 0) {
                            for (var j = 0; j < data[i].content.length; j++) {
                                if (data[i].content[j].contentType) {
                                    for (var k = 0; k < data[i].content[j].categoryId.content.length; k++) {
                                        if (data[i].content[j].contentType == data[i].content[j].categoryId.content[k]._id) {
                                            logging.consolelog(data[i].content[j].isActive, data[i].content[j].isActive == true, data[i].content[j].isActive == undefined);
                                            if (data[i].content[j].isActive == true || data[i].content[j].isActive == undefined) {
                                                content.push({
                                                    categoryId: data[i].content[j].categoryId.name,
                                                    contentType: data[i].content[j].categoryId.content[k].typeOfVideo,
                                                    topicId: data[i].content[j].topicId,
                                                    startDate: data[i].content[j].startDate,
                                                    endDate: data[i].content[j].endDate
                                                });
                                            }

                                        }
                                    }

                                } else {
                                    if (data[i].content[j].isActive == true || data[i].content[j].isActive == undefined) {
                                        content.push({
                                            categoryId: data[i].content[j].categoryId.name,
                                            topicId: data[i].content[j].topicId,
                                            startDate: data[i].content[j].startDate,
                                            endDate: data[i].content[j].endDate
                                        });
                                    }

                                }
                            }

                        }
                    }
                    cb(null);
                } else {
                    cb(err);
                }
            });
        },
        get_topic_details: ['topicHistory', function (cb) {
            logging.consolelog("content print data", "", content);
            if (content.length > 0) {
                async.forEach(content, function (item, callback) {
                    var query = {
                        'board': constants.COMMON_VARIABLES.UNIVERSAL_BOARD_NAME,
                        'isRemoved': false,
                        'isDeleted': false,
                        'isPublish': true
                    }
                    query.topicId = item.topicId;
                    var limit = {
                        limit: 1,
                        offset: 0
                    }
                    var search = "";
                    ServiceTopic.getTopicsByCurriculum(query, {}, limit, search, function (err, data) {
                        if (!err) {
                            if (data.length > 0) {
                                final.push({
                                    Id: data[0]["_id"],
                                    name: data[0].name,
                                    icon: data[0].icon,
                                    board: data[0].board,
                                    subject: data[0].subject,
                                    language: data[0].boardLanguage,
                                    topicLanguage: data[0].topicLanguage,
                                    grade: data[0].grade,
                                    chapter: data[0].chapter,
                                    addedDate: data[0].addedDate,
                                    categoryId: item.categoryId,
                                    contentType: item.contentType,
                                    startDate: item.startDate,
                                    endDate: item.endDate
                                });
                            }

                            callback(null);
                        } else {
                            callback(err);
                        }

                    })

                }, function (err) {
                    if (!err) {

                        cb(null);
                    } else {

                        cb(err);
                    }
                });
            } else {
                cb(null);
            }
        }],
        embedded_details: ['get_topic_details', function (cb) {
            logging.consolelog(final, "", "final data print");
            var object = {};
            var output = final;
            if (final && final.length > 0) {
                for (var i in output) {
                    if (object.hasOwnProperty(output[i]["name"])) {
                        object[output[i]["name"]]["rating"].push(output[i]["rating"]);

                        if (object[output[i]["name"]]["content"].hasOwnProperty(output[i]["language"])) {
                            object[output[i]["name"]]["content"][output[i]["language"]]["categoryName"].push(output[i]["categoryId"]);
                            object[output[i]["name"]]["content"][output[i]["language"]]["contentType"].push(output[i]["contentType"]);


                        } else {
                            object[output[i]["name"]]["content"][output[i]["language"]] = {
                                categoryName: [output[i]["categoryId"]],
                                contentType: output[i]["contentType"] ? [output[i]["contentType"]] : [],
                                startDate: output[i]["startDate"],
                                endDate: output[i]["endDate"]
                            }


                        }

                    } else {
                        object[output[i]["name"]] = {
                            name: output[i]["name"],
                            icon: output[i]["icon"],
                            rating: output[i]["rating"] ? [output[i]["rating"]] : [],
                            board: output[i]["board"],
                            chapter: output[i]["chapter"],
                            subject: output[i]["subject"],
                            language: [output[i]["language"]],
                            content: {
                                [output[i]["language"]]: {
                                    categoryName: [output[i]["categoryId"]],
                                    contentType: output[i]["contentType"] ? [output[i]["contentType"]] : [],
                                    startDate: output[i]["startDate"],
                                    endDate: output[i]["endDate"]
                                }
                            }

                        }
                    }
                }


            }
            var resultArray = [];
            for (var key in object) {
                resultArray.push(object[key]);
            }
            objToSave.topic = resultArray;
            cb(null);
        }]
    }, function (err, data) {
        if (!err) {
            response.status(200).send({
                statusCode: 200,
                data: objToSave
            })
        } else {
            response.send(err);
        }
    })
}


var checkingOfEmbeddedLinks = function (request, response) {
    var url = "";
    async.series([
        function (cb) {
            //customerId //contentId // domainName
            var query = {
                'customerId': request.body.customerId,
                'content.contentType': request.body.contentId,
                'embeddedLink': true,
                'domainName': request.body.domainName
            }
            ServiceCustomerBuyPackages.getTransaction(query, {
                domainName: 1
            }, {
                lean: true
            }, function (err, data) {
                logging.consolelog('logging>>>>>>',err,data)
                if (!err) {
                    if (data.length > 0) {
                        cb(null);
                    } else {
                        cb(constants.STATUS_MSG.ERROR.USER_NOT_FOUND);
                    }
                } else cb(err);
            })

        },
        function (cb) {
            var query = {
                'content._id': request.body.contentId
            }

            ServiceTopicCategory.getTopicCategory(query, {
                'content.$': 1
            }, {
                lean: true
            }, function (err, data) {
                logging.consolelog('logging>>>>>>',err,data)
                if (!err) {
                    if (data.length > 0) {
                        url = data[0].content[0].contentUrl

                    }
                    cb(null);
                } else {
                    cb(err);
                }


            })

        }
    ], function (err, data) {
        if (!err) {
            response.status(200).send({
                contentUrl: url,
                statusCode: 200
            })
        } else {
            response.send(err);
        }
    })
}


var deleteAssignedContent = function (request, response) {
    request.body.topicId = [];
    var parentCustomerId = null;
    var parentType = null;
    var x = [];
    async.series([
        function (cb) {

            var query = {
                name: request.body.name,
                language: {
                    $in: request.body.language
                }
            }

            ServiceTopic.getTopic(query, {
                _id: 1
            }, {}, function (err, data) {
                if (!err) {
                    for (var i = 0; i < data.length; i++) {
                        request.body.topicId.push(data[i]._id);
                    }
                    cb(null);
                } else {
                    cb(err);
                }
            })
        },
        function (cb) {
            // with customerId find it super agent(main root of tree)
            ServiceChildMapper.getChildParentMapper({
                childCustomerId: request.body.userId
            }, {
                parentCustomerId: 1,
                parentCustomerType: 1
            }, {
                lean: true
            }, function (err, data) {
                if (!err) {
                    if (data.length > 0) {
                        logging.consolelog(err, data, "err,data");
                        if (data[0].parentCustomerId == null) {
                            logging.consolelog("hello", "", "");
                            parentCustomerId = request.body.userId;
                            parentType = constants.USER_TYPE.AGENT;
                        } else {
                            parentCustomerId = data[0].parentCustomerId;
                            parentType = data[0].parentCustomerType;
                        }

                    }
                    cb(null);
                } else {
                    cb(err);
                }
            });

        },
        function (cb) {

            var customerIds = [];
            var temp = [];
            findChildOfParent([parentCustomerId], temp, function (err, data) {
                if (!err) {
                    //retreive child bcs if deleting from parent then delte opic also from child roots
                    x = data;
                    //also push parent node
                    x.push(request.body.userId);
                    cb(null);
                }
            });


        },
        function (cb) {
            ServiceAssignDataToCustomer.getAssignDataToCustomer({
                assignedTo: {
                    $in: x
                }
            }, {
                content: 1
            }, {
                lean: true
            }, function (err, data) {
                if (!err) {
                    if (data.length > 0) {
                        var counter = 0;
                        async.forEach(data, function (item, embeddedCb) {
                                counter++;
                                item.content.forEach(function (singleContent) {
                                    if (request.body.topicId.indexOf(singleContent.topicId) != -1) {
                                        singleContent.isActive = false;
                                    }
                                })
                                var update = {
                                    $set: {
                                        'content': item.content
                                    }
                                }
                                ServiceAssignDataToCustomer.updateAssignDataToCustomer({
                                    _id: item._id
                                }, update, {
                                    lean: true,
                                    safe: true
                                }, function (err, innerdata) {
                                    logging.consolelog(err, innerdata, "update of child data");
                                    embeddedCb(null);
                                })

                            },
                            function (err) {
                                if (!err) {
                                    if (counter == data.length) {
                                        cb(null);
                                    }
                                } else {
                                    cb(err);
                                }
                            });
                    } else {
                        cb(null);
                    }
                } else {
                    cb(null);
                }

            });
        }], function (err, data) {
        if (!err) {
            response.status(200).send(constants.STATUS_MSG.SUCCESS.TOPIC_DELETE);
        } else {
            response.send(err);
        }
    });
}


function agentsValidBookedContent (filters,embeddedLink,id,status,callback){
    var query = {
        paymentStatus:status,
        customerId:id,
        isDeleted:false
    };
    if(embeddedLink) query.embeddedLink=embeddedLink;

    var projection = {topic:1,content:1,embeddedLink:1,domainName:1,paymentStatus:1,customerId:1,isDeleted:1}
    logging.consolelog('query',projection,query);
    ServiceCustomerBuyPackages.getTransaction(query,projection,{},function(err,data){
        if(err){
            callback(err,null)
        }else{
            logging.consolelog('getTransaction',err,data);
            //  prepare valid bought data
            if(data && data.length){
                for(var booking in data){
                    logging.consolelog('booking>>>','',data[booking]);
                    var validContent = [];
                    for(content in data[booking].content){
                        var now = new Date();
                        var endDate = new Date(data[booking].content[content].endDate);
                        logging.consolelog('datacheck',now,endDate);
                        logging.consolelog('content>>>>',now < endDate,data[booking].content[content]);
                        if(now < endDate){
                            validContent.push({
                                contentId:data[booking].content[content].contentType?data[booking].content[content].contentType:""
                            })
                        }
                    }
                    data[booking].content = validContent
                }
            }
            logging.consolelog('callback','',JSON.stringify(data));
            callback(null,data)
        }
    });
}

var getEmbeddedLinksForDomains = function (request,response) {
    //authenticate and get embedded links based on filters and domains
    logging.consolelog('entering function','','');
    async.auto({
        getCustomerBookings:function (cb,data) {
            var filters = {};
            if(request.body.topics)
                filters.topics = request.body.topics;
            if(request.body.language)
                filters.language =request.body.language;
            //check for valid booking
            logging.consolelog('getCustomerBookings','',JSON.stringify(data));
            agentsValidBookedContent(filters,true,data.authenticate._id,constants.DATABASE.STATUS.COMPLETED,function(err,validBookings){
                if(err){
                    cb(err)
                }else{
                    // all valid bookings not expired
                    if(validBookings.length){
                        cb(null,validBookings)
                    }else{
                        cb(null,[])
                    }
                }
            })

        },
        prepareDomainWiseEmbeddedLinks:['getCustomerBookings',function (cb,data) {
            logging.consolelog('prepareDomainWiseEmbeddedLinks','',JSON.stringify(data));
            var prepareDataToSend = [];
            if(data.getCustomerBookings && data.getCustomerBookings.length){
                //loop on data
                for(i=0;i<data.getCustomerBookings.length;i++){
                    // loop on domain
                    logging.consolelog('loop on data','',data.getCustomerBookings[i]);
                    for(j=0;j<data.getCustomerBookings[i].domainName.length;j++){
                        //loop on content
                        logging.consolelog('loop on domain','',data.getCustomerBookings[i].domainName[j]);
                        var domainIndexObj = _.findLastIndex(prepareDataToSend,{domainName:data.getCustomerBookings[i].domainName[j]});
                        if(domainIndexObj=-1){
                            
                            prepareDataToSend.push({
                                domainName:data.getCustomerBookings[i].domainName[j],
                                embeddedLinks:[]
                            });
                            //get the index of object where domainName matches
                            for(k=0;k<data.getCustomerBookings[i].content.length;k++){
                            // prepare embedded links
                                var indexInDataToSend = _.findLastIndex(prepareDataToSend,{domainName:data.getCustomerBookings[i].domainName[j]});
                                logging.consolelog('domainIndexObj',indexInDataToSend,data.getCustomerBookings[i].domainName[j]);
                                logging.consolelog('contentId','',data.getCustomerBookings[i].content[k].contentId)

                                var embeddedLink =Templates.embeddedLinkIframe(prepareDataToSend[indexInDataToSend].domainName,data.getCustomerBookings[i].content[k].contentId,data.authenticate._id);
                                //update prepareDataToSend with these links
                                if(data.getCustomerBookings[i].content[k].contentId)
                                    prepareDataToSend[indexInDataToSend].embeddedLinks.push(embeddedLink)
                            }
                            logging.consolelog('final data','',prepareDataToSend)
                        }else{
                            continue;
                        }
                    }
                }
                cb(null,prepareDataToSend)
            }else{
                cb(null,[])
            }
        }]
    },function (err,result) {
        logging.consolelog('err,result',err,result);
        if(err){
            response.send(err)
        }else {
            response.send({
                data:result.prepareDomainWiseEmbeddedLinks
            })
        }
    })
};

module.exports = {
    addCustomer: addCustomer,
    customerUploadViaCSV: customerUploadViaCSV,
    getCustomers: getCustomers,
    getPurchasedTopics: getPurchasedTopics,
    getGradeAndCountryList: getGradeAndCountryList,
    getAgents: getAgents,
    blockAgent: blockAgent,
    unblockAgent: unblockAgent,
    editAgent: editAgent,
    shareContent: shareContent,
    sharedTopicHistory: sharedTopicHistory,
    checkingOfEmbeddedLinks: checkingOfEmbeddedLinks,
    deleteAssignedContent: deleteAssignedContent,
    getEmbeddedLinksForDomains:getEmbeddedLinksForDomains
};