var async = require('async');
var ServiceTopic = require('../../Service/topic');
var ServicePackage = require('../../Service/package');
var constants = require('../../Library/appConstants');
var PackageType = constants.DATABASE.PACKAGE_TYPE;
var fs = require('fs');
var customerTopicModule = require('./customerTopicModule');
var Templates = require('../../Templates/superAdminRegister');
var ServiceCustomerBuyPackages = require('../../Service/customerBuyPackages');
var _ = require('underscore');
var ServiceAssignDataToCustomer = require('../../Service/assignDataToCustomer');
var ServiceCustomerCart= require("../../Service/customerCart");

var getPackage = function (request, response) {
    var openPackage = [];
    var package = [];
    var customisedPackage = [];
    async.series([
        function (cb) {
            ServicePackage.getPackage({
                isDeleted: false
            }, {}, {}, function (err, data) {
                logging.consolelog("logging", "", data);
                if (!err) {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].packageType == PackageType.PACKAGE) {
                            package.push({
                                monthlyDiscount: data[i].monthlyDiscount,
                                yearlyDiscount: data[i].yearlyDiscount,
                                curriculum: data[i].curriculum,
                                language: data[i].language,
                                grade: data[i].grade,
                                monthlyPrice: data[i].monthlyPrice,
                                yearlyPrice: data[i].yearlyPrice,
                                packageId: data[i]._id,
                                packageType: PackageType.PACKAGE,
                                typeOfDiscount: data[i].typeOfDiscount
                            })
                        } else if (data[i].packageType == PackageType.OPEN_PACKAGE) {
                            openPackage.push({
                                monthlyDiscount: data[i].monthlyDiscount,
                                yearlyDiscount: data[i].yearlyDiscount,
                                monthlyPrice: data[i].monthlyPrice,
                                yearlyPrice: data[i].yearlyPrice,
                                noOfTopics: data[i].noOfTopics,
                                packageId: data[i]._id,
                                packageType: PackageType.OPEN_PACKAGE,
                                typeOfDiscount: data[i].typeOfDiscount

                            })
                        } else if (data[i].packageType == PackageType.CUSTOMISED_PACKAGE) {
                            customisedPackage.push({
                                monthlyDiscount: data[i].monthlyDiscount,
                                yearlyDiscount: data[i].yearlyDiscount,
                                monthlyPrice: data[i].monthlyPrice,
                                yearlyPrice: data[i].yearlyPrice,
                                packageId: data[i]._id,
                                packageName: data[i].packageName,
                                packageType: PackageType.CUSTOMISED_PACKAGE,
                                typeOfDiscount: data[i].typeOfDiscount

                            })
                        }
                    }
                    cb(null);
                }
            })
        }
    ], function (err, data) {
        if (!err) {
            response.status(200).send({
                package: package,
                openPackage: openPackage,
                customisedPackage: customisedPackage
            })
        }
    })
}


var prepareFilter = function (key, data, query) {
    if (data) {
        try {
            data = JSON.parse(data);
        } catch (e) {
        }
        if (!Array.isArray(data)) {
            data = [data]
        }
        if (data && data.length) {
            query[key] = {
                $in: data
            }
        }
    }
}

var getPackageInfo = function (request, response) {
    var package = {};
    var topics = [];
    var getTopics = false;
    var topicData = [];
    var userType=request.userData.userType;
    var final = {
        data: {},
        count: 0,
        offset: 0,
        limit: 10,
        statusCode: 200
    };
    async.series([
        function (cb) {
            if (request.body.packageType == "ASSIGNED_PACKAGE") {
                var query = {

                    _id: request.body.packageId
                };

                var projection = {
                    assignedBy: 1,
                    content: 1,
                    topic: 1
                }

                ServiceAssignDataToCustomer.getAssignDataToCustomer(query, projection, {
                    lean: true
                }, function (err, data) {
                    logging.consolelog("pacakge data", err, data);
                    if (!err) {
                        if (data.length) {
                            for (var i in data) {
                                var topicCount = 0;

                                if (data && data.length && data[i].topic && data[i].topic.length > 0) {
                                    for (var j = 0; j < data[i].topic.length; j++) {
                                        topicData.push(data[i].topic[j]);
                                    }
                                }
                                if (data && data.length && data[i].content && data[i].content.length > 0) {
                                    for (var k = 0; k < data[i].content.length; k++) {
                                        topicData.push(data[i].content[k].topicId);
                                    }
                                }
                                topicData = _.unique(topicData);
                            }
                        }
                        cb(null);
                    } else {
                        cb(err);
                    }
                })


            } else if (request.body.packageType == "AGENT_PACKAGE") {
                var query = {
                    '_id': request.body.packageId
                };

                ServiceCustomerBuyPackages.getTransaction(query, {
                    topic: 1,
                    content: 1
                }, {}, function (err, data) {
                    logging.consolelog("logging", "", JSON.stringify(data));
                    if (err) {
                        cb(err);
                    } else {
                        if (data.length) {
                            for (var i in data) {
                                var topicCount = 0;

                                if (data && data.length && data[i].topic.length > 0) {
                                    for (var j = 0; j < data[i].topic.length; j++) {
                                        topicData.push(data[i].topic[j].topicId);
                                    }
                                }
                                if (data && data.length && data[i].content.length > 0) {
                                    for (var k = 0; k < data[i].content.length; k++) {
                                        if (data[i].content[k].isActive == true || data[i].content[k].isActive == undefined)
                                            topicData.push(data[i].content[k].topicId);
                                    }
                                }

                            }
                        }
                        topicData = _.unique(topicData);
                        logging.consolelog("logging", topicData, "topic 123");
                    }
                    cb(null);
                });

            } else if (request.body.packageType == "TOPIC_PACKAGE") {
                logging.consolelog("TOPIC_PACKAGE", "", "");
                var query = {
                    'customerId': request.body.customerId,
                    'paymentStatus': constants.DATABASE.STATUS.COMPLETED,
                    '_id': request.body.packageId
                };
                var path = 'package.packageId';
                var select = 'name icon language  price curriculum grade';
                var populate = {
                    path: path,
                    match: {},
                    select: select,
                    options: {
                        lean: true
                    }
                };
                ServiceCustomerBuyPackages.getPackages(query, {
                    topic: 1,
                    package: 1,
                    packageName: 1,
                    buyDate: 1,
                    customerId: 1
                }, populate, {}, function (err, data) {
                    logging.consolelog("logging", "", JSON.stringify(data));
                    if (err) {
                        cb(err);
                    } else {
                        if (data.length) {
                            try {
                                for (var i in data) {
                                    var topicData = [];
                                    if (data && data.length && data[i].topic) {
                                        topicData = data[i].topic;
                                        getTopics = true;
                                    }
                                    for (var i in topicData) {
                                        topics.push(topicData[i]["topicId"]);
                                    }
                                    request.params.type = "topicPackage";
                                    request.body.topicPackage = topics;
                                }
                                logging.consolelog("topics: ", "", topics);
                                cb(null);
                            } catch (e) {
                                logging.consolelog("logging", "", e)
                            }
                        }
                    }
                });
            } else if (request.body.packageType == "CUSTOMISED_PACKAGE") {
                logging.consolelog("CUSTOMISED_PACKAGE", "", "");
                ServicePackage.getPackage({
                    isDeleted: false,
                    packageType: PackageType.CUSTOMISED_PACKAGE,
                    _id: request.body.packageId
                }, {}, {}, function (err, data) {
                    logging.consolelog("getPackage", err, data);
                    if (!err) {
                        if (data && data.length) {
                            getTopics = true;
                            request.params.type = "topicPackage";
                            request.body.topicPackage = data[0].topics;
                        }
                    } else {
                        cb(err);
                    }
                    cb(null);
                })
            } else {
                logging.consolelog("CLOSED_PACKAGE", "", "");
                ServicePackage.getPackage({
                    isDeleted: false,
                    packageType: PackageType.PACKAGE,
                    _id: request.body.packageId
                }, {}, {}, function (err, data) {
                    logging.consolelog("getPackage", err, data);
                    if (!err) {
                        if (data && data.length) {
                            getTopics = true;
                            request.body.board = data[0].curriculum;
                            request.body.language = data[0].language;
                            request.body.grade = data[0].grade;
                        }
                    } else {
                        cb(err);
                    }
                    cb(null);
                })
            }
        },
        function (cb) {
            logging.consolelog(topicData, "topic Dtaa", "")
            if (getTopics == false && topicData && topicData.length > 0) {
                var topics = [];
                var query = {
                    'board': constants.COMMON_VARIABLES.UNIVERSAL_BOARD_NAME,
                    'isRemoved': false,
                    'isDeleted': false,
                    'isPublish': true
                }
                var sort = {
                    sortBy: "sequenceOrder"
                };
                prepareFilter('board', request.body.board, query);
                prepareFilter('curriculum.language', request.body.language, query);
                prepareFilter('grade', request.body.grade, query);
                prepareFilter('subject', request.body.subject, query);
                prepareFilter('chapter', request.body.chapter, query);

                if (request.body.offset) {
                    var offsetCount = request.body.offset ? request.body.offset : 0;
                }
                if (request.body.limit) {
                    var limitCount = request.body.limit ? request.body.limit : 200;
                }

                query.topicId = {
                    $in: topicData
                }


                if (request.body.search) {
                    var search = " and match(name, authorName, description, gtinCode, topic.language) against ('" + request.body.search + "*' IN BOOLEAN MODE)";
                }

                var limit = {
                    limit: limitCount,
                    offset: offsetCount
                }
                final["count"] = 0;
                final["offset"] = offsetCount;
                final["limit"] = limitCount;

                ServiceTopic.getTopicsByCurriculum(query, sort, limit, search, function (err, data) {
                    var curriculumsIds = [];
                    if (!err) {
                        if (data.length > 0) {
                            data.forEach(function (curriculum) {
                                if (curriculum && curriculum.name) {
                                    topics.push({
                                        Id: curriculum["_id"],
                                        name: curriculum.name,
                                        icon: curriculum.icon,
                                        authorName: curriculum.authorName,
                                        averageRating: curriculum.averageRating,
                                        price: curriculum.price,
                                        description: curriculum.description,
                                        board: curriculum.board,
                                        subject: curriculum.subject,
                                        language: curriculum.boardLanguage,
                                        topicLanguage: curriculum.topicLanguage,
                                        grade: curriculum.grade,
                                        chapter: curriculum.chapter,
                                        addedDate: curriculum.addedDate
                                    });
                                }
                            });
                            final["data"]["topicLibrary"] = topics;
                            ServiceTopic.getTopicsByCurriculumCount(query, {}, {}, search, function (err, countData) {
                                if (!err) {
                                    final["count"] = countData ? countData[0]["cnt"] : 0;
                                }
                                cb(null);
                            });
                        } else {
                            cb(null);
                        }
                    } else {
                        cb(constants.STATUS_MSG.ERROR.TOPIC_NOT_FOUND);
                    }
                });


            } else {
                cb(null);
            }


        }], function (err) {
        if (getTopics) {
            customerTopicModule.topicLibrary(request, response);
        } else {

            response.status(200).send(final);
        }
    })
}

/*if (!err) {
 for (var i = 0; i < data.length; i++) {
 if (data[i].topic.length > 0) {
 buyDetails.push({
 packageName: data[i].packageName,
 noOfTopics: data[i].topic.length,
 purchasedDate: data[i].buyDate,
 expiredOn: data[i].topic[0].endDate,
 topicPayments: data[i].topicPayments.totalPrice,
 packageId: data[i]._id,
 topic: true
 });
 }
 for (var j = 0; j < data[i].package.length; j++) {
 packageDetails.push({
 board: data[i].package[j].packageId.curriculum,
 grade: data[i].package[j].packageId.grade,
 "curriculum.language": data[i].package[j].packageId.language
 })
 packages[data[i].package[j].packageId.curriculum + "||" + data[i].package[j].packageId.grade + "||" + data[i].package[j].packageId.language + "||" + data[i].package[j].packageId._id] = {
 packageId: data[i].package[j].packageId._id,
 packageName: data[i].package[j].packageId.grade,
 topic: false,
 purchasedDate: data[i].buyDate,
 expiredOn: data[i].package[j].endDate,
 topicPayments: data[i].package[j].finalPrice

 }
 }

 }
 ServicePackage.getPackageDetails(packageDetails, function (err, data) {
 if (data && data.length) {
 for (var i in data) {
 var key = data[i].board + "||" + data[i].grade + "||" + data[i].language;
 for (var j in packages) {
 if (j.indexOf(key) != -1) {
 packages[j]["price"] = data[i].price;
 packages[j]["noOfTopics"] = data[i].topicCount;
 packages[j]["subjectCount"] = data[i].subjectCount;
 packages[j]["chapterCount"] = data[i].chapterCount;
 packages[j]["board"] = data[i].board;
 packages[j]["grade"] = data[i].grade;
 packages[j]["language"] = data[i].language;
 }
 }

 }
 }

 for (var k in packages) {
 buyDetails.push({
 packageName: packages[k].packageName,
 noOfTopics: packages[k].noOfTopics,
 purchasedDate: packages[k].purchasedDate,
 expiredOn: packages[k].expiredOn,
 topicPayments: packages[k].topicPayments,
 packageId: packages[k].packageId,
 topic: false

 })
 }

 cb(null);

 })
 }*/
function getCustomerPurchases(customerId, callback) {
    var topics = [];
    var packages = {};
    var packageDetails = [];
    var packageIds=[];
    async.series([
        function (cb) {
            var query = {
                'customerId': customerId,
                'paymentStatus': constants.DATABASE.STATUS.COMPLETED
            };
            var path = 'package.packageId';
            var select = 'name icon language  price curriculum grade';
            var populate = {
                path: path,
                match: {},
                select: select,
                options: {
                    lean: true
                }
            };

            ServiceCustomerBuyPackages.getPackages(query, {
                topic: 1,
                package: 1,
                packageName: 1,
                buyDate: 1,
                customerId: 1
            }, populate, {}, function (err, data) {
                logging.consolelog(err, JSON.stringify(data), "data::purchase topic data")
                if (err) {
                    cb(err);
                } else {
                    if (data.length) {
                        for (var i in data) {
                            var topicData = [];
                           
                            

                            if (data && data.length && data[i].package && data[i].package.length) {
                                packageData = data[i].package;
                                for (var j in packageData) {
                                    if (new Date() < new Date(packageData[j].endDate)) {
                                        if (packageData[j].packageId) {
                                            packageIds.push(packageData[j].packageId["_id"]);
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                    cb(null);
                });
            }], function () {
        callback(packageIds);
    });

}




function getCustomerCartPackage(customerId, callback) {
    var packageCart = [];
    async.series([
        function (cb) {
    
    
            var query = {
                'customerId': customerId
            };
            ServiceCustomerCart.getCart(query, {
                packageCart: 1
            }, {
                lean: true
            }, function (err, data) {
                logging.consolelog("logging", "", data);
                if (err) {
                    cb(err);
                } else {
                    if (data && data.length && data[0].packageCart) {
                        for (var i = 0; i < data[0].packageCart.length; i++) {
                            packageCart.push(data[0].packageCart[i]);
                        }
                    }
                    cb(null, packageCart);
                }
            });
            }], function () {
        callback(packageCart);
    });

}

var getPackages = function (request, response) {
    var openPackage = [];
    var packages = {};
    var grade, chapter, subject, topic = 0;
    var closedPackage = [];
    var packageDetails = [];
    var customisedPackage = [];
    var customisedPackageTopics = [];
    var customisedPackageAllTopics = [];
    var topics = {};
    var purchasedPackages=[];
    var CartPackages=[];
    async.auto({
        purchased_customer_packages:function (cb) {
            getCustomerPurchases(request.userData.id, function (data) {
                // logging.consolelog("data::output::purchased data", "", data);
                console.log(JSON.stringify(data), "purchased  package data ******Data")
                for (var i = 0; i < data.length; i++) {
                    purchasedPackages.push(data[i].toString());
                }
                
                cb(null);
            })
        },
        cart_packages:function(cb){
   getCustomerCartPackage( request.userData.id, function(data){
    console.log(JSON.stringify(data), "purchased  package data ******Data")
    for (var i = 0; i < data.length; i++) {
        CartPackages.push(data[i].toString());
    }
    
    cb(null);

   })
            
        },
        open_package: function (cb) {
            var setOptions = {
                // skip:parseInt(request.body.skip),
                // limit:10
            }
            var query = {
                isDeleted: false
            };
            if (request.body.search) {
                query["$text"] = {
                    $search: request.body.search
                };
            }
            ServicePackage.getAllPackage(query, {}, {}, setOptions, function (err, data) {
                logging.consolelog("logging", err, data);
                if (!err) {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].packageType == PackageType.PACKAGE) {
                            packages[data[i].curriculum + "||" + data[i].grade + "||" + data[i].language + "||" + data[i]._id] = {
                                monthlyDiscount: data[i].monthlyDiscount,
                                yearlyDiscount: data[i].yearlyDiscount,
                                curriculum: data[i].curriculum,
                                language: data[i].language,
                                grade: data[i].grade,
                                icon: data[i].icon,
                                monthlyPrice: data[i].monthlyPrice,
                                yearlyPrice: data[i].yearlyPrice,
                                packageId: data[i]._id,
                                packageType: PackageType.PACKAGE,
                                typeOfDiscount: data[i].typeOfDiscount
                            }
                            packageDetails.push({
                                board: data[i].curriculum,
                                grade: data[i].grade,
                                "curriculum.language": data[i].language
                            })
                        } else if (data[i].packageType == PackageType.OPEN_PACKAGE) {
                            openPackage.push({
                                monthlyDiscount: data[i].monthlyDiscount,
                                yearlyDiscount: data[i].yearlyDiscount,
                                monthlyPrice: data[i].monthlyPrice,
                                yearlyPrice: data[i].yearlyPrice,
                                noOfTopics: data[i].noOfTopics,
                                packageId: data[i]._id,
                                packageType: PackageType.OPEN_PACKAGE,
                                typeOfDiscount: data[i].typeOfDiscount
                            })
                        } else if (data[i].packageType == PackageType.CUSTOMISED_PACKAGE) {
                            customisedPackage.push({
                                monthlyDiscount: data[i].monthlyDiscount,
                                yearlyDiscount: data[i].yearlyDiscount,
                                monthlyPrice: data[i].monthlyPrice,
                                yearlyPrice: data[i].yearlyPrice,
                                packageId: data[i]._id,
                                packageName: data[i].packageName,
                                packageType: PackageType.CUSTOMISED_PACKAGE,
                                typeOfDiscount: data[i].typeOfDiscount,
                                price: 0,
                                topicCount: data[i].topics.length,
                                topics: data[i].topics,
                                icon: data[i].icon
                            })
                            customisedPackageAllTopics = customisedPackageAllTopics.concat(data[i].topics);
                        }
                    }
                    logging.consolelog("logging", "", packages);
                    cb(null);
                } else {
                    cb(err);
                }
            });
        },
        get_more_details: ['open_package','cart_packages', 'purchased_customer_packages',function (cb) {
            if (packageDetails.length) {
                ServicePackage.getPackageDetails(packageDetails, function (err, data) {
                    if (data && data.length) {
                        logging.consolelog("logging", "", data);
                        for (var i in data) {
                            var key = data[i].board + "||" + data[i].grade + "||" + data[i].language;
                            for (var j in packages) {
                                if (j.indexOf(key) != -1) {
                                    packages[j]["price"] = data[i].price;
                                    packages[j]["topicCount"] = data[i].topicCount;
                                    packages[j]["subjectCount"] = data[i].subjectCount;
                                    packages[j]["chapterCount"] = data[i].chapterCount;
                                    packages[j]["board"] = data[i].board;
                                    packages[j]["grade"] = data[i].grade;
                                    packages[j]["language"] = data[i].language;
                                }
                            }
                        }
                    }
                    for (var key in packages) {
                        if (packages[key]["topicCount"]) {
                            if (packages[key].monthlyPrice) {
                                packages[key]["price"] = packages[key].monthlyPrice;
                            } else if (packages[key].yearlyPrice) {
                                packages[key]["price"] = packages[key].yearlyPrice;
                            } else if (packages[key].monthlyDiscount) {
                                var price = packages[key].price;
                                var discount = packages[key].monthlyDiscount;
                                var type = packages[key].typeOfDiscount;
                                if (type == "%") {
                                    price -=(discount * price) / 100;
                                } else {
                                    price = price - discount;
                                }

                                packages[key]["price"] = price;
                            } else if (packages[key].yearlyDiscount) {
                                logging.consolelog('checking price>>>>>','1','')
                                var price = parseInt(packages[key].price);
                                var discount = packages[key].yearlyDiscount;
                                var type = packages[key].typeOfDiscount;
                                if (type == "%") {
                                    logging.consolelog('checking price>>>>>','2','')
                                    price -= (discount * price) / 100;
                                } else {
                                    logging.consolelog('checking price>>>>>','3','')
                                    price = price - discount;
                                }
                                packages[key]["price"] = price;
                            }
                            console.log(purchasedPackages, "====> purchased packages",packages[key].packageId,purchasedPackages.indexOf(packages[key].packageId.toString()));
                            closedPackage.push({
                                packageId: packages[key].packageId,
                                curriculum: packages[key].curriculum,
                                language: packages[key].language,
                                price: packages[key].price,
                                topic: packages[key].topicCount,
                                chapter: packages[key].chapterCount,
                                grade: packages[key].grade,
                                subject: packages[key].subjectCount,
                                icon: packages[key].icon,
                                isPurchased: purchasedPackages.indexOf(packages[key].packageId.toString()) != -1 ? true : false,
                                isCart: CartPackages.indexOf(packages[key].packageId.toString()) != -1 ? true : false
                            })
                        }
                    }

                    cb(null);
                })
            } else {
                cb(null);
            }
        }],
        remove_invalid_topics: ['open_package', function (cb) {
            if (customisedPackage.length) {
                ServiceTopic.getValidTopics(customisedPackageAllTopics, function (err, data) {

                    if (data && data.length) {
                        customisedPackageTopics = customisedPackageTopics.concat(data);
                    }
                    for (var i in customisedPackage) {
                        var validTopics = [];
                        for (var j = 0; j < customisedPackage[i]["topics"].length; j++) {
                            if (customisedPackageTopics.indexOf(customisedPackage[i]["topics"][j]) != -1) {
                                validTopics.push(customisedPackage[i]["topics"][j]);
                            }
                        }
                        customisedPackage[i]["topics"] = validTopics;
                        customisedPackage[i]["topicCount"] = validTopics.length;
                    }
                    cb(null);
                });
            } else {
                cb(null);
            }
        }],
        get_more_detail: ['remove_invalid_topics', function (cb) {
            if (customisedPackageTopics.length) {
                request.body.topics = customisedPackageTopics;
                customerTopicModule.getTopics(request, function (data) {
                    var lib = data.data.topicLibrary;
                    for (var i in lib) {
                        topics[lib[i]["Id"]] = lib[i];
                    }
                    for (var i in customisedPackage) {
                        for (var j = 0; j < customisedPackage[i].topics.length; j++) {
                            if (topics[customisedPackage[i].topics[j]]) {
                                customisedPackage[i].price += topics[customisedPackage[i].topics[j]]["price"];
                            }
                        }
                        if (customisedPackage[i].monthlyPrice) {
                            customisedPackage[i]["price"] = customisedPackage[i].monthlyPrice;
                        } else if (customisedPackage[i].yearlyPrice) {
                            customisedPackage[i]["price"] = customisedPackage[i].yearlyPrice;
                        } else if (customisedPackage[i].monthlyDiscount) {
                            var price = customisedPackage[i].price;
                            var discount = customisedPackage[i].monthlyDiscount;
                            var type = customisedPackage[i].typeOfDiscount;
                            if (type == "%") {
                                price -= ((discount * price) / 100);
                            } else {
                                price = price - discount;
                            }

                            customisedPackage[i]["price"] = price;
                        } else if (customisedPackage[i].yearlyDiscount) {
                            var price = parseInt(customisedPackage[i].price);
                            var discount = customisedPackage[i].yearlyDiscount;
                            var type = customisedPackage[i].typeOfDiscount;
                            if (type == "%") {
                                price -= ((discount * price) / 100);
                            } else {
                                price = price - discount;
                            }
                            customisedPackage[i]["price"] = price;
                        }
                    }
                    cb(null);
                });
            } else {
                cb(null);
            }
        }]
    }, function (err, data) {
        if (!err) {
            response.status(200).send({
                closedPackage: closedPackage,
                openPackage: openPackage,
                customisedPackage: customisedPackage,
                statusCode: 200
            })
        } else {
            response.send(err);
        }
    })
}


module.exports = {
    getPackages: getPackages,
    getPackageInfo: getPackageInfo
}