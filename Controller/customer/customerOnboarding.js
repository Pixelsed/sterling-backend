var async = require('async');
var ServiceCurriculum = require('../../Service/curriculum');
var constants = require('../../Library/appConstants');
var UploadManager = require('../../Library/uploadManager');
var NotificationManager = require('../../Library/notificationManager');
var TokenManager = require('../../Library/tokenManager');
var UniversalFunctions = require('../../Library/universalFuctions');
var ServiceCustomer = require("../../Service/customer");
var AwsConfig = Config.get('awsConfig');
var Templates = require('../../Templates/customerRegister');
var moment = require('moment');
var TemplatesForget = require('../../Templates/forgetPassword');
var ServiceChildMapper = require('../../Service/childParentMapper');
var ServiceAnalysisData = require('../../Service/analysisData')
var ServiceMarketingDays = require("../../Service/marketingDays");

var API = require('phone-number-validation');

var api = new API({
	access_key: "5ed5d7c18ef74d0540a22519ac4853ab",
	secure: false
});
function addChildMapper(customerId) {
	var dataToSave = {
		'childCustomerId': customerId,
		parentCustomerId: customerId,
		superParentCustomerId: customerId
	};
	ServiceChildMapper.createChildParentMapper(dataToSave, function (err, data) {
		if (err) {
			logging.consolelog("logging", "", err);
		}
	})
}
/*
 Customer Register

 1. Validating Parameters
 2/3. Validating Unique Parameters
 4. Upload Profile Picture of Super-Admin
 5. Save Super Admin Data(Use MD5 for encryption of passwords)
 6. Send Password to Admin via SES service of AWS

 */
var customerRegister = function (request, response) {
	var loginType = request.headers.logintype;
	request.body.emailId = request.body.emailId.toLowerCase();
	logging.consolelog("logging", request.body, request.ip);
	logging.consolelog("logging", "", request.connection.remoteAddress);
	logging.consolelog("request.headers", '', request.headers);
	var ip = request.headers['x-forwarded-for'] ? request.headers['x-forwarded-for'] : request.headers['host'];

	var accessToken = null;
	var uniqueCode = null;
	var dataToSave = request.body;

	var filename1 = null;
	var customerData = null;
	var timezoneOffset;
	if (dataToSave.password) dataToSave.password = UniversalFunctions.encryptData(dataToSave.password);
	async.series([
		
        function (cb) {
			if (request.body.socialId) {
				var query = {
					$or: [{
						emailId: request.body.emailId
                    }, {
						socialId: request.body.socialId
                    }]
				}
				ServiceCustomer.getCustomer(query, {}, {
					lean: true
				}, function (error, data) {
					if (error) {
						cb(error);
					} else {
						if (data && data.length > 0) {
							if (data[0].phoneVerified == true) {
								cb(constants.STATUS_MSG.ERROR.USER_ALREADY_REGISTERED);
							} else {
								ServiceCustomer.deleteCustomer({
									_id: data[0]._id
								}, function (err, updatedData) {
									if (err) cb(err)
									else cb(null);
								});
							}
						} else {
							cb(null);
						}
					}
				});
			} else {
				var query = {
					$or: [{
						emailId: request.body.emailId
                    }]
				};
				ServiceCustomer.getCustomer(query, {}, {
					lean: true
				}, function (error, data) {
					if (error) {
						cb(error);
					} else {
						if (data && data.length > 0) {
							if (data[0].phoneVerified == true) {
								cb(constants.STATUS_MSG.ERROR.USER_ALREADY_REGISTERED)
							} else {
								ServiceCustomer.deleteCustomer({
									_id: data[0]._id
								}, function (err, updatedData) {
									if (err) cb(err)
									else cb(null);
								})
							}
						} else {
							cb(null);
						}
					}
				});
			}
        },
        function (cb) {
			//verify email address
			if (!UniversalFunctions.verifyEmailFormat(dataToSave.emailId)) {
				cb(constants.STATUS_MSG.ERROR.INVALID_EMAIL);
			} else {
				cb();
			}
        },
        function (cb) {
			//Validate for socialId and password
			if (dataToSave.socialId) {
				if (dataToSave.password) {
					cb(constants.STATUS_MSG.ERROR.SOCIAL_ID_PASSWORD_ERROR);
				} else {
					cb();
				}
			} else if (!dataToSave.password) {
				cb(constants.STATUS_MSG.ERROR.PASSWORD_REQUIRED);
			} else {
				cb();
			}
        },
        
	     function (cb) {
			uniqueCode = Math.floor(Math.random() * 900000) + 100000;
			cb(null);
	     },
       
        function (cb) {
			timezoneOffset = '300';
			if (request.body.sessionTo) {
				request.body.sessionTo = moment(new Date(request.body.sessionTo)).utcOffset(-timezoneOffset).format('YYYY-MM-DD HH:mm:ss');
			}
			if (request.body.sessionFrom) {
				request.body.sessionFrom = moment(new Date(request.body.sessionFrom)).utcOffset(-timezoneOffset).format('YYYY-MM-DD HH:mm:ss');
			}

			if (request.body.dob) {
				request.body.dob = moment(new Date(request.body.dob)).utcOffset(-timezoneOffset).format('YYYY-MM-DD');
			}
			cb(null);
        },
        function (cb) {
			if (request.body.socialType == constants.DATABASE.SOCIAL_TYPE.FACEBOOK) {
				filename1 = AwsConfig.fbUrl + request.body.socialId + "/picture?width=160&height=160";

				cb(null);
			} else if (request.body.socialType == constants.DATABASE.SOCIAL_TYPE.GOOGLEPLUS) {
				filename1 = AwsConfig.gPlusUrl + request.body.socialId + "?sz=150";


				cb(null);
			} else if (request.files) {
				UploadManager.uploadPicToS3Bucket(request.files.profilePic, "profile", function (filename) {
					if (filename == "No File") {
						cb(null);
					} else {
						filename1 = AwsConfig.s3BucketCredentials.s3URL + '/profile/' + filename;
						cb(null);
					}
				});
			} else {
				cb(null);
			}
        },
        function (cb) {
			//Insert Into DB

			dataToSave.otp = uniqueCode;
			
			dataToSave.registrationDate = new Date().toISOString();
			dataToSave.lastName = request.body.lastName;
			dataToSave.firstName = request.body.firstName;
			dataToSave.profilePicUrl = filename1;
			dataToSave.curriculum = request.body.curriculum;
			dataToSave.grade = request.body.grade;
			if (request.body.dob) dataToSave.dob = request.body.dob;

			dataToSave.addressValue = request.body.addressValue;
			dataToSave.state = request.body.state;
			dataToSave.city = request.body.city;
			dataToSave.zip = request.body.zip,
				dataToSave.gender = request.body.gender;
			if (request.body.longitude && request.body.latitude) {
				dataToSave.location = {
					"coordinates": [request.body.longitude, request.body.latitude],
					"type": "Point"
				}
			} else {
				request.body.longitude = 0;
				request.body.latitude = 0;
				dataToSave.location = {
					"coordinates": [request.body.longitude, request.body.latitude],
					"type": "Point"

				}
			}

			dataToSave.password = request.body.password;
			dataToSave.emailId = request.body.emailId;
			if (request.body.socialId) {
				dataToSave.socialId = request.body.socialId;
				dataToSave.socialType = request.body.socialType;
			}

			// dataToSave.countryCode = request.body.countryCode;
			dataToSave.userType = request.body.userType;
			dataToSave.country = request.body.country;
			dataToSave.accessRights = request.body.accessRights;

			ServiceCustomer.createCustomer(dataToSave, function (err, customerDataFromDB) {
				logging.consolelog('logging', err, customerDataFromDB)
				if (err) {
					cb(err)
				} else {
					customerData = customerDataFromDB;
					cb();
				}
			})
        },
        function (cb) {
			var emailSend = {
				emailId: request.body.emailId,
				code: uniqueCode,
				name: request.body.name
			}
			var text;
			text = Templates.customerRegister(emailSend);
			NotificationManager.sendEmailToUser(request.body.emailId, 'The OTP for registeration on PixelsED is ' + uniqueCode, text, function (err, data) {
				if (!err) {
					logging.consolelog('mail', err, data);
				}
				cb(null);
			});
		},
		// function (cb) {
		// 	var data = {
		// 		otp: uniqueCode,
		// 		mobileNo: request.body.countryCode + request.body.mobileNo
		// 	}
		// 	NotificationManager.sendSMSToUser(data, 'REGISTRATION', function (err, data) {
		// 		logging.consolelog(err, data, "sms config");
		// 		cb(null);
		// 	})
		// },
		function (cb) {
			if (request.body.userType == constants.USER_TYPE.AGENT) {
				var dataToSave = {
					'childCustomerId': customerData._id,
					'superParentCustomerId': customerData._id,
					'parentCustomerType': constants.USER_TYPE.AGENT
				};
				ServiceChildMapper.createChildParentMapper(dataToSave, function (err, data) {
					if (err) {
						logging.consolelog("logging", "", err);
					}
					cb(null);
				})

			} else {
				cb(null);
			}
		},
		function (cb) {

			if (request.body.userType == constants.USER_TYPE.AGENT) {
				var query = {
					_id: customerData._id
				}

				var update = {
					$push: {
						license: {
							$each: [{
								parentCustomerId: customerData._id,
								allocatedLicense: 0,
								usedLicense: 0,
								isActive: true
							}]
						}
					}
				}
				ServiceCustomer.updateCustomer(query, update, {
					lean: true,
					safe: true
				}, function (err, data) {
					if (!err) {
						cb(null);
					} else {
						cb(err);
					}
				})
			} else {
				cb(null);
			}
		},
         function (cb) {
			//Set Access Token
			if (customerData) {
				var tokenData = {
					id: customerData._id,
					type: request.body.userType,
					ip: ip,
					loginType: loginType,
					userType: 'CUSTOMER',
					date: Date.now()
				};
				logging.consolelog('sending token', '', tokenData);
				TokenManager.setToken(tokenData, function (err, output) {

					if (err) {
						cb(err);
					} else {
						accessToken = output && output.accessToken || null;
						cb();
					}
				})
			} else {
				cb(constants.STATUS_MSG.ERROR.IMP_ERROR)
			}
         },
		function (cb) {
			NotificationManager.sendNotifications(customerData._id, 'CUSTOMER_ONBOARDING', {});
			cb(null);

		},

    ], function (err, data1) {
		if (!err) {
			var dataTosend = {
				data: {
					accessToken: accessToken,
					userType: customerData.userType,
					countryCode: customerData.countryCode,
					emailId: customerData.emailId,
					mobileNo: customerData.mobileNo,
					_id: customerData._id,
					zip: customerData.zip,
					city: customerData.city,
					state: customerData.state,
					country: customerData.country,
					registrationDate: customerData.registrationDate,
					phoneVerified: customerData.phoneVerified,
					dob: customerData.dob,
					profilePicUrl: customerData.profilePicUrl,
					lastName: customerData.lastName,
					firstName: customerData.firstName
				},
				accessToken: accessToken
			};
			logging.consolelog('customerData', "", dataTosend);
			response.status(200).send(dataTosend)
		} else {
			response.send(err);
		}

	});
}

var verifyOTP = function (request, response) {
	var userDetails = request.userData;
	var loginType = request.body.loginType;
	var update = null;
	async.series([
        function (cb) {

			var query = {
				_id: userDetails.id,
				otp: request.body.otp
			}
			var setQuery = {
				$set: {
					phoneVerified: true,
					loginTime: new Date(),
					deviceToken: request.body.deviceToken,
					deviceType: request.body.deviceType
				},
				$unset: {
					otp: 1
				}
			};
			var options = {
				new: true
			};
			ServiceCustomer.updateCustomer(query, setQuery, options, function (err, updatedData) {
				logging.consolelog(err, updatedData, "values");
				if (err) {
					cb(err)
				} else {
					if (!updatedData) {
						cb(constants.STATUS_MSG.ERROR.INVALID_CODE)
					} else {
						cb(null);
					}
				}
			})


        },
        function (cb) {
			var query = {
				_id: userDetails.id

			}
			ServiceCustomer.getCustomer(query, {}, {}, function (err, data) {
				if (!err) {
					update = data[0];
					cb(null);
				} else {
					cb(err);
				}
			})

        },
		function (cb) {
			NotificationManager.sendNotifications(update._id, 'COMPLETE_PROFILE', {});
			cb(null);
		},
		function (cb) {
			console.log("=====>login type", loginType,request.body.deviceToken );
			if (loginType == "ANDROIDAPP") {
				NotificationManager.sendAndroidPushNotification(request.body.deviceToken, 'Sterling welcomes you!!', "REGISTRATION", function (err, data) {
					console.log(err,data,"===> first push");
					

						NotificationManager.sendAndroidPushNotification(request.body.deviceToken, "Please complete your profile information to get relevant offers and discounts.", "COMPLETE_PROFILE", (errr, dataa) => {

						})
						cb(null);
					
				})
			} else {
				cb(null);
			}

		}
    ], function (err, data) {
		if (!err) {
			if (request.userData.loginType == "WEBAPP")
				userDetails.accessToken = userDetails.accessToken.WEBAPP;
			if (request.userData.loginType == "ANDROIDAPP")
				userDetails.accessToken = userDetails.accessToken.ANDROIDAPP;
			if (request.userData.loginType == "IOSAPP")
				userDetails.accessToken = userDetails.accessToken.IOSAPP;
			response.status(200).send({
				data: userDetails,
				accessToken: userDetails.accessToken
			})
		} else {
			response.send(err);
		}

	});
}


var resendOTP = function (request, response) {
	var uniqueCode = null;
	var userDetails = request.userData;
	var loginType = request.userData.loginType;
	async.series([
        // function (cb) {
        //     UniversalFunctions.generateUniqueCode(4, constants.USER_TYPE.TEACHER, function (err, numberObj) {
        //         if (err) {
        //             cb(err);
        //         } else {
        //             if (!numberObj || numberObj.number == null) {
        //                 cb(ERROR.UNIQUE_CODE_LIMIT_REACHED);
        //             } else {
        //                 uniqueCode = numberObj.number;
        //                 cb();
        //             }
        //         }
        //     })
        // },
        function (cb) {
			uniqueCode = Math.floor(Math.random() * 900000) + 100000;

			var criteria = {
				_id: userDetails.id
			};
			var setQuery = {
				$set: {
					otp: uniqueCode

				}
			};
			var options = {
				lean: true
			};
			ServiceCustomer.updateCustomer(criteria, setQuery, options, function (err, data) {
				if (!err) {
					cb(null);
				} else {
					cb(err);
				}
			});
        },
        function (cb) {
			var query = {
				_id: userDetails.id

			}
			ServiceCustomer.getCustomer(query, {}, {}, function (err, data) {
				if (!err) {
					userDetails = data[0];
					cb(null);
				} else {
					cb(err);
				}
			})


        },
		function (cb) {
			var emailSend = {
				emailId: userDetails.emailId,
				code: uniqueCode,
				name: userDetails.name
			}
			var text;
			text = Templates.customerRegister(emailSend);
			NotificationManager.sendEmailToUser(userDetails.emailId, 'The OTP for registeration on PixelsED is ' + uniqueCode, text, function (err, data) {
				if (!err) {
					logging.consolelog('mail', err, data);
				}
				cb(null);
			});
		},
		function (cb) {
			var data = {
				otp: uniqueCode,
				mobileNo: userDetails.countryCode + userDetails.mobileNo
			}
			NotificationManager.sendSMSToUser(data, 'REGISTRATION', function (err, data) {
				logging.consolelog(err, data, "sms config");
				cb(null);
			})
		},
        // function(cb) {
        //     //Send SMS to User
        //     NotificationManager.sendSMSToUser(uniqueCode, countryCode, phoneNo, function(err, data) {
        //         cb();
        //     })
        // }
    ], function (err, data) {

		if (!err) {
			var dataTosend = {
				data: userDetails,
				accessToken: ""
			}
			if (loginType === constants.DATABASE.LOGIN_TYPE.WEBAPP)
				dataTosend.accessToken = userDetails.accessToken.WEBAPP;
			if (loginType === constants.DATABASE.LOGIN_TYPE.ANDROIDAPP)
				dataTosend.accessToken = userDetails.accessToken.ANDROIDAPP;
			if (loginType === constants.DATABASE.LOGIN_TYPE.IOSAPP)
				dataTosend.accessToken = userDetails.accessToken.IOSAPP;
			logging.consolelog('show data', userDetails.loginType, dataTosend.accessToken)
			userDetails.accessToken = dataTosend.accessToken;
			response.status(200).send(dataTosend)
		} else {
			response.send(err);

		}
	})


}


var customerLogin = function (request, response) {
	var customerData = null;
	var accessToken = null;
	var userpassbkp = null;
	var loginType = request.headers.logintype;
	console.log(" x- forwardeed for", request.headers['x-forwarded-for']);
	console.log("request.headers['host']", request.headers['host']);
	logging.consolelog("-----------?>ip", request.body, request.ip);
	logging.consolelog("request.connection.remoteAddress", "", request.connection.remoteAddress);
	logging.consolelog("request.headers", '', request.headers);
	var ip = request.headers['x-forwarded-for'] ? request.headers['x-forwarded-for'] : request.headers['host'] || null;

	console.log("---<", ip);
	async.series([
        function (cb) {
			var query = {
				emailId: request.body.emailId
			}
			ServiceCustomer.getCustomer(query, {}, {}, function (err, data) {
				if (!err) {
					if (data.length > 0) {
						if (data[0].phoneVerified == false) {

							cb(constants.STATUS_MSG.ERROR.USER_NOT_FOUND);
						} else {
							userpassbkp = data[0];
							cb(null)
						}
					} else {
						cb(constants.STATUS_MSG.ERROR.USER_NOT_FOUND);
					}
				} else {
					cb(err);
				}
			});
        },
        function (cb) {
			var query = {
				emailId: request.body.emailId,
				password: {
					$in: [UniversalFunctions.CryptData(request.body.password), UniversalFunctions.encryptData(request.body.password)]
				}
				// password: UniversalFunctions.CryptData(request.body.password)
				// password: UniversalFunctions.decryptData(request.body.password)
			};
			// logging.consolelog('password',request.body.password,UniversalFunctions.encryptData(request.body.password))
			// var today=new Date("2017-02-24T10:36:47.465Z");
			// var regDate = new Date(userpassbkp.registrationDate)
			// logging.consolelog('check old customer',regDate ,today.toISOString());
			// logging.consolelog('date diff>>',"",regDate.toISOString()< today.toISOString());
			// if(regDate.toISOString()< today.toISOString()){
			// 	logging.consolelog('old customer in>>>',UniversalFunctions.CryptData(request.body.password))
			// 	query.password['$in'] =[UniversalFunctions.CryptData(request.body.password),UniversalFunctions.encryptData(request.body.password)]
			// }else {
			// 	query.password= UniversalFunctions.encryptData(request.body.password);
			// }
			logging.consolelog('query??????', "", query)
			ServiceCustomer.getCustomer(query, {}, {}, function (err, data) {
				if (!err) {
					if (data.length > 0) {
						customerData = data[0];
						// if(loginType=="WEBAPP")
						//     customerData.accessToken=customerData.accessToken.WEBAPP;
						// if(loginType=="ANDROIDAPP")
						//     customerData.accessToken=customerData.accessToken.ANDROIDAPP;
						// if(loginType=="IOSAPP")
						//     customerData.accessToken=customerData.accessToken.IOSAPP;
						cb(null);
					} else {
						cb(constants.STATUS_MSG.ERROR.PASSWORD_MISMATCH);
					}
				} else {
					cb(err);
				}
			});
        },
		function (cb) {
			var query = {
				emailId: request.body.emailId

			}

			var update = {
				loginTime: new Date()
			}
			ServiceCustomer.updateCustomer(query, update, {
				lean: true
			}, function (err, data) {
				if (!err) {
					cb(null);
				} else {
					cb(err);
				}
			})


		},
         function (cb) {
			logging.consolelog(customerData, "", "customerData");
			if (customerData) {
				var tokenData = {
					id: customerData._id,
					type: customerData.userType,
					ip: ip,
					loginType: loginType,
					userType: 'CUSTOMER',
					date: Date.now()
				};
				TokenManager.setToken(tokenData, function (err, output) {
					logging.consolelog(err, output, "token result");
					if (err) {
						cb(err);
					} else {
						accessToken = output && output.accessToken || null;
						customerData.accessToken = accessToken;
						cb();
					}
				})
			} else {
				cb(constants.STATUS_MSG.ERROR.IMP_ERROR)
			}

         }
    ], function (err, data) {

		if (!err) {
			delete customerData['accessToken'];
			response.status(200).send({
				data: customerData,
				accessToken: accessToken,
				statusCode: 200
			})
		} else {
			response.send(err);

		}
	})


}


var updatePassword = function (request, response) {
	var customerData = null;
	var accessToken = null;
	var ip = request.connection.remoteAddress;
	async.series([
        function (cb) {
			var query = {
				_id: request.body.customerId,
				password: {
					$in: [UniversalFunctions.CryptData(request.body.password), UniversalFunctions.encryptData(request.body.password)]
				}
			}
			ServiceCustomer.getCustomer(query, {}, {}, function (err, data) {
				if (!err) {
					if (data.length > 0) {
						logging.consolelog("logging", "", data)
						cb(null);
					} else {
						cb(constants.STATUS_MSG.ERROR.PASSWORD_MISMATCH);
					}
				} else {
					cb(err);
				}
			});
		},
		function(cb){
			if(UniversalFunctions.CryptData(request.body.password) == UniversalFunctions.encryptData(request.body.newPassword) || UniversalFunctions.encryptData(request.body.password)== UniversalFunctions.encryptData(request.body.newPassword)){
				cb(constants.STATUS_MSG.ERROR.OLD_PASSWORD);
			}else{
				cb(null);

			}
		},
        function (cb) {
			var query = {
				_id: request.body.customerId
			}
			var dataToSet = {
				$set: {
					password: UniversalFunctions.encryptData(request.body.newPassword)
				}
			}
			ServiceCustomer.updateCustomer(query, dataToSet, {}, function (err, data) {
				if (!err) {
					cb(null);
				} else {
					cb(err);
				}
			});
        },
        // function(cb) {
        //     if (customerData) {
        //       var tokenData = {
        //           id: customerData._id,
        //           type: request.body.userType,
        //           ip:ip,
        //           loginType:request.body.loginType,
        //           userType:'CUSTOMER'
        //       };
        //         TokenManager.setToken(tokenData, function(err, output) {
        //             if (err) {
        //                 cb(err);
        //             } else {
        //                 accessToken = output && output.accessToken || null;
        //                 cb();
        //             }
        //         })
        //     } else {
        //         cb(ERROR.IMP_ERROR)
        //     }
        //
        // }
    ], function (err, data) {
		if (!err) {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.PASSWORD_CHANGE)
		} else {
			response.send(err);

		}
	})


}


var listing = function (request, response) {
	logging.consolelog("loggging", "", request.body);
	var curriculum = [];
	var grade = [];
	async.series([
        function (cb) {
			ServiceCurriculum.getUniqueCurriculum({
				isRemoved: false
			}, 'board', function (err, data) {
				for (var i = 0; i < data.length; i++) {
					curriculum.push(data[i].board);
				}

				cb(null);

			})
        },
        function (cb) {
			logging.consolelog("logging", "", request.body.curriculum)
			if (request.body.curriculum) {
				ServiceCurriculum.getSortedData({
					isRemoved: false,
					board: request.body.curriculum,

				}, 'grade', function (err, data) {
					for (var i = 0; i < data.length; i++) {
						grade.push(data[i].grade_name);
					}
					cb(null);

				})
			} else {
				cb(null);
			}


        }
    ], function (err, data) {
		if (!err) {
			response.status(200).send({
				statusCode: 200,
				curriculumList: curriculum,
				gradeList: grade
			})
		} else {
			response.send(err);
		}

	})


}


var socialLogin = function (request, response) {
	var userFound = false;
	var accessToken = null;
	var successLogin = false;
	var updatedUserDetails = null;
	var payloadData = request.body;
	var ip = request.headers['x-forwarded-for'] ? request.headers['x-forwarded-for'] : request.headers['host']
	var loginType = request.headers.logintype;
	async.series([
        function (cb) {
			var criteria = {
				socialId: payloadData.socialId,
				socialType: payloadData.socialType
			};
			var projection = {};
			var option = {
				lean: true
			};
			ServiceCustomer.getCustomer(criteria, projection, option, function (err, result) {
				if (err) {
					cb(err)
				} else {
					userFound = result && result[0] || null;
					cb();
				}
			});

        },
        function (cb) {
			//validations
			if (!userFound) {
				cb(constants.STATUS_MSG.ERROR.SOCIAL_ID_NOT_FOUND);
			} else if (userFound.phoneVerified == false) {

				cb(constants.STATUS_MSG.ERROR.PHONENUMBER_NOT_REGISTERED);

			} else {
				successLogin = true;
				cb();
			}
        },
        function (cb) {
			//Clear Device Tokens if present anywhere else

			var criteria = {
				deviceToken: payloadData.deviceToken
			};
			var setQuery = {
				$unset: {
					deviceToken: 1
				}
			};
			var options = {
				multi: true
			};
			ServiceCustomer.updateCustomer(criteria, setQuery, options, function (err, data) {
				cb(null);
			})

        },
        function (cb) {
			var criteria = {
				_id: userFound._id
			};
			var setQuery = {
				deviceToken: payloadData.deviceToken,
				deviceType: payloadData.deviceType,
				loginTime: new Date()
			};
			ServiceCustomer.updateCustomer(criteria, setQuery, {
				new: true
			}, function (err, data) {
				updatedUserDetails = data;
				cb(err, data);
			});

        },
        function (cb) {
			var criteria = {
				_id: userFound._id

			};
			var projection = {};
			var option = {
				lean: true
			};
			ServiceCustomer.getCustomer(criteria, projection, option, function (err, result) {
				if (err) {
					cb(err)
				} else {
					userFound = result && result[0] || null;
					cb();
				}
			});
        },

         function (cb) {

			var tokenData = {
				id: userFound._id,
				type: userFound.userType,
				ip: ip,
				loginType: loginType,
				userType: 'CUSTOMER',
				date: Date.now()
			};

			TokenManager.setToken(tokenData, function (err, output) {
				if (err) {
					cb(err);
				} else {
					if (output && output.accessToken) {
						accessToken = output && output.accessToken;
						cb();
					} else {
						cb(constants.STATUS_MSG.ERROR.IMP_ERROR)
					}
				}
			})


         }
    ], function (err, data) {
		if (err) {
			response.send(err);
		} else {
			delete userFound['accessToken'];
			response.status(200).send({
				statusCode: 200,
				accessToken: accessToken,
				data: userFound
			});
		}
	});
};


var forgetPassword = function (request, response) {
	var customerData;
	var uniqueCode;
	async.series([
        function (cb) {
			var query = {
				emailId: request.body.emailId
			}
			ServiceCustomer.getCustomer(query, {}, {
				lean: true
			}, function (err, data) {
				if (!err) {
					if (data.length > 0) {
						customerData = data[0];
						cb(null);
					} else {
						cb(constants.STATUS_MSG.ERROR.USER_NOT_FOUND);
					}
				} else {
					cb(err);
				}
			})
        },
       function (cb) {
			uniqueCode = Math.floor(Math.random() * 900000) + 100000;
			var emailSend = {
				emailId: request.body.emailId,
				code: uniqueCode,
				name: customerData.name
			}
			var text;
			text = TemplatesForget.forgetPassword(emailSend);
			NotificationManager.sendEmailToUser(request.body.emailId, 'Sterling Pixels Account OTP', text, function (err, data) {
				if (!err) {
					logging.consolelog('mail', err, data);
				}
				cb(null);
			});



		},
		function (cb) {
			var data = {
				otp: uniqueCode,
				mobileNo: customerData.countryCode + customerData.mobileNo
			}
			NotificationManager.sendSMSToUser(data, 'FORGET_PASSWORD', function (err, data) {
				if (!err) {
					logging.consolelog('sms', err, data);
				}
				cb(null);
			});


		},
        function (cb) {
			ServiceCustomer.updateCustomer({
				emailId: request.body.emailId
			}, {
				$set: {
					code: uniqueCode
				}
			}, {
				lean: true
			}, function (err, data) {
				if (!err) {
					cb(null);
				} else {
					cb(err);
				}
			})

        }

    ], function (err, data1) {
		if (!err) {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.SENT_CODE);
		} else {
			response.send(err);
		}
	});
}


var checkOTP = function (request, response) {
	var customerData;
	async.series([
        function (cb) {
			var query = {
				emailId: request.body.emailId,
				code: request.body.code
			}
			ServiceCustomer.getCustomer(query, {}, {
				lean: true
			}, function (err, data) {
				if (!err) {
					if (data.length > 0) {
						customerData = data[0];
						cb(null);
					} else {
						cb(constants.STATUS_MSG.ERROR.INVALID_CODE);
					}
				} else {
					cb(err);
				}
			})
        }
    ], function (err, data1) {
		if (!err) {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.DEFAULT);
		} else {
			response.send(err);
		}
	});
}


var resetPassword = function (request, response) {

	async.series([
        function (cb) {
			var query = {
				emailId: request.body.emailId,
				code: request.body.code,

			}
			var update = {
				$set: {
					password: UniversalFunctions.encryptData(request.body.password)
				},
				$unset: {
					code: ""
				}
			}
			ServiceCustomer.updateCustomer(query, update, {
				lean: true
			}, function (err, data) {
				if (!err) {
					cb(null);
					// if (data.length > 0) {
					//     customerData = data[0];
					//     cb(null);
					// } else {
					//     cb(ERROR.INVALID_CODE);
					// }
				} else {
					cb(err);
				}
			})
        }
    ], function (err, data1) {
		if (!err) {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.PASSWORD_RESET);
		} else {
			response.send(err);
		}
	});
}

var editProfile = function (request, response) {
	var update = {};
	var sendOtp = false;
	var uniqueCode = null;
	var userDetails = request.userData;
	async.auto({
		verify_mobile:function(cb){
			var total= request.body.countryCode+ request.body.mobileNo;
			var numbers=total.split("+");
			console.log(numbers,"=====> numbers");

			var query={
				number:numbers[1]
			}
			api.validate(query, function (err, result) {
			if (err) {
   				 	 console.log('Validate Callback (Error): ' + JSON.stringify(err));
                     cb(null);
					}
   				else{

   					 console.log('Validate Callback (Success): ' + JSON.stringify(result));
   					 if(result.valid){
   					 	cb(null);
   					 }else{
   					 	cb(constants.STATUS_MSG.ERROR.VALID_NUMBER);
   					 }

   				}
					});


		},
		edit_profile_pic: function (cb) {
			if (request.files) {
				if (request.files.profilePic) {
					UploadManager.uploadPicToS3Bucket(request.files.profilePic, AwsConfig.s3BucketCredentials.folder.profile, function (filename) {
						if (filename == "No File") {
							cb(null);
						} else {
							update.profilePicUrl = AwsConfig.s3BucketCredentials.s3URL + '/profile/' + filename;
							cb(null);
						}
					});
				} else {
					cb(null);
				}
			} else {
				cb(null);
			}
		},
		edit_profile: ['edit_profile_pic','verify_mobile', function (cb) {

			if (request.body.firstName != undefined) {
				update.firstName = request.body.firstName;
			}
			if (request.body.lastName != undefined) {
				update.lastName = request.body.lastName;
			}
			if (request.body.addressValue != undefined) {
				update.addressValue = request.body.addressValue;
			}
			if (request.body.countryCode != undefined) {
				update.countryCode = request.body.countryCode;
			}
			// if (request.body.mobileNo != undefined) {
			//     update.mobileNo = request.body.mobileNo;
			// }
			if (request.body.curriculum != undefined)
				update.curriculum = request.body.curriculum;
			if (request.body.grade != undefined) {
				update.grade = request.body.grade;
			}
			if (request.body.dob != undefined) {
				logging.consolelog("logging", "", request.body.dob)
				update.dob = new Date(request.body.dob);
				logging.consolelog("logging", "", update.dob);
			}
			if (request.body.sessionTo != undefined) {
				logging.consolelog("logging", "", request.body.sessionTo)
				update.sessionTo = new Date(request.body.sessionTo);
				logging.consolelog("logging", "", update.sessionTo);
			}
			if (request.body.sessionFrom != undefined) {
				logging.consolelog("logging", "", request.body.sessionFrom)
				update.sessionFrom = new Date(request.body.sessionFrom);
				logging.consolelog("logging", "", update.sessionFrom);
			}
			if (request.body.mobileNo != userDetails.mobileNo) {
				update.mobileNo= request.body.mobileNo;
			}
			if (request.body.city != undefined) {
				update.city = request.body.city;
			}
			if (request.body.state != undefined) {
				update.state = request.body.state;
			}
			if (request.body.zip != undefined) {
				update.zip = request.body.zip;
			}
			if (request.body.country != undefined) {
				update.country = request.body.country;
			}
			if (request.body.city != undefined) {
				update.city = request.body.city;
			}
			if (request.body.gender != undefined) {
				update.gender = request.body.gender;
			}
			logging.consolelog("logging", "", update);
			ServiceCustomer.update({
				_id: request.body.customerId
			}, update, {}, function (err, data) {
				logging.consolelog("logging", err, data);
				if (!err) {
					cb(null);
				} else {
					cb(err);
				}
			})


        }],
		//		verify_mobileNo:['edit_profile',function(cb){
		//
		//			ServiceCustomer.getCustomer({_id:})
		//
		//		}],
		// send_otp: ['edit_profile', function (cb) {
		// 	if (sendOtp) {
		// 		uniqueCode = Math.floor(Math.random() * 900000) + 100000;
		// 		ServiceCustomer.updateCustomer({
		// 			_id: request.body.customerId
		// 		}, {
		// 			$set: {
		// 				otp: uniqueCode
		// 			}
		// 		}, {}, function (err, data) {
		// 			if (!err) {
		// 				cb(null);
		// 			} else {
		// 				cb(err);
		// 			}
		// 		})

		// 	} else {
		// 		cb(null);
		// 	}
        // }],
		// send_mail: ['send_otp', function (cb) {
		// 	if (sendOtp) {
		// 		var emailSend = {
		// 			emailId: request.body.emailId,
		// 			code: uniqueCode,
		// 			name: request.body.name
		// 		}
		// 		var text;
		// 		text = Templates.customerRegister(emailSend);
		// 		NotificationManager.sendEmailToUser(request.body.emailId, 'The OTP for registeration on PixelsED is ' + uniqueCode, text, function (err, data) {
		// 			if (!err) {
		// 				logging.consolelog('mail', err, data);
		// 			}
		// 			cb(null);
		// 		});
		// 	} else {
		// 		cb(null);
		// 	}
		// }],
		// send_sms: ['send_otp', function (cb) {
		// 	if (sendOtp) {
		// 		var data = {
		// 			otp: uniqueCode,
		// 			mobileNo: request.body.countryCode + request.body.mobileNo
		// 		}
		// 		NotificationManager.sendSMSToUser(data, 'REGISTRATION', function (err, data) {
		// 			logging.consolelog(err, data, "sms config");
		// 			cb(null);
		// 		})
		// 	} else {
		// 		cb(null);
		// 	}
		// }]

	}, function (err, data) {
		if (!err) {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.EDIT_SUCCESSFULLY);
		} else {
			response.send(err);
		}
	})
}


var getProfile = function (request, response) {
	request.body.loginType = request.userData.loginType;
	var customerData = null;
	async.auto({
		get_profile: function (cb) {

			ServiceCustomer.getCustomer({
				_id: request.body.customerId
			}, {}, {}, function (err, data) {
				if (!err) {
					if (data.length > 0) {
						customerData = data[0];
						cb(null);
					} else {
						cb(constants.STATUS_MSG.ERROR.USER_NOT_FOUND);
					}


				} else {
					cb(err);
				}
			})
		}
	}, function (err, data) {
		if (!err) {
			if (request.body.loginType == "WEBAPP")
				customerData.accessToken = customerData.accessToken.WEBAPP;
			if (request.body.loginType == "ANDROIDAPP")
				customerData.accessToken = customerData.accessToken.ANDROIDAPP;
			if (request.body.loginType == "IOSAPP")
				customerData.accessToken = customerData.accessToken.IOSAPP;
			delete customerData['accessToken'];
			response.status(200).send({
				statusCode: 200,
				data: customerData,
				accessToken: customerData.accessToken
			})
		} else {
			response.send(err);
		}
	})
}

var verifyOTPeditProfile = function (request, response) {
	var sendOtp = false;
	async.auto({
		verify_new_number: function (cb) {
			if (request.body.newMobileNo != undefined) {
				//				var query = {
				//
				//					mobileNo: request.body.newMobileNo,
				//
				//
				//				};
				//				ServiceCustomer.getCustomer(query, {}, {
				//					lean: true
				//				}, function (error, data) {
				//					if (error) {
				//						cb(error);
				//					} else {
				//						if (data && data.length > 0) {
				//							if (data[0].phoneVerified == true && data[0].emailId == request.body.emailId) {
				//								cb(null);
				//							} else if (data[0].phoneVerified == true) {
				//								cb(ERROR.USER_ALREADY_REGISTERED);
				//							} else {
				//								sendOtp = true;
				//								ServiceCustomer.deleteCustomer({
				//									_id: data[0]._id
				//								}, function (err, updatedData) {
				//									if (err) cb(err)
				//									else cb(null);
				//								})
				//							}
				//						} else {
				//							cb(null);
				//						}
				//					}
				//				});
				cb(null);

			} else {
				cb(null);
			}


		},
		verify_otp: function (cb) {
			sendOtp = true;
			var query = {
				_id :request.userData.id,
				otp: request.body.otp
			}
			ServiceCustomer.getCustomer(query, {
				_id: 1
			}, {
				lean: true
			}, function (err, data) {
				if (!err) {
					if (data.length > 0) {
						cb(null);
					} else {
						cb(constants.STATUS_MSG.ERROR.INVALID_CODE);
					}
				} else {
					cb(err);
				}
			})
		},
		update_new_number: ['verify_otp', 'verify_new_number', function (cb) {
			if (sendOtp) {
				var query = {
					 _id :request.userData.id,
					otp: request.body.otp
				}
				var update = {
					$set: {
						mobileNo: request.body.newMobileNo
					},
					$unset: {
						otp: 1
					}
				}
				ServiceCustomer.updateCustomer(query, update, {}, function (err, data) {
					if (!err) {
						cb(null);
					} else {
						cb(err);
					}
				})
			}

    }]


	}, function (err, data) {
		if (!err) {
			response.status(200).send(constants.STATUS_MSG.SUCCESS.EDIT_SUCCESSFULLY);
		} else {
			response.send(err);
		}
	})
}

function findChildOfParent(customerId, temp, callback) {
	logging.consolelog("hello, findChildOfParent", "", customerId);
	var childCustomers = []
	var query = {
		parentCustomerId: {
			$in: customerId
		},
		$or: [{
			childCustomerType: 'SUB_AGENT'
        }, {
			childCustomerType: 'SCHOOL'
        }],
	}
	ServiceChildMapper.getChildParentMapper(query, {
		childCustomerId: 1
	}, {
		lean: true
	}, function (err, data) {
		if (!err) {
			logging.consolelog("hello", data, temp);
			if (data.length > 0) {

				for (var i = 0; i < data.length; i++) {
					temp.push({
						parent: data[i].parentCustomerId,
						child: data[i].childCustomerId
					});
					childCustomers.push(data[i].childCustomerId);
				}
				findChildOfParent(childCustomers, temp, callback);
			} else {
				callback(null, temp);
			}

		} else {
			callback(err);
		}
	});

}

var agentDashboardData = function (request, response) {
	//get agent Id
	var agentId = request.userData.id;
	var license = {};
	var childData = [];
	var customers = {
		teachers: 0,
		students: 0,
		school: 0,
		sub_agent: 0

	};
	async.auto({
			get_license_count: function (cb) {
				var query = {
					_id: agentId
				}
				ServiceCustomer.getCustomer(query, {
					license: 1
				}, {
					lean: true
				}, function (err, data) {

					if (!err) {

						if (data.length > 0 && data[0].license.length > 0) {
							license.allocatedLicense = data[0].license[0].allocatedLicense;
							license.usedLicense = data[0].license[0].usedLicense;
						}
						cb(null);
					} else {
						cb(err);
					}
				});
			},
			get_childOf_agent: function (cb) {
				var query = {
					superParentCustomerId: agentId,
					parentCustomerId: agentId,
					$or: [{
						childCustomerType: 'SUB_AGENT'
                    }, {
						childCustomerType: 'SCHOOL'
                    }],
				}
				var projection = {
					childCustomerId: 1
				}
				var path = 'childCustomerId';
				var select = 'name emailId'

				var populate = {
					path: path,
					match: {},
					select: select,
					options: {
						lean: true
					}
				};
				logging.consolelog(query, projection, populate);
				ServiceChildMapper.childParentMapperPopulate(query, projection, populate, {}, {}, function (err, data) {
					logging.consolelog(err, data, "print data");
					if (!err) {
						for (var i = 0; i < data.length; i++) {
							childData.push({
								_id: data[i].childCustomerId._id,
								emailId: data[i].childCustomerId.emailId,
								name: data[i].childCustomerId.name,
								studentCount: 0,
								teacherCount: 0,
								sub_agentCount: 0,
								schoolCount: 0,
								childId: [],
								engangedTimeStudent: 0,
								engangedTimeTeacher: 0
							})
						}
						cb(null);
					} else {
						cb(err);
					}
				})
			},
			get_child_of_subagent: ['get_childOf_agent', function (cb) {
				logging.consolelog(childData, "", "child data");
				var childDataIds = [];
				if (childData.length > 0) {
					for (var i = 0; i < childData.length; i++) {
						childDataIds.push(childData[i]._id);
					}
					var query = {
						superParentCustomerId: agentId,
						parentCustomerId: {
							$in: childDataIds
						}

					}
					ServiceChildMapper.getChildParentMapper(query, {
						childCustomerType: 1,
						parentCustomerId: 1
					}, {
						lean: true
					}, function (err, data) {
						logging.consolelog("logging", err, data);
						if (!err) {
							for (var i = 0; i < data.length; i++) {
								for (var j = 0; j < childData.length; j++) {
									if (data[i].parentCustomerId == childData[j]._id) {
										if (data[i].childCustomerType == 'STUDENT') {
											childData[j].studentCount += childData[j].studentCount + 1;
										}
										if (data[i].childCustomerType == 'TEACHER') {
											childData[j].teacherCount += childData[j].teacherCount + 1;
										}
										if (data[i].childCustomerType == 'SUB_AGENT') {
											childData[j].sub_agentCount += childData[j].sub_agentCount + 1;
										}
										if (data[i].childCustomerType == 'SCHOOL') {
											childData[j].schoolCount += childData[j].schoolCount + 1;
										}
									}
								}

							}
							cb(null);
						} else {
							cb(err);
						}

					});
				} else {
					cb(null);
				}
            }],
			total_customers_count: function (cb) {


				var query = {
					superParentCustomerId: agentId
				}
				var projection = {
					childCustomerType: 1,
					childCustomerId: 1
				}
				logging.consolelog("logging", query, projection);
				ServiceChildMapper.getChildParentMapper(query, projection, {
					lean: true
				}, function (err, data) {
					logging.consolelog(err, data, "total customers count");
					if (!err) {
						for (var i = 0; i < data.length; i++) {
							if (data[i].childCustomerType == 'STUDENT') {
								customers.students += customers.students + 1;
							}
							if (data[i].childCustomerType == 'TEACHER') {
								customers.teachers += customers.teachers + 1;
							}
							if (data[i].childCustomerType == 'SUB_AGENT') {
								customers.sub_agent += customers.sub_agent + 1;
							}
							if (data[i].childCustomerType == 'SCHOOL') {
								customers.school += customers.school + 1;
							}

						}
						cb(null);
					} else {
						cb(err);
					}
				})
			},
			engangement_hours_childData: ['get_childOf_agent', function (cb) {
				var childDataIds = [];
				var temp = []
				if (childData.length > 0) {
					for (var i = 0; i < childData.length; i++) {
						childDataIds.push(childData[i]._id);
					}
					findChildOfParent(childDataIds, temp, function (err, data) {
						if (!err) {
							logging.consolelog(data, "", "child of parents");
							for (var i = 0; i < data.length; i++) {
								for (var j = 0; j < childData.length; j++) {
									if (data[i].parent == childData[j]._id) {
										childData[j].childId.push(data[i].child);
									}
								}
							}
							cb(null);
						}
					});
				} else {
					cb(null);
				}
            }],
			engangement_hours: ['engangement_hours_childData', function (cb) {
				if (childData.length > 0) {
					var counter = 0;
					async.forEach(childData, function (item, embeddedcb) {
						counter++
						var query = {
							customerId: {
								$in: item.childId
							}
						}

						ServiceAnalysisData.getAnalysisData({}, {
							userType: 1,
							engangedTime: 1
						}, {
							lean: true
						}, function (err, data) {
							logging.consolelog(err, data, counter);
							if (!err) {
								for (var i = 0; i < data.length; i++) {
									if (data[i].userType == 'STUDENT') {
										item.engangedTimeStudent += data[i].engangedTime;

									}
									if (data[i].userType == 'TEACHER') {
										item.engangedTimeTeacher += data[i].engangedTime;

									}

								}
								embeddedcb(null);
							} else {
								embeddedcb(err);
							}
						})
					}, function (err) {
						if (counter == childData.length) {
							cb();
						}
					});

				} else {
					cb(null);
				}
            }]
		},
		function (err, data) {

			if (!err) {
				response.status(200).send({
					statusCode: 200,
					childData: childData,
					license: license,
					customers: customers
				})
			} else {
				response.send(err);
			}
		})
}


var renewToken = function (request, response) {
	var userDetails = request.userData;
	var loginType = request.headers.logintype;
	// request.body.emailId = request.body.emailId.toLowerCase();
	var ip = request.headers['x-forwarded-for'] ? request.headers['x-forwarded-for'] : request.headers['host'];
	var accessToken = "";
	async.auto({

		"renew_token": (cb) => {
    if(loginType== " ANDROIDAPP"){
		var tokenData = {
			id: userDetails.id,
			type: request.body.userType,
			ip: ip,
			loginType: loginType,
			userType: 'CUSTOMER',
			date: Date.now()
		};
		logging.consolelog('sending token', '', tokenData);
		TokenManager.setToken(tokenData, function (err, output) {

			if (err) {
				cb(err);
			} else {
				accessToken = output && output.accessToken || null;
				cb();
			}
		})
	}else{
		cb();
	}
	}
			
	}, (err, data) => {
		if (!err) {
			var dataTosend = {

				accessToken: accessToken
			};

			response.status(200).send(dataTosend)

		} else {
			response.send(err);
		}

	})

}

var getDays = function (request, response) {
	console.log("ServiceMarketingDays ::");
	ServiceMarketingDays.getDays({}, {
		days: 1
	}, {}, (err, data) => {
		if (!err) {
			response.status(200).send({
				"data": data,
				statusCode: 200
			});
		} else {
			response.send(err);
		}


	});
}


var updateNotification = function (request, response) {

	console.log("update notifications");
	var query = {
		_id: request.userData.id
	}
	var update = {
		$set: {}
	}

	if (request.body.notificationType) {
		update["$set"]["notificationType"] = request.body.notificationType
	}

	if (request.body.deviceToken) {
		update["$set"]["deviceToken"] = request.body.deviceToken;
		update["$set"]["deviceType"] = request.body.deviceType;
	}
	ServiceCustomer.updateCustomer(query, update, {
		lean: true
	}, (err, data) => {
		if (!err) {
			response.status(200).send({
				"data": data,
				"statusCode": 200
			});
		} else {
			response.send(err);
		}
	})
}

module.exports = {
	customerRegister: customerRegister,
	verifyOTP: verifyOTP,
	resendOTP: resendOTP,
	customerLogin: customerLogin,
	socialLogin: socialLogin,
	listing: listing,
	forgetPassword: forgetPassword,
	checkOTP: checkOTP,
	resetPassword: resetPassword,
	editProfile: editProfile,
	getProfile: getProfile,
	verifyOTPeditProfile: verifyOTPeditProfile,
	updatePassword: updatePassword,
	agentDashboardData: agentDashboardData,
	renewToken: renewToken,
	getDays: getDays,
	updateNotification: updateNotification
}