/**
 * Created by sterlingpixels on 18/03/17.
 */
module.exports = {
    customerContentSharing: require('./customerContentSharing'),
    customerManagePackages: require('./customerManagePackages'),
    customerNotifications: require('./customerNotifications'),
    customerOnboarding: require('./customerOnboarding'),
    customerSettingBoard: require('./customerSettingBoard'),
    customerTopicModule: require('./customerTopicModule'),
};