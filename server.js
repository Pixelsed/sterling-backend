console.log('NODE_ENV', process.env.NODE_ENV)
Config=require('config')

var http = require('http'),
	https = require('https'),
	express = require('express'),
	bodyParser = require('body-parser'),
	path = require('path'),
	fs = require('fs'),startServer,
	mongoose = require('mongoose'),
	app = express();

logging = require("./Library/logging");
var NotificationManager= require("./Library/notificationManager");
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json())
app.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE, HEAD');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With, X-HTTP-Method-Override,loginType');
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware
    next();
});

app.use(function (req, res, next) {
	var contentType = req.headers['content-type'] || '',
		mime = contentType.split(';')[0];

	if (mime != 'text/plain') {
		return next();
	}

	var data = '';

	req.on('data', function (chunk) {

		data += chunk;
	});
	req.on('end', function () {
		req.rawBody = data;

		next();
	});
});

app.use('/api/customer',require('./Route/customerRoutes'));
app.use('/api/superAdmin',require('./Route/superAdminRoutes'));
app.use(express.static('/home/sterling/sterling-frontend/app/website'))
app.use(express.static('/home/sterling/sterling-frontend/app'));
app.get('/*', function (req, res) {
	res.sendFile('/home/sterling/sterling-frontend/app/website/index.html')
});



app.post("/testNotification",(req,res)=>{

	NotificationManager.sendAndroidPushNotification("","",(err,data)=>{
		if(!err){
			res.send(data);
		}else{
			res.send(err);
		}
	})
})
if (process.env.NODE_ENV == 'LIVE') {
//	var options = {
//		key: fs.readFileSync('/home/sterling/server.key'),
//		cert: fs.readFileSync('/home/sterling/e9b426921a39b52e.crt'),
//		ca: fs.readFileSync('/home/sterling/gd_bundle-g2-g1.crt')
//	};
    console.log('STARTING ================ LIVE');
	startServer = http.createServer(app);
	startServer.listen(Config.get('PORT'), function () {
		console.log('LISTENING ================ ',Config.get('PORT'));
	});
} else if (process.env.NODE_ENV == 'STAGING') {
    console.log('STARTING ================ STAGING');
	startServer = http.createServer(app)
	startServer.listen(Config.get('PORT'), function () {
		console.log('LISTENING ================ ',Config.get('PORT'));
	});
} else {
    console.log('STARTING ================ TEST');
	startServer = http.createServer(app);
	startServer.listen(Config.get('PORT'), function () {
		console.log('LISTENING ================ ',Config.get('PORT'));
	});
}

mongoose.connect(Config.get('mongoConfig.url'),{},function (error) {
	// body...
	if(error)
		throw error;
	console.log('MONGODB ================ CONNECTED')
})