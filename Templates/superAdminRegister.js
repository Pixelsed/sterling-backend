/**
 * Created by priya on 8/4/16.
 */
var Handlebars = require('handlebars');
//invoice for completing booking
exports.adminRegister=function(data)
{
    console.log(data);
	
	var messagehtml = '<!doctype html><html><head><meta charset="utf-8"><title>Sterling</title></head><body style="margin:0px; padding:15px; font-family:Arial, sans-serif; font-size:15px; line-height:22px; color:#333; background:#f6f6f6;"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="max-width:600px;"><tbody><tr><td style="background-color:#fff; text-align:center; padding:10px;"><img src="https://www.pixelsed.com/website/assets/images/logo.png" style="border:0px; outline:none;vertical-align:bottom;"></td></tr><tr><td style="background-color:#fff;"><div style="height:4px;"><img src="https://www.pixelsed.com/website/assets/images/header-bg.png" style="border:0px; outline:none; vertical-align:top; width:100%; height:4px;"></div><div><img src="https://www.pixelsed.com/website/assets/images/login-banner.png" style="border:0px; outline:none; vertical-align:top; width:100%;"></div></td></tr>';
	messagehtml += '<tr><td style="background-color:#fff; padding:20px;">';
	
	/*messagehtml += '<div style="font-size:20px; color:#00a8ff; padding-bottom:10px;">Hi {{name}}</div>';
    messagehtml += '<div>PixelsEd give you sub-Admin access.</div>';
    messagehtml += '<div style="padding:10px 0;"><table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" style="padding:10px; background-color:#f6f6f6;"><tbody>';
    messagehtml += '<tr><td align="left" style="color:#999;">Username :</td><td align="right">{{emailId}}</td></tr>';
	messagehtml += '<tr><td align="left" style="color:#999;">Password :</td><td align="right">{{password}}</td></tr>';
    messagehtml += '</tbody></table></div>';
    messagehtml += '<div>Please login to this web-panel. If you have questions, we suggest you read our <a href ="{{FAQ}}" style="color:#333; text-decoration:underline;">FAQs.</a> If you can’t find the answers there, give us a call at +919115708391.</div>';*/
	
	messagehtml += '<div style="font-size:20px; color:#00a8ff; padding-bottom:10px;">Hello {{name}}</div>';
    messagehtml += '<div style="font-size:14px; color:#666; padding-bottom:20px;">You have successfully registered with PixelsED.<br>Your registered login ID is <span style="background-color:#f6f6f6; color:#00a8ff;">{{emailId}}</span>, and password is <span style="background-color:#f6f6f6; color:#00a8ff;">{{password}}</span>.<br>Have fun learning and teaching with our 3D animation, assessments and resources.</div>';
    messagehtml += '<div style="font-size:13px; line-height:18px; color:#666;">Warmest Regards<br><strong>Team PixelsED</strong></div>';
	
	
	messagehtml += '</td></tr>';
	messagehtml += '<tr><td style="text-align:center; padding-top:10px;"><a href="#"><img src="https://cdn3.iconfinder.com/data/icons/free-social-icons/67/facebook_circle_color-32.png" style="border:0px; vertical-align:top;"></a>&nbsp; <a href="#"><img src="https://cdn3.iconfinder.com/data/icons/free-social-icons/67/twitter_circle_color-32.png" style="border:0px; vertical-align:top;"></a>&nbsp; <a href="#"><img src="https://cdn3.iconfinder.com/data/icons/free-social-icons/67/linkedin_circle_color-32.png" style="border:0px; vertical-align:top;"></a>&nbsp; <a href="#"><img src="https://cdn3.iconfinder.com/data/icons/free-social-icons/67/google_circle_color-32.png" style="border:0px; vertical-align:top;"></a> </td> </tr> <tr> <td style="font-size:12px; text-align:center; color:#999; padding:10px;"> Questions? Check our <a href="#" style="color:#666; text-decoration:underline;">FAQs</a> or drop us a line.<div><a href="#" style="color:#666; text-decoration:underline;">Privacy</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="#" style="color:#666; text-decoration:underline;">Terms</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="#" style="color:#666; text-decoration:underline;">Cancellation</a></div> Okhla New Delhi<br> &copy; 2016 PixelsEd. All Rights Reserved.</td></tr></tbody></table></body></html>';
	
    var template = Handlebars.compile(messagehtml);
    return template(data);
}