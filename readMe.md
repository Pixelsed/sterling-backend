**Sterling Pixels : E-Learning System**
===============================================

PixelsEd is an e-learning project of Sterling Publishers. The project is for digital content development for schools/universities/distributors. The project flows both in B2C and B2B models. Customers are Teachers and Students. Agent user type is business type for pixelsed. CC-Avenue payment gateway is integrated in the system. There is offline  payment transfer between agent and pixelsed.

CONFIGURATION AND INSTALLATION
-------------------------------

Find the access of the git repository in which you need to get started with the connected web apps. follow below steps to configure it

 - Clone the GIT repository
 - Checkout into the latest git branch
 - Install Node package dependencies using  ``npm install``
 - Install Gulp using ``npm install -g gulp``
 - Develop or modify changes
 - Build and run it into the browser using ``gulp default`` command

MORE INFORMATION
----------------

