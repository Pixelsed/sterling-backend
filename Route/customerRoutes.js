var express = require('express'),
	router = express.Router();
var customer = require('../Controller/customer/index');
var constants = require('../Library/appConstants');
var social = constants.DATABASE.SOCIAL_TYPE;
var userType = constants.USER_TYPE;
var deviceType = constants.DATABASE.DEVICE_TYPE;
var loginType = constants.DATABASE.LOGIN_TYPE;
var expressJoi = require('express-joi-validator');
var Joi = require('joi');
var multipart = require('connect-multiparty');
var TokenManager = require('../Library/tokenManager');
var _ = require('underscore')

router.use(
	function (request, res, next) {
		console.log('request path>>', request.path);
		var excludedPath = ['/login', '/register', '/forgetPassword', '/resetPassword', '/checkOTP', '/checkingOfEmbeddedLinks',"/getMarketingDays",'/getFilters',"/learnTopicLibrary","/website/topicDetails","/socialLogin", "/transactionCompleted/","/cartNumber"]

		// if(request.path.includes('/renewApi')){
		// 	console.log(" renew api path", request.headers.authorization, request.path);
        //  TokenManager.verifyToken(request.headers.authorization, function (err, token) {
		// 		logging.consolelog(err, token, "token authorization renew api ");
		// 		if (!err) {
					
		// 			var secCheck = request.headers['x-forwarded-for'] ? request.headers['x-forwarded-for'] : request.headers['host'];
					
					
		// 				request.userData = token.userData;
		// 				next();
					
               
		// 			}
				
		// 		else {
		// 			logging.consolelog('error 2')
		// 			res.send(JSON.stringify(constants.STATUS_MSG.ERROR.INVALID_TOKEN));
		// 		}
		// 	});

		// }
		if (_.contains(excludedPath, request.path) || request.path.includes("/transactionCompleted/")) {
			return next()
		} else {
			console.log(" check remaining api",request.path,request.headers.authorization );
			TokenManager.verifyToken(request.headers.authorization, function (err, token) {
				logging.consolelog(err, token, "token authorization");
				if (!err) {
					logging.consolelog('header type>>>', err, request.headers)
					var secCheck = request.headers['x-forwarded-for'] ? request.headers['x-forwarded-for'] : request.headers['host'];
					logging.consolelog('check ip>>>>', err, token.userData.ip === secCheck, token.userData.ip, secCheck );
					if (token.userData.ip === secCheck) {
						request.userData = token.userData;
						next();
					} else if(Date.now- token.userData.date< 30*60000){
                        request.userData = token.userData;
						next();
             
					}
					else {
						logging.consolelog('error 1');
						request.userData = token.userData;
						// res.send(JSON.stringify(constants.STATUS_MSG.ERROR.RENEW_TOKEN));
						next();
					}

				} else {
					logging.consolelog('error 2')
					res.send(JSON.stringify(constants.STATUS_MSG.ERROR.INVALID_TOKEN));
				}
			});
		}
	})



//-------------------------CUSTOMER ON BOARDING-------------------------------//


router.post('/renewApi', function(req,res){
	customer.customerOnboarding.renewToken(req,res)

});


router.post("/paymentHistory", function(req,res){

    customer.customerTopicModule.paymentHistory(req,res)

});

router.post('/register', [multipart()], function (req, res) {
	customer.customerOnboarding.customerRegister(req, res)
});

router.post('/dashboard', function (req, res) {
	customer.customerOnboarding.agentDashboardData(req, res);
});
router.post('/listing', function (req, res) {
	customer.customerOnboarding.listing(req, res)
});

router.post('/resendOTP', function (req, res) {
	customer.customerOnboarding.resendOTP(req, res)
});

router.post('/login', function (req, res) {
	req.body.emailId = req.body.emailId.toLowerCase()
	customer.customerOnboarding.customerLogin(req, res)
});

router.post('/socialLogin', function (req, res) {
	customer.customerOnboarding.socialLogin(req, res)
});

router.post('/forgetPassword', function (req, res) {
	customer.customerOnboarding.forgetPassword(req, res)
});

router.post('/verifyOTP', function (req, res) {
	customer.customerOnboarding.verifyOTP(req, res)
});

router.post('/checkOTP', function (req, res) {
	customer.customerOnboarding.checkOTP(req, res)
});

router.post('/resetPassword', function (req, res) {
	customer.customerOnboarding.resetPassword(req, res)
});

router.post('/updatePassword', function (req, res) {
	customer.customerOnboarding.updatePassword(req, res)
});

router.post('/editProfile', [multipart()], function (req, res) {
	customer.customerOnboarding.editProfile(req, res)
});

router.post('/getProfile', [multipart()], function (req, res) {
	customer.customerOnboarding.getProfile(req, res)
});

router.post('/verifyOTPeditProfile', function (req, res) {
	customer.customerOnboarding.verifyOTPeditProfile(req, res)
});

//-------------------------CUSTOMER ON TOPIC MODULE-------------------------------//


router.post('/topicLibrary/:type',
	function (request, response) {
		customer.customerTopicModule.topicLibrary(request, response);
	});

router.post("/learnTopicLibrary", function(request,response){
	customer.customerTopicModule.learnTopicLibrary(request,response);
})

router.post('/topicDetails',
	function (request, response) {
		customer.customerTopicModule.topicDetails(request, response);
	});

router.post('/createComment',
	function (request, response) {
		if (request.headers.authorization) {
			customer.customerTopicModule.createComment(request, response);
		} else {
			response.send(constants.STATUS_MSG.ERROR.INVALID_TOKEN);
		}
	});


// not using right now
//
// router.post('/downloadContent', expressJoi({
//         body: {
//
//         }
//     }),
//     function (request, response) {
//         if (request.headers.authorization) {
//             customer.downloadContent(request, response);
//         } else {
//             response.send(constants.STATUS_MSG.ERROR.INVALID_TOKEN);
//         }
//
//     });

router.post('/getFilters',
	function (request, response) {
		customer.customerTopicModule.getFilters(request, response);
	});

/*

 headers:
 authorisation:required,string

 body:

 topicId:number, required,
 */
router.post('/getAssessments',
	function (request, response) {
		if (request.headers.authorization) {
			customer.customerTopicModule.getAssessments(request, response);
		} else {
			response.send(constants.STATUS_MSG.ERROR.INVALID_TOKEN);
		}

	});

router.post('/getCategoryContent',
	function (request, response) {
		if (request.headers.authorization) {
			customer.customerTopicModule.getCategoryContent(request, response);
		} else {
			response.send(constants.STATUS_MSG.ERROR.INVALID_TOKEN);
		}

	});

router.post('/getSubCategoryContent',
	function (request, response) {
		if (request.headers.authorization) {
			customer.customerTopicModule.getSubCategoryContent(request, response);
		} else {
			response.send(constants.STATUS_MSG.ERROR.INVALID_TOKEN);
		}

	});

router.post('/addToCart/:type',
	function (request, response) {
		customer.customerTopicModule.addToCart(request, response);

	});

router.post('/getCart',
	function (request, response) {
		customer.customerTopicModule.getCart(request, response);
	}
);

router.post('/removeCart/:type',
	function (request, response) {
		customer.customerTopicModule.removeCart(request, response);
	});

router.post('/cartNumber',
	function (request, response) {
		customer.customerTopicModule.cartNumber(request, response);
	});

router.post('/checkout',
	function (request, response) {
		customer.customerTopicModule.checkout(request, response);
	});

router.post('/transactionFailed/:transactionId',
	function (request, response) {
		customer.customerTopicModule.transactionFailed(request, response);
	});

router.post('/transactionCompleted/:transactionId',
	function (request, response) {
		customer.customerTopicModule.transactionCompleted(request, response);
	});

router.post('/transactionSuccessful',
	function (request, response) {
		if (request.headers.authorization) {
			customer.customerTopicModule.transactionSuccessful(request, response);
		} else {
			response.send(constants.STATUS_MSG.ERROR.INVALID_TOKEN);
		}
	});

router.post('/markFavourite',
	function (request, response) {
		customer.customerTopicModule.markFavourite(request, response);
	});

router.post('/saveNote', expressJoi({
		body: {
			topicId: Joi.number(),
			note: Joi.string().optional().allow('')
		}
	}),
	function (request, response) {
		if (request.headers.authorization) {
			customer.customerTopicModule.saveNote(request, response);
		} else {
			response.send(constants.STATUS_MSG.ERROR.INVALID_TOKEN);
		}
	});


/*

 headers:
 authorisation:required,string

 body:
 customerId:string, required,
 topicId:string, required
 */
router.post('/getNote',
	function (request, response) {
		if (request.headers.authorization) {
			customer.customerTopicModule.getNote(request, response);
		} else {
			response.send(constants.STATUS_MSG.ERROR.INVALID_TOKEN);
		}

	}
);

router.post('/getCustomerPackages',
	function (request, response) {
		customer.customerTopicModule.getCustomerPackages(request, response);
	});

router.post('/getCustomerTopicPackages',
	function (request, response) {
		if (request.headers.authorization) {
			customer.customerTopicModule.getCustomerTopicPackages(request, response);
		} else {
			response.send(constants.STATUS_MSG.ERROR.INVALID_TOKEN);
		}

	});

router.post('/unmarkFavourite',
	function (request, response) {
		customer.customerTopicModule.unmarkFavourite(request, response);
	});


// router.post('/getCustomerPurchases', expressJoi({
//         body: {
//
//         }
//     }),
//     function (request, response) {
//         if (request.headers.authorization) {
//             customer.getCustomerPurchases(request, response);
//         } else {
//             response.send(constants.STATUS_MSG.ERROR.INVALID_TOKEN);
//         }
//
//     });

router.post('/addAnalysisData',
	function (request, response) {
		if (request.headers.authorization) {
			customer.customerTopicModule.addAnalysisData(request, response);
		} else {
			response.send(constants.STATUS_MSG.ERROR.INVALID_TOKEN);
		}

	});

router.post('/selectAllAddToCart',
	function (request, response) {
		customer.customerTopicModule.selectAllAddToCart(request, response);
	});

router.post('/renewExpirey', function (request, response) {

	if (request.headers.authorization) {
		customer.customerTopicModule.renewExpirey(request, response);
	} else {
		response.send(constants.STATUS_MSG.ERROR.INVALID_TOKEN);
	}

});

// router.post('/addEditScoreBoard', expressJoi({
//     body:{
//
//     }
// }),function (request, response) {
//     customer.addEditScoreBoard(request, response);
// })


//----------------------------CUSTOMER CONTENTSHARING-----------------------------//

router.post('/addCustomer', function (req, res) {
	customer.customerContentSharing.addCustomer(req, res)
});

router.post('/customerUploadViaCSV', [multipart()],
	function (req, res) {
		customer.customerContentSharing.customerUploadViaCSV(req, res)
	});

router.post('/getCustomers', function (req, res) {
	customer.customerContentSharing.getCustomers(req, res)
});

router.post('/getPurchasedTopics', function (req, res) {
	customer.customerContentSharing.getPurchasedTopics(req, res)
});

router.post('/getGradeAndCountryList', function (req, res) {
	customer.customerContentSharing.getGradeAndCountryList(req, res)
});

router.post('/getAgents', function (req, res) {
	customer.customerContentSharing.getAgents(req, res)
});

router.post('/blockAgent', function (req, res) {
	customer.customerContentSharing.blockAgent(req, res)
});

router.post('/unblockAgent', function (req, res) {
	customer.customerContentSharing.unblockAgent(req, res)
});

router.post('/editAgent',
	function (req, res) {
		customer.customerContentSharing.editAgent(req, res)
	});

router.post('/shareContent',
	function (req, res) {
		customer.customerContentSharing.shareContent(req, res)
	});

router.post('/sharedTopicHistory',
	function (req, res) {
		customer.customerContentSharing.sharedTopicHistory(req, res)
	});

router.post('/checkingOfEmbeddedLinks', function (req, res) {
	customer.customerContentSharing.checkingOfEmbeddedLinks(req, res)
});

router.post('/deleteAssignedContent', function (req, res) {
	customer.customerContentSharing.deleteAssignedContent(req, res)
});
router.post('/getEmbeddedLinksForDomains', function (req, res) {
	customer.customerContentSharing.getEmbeddedLinksForDomains(req, res)
});

router.post('/getPackage',
	function (request, response) {
		customer.customerManagePackages.getPackages(request, response);
	});

router.post('/getPackageInfo',
	function (request, response) {
		if (request.headers.authorization) {
			customer.customerManagePackages.getPackageInfo(request, response);
		} else {
			response.send(constants.STATUS_MSG.ERROR.INVALID_TOKEN);
		}

	});

router.post('/getExpiredPackages', function (request, response) {
	customer.customerTopicModule.getExpiredPackages(request, response);
});

router.post('/deleteBooking', function (request, response) {
	if (request.headers.authorization) {
		customer.customerTopicModule.deleteBooking(request, response);
	} else {
		response.send(constants.STATUS_MSG.ERROR.INVALID_TOKEN);
	}
});

router.get('/notificationsCount', function (req, res) {
	customer.customerNotifications.notificationsCount(req, res)
});

router.post('/getAllNotifications', function (req, res) {
	customer.customerNotifications.getAllNotifications(req, res)
});



//----------------Customer App APIS-----------------------------------
router.post('/app/topicDetails', function (req, res) {
	customer.customerTopicModule.appTopicDetails(req, res)
});



router.post('/app/topicCategories', (req,res)=>{
	customer.customerTopicModule.appTopicCategories(req,res)
});


router.post('/app/topicCategories', (req,res)=>{
	customer.customerTopicModule.appTopicCategories(req,res)
});


router.post('/app/topicAssessment', (req,res)=>{
	customer.customerTopicModule.appTopicAssessment(req,res)
});


router.get("/getMarketingDays",function(req,res){
    console.log("---> get days");
    customer.customerOnboarding.getDays(req,res);
});


router.post('/app/topicComments', (req,res)=>{
	customer.customerTopicModule.appTopicComments(req,res)
});


router.post("/website/topicDetails", function(req,res){
	console.log("===> website topic details");
	customer.customerTopicModule.websiteTopicDetails(req,res);
});


router.post("/app/updateNotification", function(req,res){
	console.log("====> update notification");
	customer.customerOnboarding.updateNotification(req,res);
});





module.exports = router;