/**
 * Created by sterlingpixels on 17/03/17.
 */

var express = require('express'),
    router = express.Router();
var expressJoi = require('express-joi-validator');
var Joi = require('joi');
var constants = require('../Library/appConstants');
var multipart = require('connect-multiparty');
var superAdmin = require('../Controller/superAdmin/index');
var TokenManager = require('../Library/tokenManager');
var _ = require('underscore')

router.use(
function (request, res, next) {
    logging.consolelog('request path',request.path,'')
    var excludedPath = ['/testzipFile1','/login','/register','/forgetPassword','/resetPassword','/checkCode',"/getMarketingDays","/updateDays","/offlineKey"]

    console.log("*****days*****", excludedPath);
    if ( _.contains(excludedPath, request.path) ){
        next();
    }else{
        TokenManager.verifyToken(request.headers.authorization, function (err, token) {
            logging.consolelog(err, token, "token authorization");
            if (!err) {
                logging.consolelog('header type>>>',err,request.headers)
                var secCheck = request.headers['x-forwarded-for']?request.headers['x-forwarded-for']:request.headers['host'];
                logging.consolelog('check ip>>>>',err,token.userData.ip === secCheck);
                if (token.userData.ip === secCheck) {
                    request.userData = token.userData;
                    next()
                } else {
                    logging.consolelog('error 1')
                    res.send(JSON.stringify(constants.STATUS_MSG.ERROR.INVALID_TOKEN));
                }

            } else {
                logging.consolelog('error 2')
                res.send(JSON.stringify(constants.STATUS_MSG.ERROR.INVALID_TOKEN));
            }
        });
    }
});

router.post('/register', [multipart()
    
    ], function (req, res) {
    superAdmin.superAdminOnboarding.superAdminRegister(req, res);
});


router.post('/login'
    
, function (req, res) {
    req.body.emailId = req.body.emailId.toLowerCase();
    superAdmin.superAdminOnboarding.superAdminLogin(req, res);
});

router.post('/forgetPassword',
 
 function (req, res) {
    superAdmin.superAdminOnboarding.superAdminForgetPassword(req, res);
});

router.post('/checkCode',
 
 function (req, res) {
    superAdmin.superAdminOnboarding.superAdminCheckOTP(req, res);
});

router.post('/resetPassword',
 
 function (req, res) {
    superAdmin.superAdminOnboarding.superAdminresetPassword(req, res);
});

router.post('/changePassword', function (req, res) {
    superAdmin.superAdminOnboarding.changePassword(req, res);
});

router.post('/getList', function (req, res) {

    superAdmin.superAdminOnboarding.getList(req, res);
});

router.post('/blockAdmin',  function (req, res) {
    superAdmin.superAdminOnboarding.blockAdmin(req, res);
});

router.post('/unblockAdmin', function (req, res) {
    superAdmin.superAdminOnboarding.unblockAdmin(req, res);
});

router.post('/editAdmin', [multipart()], function (req, res) {
    superAdmin.superAdminOnboarding.editAdmin(req, res);
})

router.post('/getDetails', function (req, res) {
    superAdmin.superAdminOnboarding.getDetails(req, res);
});

router.post('/feedbackMessage',  function (req, res) {
    superAdmin.superAdminOnboarding.feedbackMessage(req, res);
});


router.post('/curriculumUploadViaCSV', [multipart()], function (req, res) {
    superAdmin.superAdminCGCSModule.curriculumUploadViaCSV(req, res);
});


router.post("/uploadCurriculumViaManually", function (req, res) {
    superAdmin.superAdminCGCSModule.curriculumUploadViaManually(req, res);
});


router.post("/curriculumListing",function (req, res) {
    superAdmin.superAdminCGCSModule.curriculumListing(req, res);
});


router.post("/copyExistingCurriculum",function (req, res) {
    superAdmin.superAdminCGCSModule.copyExistingCurriculumByLanguage(req, res);

});

router.post("/curriculumGradeListing", function (req, res) {
    superAdmin.superAdminCGCSModule.curriculumGradeListing(req, res);
});

router.post("/addChapterManually",function (req, res) {
    superAdmin.superAdminCGCSModule.addChapterManually(req, res);
});

router.post("/chapterDetailsForSubject", function (req, res) {
    superAdmin.superAdminCGCSModule.chapterDetailsForSubject(req, res);

});

router.post("/deleteChapterOfSubject",function (req, res) {
    superAdmin.superAdminCGCSModule.deleteChapterOfSubject(req, res);
});


router.post("/deleteSubjectOfGrade", function (req, res) {
    superAdmin.superAdminCGCSModule.deleteSubjectOfGrade(req, res);
});

router.post("/deleteGradeOfCurriculum",  function (req, res) {
    superAdmin.superAdminCGCSModule.deleteGradeOfCurriculum(req, res);
});

router.post("/deleteCurriculum", function (req, res) {
    superAdmin.superAdminCGCSModule.deleteCurriculum(req, res);

});

router.post("/editGrade",  function (req, res) {
    superAdmin.superAdminCGCSModule.editGrade(req, res);
});

router.post("/editSubject", function (req, res) {
    superAdmin.superAdminCGCSModule.editSubject(req, res);
});


router.post("/editChapter",  function (req, res) {
    superAdmin.superAdminCGCSModule.editChapter(req, res);
});

router.post("/addGradeManually", expressJoi({
    body: {
        board: Joi.string(),
        language: Joi.string(),
        grade: Joi.string()
    }
}), function (req, res) {
    superAdmin.superAdminCGCSModule.addGrade(req, res);
});

router.post("/addSubjectManually",function (req, res) {
    superAdmin.superAdminCGCSModule.addSubject(req, res);
});

router.post("/dragAndDropList", function (req, res) {
    superAdmin.superAdminCGCSModule.dragAndDropList(req, res);
});

router.post("/dragAndDrop", function (req, res) {
    superAdmin.superAdminCGCSModule.dragAndDrop(req, res);
});

router.post("/exportCSV", function (req, res) {
    superAdmin.superAdminCGCSModule.exportCSV(req, res);
});

router.post("/gradeListForSorting",function (req, res) {
    superAdmin.superAdminCGCSModule.gradeListForSorting(req, res);
});

router.post("/dragAndDropGrade", function (req, res) {
    superAdmin.superAdminCGCSModule.dragAndDropGrade(req, res);
});

router.post("/pendingAgents", function (req, res) {
    superAdmin.superAdminManageAgents.pendingAgents(req, res);
});

router.post("/approvedAgents",function (req, res) {
    superAdmin.superAdminManageAgents.approvedAgents(req, res);
});

router.post("/addAgents", [multipart()], function (req, res) {
    superAdmin.superAdminManageAgents.addAgents(req, res);
});

router.post("/addGeoAgents", [multipart()], function (req, res) {
    superAdmin.superAdminManageAgents.addGeoAgents(req, res);
});

router.post("/editAgent", [multipart()],
    function (req, res) {
        superAdmin.superAdminManageAgents.editAgent(req, res);

    });


router.post("/getTopicListData",function (req, res) {
    superAdmin.superAdminManageAgents.getTopicListData(req, res);
});

router.post("/agentPackages",function (req, res) {
    superAdmin.superAdminManageAgents.agentPackages(req, res);
});

router.post("/getAgentDetail", function (req, res) {
    superAdmin.superAdminManageAgents.getAgentDetail(req, res);
});

router.post('/bookingDetails', function (req, res) {
    superAdmin.superAdminManageAgents.getBookingDetails(req, res)
});

router.post('/bookingAction', function (req, res) {
    superAdmin.superAdminManageAgents.bookingAction(req, res)
});

router.post('/getAgents', function (req, res) {
    superAdmin.superAdminManageAgents.getAgents(req, res)
});
router.post('/agentUploadViaCSV', function (req,res) {
    superAdmin.superAdminManageAgents.agentUploadViaCSV(req,res)
});

router.post('/purchasedPackageView', function (req, res) {
    superAdmin.superAdminManageAgents.purchasedPackageView(req, res)
});

router.post('/deletePurchasedPackage', function (req, res) {
    superAdmin.superAdminManageAgents.deletePackage(req, res)
});

router.post('/editPurchasedPackageView',  function (req, res) {
    superAdmin.superAdminManageAgents.editPurchasedPackageView(req, res)
});

router.post('/purchasedPackageDetail',  function (req, res) {
    superAdmin.superAdminManageAgents.purchasedPackageDetail(req, res)
});

router.post('/deletePurchasedTopic',function (req, res) {
    superAdmin.superAdminManageAgents.deletePurchasedTopic(req, res)
});

router.post('/addTopicManually',[multipart()], function (req, res) {
    superAdmin.superAdminTopicModule.addTopicManually(req, res)
});

router.post('/uploadContent', [multipart()], function (req, res) {
    superAdmin.superAdminTopicModule.uploadContent(req, res);
});

router.post('/readContentStatus', expressJoi({
    body: {
        contentId: Joi.string()
    }
}), function (req, res) {
    superAdmin.superAdminTopicModule.readContentStatus(req, res);
});

router.post('/addContentToTopic', [multipart()], function (req, res) {
    superAdmin.superAdminTopicModule.addContentToTopic(req, res)
});

router.get('/getCategorySubCategory', function (req, res) {
    superAdmin.superAdminTopicModule.getCategorySubCategory(req, res)
});

router.post('/getBoardForLanguage', function (req, res) {
    superAdmin.superAdminTopicModule.getBoardForLanguage(req, res);
});

router.post('/editTopic', [multipart()], function (req, res) {
    superAdmin.superAdminTopicModule.editTopic(req, res);
});

router.post('/topicDetails',  function (req, res) {
    superAdmin.superAdminTopicModule.topicDetails(req, res);
});

router.post('/deleteTopic',  function (req, res) {
    superAdmin.superAdminTopicModule.deleteTopic(req, res);
});

router.post('/topicLinkingWithCGCS',  function (req, res) {
    superAdmin.superAdminTopicModule.topicLinkingWithCGCS(req, res);
});

router.post('/deleteContentOfTopic', function (req, res) {
    superAdmin.superAdminTopicModule.deleteContentOfTopic(req, res);
});

router.post('/uploadAssessment', [multipart()], function (req, res) {
    superAdmin.superAdminTopicModule.uploadAssessment(req, res);
});

router.post('/addAssessment',  function (req, res) {
    superAdmin.superAdminTopicModule.addAssessment(req, res);
});

router.post('/getNewAssessments', function (req, res) {
    superAdmin.superAdminTopicModule.getNewAssessments(req, res);
});

router.post('/getCategoryContent', function (req, res) {
    superAdmin.superAdminTopicModule.getCategoryContent(req, res);
});

router.post('/getSubCategoryContent',function (req, res) {
    superAdmin.superAdminTopicModule.getSubCategoryContent(req, res);
});
/*
 * Input Parameters::Body
 subCategoryId:string,required

 */

router.post('/getContent', function (req, res) {
    superAdmin.superAdminTopicModule.getContent(req, res);
});

router.post('/getAssessment', function (req, res) {
    superAdmin.superAdminTopicModule.getAssessment(req, res);
});

router.post('/getComments',  function (req, res) {
    superAdmin.superAdminTopicModule.feedback(req, res);
});

router.post('/blockFeedback', function (req, res) {
    superAdmin.superAdminTopicModule.blockFeedback(req, res);
});

router.post('/unblockFeedback', function (req, res) {
    superAdmin.superAdminTopicModule.unblockFeedback(req, res);
});

router.post('/editCategory', [multipart()],
    function (req, res) {
        superAdmin.superAdminTopicModule.editCategory(req, res);
    });

router.post('/editSubCategory', [multipart()],
    function (req, res) {
        superAdmin.superAdminTopicModule.editSubCategory(req, res);
    });

router.post('/deleteCategory',function (req, res) {
    superAdmin.superAdminTopicModule.deleteCategory(req, res);
});

router.post('/deleteSubCategory',  function (req, res) {
    superAdmin.superAdminTopicModule.deleteSubCategory(req, res);
});

router.post('/editContent', [multipart()], function (req, res) {
    superAdmin.superAdminTopicModule.editContent(req, res);
});

router.post('/cloneTopic',  function (req, res) {
    superAdmin.superAdminTopicModule.cloneTopic(req, res);
});

router.post('/deleteLinking', function (req, res) {
    superAdmin.superAdminTopicModule.deleteLinking(req, res);
});

router.post('/deleteAssessment', function (req, res) {
    superAdmin.superAdminTopicModule.deleteAssessment(req, res);
});

// router.post('/deleteAssessment', expressJoi({
//     body: {
//         contentId: Joi.string()
//     }
// }), function (req, res) {
//     superAdmin.superAdminTopicModule.getDataBasedOnContentId(req, res);
// });

router.post('/manageContent', [multipart()], function (req, res) {
    superAdmin.superAdminTopicModule.uploadContent(req, res);
});

router.post('/dashboard',  function (req, res) {
    superAdmin.superAdminManageContent.dashboard(req, res);
});

router.post('/activeUsers',function (req, res) {
    superAdmin.superAdminManageContent.activeUsersData(req, res);
});


router.post('/agentDetail',function(req,res){
    superAdmin.superAdminManageAgents.getAgentDetail(req,res)
})

router.post('/payments', function (req, res) {
    superAdmin.superAdminManageContent.paymentReconcillation(req, res);
});

router.post('/customersUploadViaCSV', function (req, res) {
    superAdmin.superAdminManageCustomers.customersUploadViaCSV(req, res);
});

router.post('/getCustomer',  function (req, res) {
    superAdmin.superAdminManageCustomers.getCustomer(req, res);
});

router.post('/blockCustomer',  function (req, res) {
    superAdmin.superAdminManageCustomers.blockCustomer(req, res);
});

router.post('/unblockCustomer', function (req, res) {
    superAdmin.superAdminManageCustomers.unblockCustomer(req, res);
});

router.post('/topicListingForPublishing',  function (req, res) {
    superAdmin.superAdminManageCustomers.topicListingForPublishing(req, res);
});

router.post('/publishTopic', function (req, res) {
    superAdmin.superAdminManageCustomers.publishTopics(req, res);
});

router.post('/customerDetail', function (req, res) {
    superAdmin.superAdminManageCustomers.customerDetail(req, res);
});

router.post('/packageDetail', function (req, res) {
    superAdmin.superAdminManageCustomers.packageDetails(req, res);
});

router.post('/createPackage', [multipart()], function (req, res) {
    superAdmin.superAdminManagePackages.createPackage(req, res);
});

router.post('/getPackage', function (req, res) {
    superAdmin.superAdminManagePackages.getPackage(req, res);
});

router.post('/editPackage', function (req, res) {
    superAdmin.superAdminManagePackages.editPackage(req, res);
});

router.post('/deletePackage',function (req, res) {
    superAdmin.superAdminManagePackages.deletePackage(req, res);
});

router.post("/updateDays",function(req,res){
    superAdmin.superAdminOnboarding.updateDays(req,res);
});


router.get("/getMarketingDays",function(req,res){
    console.log("---> get days");
    superAdmin.superAdminOnboarding.getDays(req,res);
});

router.post('/addCustomisePackage', [multipart()], function (req, res) {
    superAdmin.superAdminManagePackages.createCustomisePackage(req, res);
});
router.post('/testzipFile1',[multipart()],function (req,res) {
    // body...
    logging.consolelog('sending req','','')
    superAdmin.superAdminManageContent.testZipFile(req,res)
});


router.post("/editUniversalCat", function(req,res){
    superAdmin.superAdminTopicModule.editUniversalCat(req,res);
});

router.post("/editUniversalSubCat", function(req,res){
    superAdmin.superAdminTopicModule.editUniversalSubCat(req,res);
});

router.post("/generateNewKey", function(req,res){
    superAdmin.superAdminOfflineManagement.addNewKey(req,res);
});


router.get("/getKeys",function(req,res){  
superAdmin.superAdminOfflineManagement.getKeys(req,res);
})



// pass macId, license  and send result

router.post("/offlineKey",function(req,res){  
 superAdmin.superAdminOfflineManagement.offlineKey(req,res);
 })
module.exports = router;