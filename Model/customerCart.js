var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Config = require('../Library/appConstants');
var userType = Config.USER_TYPE;



var CustomerCart = new Schema({
	customerId: {
		type: Schema.ObjectId,
		ref: 'Customer'
	},
	lastUpdatedTime: {
		type: Date
	},
	topicCart: {
		type: Array
	},
	packageCart: [{
		type: Schema.ObjectId,
		ref: 'Package'
	}],
	customPackageCart: [{
		type: Schema.ObjectId,
		ref: 'Package'
	}],
	content: [{
		topicId: {
			type: Number
		},
		categoryId: {
			type: Schema.ObjectId,
			ref: 'topicCategory'
		},
		contentType: {
			type: String
		}
}],
});
//CustomerCart.index({
//	'content.topicId': 1,
//	'content.categoryId': 1,
//	'content.contentId': 1
//}, {
//	unique: true
//});



module.exports = mongoose.model('CustomerCart', CustomerCart);