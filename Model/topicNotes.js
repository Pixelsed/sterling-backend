var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Config = require('../Library/appConstants');

var TopicNotes = new Schema({
	customerId: {
		type: Schema.ObjectId,
		ref: 'Customer'
	},
	topics: [{
		topicId: {
			type: String,
			unique: true
		},
		notes: {
			type: String
		}
}]
});

//TopicNotes.index({
//	'topics': 1,
//	"customerId":1
//}, {
//	unique: true
//});

module.exports = mongoose.model('TopicNotes', TopicNotes);