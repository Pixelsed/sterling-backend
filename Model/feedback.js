/**
 * Created by priya on 8/11/16.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Config = require('../Library/appConstants');
var userType = Config.USER_TYPE;


var Feedback = new Schema({
	topicId: {
		type: String
	},
	userType: {
		type: String,
		enum: [
      userType.TEACHER,
      userType.STUDENT,
      userType.AGENT
  ]
	},
	ratingStars: {
		type: Number,
		default: 0
	},
	comments: {
		type: String
	},
	isActive: {
		type: Boolean,
		default: true
	},
	customerId: {
		type: Schema.ObjectId,
		ref: 'Customer'
	},
	addedDate: {
		type: Date,
		default: Date.now()
	}
});


module.exports = mongoose.model('Feedback', Feedback);
