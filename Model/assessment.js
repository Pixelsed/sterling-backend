/**
 * Created by priya on 8/11/16.
 */


var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Config = require('../Library/appConstants');
var userType = Config.USER_TYPE;



var Assesment = new Schema({
	topicId: {
		type: String
	},
	createdDateTime:{type:Date,default:Date.now},
	contributer:{
		userType:{type:String, enum:[
			Config.USER_TYPE.SUPER_ADMIN,
			Config.USER_TYPE.AGENT,
			Config.USER_TYPE.TEACHER,
			Config.USER_TYPE.SCHOOL,
			Config.USER_TYPE.SUB_AGENT
			]},
		createdBy:{type:Schema.ObjectId}
	},
	assesment: 
	[{
		addedDate: {
			type: Date,
			default: Date.now
		},
		assesmentUrl: {
			type: String,
			default:""
		},
		isDeleted: {
			type: Boolean,
			default: false
		},
		name: {
			type: String,
			default:""
		},
		language: {
			type: String,
			default:""
		},
		price: {
			type: Number,
			default:0
		},
		isFree: {
			type: Boolean,
			default: false
		}
    }],
    isDeleted: {type: Boolean,default: false},
	name: {type: String,default:""},
	language: {type: String, default:""},
	price: {type: Number, default:0},
	isFree: {type: Boolean, default: false},
    assessmentType:{type:Number, enum:[
    	Config.DATABASE.ASSESSMENT_TYPE.MCQ,
    	Config.DATABASE.ASSESSMENT_TYPE.MTC,
    	Config.DATABASE.ASSESSMENT_TYPE.DRAG_DROP,
    	Config.DATABASE.ASSESSMENT_TYPE.SINGLE_CHOICE,
    	Config.DATABASE.ASSESSMENT_TYPE.MULTI_SELECT,
    	Config.DATABASE.ASSESSMENT_TYPE.FILL_BLANKS,
    	Config.DATABASE.ASSESSMENT_TYPE.DESCRIPTIVE
    ]},
	assessmentContent:[
		{
			questions:{type:String,default:""},
			questionImgs:{type:Array,default:[]},
			multipleChoices:{type:Array,default:[]},
			answers:{type:Array,default:[]},//answer will the correct option not index
			weightage:{type:Number,default:1}
		}
	]
});


module.exports = mongoose.model('Assesment', Assesment);