

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Config = require('../Library/appConstants');
var status = Config.DATABASE.STATUS;



var AgentCollection = new Schema({
agentId:{type:Schema.ObjectId,ref:'Customer'},
content:[{
  topicId:{type:String},
  categoryId:{type:Schema.ObjectId,ref:'topicCategory'},
  contentId:[{type:String}]
}],
amount:{type:Number,default:0},
	status:{type:String,enum: [
    status.PENDING,
    status.COMPLETED
  ],default: status.PENDING},
});


module.exports = mongoose.model('AgentCollection', AgentCollection);
