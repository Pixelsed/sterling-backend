/**
 * Created by priya on 8/11/16.
 */


var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var content = new Schema({
    title: {
        type: String
    },
    price: {
        type: Number,
        default: 0
    },
    language: {
        type: String
    },
    isVideo: {
        type: Boolean,
        default: false
    },
    typeOfVideo: {
        type: String
    },
    contentUrl: {
        type: String
    },
    icon: {
        type: String,
        default: null
    },
    description: {
        type: String
    },
    ifFree: {
        type: Boolean,
        default: false
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
    typeOfContent: {
        type: String
    }
})
var topicSubCategory = new Schema({
    name: {
        type: String,
        trim: true,
        default: null
    },
    icon: {
        type: String,
        default: null
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
    content: {
        type: [content]
    },
    isPublish:{
      type:Boolean,
      default:false
    },
    isFree:{
      type:Boolean,
      default:false
    }
});

module.exports = mongoose.model('topicSubCategory', topicSubCategory);
