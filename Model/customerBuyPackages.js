var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Config = require('../Library/appConstants');
var status = Config.DATABASE.STATUS;


var CustomerBuyPackages = new Schema({
	customerId: {
		type: Schema.ObjectId,
		ref: 'Customer'
	},
	topic: [{// if individual buy comes here
		topicId: {
			type: Number
		},
		lastSeen: {
			type: Date
		},
		price: {
			type: Number
		},
		endDate: {
			type: Date
		},
		type: {
			type: String
		},
		duration: {
			type: Number
		},
		isActive:{
			type:Boolean,
			default:true
		}
  }],
	content: [{ // if agent || superadmin purchase on the behalf of agent
		topicId: {
			type: Number
		},
		categoryId:{
			type: Schema.ObjectId,
			ref: 'topicCategory'
		},
		contentType: {
			type: String
		},
		startDate:{
			type:Date
		},
		
		endDate:{
		type:Date
	},
		isActive:{
			type:Boolean,
			default:true
		}
	
}],
	package: [{// curriculum and grade wise package comes here
		packageId: {
			type: Schema.ObjectId,
			ref: 'Package'
		},
		lastSeen: {
			type: Date
		},
		discount: {
			offerName: {
				type: String
			},
			offerAmount: {
				type: Number
			}
		},
		price: {
			type: Number
		},
		finalPrice: {
			type: Number
		},
		endDate: {
			type: Date
		},
		type: {
			type: String
		},
		duration: {
			type: Number
		},
		isActive:{
			type:Boolean,
			default:true
		}
  }],
	customPackage: [{ // packages made by super admin and customer purchase comes here
		packageId: {
			type: Schema.ObjectId,
			ref: 'Package'
		},
		lastSeen: {
			type: Date
		},
		discount: {
			offerName: {
				type: String
			},
			offerAmount: {
				type: Number
			}
		},
		price: {
			type: Number
		},
		finalPrice: {
			type: Number
		},
		endDate: {
			type: Date
		},
		type: {
			type: String
		},
		topics: {
			type: Array
		},
		icon: {
			type: String
		},
		packageName: {
			type: String
		},
		duration: {
			type: Number
		},
		isActive:{
			type:Boolean,
			default:true
		}
  }],
	paymentStatus: {
		type: String,
		enum: [
    status.PENDING,
    status.COMPLETED,
		status.CANCELLED
  ]
	},
	transactionId: {
		type: String
	},
	payments: {
		subTotalPrice: {
			type: Number
		},
		discountPrice: {
			type: Number
		},
		totalPrice: {
			type: Number
		}
	},
	topicPayments: {
		subTotalPrice: {
			type: Number
		},
		discountPrice: {
			type: Number
		},
		totalPrice: {
			type: Number
		}
	},
	packagePayments: {
		subTotalPrice: {
			type: Number
		},
		discountPrice: {
			type: Number
		},
		totalPrice: {
			type: Number
		}
	},
	customPackagePayments: {
		subTotalPrice: {
			type: Number
		},
		discountPrice: {
			type: Number
		},
		totalPrice: {
			type: Number
		}
	},
	topicOfferApplied: {
		offerName: {
			type: String
		},
		offerAmount: {
			type: Number
		},
		noOfTopics: {
			type: Number
		}
	},
	packageName: {
		type: String,default:'Package'// booking name
	},
	paymentCardId: {
		type: String
	},
	buyDate: {
		type: Date
	},
	embeddedLink:{
			type:Boolean,
			default:false
		},
	domainName:{
		type:Array
	},
	license:{
		type:Number
	},
	bookingStatus:{//not used till now
			type: String,
		enum: [
    status.PENDING,
    status.COMPLETED,
		status.PROCESSED
  ],
		default:status.PENDING
	},
	topicValidity:{type:Number},
	isDeleted:{type:Boolean,default:false}
});


module.exports = mongoose.model('CustomerBuyPackages', CustomerBuyPackages);