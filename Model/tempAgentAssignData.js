var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Config = require('../Library/appConstants');

var TempAgentAssignData = new Schema({
agentId:{ type: Schema.ObjectId,ref: 'Customer'},
category:{type:Array},
topicId:{type:String, index:true},
content:{type:Array},
startDate:{type:Date},
endDate:{type:Date},
language:{type:Array},
board:{type:Array},
grade:{type:Array},
subject:{type:Array},
chapter:{type:Array},
transactionId: {type:String}

});


module.exports = mongoose.model('TempAgentAssignData', TempAgentAssignData);