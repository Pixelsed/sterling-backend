/**
 * Created by priya on 8/23/16.
 */


var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var TopicContentStorage = new Schema({
    title: {
        type: String
    },
    price: {
        type: Number,
        default: 0
    },
    language: {
        type: String
    },
    isVideo: {
        type: Boolean,
        default: false
    },
    typeOfVideo: {
        type: String
    },
    contentUrl: {
        type: String
    },
    icon: {
        type: String,
        default: null
    },
    description: {
        type: String
    },
    isFree: {
        type: Boolean,
        default: false
    },
    jobId: {
        type: String
    },
    outputKey: {
        type: String
    },
    typeOfContent: {
        type: String
    },
    srtFile:{
        type:String
    }
});

module.exports = mongoose.model('TopicContentStorage', TopicContentStorage);
