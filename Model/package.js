


var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Config = require('../Library/appConstants');
var package = Config.DATABASE.PACKAGE_TYPE;



var Package = new Schema({
    packageType: {
      type: String,
      enum: [
          package.PACKAGE,
          package.OPEN_PACKAGE,
          package.CUSTOMISED_PACKAGE
      ]
    },
    noOfTopics: {
        type: Number
    },
    monthlyDiscount: {
        type: Number
    },
    monthlyPrice:{
        type: Number
    },
    yearlyPrice:{
        type: Number
    },

    yearlyDiscount: {
        type: Number,
        trim: true,
    },
    curriculum: {
        type: String
    },
    grade: {
        type: String
    },
    language:{type:String},
    isDeleted:{type:Boolean,default:false},
    typeOfDiscount:{type:String},
    icon:{type:String,default:null},
    packageName:{type:String,trim:true},
    topics:{type:Array}



});

Package.index({
	  'curriculum':'text',
		'grade':'text',
		'packageName':'text'},{ 
"default_language": "en",
"language_override": "en"
})

module.exports = mongoose.model('Package', Package);
