/**
 * Created by priya on 8/11/16.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var content = new Schema({
    title: {
        type: String
    },
    price: {
        type: Number,
        default: 0
    },
    language: {
        type: String
    },
    isVideo: {
        type: Boolean,
        default: false
    },
    typeOfVideo: {
        type: String
    },
    contentUrl: {
        type: String
    },
    icon: {
        type: String,
        default: null
    },
    description: {
        type: String
    },
    ifFree: {
        type: Boolean,
        default: false
    },
    isActive: {
        type: Boolean,
        default: true
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
    isPublish:{
      type:Boolean,
      default:false
    },
    typeOfContent: {
        type: String
    },
    srtFile:{type:String}
})
var topicCategory = new Schema({
    name: {
        type: String,
        trim: true,
        default: null
    },
    icon: {
        type: String,
        default: null
    },
    price: {
        type: Number,
        default: 0
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
    language: {
        type: String,
        default: null
    },
    isPublish: {
        type: Boolean,
        default: false
    },
    isFree:{
      type:Boolean,
      default:false
    },
    content: {
        type: [content]
    }
});

module.exports = mongoose.model('topicCategory', topicCategory);
