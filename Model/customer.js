/**
 * Created by priya on 8/8/16.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Config = require('../Library/appConstants');
var social = Config.DATABASE.SOCIAL_TYPE;
var userType = Config.USER_TYPE;
var deviceType = Config.DATABASE.DEVICE_TYPE;
var loginType = Config.DATABASE.LOGIN_TYPE;
var gender = Config.DATABASE.GENDER;
var keyType = Config.DATABASE.KEY_TYPE_AGENT;

var Customer = new Schema({
	firstName: {
		type: String,
		trim: true,
		default: null
	},
	lastName: {
		type: String,
		trim: true,
		default: null
	},
	profilePicUrl: {
		type: String,
		default: null
	},
	emailId: {
		type: String,
		trim: true,
		lowercase: true,
		required: true,
		unique:true
	},
	curriculum: {
		type: String,
		trim: true
	},
	grade: {
		type: Array
	},
	dob: {
		type: Date,
		default: Date.now()
	},
	addressValue: {
		type: String
	},
	gender: {
		type: String,
		enum: [
      gender.MALE,
      gender.FEMALE,
      gender.OTHERS
    ]
	},
	country: {
		type: String, default:"Others"
	},
	location: {
		'type': {
			type: String,
			enum: "Point",
			default: "Point"
		},
		'coordinates': {
			type: [Number],
			default: [0, 0]
		},
	},
	mobileNo: {
		type: String,
		trim: true,
		min: 8,
		max: 15
	},
	countryCode: {
		type: String
	},
	password: {
		type: String,
		trim: true,
		select: false
	},
	otp: {
		type: String,
		trim: true
	},
	socialId: {
		type: String,
		trim: true
	},
	socialType: {
		type: String,
		enum: [
            social.FACEBOOK,
            social.GOOGLEPLUS
        ]
	},
	phoneVerified: {
		type: Boolean,
		default: false
	},
	registrationDate: {
		type: Date,
		default: Date.now()
	},
	//todo
	//handle diff accesstoken for diff devices
	accessToken: {
        WEBAPP:{type:String,default:null},
        ANDROIDAPP:{type:String,default:null},
        IOSAPP:{type:String,default:null}
	},
	sessionTo: {
		type: Date,
		default:Date.now()
	},
	sessionFrom: {
		type: Date,
		default:Date.now()
	},
	userType: {
		type: String,
		enum: [
            userType.TEACHER,
            userType.STUDENT,
            userType.AGENT,
            userType.AGENTDISTRICT,
						userType.SUB_AGENT,
						userType.SCHOOL
        ]
	},
	deviceType: {
		type: String,
		enum: [
        deviceType.IOS,
        deviceType.ANDROID

      ]
	},
	accessRights: [{
		key: {
			type: String,
			enum: [
             keyType.DASHBOARD,
             keyType.MANAGE_TOPICS,
						 keyType.SETTINGS,
             keyType.MANAGE_CUSTOMERS,
        ]
		},	
		view: {
			type: Boolean,
			default: false
		},
		edit: {
			type: Boolean,
			default: false
		},
		add: {
			type: Boolean,
			default: false
		},
		delete: {
			type: Boolean,
			default: false
		},
		assign:{
			type: Boolean,
			default: false
		},
    peer_view:{
			type:Boolean,
			default:false
		}
		
    }],
	license: [
		{
			parentCustomerId: {
				type: String,
				
			},
			allocatedLicense: {
				type: Number,
				default: 0
			},
			usedLicense: {
				type: Number,
				default: 0
			},
			isActive: {
				type: Boolean,
				default: true
			}
      }
    ],
	accountDetails: [{
		accountNumber: {
			type: String,
			default: null
		},
		ifscCode: {
			type: String,
			default: null
		},
		bankName: {
			type: String,
			default: null
		},
		branchName: {
			type: String,
			default: null
		},
		isActive: {
			type: Boolean,
			default: false
		}
    }],
	isApproved: {
		type: Boolean,
		default: false
	},
	code: {
		type: String
	},
	block: {
		type: Boolean,
		default: false
	},
	state: {
		type: String,
		default: null
	},
	city: {
		type: String,
		default: null
	},
	zip: {
		type: String,
		default: null
	},
	geoSetup: [{
		geoLocation: {
			'type': {
				type: String,
				enum: "Polygon",
				default: "Polygon"
			},
			coordinates: {
				type: Array
			}
		},
		networkAddress: {
			type: String
		}
    }],
	domainName: {
		type: Array
	},
	loginTime:{
		type:Date, default:new Date()
	},
	timeZone:{type:String},
	level:{type:Number,default:1},
	notificationType:{type:String},
	deviceToken:{type:String}
});

//Customer.plugin(textSearch);
Customer.index({
	'firstName': 'text',
	'emailId': 'text',
	'mobileNo': 'text'
}, {
	"default_language": "en",
	"language_override": "en"
})
Customer.index({
	'location': '2dsphere',
	'geoSetup.geoLocation': '2dsphere'
})



module.exports = mongoose.model('Customer', Customer);