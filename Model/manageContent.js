
/**
 * Created by priya on 8/8/16.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Config = require('../Library/appConstants');
var CONTENT= Config.DATABASE.CONTENT_TYPE;
var userType = Config.USER_TYPE;

var Content = new Schema({
  userType: {
      type: String,
      enum: [
          userType.TEACHER,
          userType.STUDENT,
          userType.AGENT,
          userType.AGENTDISTRICT
      ]
  },
    contentType:{
      type:String,enum:[
      CONTENT.E_SAFETY,
      CONTENT.PRIVACY_POLICIES,
      CONTENT.TERMS_OF_USE,
      CONTENT.ABOUT_US,
      CONTENT.ABOUT_APP,
      CONTENT.WEB_TUTORIAL
    ]},
    contentText:{type:String},
    contentURL:{type:Array},
    isDeleted:{type:Boolean,default:false}


});


module.exports = mongoose.model('Content', Content);
