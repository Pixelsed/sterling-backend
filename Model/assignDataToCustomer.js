/**
 * Created by priya on 8/11/16.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Config = require('../Library/appConstants');


var AssignDataToCustomer = new Schema({
assignedBy:{type:Schema.ObjectId,ref:'Customer'},
assignedTo:{type:Schema.ObjectId,ref:'Customer'},
superParentCustomerId:{type:Schema.ObjectId, ref:'Customer'},
content: [{
		topicId: {
			type: Number
		},
		categoryId:{
			type: Schema.ObjectId,
			ref: 'topicCategory'
		},
		contentType: {
			type: String
		},
	startDate:{type:String},
	endDate:{type:String},
	isActive:{type:Boolean,default:true}
}],
isDeleted:{type:Boolean,default:false}
});


module.exports = mongoose.model('AssignDataToCustomer', AssignDataToCustomer);
