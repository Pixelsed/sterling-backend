var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Config = require('../Library/appConstants');
var status = Config.DATABASE.STATUS;



var ScoreBoard = new Schema({
	assessment:{type:Schema.ObjectId,ref:'Assesment'}
	customer:{type:Schema.ObjectId,ref:'Customer'},
	userType:{type:String},
	attempt:{type:String,default:1}
	timeStamp: {
		type: Date,
		default:Date.now
	},
	startTime:{type:Date},
	endTime:{type:Date},
	status:{type:String},
	correct:{type:Array,default:[]},//index of assessment content
	wrong:{type:Array,default:[]},//index of assessment wrong answers
	score:{type:Number},
});


module.exports = mongoose.model('ScoreBoard', ScoreBoard);