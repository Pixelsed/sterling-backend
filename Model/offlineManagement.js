var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Config = require('../Library/appConstants');
var status = Config.DATABASE.STATUS;



var OfflineManagement = new Schema({
    licensekey:{type:Number, unique:true},
    macId:{type:String, index:true},
    createdDate:{type:String},
    validDays:{type:Number},
    emailId:{type:String},
    phoneNumber:{type:Number}
});


module.exports = mongoose.model('OfflineManagement', OfflineManagement);