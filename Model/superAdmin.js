/**
 * Created by priya on 8/3/16.
 */


var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Config = require('../Library/appConstants');
var keyType = Config.DATABASE.KEY_TYPE_SUPER_ADMIN;


var SuperAdmin = new Schema({
	name: {
		type: String,
		trim: true,
		default: null
	},
	emailId: {
		type: String,
		trim: true,
		default: null,
		unique:true
	},
	countryCode: {
		type: String
	},
	mobileNo: {
		type: String,
		default: null
	},
	emailVerified: {
		type: Boolean,
		default: false
	},
	accessRights: [{
		key: {
			type: String,
			enum: [
             keyType.MANAGE_AGENTS,
             keyType.MANAGE_CONTENT,
						 keyType.CURRICULUM_DIRECTORY,
             keyType.MANAGE_TOPICS,
						 keyType.SETTINGS,
             keyType.DASHBOARD,
						 keyType.MANAGE_PACKAGES,
						 keyType.MANAGE_CUSTOMERS
        ]
		},
		view: {
			type: Boolean,
			default: false
		},
		edit: {
			type: Boolean,
			default: false
		},
		add: {
			type: Boolean,
			default: false
		},
		delete: {
			type: Boolean,
			default: false
		}
    }],
	address: {
		type: String,
		trim: true,
		default: null
	},
	state: {
		type: String,
		trim: true,
		default: null
	},
	country: {
		type: String,
		trim: true,
		default: null
	},
	profilePicUrl: {
		type: String,
		default: null
	},
	code: {
		type: String,
		default: null
	},
	emailUrl: {
		type: String,
		default: null
	},
	password: {
		type: String,
		default: null
	},
	accessToken: {
		type: String
	},
	block:{
		type:Boolean,
		default:false
	}
});

SuperAdmin.index({
	emailId: 1,
	mobileNo: 1
}, {
	unique: true
});
module.exports = mongoose.model('SuperAdmin', SuperAdmin);