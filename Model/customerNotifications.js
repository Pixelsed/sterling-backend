/**
 * Created by Priya on 17/05/16.
 */


var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var customerNotifications = new Schema ({
    customerId: {type:Schema.ObjectId,ref:'Customer', required: true},
    notificationMessage: {type: String, trim: true, required: true},
    notificationType: {type: String, trim: true, required: true},
    dateTime : {type: Date, default: Date.now},
    isRead : {type: Boolean , default: false}
});

module.exports = mongoose.model('customerNotifications', customerNotifications);