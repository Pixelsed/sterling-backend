var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Config = require('../Library/appConstants');
var userType = Config.USER_TYPE;

var childParentMapper = new Schema({
	childCustomerId: {
		type: Schema.ObjectId,
		ref: 'Customer'
	},
	childCustomerType: {
		type: String,
		enum: [
            userType.TEACHER,
            userType.STUDENT,
            userType.AGENT,
            userType.AGENTDISTRICT,
						userType.SUB_AGENT,
						userType.SCHOOL
        ]
	},
	parentCustomerId: {
		type: String,
		default: null
	},
	parentCustomerType: {
		type: String,
		enum: [
            userType.TEACHER,
            userType.STUDENT,
            userType.AGENT,
            userType.AGENTDISTRICT,
						userType.SUB_AGENT,
						userType.SCHOOL
        ]
	},
	superParentCustomerId: {
		type: String,
		default: null
	},
	timeStamp: {
		type: Date,
		default: Date.now()
	},
});

module.exports = mongoose.model('childParentMapper', childParentMapper);