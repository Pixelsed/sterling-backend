var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Config = require('../Library/appConstants');
var status = Config.DATABASE.STATUS;



var AnalysisData = new Schema({
	customerId:{type:Schema.ObjectId,ref:'Customer'},
	userType:{type:String},
	topicId: {
		type: Number
	},
	engangedTime: {
		type: Number
	},
	timeStamp: {
		type: Date
	}
});


module.exports = mongoose.model('AnalysisData', AnalysisData);