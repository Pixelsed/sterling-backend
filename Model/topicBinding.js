/**
 * Created by priya on 8/11/16.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var TopicBinding = new Schema({
    topicId: {
        type:String
    },
    categoryId: {
        type: Schema.ObjectId,
        ref: 'topicCategory',
        required: true
    },
    categoryLanguage: {
        type: String
    },
    subCategoryId: {
        type: Schema.ObjectId,
        ref: 'topicSubCategory'
    },
    subCategoryLanguage: {
        type: String
    }
});

module.exports = mongoose.model('TopicBinding', TopicBinding);
