/**
 * Created by priya on 8/11/16.
 */


var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Config = require('../Library/appConstants');
var userType = Config.USER_TYPE;



var FavouriteTopic = new Schema({
	customerId: {
		type: Schema.ObjectId,
		ref: 'Customer'
	},
	topicId: [{
		type: String
	}],
	topicHistory: [{
		type: String
	}]
});

module.exports = mongoose.model('FavouriteTopic', FavouriteTopic);
