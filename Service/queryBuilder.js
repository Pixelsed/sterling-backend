exports.queryBuilder = function (criteria) {
	logging.consolelog("logging","",criteria);
	var whereCondition = " ";
	for (var i in criteria) {
		if (typeof criteria[i] == "string") {
			whereCondition += " and " + i + " = '" + criteria[i].replace("'", "''") + "'";

			logging.consolelog("logging","",whereCondition);
		} else if (typeof criteria[i] == "boolean") {
			whereCondition += " and " + i + " = " + criteria[i] + " ";
		} else if (typeof criteria[i] == "number") {
			whereCondition += " and " + i + " = " + criteria[i] + " ";
		} else if (typeof criteria[i] == "object") {
			if (Object.keys(criteria[i])[0] == "$in") {
				if(Array.isArray(criteria[i][Object.keys(criteria[i])[0]]) && criteria[i][Object.keys(criteria[i])[0]].length){
					whereCondition += " and " + i + " in (";
					criteria[i]["$in"].forEach(function (queryData) {
						whereCondition += "'" + queryData.toString().replace("'", "''") + "',";
					});
					whereCondition = whereCondition.substring(0, whereCondition.length - 1) + ") "
				} else {
					whereCondition += " and " + i + " in (";
					whereCondition += "'" + criteria[i]["$in"].toString().replace("'", "''") + "',";
					whereCondition = whereCondition.substring(0, whereCondition.length - 1) + ") " ;
				}
			} else if (Object.keys(criteria)[0] == "$set" && typeof criteria["$set"] == "object") {
				for(var key in criteria["$set"]){
					logging.consolelog("logging","",key);
					if (typeof criteria["$set"][key] == "boolean") {
						whereCondition += key + " = " + criteria["$set"][key] + ", ";
					} else {
						whereCondition += key + " = '" + criteria["$set"][key].toString().replace("'", "''") + "', ";
					}
				}
				whereCondition = whereCondition.substring(0, whereCondition.length - 2)
			}
		}
	}
	return whereCondition;
}
// exports.mysqlQueryBuilder = function (criteria,alias) {
// 	logging.consolelog('criteria>','',criteria,alias)
// 	var whereCondition = " ";
// 	for (var i in criteria) {
// 		if (typeof criteria[i] == "string") {
// 			whereCondition += " and " + i + " = '" + criteria[i].replace("'", "''") + "'";

// 			logging.consolelog("logging","",whereCondition);
// 		} else if (typeof criteria[i] == "boolean") {
// 			whereCondition += " and " + i + " = " + criteria[i] + " ";
// 		} else if (typeof criteria[i] == "number") {
// 			whereCondition += " and " + i + " = " + criteria[i] + " ";
// 		} else if (typeof criteria[i] == "object") {
// 			if (Array.isArray(criteria[i]) && criteria[i].length && !alias) {
// 				whereCondition += " and " + i + " in (";
// 				criteria[i]["$in"].forEach(function (queryData) {
// 					whereCondition += "'" + queryData + "',";
// 				});
// 				whereCondition = whereCondition.substring(0, whereCondition.length - 1) + ") "
// 			} else if(alias && Array.isArray(criteria[i]) && criteria[i].length){
// 				whereCondition += " and " + alias.i + " in (";
// 				criteria[i]["$in"].forEach(function (queryData) {
// 					whereCondition += "'" + queryData + "',";
// 				});
// 				whereCondition = whereCondition.substring(0, whereCondition.length - 1) + ") "
// 			}
// 		}
// 	}
// 	return whereCondition;
// }