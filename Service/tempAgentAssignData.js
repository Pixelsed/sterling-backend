/**
 * Created by priya on 8/3/16.
 */
'use strict';

var Models = require('../Model/tempAgentAssignData');


var updateSuperAdmin = function (criteria, dataToSet, options, callback) {
    options.lean = true;
    options.new = true;
    Models.findOneAndUpdate(criteria, dataToSet, options, callback);
};
//Insert User in DB
var createSuperAdmin = function (objToSave, callback) {
    new Models(objToSave).save(callback)
};
//Delete User in DB
var deleteSuperAdmin = function (criteria, callback) {
    Models.findOneAndRemove(criteria, callback);
};

//Get Users from DB
var getSuperAdmin = function (criteria, projection, options, callback) {
    options.lean = true;
    Models.find(criteria, projection, options, callback);
};

var getSuperAdminCount = function(criteria, callback) {
    logging.consolelog("----count" ,"", criteria)
    Models.count(criteria, callback);
};


var getSuperAdminAggregateCount = function(match, group, sort, project, matchMonthQuery, callback) {
    Models.aggregate([
        { $match : match},
        { $sort : sort },
        { $project : project},
        { $group : group },
        { $match : matchMonthQuery}
    ],callback);
};
var update = function (criteria, dataToSet, options, callback) {
    options.lean = true;
    options.new = true;
    Models.update(criteria, dataToSet, options, callback);
};
var getAllSuperAdmins = function (criteria, projection, sortOptions,setOptions,callback)
{
    logging.consolelog("dao........",criteria, projection)
    Models.find(criteria).select(projection).sort(sortOptions).setOptions(setOptions).exec(function(err, result){
        callback(err, result);
    });
};


var insertData = function (objToSave, callback) {
    logging.consolelog('insert_________',"",objToSave)
    Models.collection.insert(objToSave,callback)

};
module.exports = {
    updateSuperAdmin: updateSuperAdmin,
    createSuperAdmin: createSuperAdmin,
    deleteSuperAdmin: deleteSuperAdmin,
    getSuperAdmin:getSuperAdmin,
    getSuperAdminCount:getSuperAdminCount,
    getAllSuperAdmins:getAllSuperAdmins,
    update:update,
    getSuperAdminAggregateCount:getSuperAdminAggregateCount,
    insertData:insertData
};
