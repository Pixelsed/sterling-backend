/**
 * Created by Priya.
 */

'use strict';

var Models = require('../Model/customerNotifications');

//Get Users from DB
var getNotifications = function (criteria, projection, options, callback) {
    options.lean = true;
    Models.find(criteria, projection, options, callback);
};

//Insert User in DB
var createNotifications = function (objToSave, callback) {
    new Models(objToSave).save(callback)
};

//Update User in DB
var updateNotifications = function (criteria, dataToSet, options, callback) {
    options.lean = true;
    options.new = true;
    Models.update(criteria, dataToSet, options, callback);
};

module.exports = {
    getNotifications: getNotifications,
    createNotifications: createNotifications,
    updateNotifications: updateNotifications
};