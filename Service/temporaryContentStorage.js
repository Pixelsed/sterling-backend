/**
 * Created by priya on 8/23/16.
 */
 'use strict';

 var Models = require('../Model/temporaryContentStorage');


 var updateTemporaryContentStorage= function (criteria, dataToSet, options, callback) {
     options.lean = true;
     options.new = true;
     Models.findOneAndUpdate(criteria, dataToSet, options, callback);
 };
 //Insert User in DB
 var createTemporaryContentStorage = function (objToSave, callback) {
     new Models(objToSave).save(callback)
 };
 //Delete User in DB
 var deleteTemporaryContentStorage = function (criteria, callback) {
     Models.findOneAndRemove(criteria, callback);
 };

 //Get Users from DB
 var getTemporaryContentStorage = function (criteria, projection, options, callback) {
     options.lean = true;
     Models.find(criteria, projection, options, callback);
 };

 var getTemporaryContentStorageCount = function(match, group, sort, project, matchMonthQuery, callback) {
     Models.aggregate([
         { $match : match},
         { $sort : sort },
         { $project : project},
         { $group : group },
         { $match : matchMonthQuery}
     ],callback);
 };
 var update = function (criteria, dataToSet, options, callback) {
     options.lean = true;
     options.new = true;
     Models.update(criteria, dataToSet, options, callback);
 };
 var getAllTemporaryContentStorage = function (criteria, projection, sortOptions,setOptions,callback)
 {
     logging.consolelog("dao........",criteria, projection)
     Models.find(criteria).select(projection).sort(sortOptions).setOptions(setOptions).exec(function(err, result){
         callback(err, result);
     });
 };
 module.exports = {
     updateTemporaryContentStorage: updateTemporaryContentStorage,
     createTemporaryContentStorage: createTemporaryContentStorage,
     deleteTemporaryContentStorage: deleteTemporaryContentStorage,
     getTemporaryContentStorage:getTemporaryContentStorage,
     getTemporaryContentStorageCount:getTemporaryContentStorageCount,
     getAllTemporaryContentStorage:getAllTemporaryContentStorage,
     update:update
 };
