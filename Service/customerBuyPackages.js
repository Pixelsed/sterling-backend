'use strict';

var Models = require('../Model/customerBuyPackages');

var createTransaction = function (objToSave, callback) {
	new Models(objToSave).save(callback);
};

var deleteTransaction = function (criteria, callback) {
    Models.findOneAndRemove(criteria, callback);
};

var updateTransaction = function (criteria, dataToSet, options, callback) {
    options.lean = true;
    options.new = true;
    Models.update(criteria, dataToSet, options, callback);
};

var getTransaction= function (criteria, projection, options, callback) {
		options.lean = true;
		Models.find(criteria, projection, options, callback);
};

var getPackages = function (criteria, projection, populate, sortOptions, callback) {
	logging.consolelog(criteria,populate,sortOptions)
	 Models.find(criteria).select(projection).populate(populate).sort(sortOptions).setOptions({lean:true}).exec(function(err, result){
        callback(err, result);
    });
};

var insertPackages = function (objToSave, callback) {
    logging.consolelog('insert_________',"",objToSave)
    Models.collection.insert(objToSave,callback)

};


module.exports = {
	createTransaction: createTransaction,
	deleteTransaction: deleteTransaction,
	updateTransaction: updateTransaction,
	getTransaction:getTransaction,
	getPackages: getPackages,
	insertPackages:insertPackages
};
