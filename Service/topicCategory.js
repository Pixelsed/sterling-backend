/**
 * Created by priya on 8/17/16.
 */



'use strict';

var Models = require('../Model/topicCategory');


var updateTopicCategory = function (criteria, dataToSet, options, callback) {
    options.lean = true;
    options.new = true;
    Models.findOneAndUpdate(criteria, dataToSet, options, callback);
};
//Insert User in DB
var createTopicCategory  = function (objToSave, callback) {
    new Models(objToSave).save(callback)
};
//Delete User in DB
var deleteTopicCategory = function (criteria, callback) {
    Models.findOneAndRemove(criteria, callback);
};

//Get Users from DB
var getTopicCategory = function (criteria, projection, options, callback) {
    options.lean = true;
    Models.find(criteria, projection, options, callback);
};

var getTopicCategoryCount = function(match, group, sort, project, matchMonthQuery, callback) {
    Models.aggregate([
        { $match : match},
        { $sort : sort },
        { $project : project},
        { $group : group },
        { $match : matchMonthQuery}
    ],callback);
};
var update = function (criteria, dataToSet, options, callback) {
    options.lean = true;
    options.new = true;
    Models.update(criteria, dataToSet, options, callback);
};
var getAllTopicCategory = function (criteria, projection, sortOptions,setOptions,callback)
{
    logging.consolelog("dao........",criteria, projection)
    Models.find(criteria).select(projection).sort(sortOptions).setOptions(setOptions).exec(function(err, result){
        callback(err, result);
    });
};

var getUniqueTopicCategory = function (criteria,distinct, callback) {

    Models.find(criteria).distinct(distinct,function(err,result){
        callback(err,result);
    });
};

module.exports = {
    updateTopicCategory: updateTopicCategory,
    createTopicCategory: createTopicCategory,
    deleteTopicCategory: deleteTopicCategory,
    getTopicCategory:getTopicCategory,
    getTopicCategoryCount:getTopicCategoryCount,
    getAllTopicCategory:getAllTopicCategory,
    update:update,
    getUniqueTopicCategory:getUniqueTopicCategory
};
