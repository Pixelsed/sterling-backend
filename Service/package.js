/**
 * Created by priya on 8/3/16.
 */
'use strict';

var Models = require('../Model/package');
var mysqlConfig = require('../Library/mysqlConfig');
var queryBuilder = require('./queryBuilder').queryBuilder;


var updatePackage = function (criteria, dataToSet, options, callback) {
    options.lean = true;
    options.new = true;
    Models.findOneAndUpdate(criteria, dataToSet, options, callback);
};
//Insert User in DB
var createPackage = function (objToSave, callback) {
    new Models(objToSave).save(callback)
};
//Delete User in DB
var deletePackage = function (criteria, callback) {
    Models.findOneAndRemove(criteria, callback);
};

//Get Users from DB
var getPackage = function (criteria, projection, options, callback) {
    options.lean = true;
    Models.find(criteria, projection, options, callback);
};

//Get Users from DB
var getPackageDetails = function (criteria, callback) {
    logging.consolelog("logging","",JSON.stringify(criteria));
	var whereCondition = "";
	if (criteria.length) {
		whereCondition = " and (";
		criteria.forEach(function (obj) {
			whereCondition += "(";
			for (var i in obj) {
				whereCondition += i + " = '" + obj[i] + "' and ";
			}
			whereCondition = whereCondition.substring(0, whereCondition.length - 4) + ") or ";
		})
		whereCondition = whereCondition.substring(0, whereCondition.length - 3) + ")";
	}
	var query = 'select sum(price) as price, group_concat(topicId) as topics, count(distinct topicId) as topicCount, board, curriculum.language, grade, count(distinct subject) as subjectCount, count(distinct chapter) as chapterCount from (select board, subject, language, grade, chapter, topicId, isRemoved from curriculum left join topicCurriculumMapping on topicCurriculumMapping.curriculumId = curriculum._id) curriculum inner join topic on topic._id = curriculum.topicId where isPublish = true and isDeleted = false '  + whereCondition + ' group by board, curriculum.language, grade';
	logging.consolelog("logging","",query);
	mysqlConfig.getMysqlInstance().query(query, function (err, rows) {
		logging.consolelog("getCartInfoAndPrice data","","");
		logging.consolelog("logging","",JSON.stringify(rows));
		if (err) {
			logging.consolelog("logging","",err);
			callback(err, rows);
		} else {
			callback(null, rows);
		}
		/*options.lean = true;
		Models.find(criteria, projection, options, callback);*/
	});
};

var getPackageCount = function(criteria, callback) {
    logging.consolelog("----count" ,"", criteria)
    Models.count(criteria, callback);
};


var getPackageAggregateCount = function(match, group, sort, project, matchMonthQuery, callback) {
    Models.aggregate([
        { $match : match},
        { $sort : sort },
        { $project : project},
        { $group : group },
        { $match : matchMonthQuery}
    ],callback);
};
var update = function (criteria, dataToSet, options, callback) {
    options.lean = true;
    options.new = true;
    Models.update(criteria, dataToSet, options, callback);
};
var getAllPackage = function (criteria, projection, sortOptions,setOptions,callback)
{
    logging.consolelog("dao........",criteria, projection)
    Models.find(criteria).select(projection).sort(sortOptions).setOptions(setOptions).exec(function(err, result){
        callback(err, result);
    });
};

var getAllGeneratedCodes = function (callback) {
    var criteria = {
        otp : {$ne : null}
    };
    var projection = {
        otp : 1
    };
    var options = {
        lean : true
    };
    Models.find(criteria,projection,options, function (err, dataAry) {
        if (err){
            callback(err)
        }else {
            var generatedCodes = [];
            if (dataAry && dataAry.length > 0){
                dataAry.forEach(function (obj) {
                    generatedCodes.push(obj.otp.toString())
                });
            }
            callback(null,generatedCodes);
        }
    })
};

module.exports = {
    updatePackage: updatePackage,
    createPackage: createPackage,
    deletePackage: deletePackage,
    getPackage:getPackage,
    getAllPackage:getAllPackage,
    getPackageCount:getPackageCount,
	getPackageDetails: getPackageDetails
};
