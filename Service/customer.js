/**
 * Created by priya on 8/3/16.
 */
'use strict';

var Models = require('../Model/customer');


var updateCustomer = function (criteria, dataToSet, options, callback) {
	options.lean = true;
	options.new = true;
	Models.findOneAndUpdate(criteria, dataToSet, options, callback);
};
//Insert User in DB
var createCustomer = function (objToSave, callback) {
	new Models(objToSave).save(callback)
};
//Delete User in DB
var deleteCustomer = function (criteria, callback) {
	Models.findOneAndRemove(criteria, callback);
};

//Get Users from DB
var getCustomer = function (criteria, projection, options, callback) {
	options.lean = true;
	Models.find(criteria, projection, options, callback);
};

var getCustomerCount = function (criteria, callback) {
	logging.consolelog("----count","", criteria)
	Models.count(criteria, callback);
};

var customersCount  = function (query,project,options,callback) {
	// body...
	Models.aggregate([{$group:{_id:"$userType",count:{$sum:1}}}],function(err,result){
		if(err){
			callback(err,null)
		}else{
			callback(err,result)
		}
	})
}
var getCustomerAggregateCount = function (match, group, sort, project, matchMonthQuery, callback) {
	Models.aggregate([
		{
			$match: match
		},
		{
			$sort: sort
		},
		{
			$project: project
		},
		{
			$group: group
		},
		{
			$match: matchMonthQuery
		}
    ], callback);
};


var countryAggregateData = function (group1, group2, callback) {
logging.consolelog(group1,group2,"print data");
	Models.aggregate([
		{"$group": {
					_id: {
						"country": "$country",
						"userType": "$userType"
					},
					"userCount": {
						"$sum": 1
					}
				}}, {"$group": {
					"_id": "$_id.country",
					"userTypes": {
						"$push": {
							"userType": "$_id.userType",
							"count": "$userCount"
						},
					},
					"count": {
						"$sum": "$userCount"
					}
				}}, {
			"$limit": 100
		}
	], callback);
}
var update = function (criteria, dataToSet, options, callback) {
	options.lean = true;
	options.new = true;
	Models.update(criteria, dataToSet, options, callback);
};
var getAllCustomer = function (criteria, projection, sortOptions, setOptions, callback) {
	logging.consolelog("dao........", criteria, projection)
	Models.find(criteria).select(projection).sort(sortOptions).setOptions(setOptions).exec(function (err, result) {
		callback(err, result);
	});
};

var getAllGeneratedCodes = function (callback) {
	var criteria = {
		otp: {
			$ne: null
		}
	};
	var projection = {
		otp: 1
	};
	var options = {
		lean: true
	};
	Models.find(criteria, projection, options, function (err, dataAry) {
		if (err) {
			callback(err)
		} else {
			var generatedCodes = [];
			if (dataAry && dataAry.length > 0) {
				dataAry.forEach(function (obj) {
					generatedCodes.push(obj.otp.toString())
				});
			}
			callback(null, generatedCodes);
		}
	})
};
var getDistinctCustomer = function (criteria, distinct, callback) {

	Models.find(criteria).distinct(distinct, function (err, result) {
		callback(err, result);
	});
};
module.exports = {
	updateCustomer: updateCustomer,
	createCustomer: createCustomer,
	deleteCustomer: deleteCustomer,
	getCustomer: getCustomer,
	getCustomerCount: getCustomerCount,
	getAllCustomer: getAllCustomer,
	update: update,
	getCustomerAggregateCount: getCustomerAggregateCount,
	getAllGeneratedCodes: getAllGeneratedCodes,
	getDistinctCustomer: getDistinctCustomer,
	countryAggregateData:countryAggregateData,
	customersCount:customersCount
};