/**
 * Created by priya on 8/8/16.
 */
'use strict';
//
//var Models = require('../Model/topicCurriculumMapping');
var mysqlConfig = require('../Library/mysqlConfig');
var queryBuilder = require('./queryBuilder').queryBuilder;


var updateCurriculumMapping = function (criteria, dataToSet, options, callback) {
	options.lean = true;
	options.new = true;
	Models.update(criteria, dataToSet, options, callback);
};
//Insert User in DB
var createCurriculumMapping = function (objToSave, callback) {
	logging.consolelog("logging","",JSON.stringify(objToSave));
	mysqlConfig.getMysqlInstance().query('select COALESCE(MAX(sequenceOrder) + 1,1) as seq from topicCurriculumMapping where curriculumId in (select _id from curriculum where board in (select board from curriculum where _id = ?))', objToSave.curriculumId, function (seqErr, seqRows) {
		if(seqRows && seqRows.length && seqRows[0].seq){
			objToSave.sequenceOrder = seqRows[0].seq;
		} else {
			objToSave.sequenceOrder = 1;
		}
		mysqlConfig.getMysqlInstance().query('insert into topicCurriculumMapping set ?', objToSave, function (err, rows) {
			if (err) {
				logging.consolelog("logging","",err);
				callback(err);
			} else {
				var data = {
					_id: rows.insertId
				};
				callback(null, data);
			}
		});
	});
	//new Models(objToSave).save(callback)
};

var dragAndDropSequence = function (objToSave, callback) {
	logging.consolelog("logging","",JSON.stringify(objToSave));
	mysqlConfig.getMysqlInstance().query('update topicCurriculumMapping set sequenceOrder = sequenceOrder + 1 where sequenceOrder > ? and curriculumId in (select _id from curriculum where board in (select board from curriculum where _id = ?))', [parseInt(objToSave.PrevElementSeqNo), objToSave.curriculumId], function (seqErr, seqRows) {
		mysqlConfig.getMysqlInstance().query('update topicCurriculumMapping set sequenceOrder = ? where curriculumId = ? and topicId = ?', [parseInt(objToSave.PrevElementSeqNo) +1, objToSave.curriculumId, objToSave.topicId], function (err, rows) {
			if (err) {
				logging.consolelog("logging","",err);
				callback(err);
			} else {
				callback(null);
			}
		});
	});
	//new Models(objToSave).save(callback)
};

var updateTopicCurriculumMapping = function (criteria, objToSave, callback) {
	var whereCondition = queryBuilder(criteria);
	logging.consolelog('update topicCurriculumMapping set ? where 1 ' + whereCondition + " limit 1 ","", objToSave);
	mysqlConfig.getMysqlInstance().query('update topicCurriculumMapping set ? where 1 ' + whereCondition + " limit 1 ", objToSave, function (err, rows) {
		if (err) {
				logging.consolelog("err",err,"");
				callback(err);
			} else {
				callback(null);
			}
	});
};


var getTopicAndCurriculumDetails = function (criteria, callback) {
	logging.consolelog("logging","",JSON.stringify(criteria));
	var whereCondition = queryBuilder(criteria);
	var query = 'select *, topic.language as topicLanguage, curriculum.language as curriculumLanguage from topicCurriculumMapping left join topic on topic._id = topicCurriculumMapping.topicId left join curriculum on topicCurriculumMapping.curriculumId = curriculum._id where topicCurriculumMapping.removed=0' + whereCondition;
	logging.consolelog("logging","",query);
	mysqlConfig.getMysqlInstance().query(query, function (err, rows) {
		if (err) {
			logging.consolelog("logging",err,"");
			callback(err);
		} else {
			callback(null, rows);
		}
	});
};


var getTopicIdsByCurriculum = function (criteria, callback) {
	logging.consolelog("logging","",JSON.stringify(criteria));
	var whereCondition = "";
	if (criteria.length) {
		whereCondition = " where ";
		criteria.forEach(function (obj) {
			whereCondition += "(";
			for (var i in obj) {
				whereCondition += i + " = '" + obj[i] + "' and ";
			}
			whereCondition = whereCondition.substring(0, whereCondition.length - 4) + ") or ";
		})
		whereCondition = whereCondition.substring(0, whereCondition.length - 3);
	}
	var query = 'select topicId from topicCurriculumMapping left join topic on topic._id = topicCurriculumMapping.topicId left join curriculum on topicCurriculumMapping.curriculumId = curriculum._id ' + whereCondition;
	logging.consolelog("logging","",query);
	mysqlConfig.getMysqlInstance().query(query, function (err, rows) {
		if (err) {
			logging.consolelog("err",err,"");
			callback(err);
		} else {
			callback(null, rows);
		}
	});
};

//Delete User in DB
var deleteCurriculumMapping = function (criteria, callback) {
	Models.findOneAndRemove(criteria, callback);
};

//Get Users from DB
var getCurriculumMapping = function (criteria, projection, options, callback) {
	options.lean = true;
	Models.find(criteria, projection, options, callback);
};


var getCurriculumMappingCount = function (criteria, callback) {
	logging.consolelog("----count", "",criteria)
	Models.count(criteria, callback);
};


var getCurriculumMappingAggregateCount = function (match, group, sort, project, matchMonthQuery, callback) {
	Models.aggregate([{
		$match: match
    }, {
		$sort: sort
    }, {
		$project: project
    }, {
		$group: group
    }, {
		$match: matchMonthQuery
    }], callback);
};
var update = function (criteria, dataToSet, options, callback) {
	options.lean = true;
	options.new = true;
	Models.update(criteria, dataToSet, options, callback);
};
var getAllCurriculumMapping = function (criteria, projection, sortOptions, setOptions, callback) {
	logging.consolelog("dao........", criteria, projection)
	Models.find(criteria).select(projection).sort(sortOptions).setOptions(setOptions).exec(function (err, result) {
		callback(err, result);
	});
};

var getUniqueCurriculumMapping = function (criteria, distinct, callback) {

	Models.find(criteria).distinct(distinct, function (err, result) {
		callback(err, result);
	});
};

var addCurriculumMappinginsert = function (objToSave, callback) {
	logging.consolelog('insert_________',"", objToSave)
	Models.collection.insert(objToSave, callback)

};

var curriculumMappingPopulate = function (criteria, projection, populate, sortOptions, setOptions, callback) {
	logging.consolelog(criteria, projection, populate)
	Models.find(criteria).select(projection).populate(populate).sort(sortOptions).setOptions(setOptions).exec(function (err, result) {
		callback(err, result);
	});
};

var getDataDeepPopulate = function (query, projectionQuery, options, populateModel, nestedModel, callback) {

	Models.find(query, projectionQuery, options).populate(populateModel)
		.exec(function (err, docs) {

			if (err) {
				return callback(err, docs);
			}
			model.populate(docs, nestedModel,
				function (err, populatedDocs) {
					if (err) return callback(err);
					callback(null, populatedDocs); // This object should now be populated accordingly.
				});
		});
};


var aggregateLookUp = function (lookup, project, callback) {
	logging.consolelog("values of", lookup, project);
	Models.aggregate([{
			$lookup: lookup
        }
        //
        // {
        //     $project: project
        // }
    ], callback);
};


module.exports = {
	updateCurriculumMapping: updateCurriculumMapping,
	createCurriculumMapping: createCurriculumMapping,
	deleteCurriculumMapping: deleteCurriculumMapping,
	getCurriculumMapping: getCurriculumMapping,
	getCurriculumMappingCount: getCurriculumMappingCount,
	getAllCurriculumMapping: getAllCurriculumMapping,
	update: update,
	getCurriculumMappingAggregateCount: getCurriculumMappingAggregateCount,
	getUniqueCurriculumMapping: getUniqueCurriculumMapping,
	addCurriculumMappinginsert: addCurriculumMappinginsert,
	curriculumMappingPopulate: curriculumMappingPopulate,
	aggregateLookUp: aggregateLookUp,
	getTopicAndCurriculumDetails: getTopicAndCurriculumDetails,
	getTopicIdsByCurriculum: getTopicIdsByCurriculum,
	dragAndDropSequence: dragAndDropSequence,
	updateTopicCurriculumMapping: updateTopicCurriculumMapping
};