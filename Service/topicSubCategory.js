/**
 * Created by priya on 8/17/16.
 */

'use strict';

var Models = require('../Model/topicSubCategory');


var updateTopicSubCategory= function (criteria, dataToSet, options, callback) {
    options.lean = true;
    options.new = true;
    Models.findOneAndUpdate(criteria, dataToSet, options, callback);
};
//Insert User in DB
var createTopicSubCategory = function (objToSave, callback) {
    new Models(objToSave).save(callback)
};
//Delete User in DB
var deleteTopicSubCategory = function (criteria, callback) {
    Models.findOneAndRemove(criteria, callback);
};

//Get Users from DB
var getTopicSubCategory = function (criteria, projection, options, callback) {
    options.lean = true;
    Models.find(criteria, projection, options, callback);
};

var getTopicSubCategoryCount = function(match, group, sort, project, matchMonthQuery, callback) {
    Models.aggregate([
        { $match : match},
        { $sort : sort },
        { $project : project},
        { $group : group },
        { $match : matchMonthQuery}
    ],callback);
};
var update = function (criteria, dataToSet, options, callback) {
    options.lean = true;
    options.new = true;
    Models.update(criteria, dataToSet, options, callback);
};
var getAllTopicSubCategory = function (criteria, projection, sortOptions,setOptions,callback)
{
    logging.consolelog("dao........",criteria, projection)
    Models.find(criteria).select(projection).sort(sortOptions).setOptions(setOptions).exec(function(err, result){
        callback(err, result);
    });
};

var getUniqueTopicSubCategory = function (criteria,distinct, callback) {

    Models.find(criteria).distinct(distinct,function(err,result){
        callback(err,result);
    });
};
module.exports = {
    updateTopicSubCategory: updateTopicSubCategory,
    createTopicSubCategory: createTopicSubCategory,
    deleteTopicSubCategory: deleteTopicSubCategory,
    getTopicSubCategory:getTopicSubCategory,
    getTopicSubCategoryCount:getTopicSubCategoryCount,
    getAllTopicSubCategory:getAllTopicSubCategory,
    update:update,
     getUniqueTopicSubCategory: getUniqueTopicSubCategory
};
