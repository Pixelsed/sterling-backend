/**
 * Created by priya on 8/3/16.
 */
'use strict';

var Models = require('../Model/marketingDays');


var updateDays = function (criteria, dataToSet, options, callback) {
	options.lean = true;
	options.new = true;
	Models.findOneAndUpdate(criteria, dataToSet, options, callback);
};
//Insert User in DB


//Get Users from DB
var getDays = function (criteria, projection, options, callback) {
	options.lean = true;
	Models.find(criteria, projection, options, callback);
};

module.exports = {
	updateDays: updateDays,
	getDays: getDays
};