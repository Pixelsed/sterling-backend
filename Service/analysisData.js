'use strict';

var Models = require('../Model/analysisData');

var createAnalysisData = function (objToSave, callback) {
	new Models(objToSave).save(callback)
};


var getAnalysisData = function (query, projection, options, callback) {
	options.lean = true;
	Models.find(query, projection, options, callback);
}

var groupByUserEngagedTime = function (query, projection, options, callback) {
	// body...
	Models.aggregate([{$group:{_id:"$userType",engagedTime:{$sum:"$engangedTime"},count:{$sum:1}}}],function(err,result){
		if(err){
			callback(err,null)
		}else{
			callback(err,result)
		}
	})
}


module.exports = {

	createAnalysisData: createAnalysisData,
	getAnalysisData: getAnalysisData,
	groupByUserEngagedTime:groupByUserEngagedTime
};