

'use strict';

var Models = require('../Model/customerCart');


var createCart = function (objToSave, callback) {
    new Models(objToSave).save(callback)
};
//Delete User in DB
var deletePackage = function (criteria, callback) {
    Models.findOneAndRemove(criteria, callback);
};

var getCart=  function (criteria, projection, options, callback) {
	options.lean = true;
	Models.find(criteria, projection, options, callback);
};


var updateCart= function (criteria, dataToSet, options, callback) {
    options.lean = true;
    options.new = true;
    Models.update(criteria, dataToSet, options, callback);
};

var cartPopulate = function (criteria, projection, populate, sortOptions, setOptions, callback) {
	Models.find(criteria).select(projection).populate(populate).sort(sortOptions).setOptions(setOptions).exec(function (err, result) {
		callback(err, result);
	});
};
module.exports={
  getCart:getCart,
  createCart:createCart,
  deletePackage:deletePackage,
  updateCart:updateCart,
  cartPopulate:cartPopulate


}
