'use strict';

var Models = require('../Model/favouriteTopic');


var createFavourite = function (objToSave, callback) {
    new Models(objToSave).save(callback)
};

var getFavourite =  function (criteria, projection, options, callback) {
	options.lean = true;
	Models.find(criteria, projection, options, callback);
};

var updateFavourite= function (query, dataToSet, options, callback) {
    options.lean = true;
	Models.findOneAndUpdate(query, dataToSet, options, callback);
};

module.exports={
	getFavourite: getFavourite,
  createFavourite:createFavourite,
  updateFavourite:updateFavourite
}
