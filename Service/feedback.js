/**
 * Created by rishi on 9/16/16.
 */

'use strict';

var Models = require('../Model/feedback');
var mysqlConfig = require('../Library/mysqlConfig');
var queryBuilder = require('./queryBuilder').queryBuilder;

var getFeedback = function (criteria, projection, sortOptions, populate, callback) {
    Models.find(criteria).select(projection).populate(populate).sort(sortOptions).exec(function (err, result) {
        callback(err, result);
    });
};

var createFeedback = function (objToSave, callback) {
    new Models(objToSave).save(callback);
};

var getAvgRating = function (criteria, projection, sortOptions, populate, callback) {
    Models.aggregate([
        {
            $match: {
                topicId: criteria.topicId,
                isActive: true
            }
        },
        {
            $group: {
                _id: '$topicId',
                average: {
                    $avg: '$ratingStars'
                }
            }
        }],function (err,result) {
            logging.consolelog('logging',err,result)
        if (result.length && result[0]["_id"] && result[0]["average"]) {
            logging.consolelog("logging", "", result[0]["average"]);
            result[0]["average"] = Math.round(parseFloat(result[0]["average"]) * 10) / 10;
            logging.consolelog("logging", "", result[0]["average"]);
            mysqlConfig.getMysqlInstance().query('update topic set averageRating = ? where _id = ? ', [result[0]["average"], result[0]["_id"]], function (err, rows) {
                if (err) {
                    logging.consolelog("logging", "", err);
                } else {
                    callback(err, result);
                }
            });
        } else {
            callback(err, []);
        }
    })
};


var updateFeedback = function (criteria, dataToSet, options, callback) {
    options.lean = true;
    options.new = true;
    Models.findOneAndUpdate(criteria, dataToSet, options, callback);
};
module.exports = {
    getFeedback: getFeedback,
    createFeedback: createFeedback,
    updateFeedback: updateFeedback,
    getAvgRating:getAvgRating
};