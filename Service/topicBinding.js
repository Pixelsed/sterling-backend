/**
 * Created by priya on 8/17/16.
 */

'use strict';

var Models = require('../Model/topicBinding');


var updateTopicBinding = function (criteria, dataToSet, options, callback) {
    options.lean = true;
    options.new = true;
    Models.findOneAndUpdate(criteria, dataToSet, options, callback);
};
//Insert User in DB
var createTopicBinding  = function (objToSave, callback) {
    new Models(objToSave).save(callback)
};
//Delete User in DB
var deleteTopicBinding = function (criteria, callback) {
    Models.findOneAndRemove(criteria, callback);
};

//Get Users from DB
var getTopicBinding = function (criteria, projection, options, callback) {
    options.lean = true;
    Models.find(criteria, projection, options, callback);
};

var getTopicBindingCount = function(match, group, sort, project, matchMonthQuery, callback) {
    Models.aggregate([
        { $match : match},
        { $sort : sort },
        { $project : project},
        { $group : group },
        { $match : matchMonthQuery}
    ],callback);
};
var update = function (criteria, dataToSet, options, callback) {
    options.lean = true;
    options.new = true;
    Models.update(criteria, dataToSet, options, callback);
};
var getAllTopicBinding = function (criteria, projection,populate, sortOptions,setOptions,callback)
{
    Models.find(criteria).select(projection).populate(populate).sort(sortOptions).setOptions(setOptions).exec(function(err, result){
        callback(err, result);
    });

};

var topicBindingPopulate= function (criteria, projection, populate, sortOptions,setOptions,callback)
{
    logging.consolelog(criteria, projection, populate)
    Models.find(criteria).select(projection).populate(populate).sort(sortOptions).setOptions(setOptions).exec(function(err, result){
        callback(err, result);
    });
};
module.exports = {
    updateTopicBinding: updateTopicBinding,
    createTopicBinding: createTopicBinding,
    deleteTopicBinding: deleteTopicBinding,
    getTopicBinding:getTopicBinding,
    getTopicBindingCount:getTopicBindingCount,
    getAllTopicBinding:getAllTopicBinding,
    update:update,
    topicBindingPopulate:topicBindingPopulate 
};
