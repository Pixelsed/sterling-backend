

/**
 * Created by priya on 8/8/16.
 */
'use strict';

var Models = require('../Model/assignDataToCustomer');


var updateAssignDataToCustomer = function (criteria, dataToSet, options, callback) {
    options.lean = true;
    options.new = true;
    Models.update(criteria, dataToSet, options, callback);
};
//Insert User in DB
var createAssignDataToCustomer = function (objToSave, callback) {
    new Models(objToSave).save(callback)
};
//Delete User in DB
var deleteAssignDataToCustomer= function (criteria, callback) {
    Models.findOneAndRemove(criteria, callback);
};

//Get Users from DB
var getAssignDataToCustomer = function (criteria, projection, options, callback) {
    options.lean = true;
    Models.find(criteria, projection, options, callback);
};


//var getAssignDataToCustomer = function(criteria, callback) {
//    logging.consolelog("----sdsd" , criteria)
//    Models.count(criteria, callback);
//};


var getAssignDataToCustomerAggregateCount = function(match, group, sort, project, matchMonthQuery, callback) {
    Models.aggregate([
        { $match : match},
        { $sort : sort },
        { $project : project},
        { $group : group },
        { $match : matchMonthQuery}
    ],callback);
};
var update = function (criteria, dataToSet, options, callback) {
    options.lean = true;
    options.new = true;
    Models.update(criteria, dataToSet, options, callback);
};
var getAllAssignDataToCustomer = function (criteria, projection, sortOptions,setOptions,callback)
{
    logging.consolelog("dao........",criteria, projection)
    Models.find(criteria).select(projection).sort(sortOptions).setOptions(setOptions).exec(function(err, result){
        callback(err, result);
    });
};

//var getAssignDataToCustomer = function (criteria,distinct, callback) {
//
//    Models.find(criteria).distinct(distinct,function(err,result){
//        callback(err,result);
//    });
//};

var addChildParentMapperinsert = function (objToSave, callback) {
    logging.consolelog('insert_________',"",objToSave)
    Models.collection.insert(objToSave,callback)

};

var assignDataToCustomerPopulate= function (criteria, projection, populate, sortOptions,setOptions,callback)
{
    logging.consolelog(criteria, projection, populate)
    Models.find(criteria).select(projection).populate(populate).sort(sortOptions).setOptions(setOptions).exec(function(err, result){
        callback(err, result);
    });
};



var aggregatePopulate= function(criteria,callback)
{
  Models.aggregate(criteria,callback);
};


module.exports = {
    updateAssignDataToCustomer: updateAssignDataToCustomer,
    createAssignDataToCustomer: createAssignDataToCustomer,
    deleteAssignDataToCustomer: deleteAssignDataToCustomer,
    getAssignDataToCustomer:getAssignDataToCustomer,
    // getCurriculumCount:getCurriculumCount,
    // getAllCurriculum:getAllCurriculum,
    update:update,
    getAssignDataToCustomerAggregateCount:getAssignDataToCustomerAggregateCount,
    
    addChildParentMapperinsert:addChildParentMapperinsert,
    assignDataToCustomerPopulate:assignDataToCustomerPopulate,
    aggregatePopulate:aggregatePopulate
};
