/**
 * Created by priya on 8/8/16.
 */
'use strict';


var mysqlConfig = require('../Library/mysqlConfig');
var queryBuilder = require('./queryBuilder').queryBuilder;

var updateCurriculum = function (criteria, dataToSet, callback) {
	var whereCondition = queryBuilder(criteria);
	logging.consolelog("whereCondition","",whereCondition);
	var setQuery = queryBuilder(dataToSet);
	logging.consolelog("setquery","",setQuery);
	logging.consolelog('query',"",'update curriculum set ' + setQuery + ' where 1 ' + whereCondition);
	mysqlConfig.getMysqlInstance().query('update curriculum set ' + setQuery + ' where 1 ' + whereCondition, function (err, rows) {
		if (err) {
			logging.consolelog("err",err,"");
			callback(err);
		} else {
			callback(null);
		}
	});
	/*options.lean = true;
	options.new = true;
	Models.update(criteria, dataToSet, options, callback);*/
};
//Insert Curriculum in DB
var createCurriculum = function (objToSave, callback) {
	mysqlConfig.getMysqlInstance().query('insert into curriculum set ?', objToSave, function (err, rows) {
		if (err) {
            logging.consolelog("err",err,"");
			callback(err);
		} else {
			callback(null,rows);
		}
	});
	//new Models(objToSave).save(callback)
};

//Copy Curriculum in DB
var cloneCurriculumByLanguage = function (cloneObj, callback) {
	mysqlConfig.getMysqlInstance().query('insert into curriculum(grade, chapter, subject, isRemoved, lastUpdateTime, board, language) select grade, chapter,subject, isRemoved, lastUpdateTime, ? as board, ? as language from curriculum where _id = ?', [cloneObj.newBoardName, cloneObj.newBoardLanguage, cloneObj.existingBoardId], function (err, rows) {
		if (err) {
			logging.consolelog("err",err,"");
			callback(err);
		} else {
//				mysqlConfig.getMysqlInstance().query('insert into topicCurriculumMapping(curriculumId, topicId, isUniversal, sequenceOrder, createdTimestamp) select ? as curriculumId, topicId, 0 as isUniversal, sequenceOrder, createdTimestamp from topicCurriculumMapping where curriculumId = ?', [rows.insertId, cloneObj.existingBoardId], function (err, rows) {
//					callback(null);
//				});
			callback(null);
		}
	});
	//new Models(objToSave).save(callback)
};

var disableCurriculum = function (criteria, callback) {
	var whereCondition = queryBuilder(criteria);
	logging.consolelog("wherecondition","",whereCondition);
	mysqlConfig.getMysqlInstance().query('update curriculum set isRemoved = true where 1 ' + whereCondition, function (err, rows) {
		if (err) {
			logging.consolelog("err",err,"");
			callback(err);
		} else {
				callback(null);
		}
	});
	//new Models(objToSave).save(callback)
};

//Delete User in DB
var deleteCurriculum = function (criteria, callback) {
	Models.findOneAndRemove(criteria, callback);
};

/*select * from topicCurriculumMapping left join curriculum on topicCurriculumMapping.curriculumId = curriculum._id  where topicId = 1;*/

//Get Users from DB
var getCurriculum = function (criteria, projection, options, callback) {
	logging.consolelog("criteria","",JSON.stringify(criteria));
	var whereCondition = queryBuilder(criteria);
	logging.consolelog("wherecondition","",whereCondition);
	mysqlConfig.getMysqlInstance().query('select * from curriculum where 1 ' + whereCondition, function (err, rows) {
		logging.consolelog("mysql data","",JSON.stringify(rows));
		if (err) {
			logging.consolelog("err",err,"");
			callback(err, rows);
		} else {
			callback(null, rows);
		}
	});
};

var getAllFiltersForCurriculum = function (criteria, callback) {
	logging.consolelog("criteria","",JSON.stringify(criteria));
	var whereCondition = queryBuilder(criteria);
	logging.consolelog("wherecondition","",whereCondition);
	mysqlConfig.getMysqlInstance().query('select group_concat(distinct chapter) as chapters, group_concat(distinct subject) as subjects, group_concat(distinct board) as boards, group_concat(distinct language) as languages, group_concat(distinct grade) as grades from curriculum where 1 ' + whereCondition + ' group by isRemoved', function (err, rows) {
        logging.consolelog("mysql data","",JSON.stringify(rows));
		if (err) {
			logging.consolelog("err",err,"");
			callback(err, rows);
		} else {
			callback(null, rows);
		}
	});
};

var getConditionalFilterData = function(criteria, distinct,callback){
	var whereCondition = '';
	logging.consolelog('criteria>>>',criteria,criteria.curriculum.length,distinct=='board')
	if(criteria.curriculum.length && distinct!="board"){
		whereCondition +="`board` IN("
		criteria.curriculum.forEach(function(item){
			logging.consolelog('board',"","")
			whereCondition += "'"+item+"',";
		})
		whereCondition=whereCondition.substring(0,whereCondition.length-1)+") AND "
	}
	if(criteria.subject.length && distinct!="subject"){
		whereCondition +="`subject` IN("
		criteria.subject.forEach(function(item){
			logging.consolelog('subject',"","")
			whereCondition += "'" + item+"',";
		})
		whereCondition=whereCondition.substring(0,whereCondition.length-1)+") AND "
	}
	if(criteria.language.length && distinct!="language"){
		whereCondition +="`language` IN("
		criteria.language.forEach(function(item){
			logging.consolelog(' language',"","")
			whereCondition += "'" + item+"',";
		})
		whereCondition= whereCondition.substring(0,whereCondition.length-1)+") AND "
	}
	if(criteria.grade.length && distinct != "grade"){
		whereCondition +="`grade` IN("
		criteria.grade.forEach(function(item){
			logging.consolelog(' grade',"","")
			whereCondition += "'" + item+"',";
		})
		whereCondition= whereCondition.substring(0,whereCondition.length-1)+") AND "
	}
  
	if(distinct== "grade"){
		var sql = "select " + distinct + "_name as "+distinct+" from " + distinct + "_seq where " + distinct + "_name in (select distinct `"+distinct+"` from `curriculum` where 1 AND "+whereCondition +"`isRemoved`=0 ) order by sequence";
	}else{
		var sql = "select distinct `"+distinct+"` from `curriculum` where 1 AND "+whereCondition +"`isRemoved`=0 order by " + distinct;
	}
	
	
	logging.consolelog('sql console',"",sql)
	mysqlConfig.getMysqlInstance().query(sql, function (err, rows) {
		logging.consolelog("getConditionalFilterData","",JSON.stringify(rows));
		if (err) {
			logging.consolelog("err",err,"");
			callback(err, rows);
		} else {
			callback(null, rows);
		}
	});
}

//Get Users from DB
var getCurriculumListingCount = function (criteria, search, callback) {
	logging.consolelog("criteria","",JSON.stringify(criteria));
	var whereCondition = queryBuilder(criteria);
	var searchQuery = "";
	if(search && typeof search == "string"){
		searchQuery = search;
	}

	var query = 'select board, count(distinct grade) as gradeCount, count(distinct subject) as subjectCount, count(distinct chapter) as chapterCount, group_concat(distinct curriculum.language) as language, count(distinct topic._id) as topicCount from (select * from curriculum where isRemoved = false) curriculum left join topicCurriculumMapping on topicCurriculumMapping.curriculumId = curriculum._id left join (select * from  topic where isDeleted = false) topic on topic._id = topicCurriculumMapping.topicId ' + searchQuery + whereCondition + '  group by board';
	logging.consolelog("query","",query);
	mysqlConfig.getMysqlInstance().query(query, function (err, rows) {
        logging.consolelog("mysql data","",JSON.stringify(rows));
		if (err) {
			logging.consolelog("err",err,"");
			callback(err, rows);
		} else {
			callback(null, rows);
		}
	});
};

//Get Users from DB
var getCurriculumByTopic = function (criteria, projection, options, callback) {
	logging.consolelog("criteria","",JSON.stringify(criteria));
	var whereCondition = queryBuilder(criteria);
	logging.consolelog("wherecondition","",whereCondition);
	mysqlConfig.getMysqlInstance().query('select * from topicCurriculumMapping left join curriculum on topicCurriculumMapping.curriculumId = curriculum._id  where 1 ' + whereCondition, function (err, rows) {
        logging.consolelog("mysql data","",JSON.stringify(rows));
		if (err) {
			logging.consolelog("err",err,"");
			callback(err, rows);
		} else {
			callback(null, rows);
		}
	});
};

//Get curriculum, topic and mapping from MYSQL DB
var getCurriculumAndTopic = function (criteria, limit,  search, callback) {
	var whereCondition = queryBuilder(criteria);
	var searchQuery = "";
	if(search && typeof search == "string"){
		searchQuery = search;
	}
	var query = 'select distinct * from (select topicId from curriculum inner join topicCurriculumMapping on' +
		' curriculum._id = topicCurriculumMapping.curriculumId  where removed=0 ' + whereCondition + ') ' +
		'curriculums inner join (select curriculumId as universalCurriculumId, topicId' +
		' from topicCurriculumMapping where isUniversal = true) universal on universal.topicId = ' +
		'curriculums.topicId inner join (select _id as _id,authorName,description, icon,isPublish,name, group_concat(language) as ' +
		'topicLanguage, group_concat(_id) as ids,group_concat(averageRating) as rating  from topic where' +
		' isDeleted = false ' + searchQuery + ' group by name) topic on FIND_IN_SET(curriculums.topicId, topic.ids)  inner' +
		' join curriculum on curriculum._id = universal.universalCurriculumId limit ' + limit.limit +
		' offset ' + limit.offset;
	// var query = "select name ,description,authorName,isPublish, T_id as topicId,grade,subject,"+
	// " chapter,board,C_id as universalCurriculumId,group_concat(T_id) as ids, group_concat(Clang) as topicLanguage,"+
	// " group_concat(averageRating) as rating from (select distinct * from "+
	// " (select T.authorName,T.description,T.icon, T.name,T.isPublish,T.isDeleted,C.isRemoved,"+
	// " T. averageRating ,C.board,C.grade,C.subject,C.chapter,map.curriculumId as C_id,T._id as "+
	// " T_id,T.language as Tlang ,C.language as Clang from topicCurriculumMapping map inner"+
	// " join topic T on T._id=map.topicId inner join curriculum C on C._id=map.curriculumId "+
	// " where C.isRemoved=0 and map.removed=0 and T.isDeleted=0 and C.board='UNIVERSAL') tab)newTab where 1 "+whereCondition+" "+searchQuery+
	// " group by name limit "+limit.limit+" offset " + limit.offset;
	logging.consolelog("query","",query);
	mysqlConfig.getMysqlInstance().query(query, function (err, rows) {
		if (err) {
			logging.consolelog("err",err,"");
			callback(err, rows);
		} else {
			callback(null, rows);
		}
	});
};

//Get curriculum, topic and mapping from MYSQL DB
var getCurriculumAndTopicCount = function (criteria, limit,  search, callback) {
	var whereCondition = queryBuilder(criteria);
	var searchQuery = "";
	if(search && typeof search == "string"){
		searchQuery = search;
	}
	// var query = 'select count(*) as cnt from (select topicId from curriculum inner join topicCurriculumMapping on curriculum._id = topicCurriculumMapping.curriculumId  where 1 ' + whereCondition + ') curriculums inner join (select curriculumId as universalCurriculumId, topicId from topicCurriculumMapping where isUniversal = true) universal on universal.topicId = curriculums.topicId inner join (select min(_id) as _id, name, group_concat(language) as topicLanguage from topic where isDeleted = false' + searchQuery + '  group by name) topic on curriculums.topicId = topic._id inner join curriculum on curriculum._id = universal.universalCurriculumId';
    var query = 'select distinct count(*) as cnt from (select topicId from curriculum inner join topicCurriculumMapping on' +
        ' curriculum._id = topicCurriculumMapping.curriculumId  where removed=0 ' + whereCondition + ') ' +
        'curriculums inner join (select curriculumId as universalCurriculumId, topicId' +
        ' from topicCurriculumMapping) universal on universal.topicId = ' +
        'curriculums.topicId inner join (select _id as _id,authorName,description, icon,isPublish,name, group_concat(language) as ' +
        'topicLanguage, group_concat(_id) as ids,group_concat(averageRating) as rating  from topic where' +
        ' isDeleted = false ' + searchQuery + ' group by name) topic on FIND_IN_SET(curriculums.topicId, topic.ids)  inner' +
        ' join curriculum on curriculum._id = universal.universalCurriculumId';
 //   var query = "select description,authorName,isPublish, T_id as topicId,grade,subject,"+
	// " chapter,board,C_id as universalCurriculumId, group_concat(Clang) as topicLanguage,"+
	// " group_concat(averageRating) as rating from (select distinct * from "+
	// " (select T.authorName,T.description,T.icon, T.name,T.isPublish,T.isDeleted,C.isRemoved,"+
	// " T. averageRating ,C.board,C.grade,C.subject,C.chapter,map.curriculumId as C_id,T._id as "+
	// " T_id,T.language as Tlang ,C.language as Clang from topicCurriculumMapping map inner"+
	// " join topic T on T._id=map.topicId inner join curriculum C on C._id=map.curriculumId "+
	// " where C.isRemoved=0 and map.removed=0 and T.isDeleted=0 and C.board='UNIVERSAL') tab)newTab where 1 "+whereCondition+" "+searchQuery+
	// " group by topicId";
    logging.consolelog("query","",query);
	mysqlConfig.getMysqlInstance().query(query, function (err, rows) {
		if (err) {
			logging.consolelog("err",err,"");
			callback(err, rows);
		} else {
			callback(null, rows);
		}
	});
};
var getTopicCount2 = function (criteria,limit,search,callback) {
	var whereCondition = queryBuilder(criteria);
	var searchQuery = "";
	if(search && typeof search == "string"){
		searchQuery = search;
	}
	var query = "select description,authorName,isPublish, T_id as topicId,grade,subject,"+
	" chapter,board,C_id as universalCurriculumId, group_concat(Clang) as topicLanguage,"+
	" group_concat(averageRating) as rating from (select distinct * from "+
	" (select T.authorName,T.description,T.icon, T.name,T.isPublish,T.isDeleted,C.isRemoved,"+
	" T. averageRating ,C.board,C.grade,C.subject,C.chapter,map.curriculumId as C_id,T._id as "+
	" T_id,T.language as Tlang ,C.language as Clang from topicCurriculumMapping map inner"+
	" join topic T on T._id=map.topicId inner join curriculum C on C._id=map.curriculumId "+
	" where C.isRemoved=0 and map.removed=0 and T.isDeleted=0) tab)newTab where 1 "+whereCondition+" "+searchQuery+
	" group by topicId";
	logging.consolelog("query","",query);
	mysqlConfig.getMysqlInstance().query(query, function (err, rows) {
		if (err) {
			logging.consolelog("err",err,"");
			callback(err, rows);
		} else {
			callback(null, rows);
		}
	});
}

var getCurriculumCount = function (criteria, callback) {
	logging.consolelog("----sdsd", "",criteria)
	Models.count(criteria, callback);
};


var getCurriculumAggregateCount = function (match, group, sort, project, matchMonthQuery, callback) {
	Models.aggregate([{
		$match: match
    },{
		$group: group
    }], callback);
};
var update = function (criteria, dataToSet, options, callback) {
	options.lean = true;
	options.new = true;
	Models.update(criteria, dataToSet, options, callback);
};
var getAllCurriculum = function (criteria, projection, sortOptions, setOptions, callback) {
	logging.consolelog("dao........", criteria, projection)
	Models.find(criteria).select(projection).sort(sortOptions).setOptions(setOptions).exec(function (err, result) {
		callback(err, result);
	});
};

var getUniqueCurriculum = function (criteria, distinct, callback) {
	logging.consolelog("criteria","",JSON.stringify(criteria));
	var whereCondition = queryBuilder(criteria);
	logging.consolelog("wherecondition","",whereCondition);
	logging.consolelog("query","",'select distinct(' + distinct + ') from curriculum where 1 ' + whereCondition);
	mysqlConfig.getMysqlInstance().query('select distinct(' + distinct + ') from curriculum where 1 ' + whereCondition, function (err, rows) {
        logging.consolelog("getUniqueCurriculum","",JSON.stringify(rows));
		if (err) {
			logging.consolelog("err",err,"");
			callback(err, rows);
		} else {
			callback(null, rows);
		}
	});
};

var getUniqueAndSortCurriculum=  function (criteria, distinct, callback) {
	logging.consolelog("criteria","",JSON.stringify(criteria));
	var whereCondition = queryBuilder(criteria);
	logging.consolelog("wherecondition","",whereCondition);
	logging.consolelog("query","",'select distinct(' + distinct + ') from curriculum where 1 ' + whereCondition);
	mysqlConfig.getMysqlInstance().query('select distinct(' + distinct + ') from curriculum where 1 ' + whereCondition+ ' ORDER BY grade_seq', function (err, rows) {
		logging.consolelog("getUniqueCurriculum data","",JSON.stringify(rows));
		if (err) {
			logging.consolelog("err",err,"");
			callback(err, rows);
		} else {
			callback(null, rows);
		}
	});
	/*Models.find(criteria).distinct(distinct, function (err, result) {
		callback(err, result);
	});*/
};

var getCurriculumAndTopics = function (callback) {
	Models.aggregate([

		{
			$lookup: {
				from: "topiccurriculummappings",
				localField: "_id",
				foreignField: "curriculumId",
				as: "topics"
			}
   },
		{
			$match: {
				"topics": {
					$ne: []
				}
			}
       }
], function (err, result) {
		callback(err, result);
	})
}

var addCirriculuminsert = function (objToSave, callback) {
	logging.consolelog('insert_________',"", objToSave)
	Models.collection.insert(objToSave, callback)

};

var curriculumPopulate = function (criteria, projection, populate, sortOptions, setOptions, callback) {
	logging.consolelog(criteria, projection, populate)
	Models.find(criteria).select(projection).populate(populate).sort(sortOptions).setOptions(setOptions).exec(function (err, result) {
		callback(err, result);
	});
};

var getDataDeepPopulate = function (query, projectionQuery, options, populateModel, nestedModel, callback) {

	Models.find(query, projectionQuery, options).populate(populateModel)
		.exec(function (err, docs) {

			if (err) {
				return callback(err, docs);
			}
			model.populate(docs, nestedModel,
				function (err, populatedDocs) {
					if (err) return callback(err);
					callback(null, populatedDocs); // This object should now be populated accordingly.
				});
		});
};


var aggregateLookUp = function (lookup, match, project, callback) {
	Models.aggregate([
		{
			$match: {
				'curriculum.board': 'UNIVERSAL',
				'curriculum.isRemoved': false
			}
      },
    //         $match: match
    //     },
		{
			$lookup: lookup
        }

        // {
        //     $project: project
        // }
    ], callback);
};


var getCurriculumAndTopicList = function (criteria, limit,  options, callback) {
	var whereCondition = queryBuilder(criteria);
	var query = 'select * from (select topicId, sequenceOrder, curriculum._id as curriculumId from curriculum inner join topicCurriculumMapping on curriculum._id = topicCurriculumMapping.curriculumId  where removed=0 ' + whereCondition + ') curriculums inner join (select curriculumId as universalCurriculumId, topicId from topicCurriculumMapping where isUniversal = true) universal on universal.topicId = curriculums.topicId inner join (select _id as _id, name,language as topicLanguage from topic where isDeleted = false) topic on curriculums.topicId = topic._id inner join curriculum on curriculum._id = universal.universalCurriculumId order by curriculums.sequenceOrder limit ' + limit.limit + ' offset ' + limit.offset;
	logging.consolelog("query","",query);
	mysqlConfig.getMysqlInstance().query(query, function (err, rows) {
		if (err) {
			logging.consolelog("err",err,"");
			callback(err, rows);
		} else {
			callback(null, rows);
		}
	});
};

var exportCSV= function(callback){
	var query='select * from curriculum where isRemoved=false'
	mysqlConfig.getMysqlInstance().query(query, function (err, rows) {
		if (err) {
			logging.consolelog("err",err,"");
			callback(err, rows);
		} else {
			callback(null, rows);
		}
	});
}


//Copy Curriculum in DB
var cloneCurriculumByLanguageByTopic = function (cloneObj, callback) {
	mysqlConfig.getMysqlInstance().query('insert into curriculum(grade, chapter, subject, isRemoved, lastUpdateTime, board, language) select grade, chapter,subject, isRemoved, lastUpdateTime, ? as board, ? as language from curriculum where _id = ?', [cloneObj.newBoardName, cloneObj.newBoardLanguage, cloneObj.existingBoardId], function (err, rows) {
		if (err) {
			logging.consolelog("err",err,"");
			callback(err);
		} else {
				mysqlConfig.getMysqlInstance().query('insert into topicCurriculumMapping(curriculumId, topicId, isUniversal, sequenceOrder, createdTimestamp) select ? as curriculumId, topicId, 0 as isUniversal, sequenceOrder, createdTimestamp from topicCurriculumMapping where curriculumId = ?', [rows.insertId, cloneObj.existingBoardId], function (err, rows) {
					callback(null);
				});
			
		}
	});
	//new Models(objToSave).save(callback)
};


var dragAndDropGrade = function (objToSave, callback) {
	logging.consolelog("cirteria","",JSON.stringify(objToSave));
	mysqlConfig.getMysqlInstance().query('update grade_seq set sequence = sequence + 1 where sequence > ?', [objToSave.prevGradeSeq], function (seqErr, seqRows) {
		mysqlConfig.getMysqlInstance().query('update grade_seq set sequence = ? where grade_name = ?', [parseInt(objToSave.prevGradeSeq) +1, objToSave.gradeName], function (err, rows) {
			if (err) {
				logging.consolelog("err",err,"");
				callback(err);
			} else {
				callback(null);
			}
		});
	});
	//new Models(objToSave).save(callback)
};



var getSortedData = function(criteria, distinct,callback){

	logging.consolelog("criteria","",JSON.stringify(criteria));
	var whereCondition = queryBuilder(criteria);
	logging.consolelog("wherecondition","",whereCondition);
	
	var sql = "select " + "* from " + distinct + "_seq where " + distinct + "_name in (select distinct `"+distinct+"` from `curriculum` where 1 "+whereCondition+ ") order by sequence";
	
	logging.consolelog('sql console',sql,"check filter data")
	mysqlConfig.getMysqlInstance().query(sql, function (err, rows) {
		logging.consolelog("getConditionalFilterData","",JSON.stringify(rows));
		if (err) {
			logging.consolelog("err",err,"");
			callback(err, rows);
		} else {
			callback(null, rows);
		}
	});
}

var checkIfGradeExistInSorting = function(criteria,callback){
	var sql = "SELECT distinct(`grade_name`),`sequence` from `grade_seq` where `grade_name`=?";
	mysqlConfig.getMysqlInstance().query(sql,[criteria.grade],function(err,rows){
		if(err){
			callback(1)
		}else{
			if(rows && rows.length){
				logging.consolelog('checkIfGradeExistInSorting>>>>>>>>>>>>>>>2',"",rows)
				callback(0)
			}else{
				//get max seq
				mysqlConfig.getMysqlInstance().query('SELECT MAX(`sequence`)+1 as sequence FROM `grade_seq`',function(err,maxSeq){
					//add grade seq
					if(err){
						callback(1)
					}else{
						logging.consolelog('checkIfGradeExistInSorting>>>>>>>>>>>>>>>3',"",maxSeq)
						if(maxSeq && maxSeq.length){
							var insert = "INSERT INTO `grade_seq` (`grade_name`,`sequence`) VALUES(?,?)";
							mysqlConfig.getMysqlInstance().query(insert,[criteria.grade,maxSeq[0].sequence],function(err,data){
								logging.consolelog('checkIfGradeExistInSorting>>>>>>>>>>>>>>>4',err,data)
								if(err){
									callback(1)
								}else{
									callback(0)
								}
							})
						}else{
							callback(1)
						}
					}
				})
			}
		}
	})
};
var getUniqueTable = function (distinct, callback) {
	mysqlConfig.getMysqlInstance().query("select distinct `"+distinct+"` from `curriculum` where 1 and `isRemoved`=0 order by " + distinct, function (err, rows) {
		logging.consolelog("mysql data","",JSON.stringify(rows));
		if (err) {
			logging.consolelog("err",err,"");
			callback(err, rows);
		} else {
			callback(null, rows);
		}
	});
};

module.exports = {
	updateCurriculum: updateCurriculum,
	createCurriculum: createCurriculum,
	deleteCurriculum: deleteCurriculum,
	getCurriculum: getCurriculum,
	getCurriculumCount: getCurriculumCount,
	getAllCurriculum: getAllCurriculum,
	update: update,
	getCurriculumAggregateCount: getCurriculumAggregateCount,
	getUniqueCurriculum: getUniqueCurriculum,
	addCirriculuminsert: addCirriculuminsert,
	curriculumPopulate: curriculumPopulate,
	aggregateLookUp: aggregateLookUp,
	getCurriculumAndTopics: getCurriculumAndTopics,
	getCurriculumAndTopic: getCurriculumAndTopic,
	getCurriculumByTopic: getCurriculumByTopic,
	getCurriculumListingCount: getCurriculumListingCount,
	cloneCurriculumByLanguage: cloneCurriculumByLanguage,
	disableCurriculum: disableCurriculum,
	getAllFiltersForCurriculum: getAllFiltersForCurriculum,
	getCurriculumAndTopicCount: getCurriculumAndTopicCount,
	getCurriculumAndTopicList:getCurriculumAndTopicList,
	getUniqueAndSortCurriculum:getUniqueAndSortCurriculum,
	exportCSV:exportCSV,
	cloneCurriculumByLanguageByTopic:cloneCurriculumByLanguageByTopic,
	getConditionalFilterData:getConditionalFilterData,
	getSortedData:getSortedData,
	dragAndDropGrade:dragAndDropGrade,
	checkIfGradeExistInSorting:checkIfGradeExistInSorting,
	getTopicCount2:getTopicCount2,
	getUniqueTable:getUniqueTable
};
