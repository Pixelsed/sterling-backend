'use strict';

var Models = require('../Model/topicNotes');

var getNote =  function (criteria, projection, options, callback) {
	options.lean = true;
	Models.find(criteria, projection, options, callback);
};

var updateNote= function (query, dataToSet, options, callback) {
    options.lean = true;
	Models.findOneAndUpdate(query, dataToSet, options, callback);
};

module.exports={
	getNote: getNote,
  updateNote:updateNote
}
