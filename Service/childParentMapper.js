/**
 * Created by priya on 8/8/16.
 */
'use strict';

var Models = require('../Model/childParentMapper');


var updateChildParentMapper = function (criteria, dataToSet, options, callback) {
    options.lean = true;
    options.new = true;
    Models.update(criteria, dataToSet, options, callback);
};
//Insert User in DB
var createChildParentMapper = function (objToSave, callback) {
    new Models(objToSave).save(callback)
};
//Delete User in DB
var deleteChildParentMapper= function (criteria, callback) {
    Models.findOneAndRemove(criteria, callback);
};

//Get Users from DB
var getChildParentMapper = function (criteria, projection, options, callback) {
    options.lean = true;
    Models.find(criteria, projection, options, callback);
};


// var getChildParentMapper = function(criteria, callback) {
//     logging.consolelog("----sdsd" , criteria)
//     Models.count(criteria, callback);
// };


var getChildParentMapperAggregateCount = function(match, group, sort, project, matchMonthQuery, callback) {
    Models.aggregate([
        { $match : match},
        { $sort : sort },
        { $project : project},
        { $group : group },
        { $match : matchMonthQuery}
    ],callback);
};
var update = function (criteria, dataToSet, options, callback) {
    options.lean = true;
    options.new = true;
    Models.update(criteria, dataToSet, options, callback);
};
var getAllChildParentMapper = function (criteria, projection, sortOptions,setOptions,callback)
{
    logging.consolelog("dao........",criteria, projection)
    Models.find(criteria).select(projection).sort(sortOptions).setOptions(setOptions).exec(function(err, result){
        callback(err, result);
    });
};

var getUniqueChildParentMapper = function (criteria,distinct, callback) {

    Models.find(criteria).distinct(distinct,function(err,result){
        callback(err,result);
    });
};

var addChildParentMapperinsert = function (objToSave, callback) {
    logging.consolelog('insert_________',"",objToSave)
    Models.collection.insert(objToSave,callback)

};

var childParentMapperPopulate= function (criteria, projection, populate, sortOptions,setOptions,callback)
{
    logging.consolelog(criteria, projection, populate)
    Models.find(criteria).select(projection).populate(populate).sort(sortOptions).setOptions(setOptions).exec(function(err, result){
        callback(err, result);
    });
};



var aggregatePopulate= function(criteria,callback)
{
  Models.aggregate(criteria,callback);
};


module.exports = {
    updateChildParentMapper: updateChildParentMapper,
    createChildParentMapper: createChildParentMapper,
    deleteChildParentMapper: deleteChildParentMapper,
    getChildParentMapper:getChildParentMapper,
    // getCurriculumCount:getCurriculumCount,
    getChildParentMapperAggregateCount:getChildParentMapperAggregateCount,
    update:update,
    getAllChildParentMapper:getAllChildParentMapper,
    getUniqueChildParentMapper:getUniqueChildParentMapper,
    addChildParentMapperinsert:addChildParentMapperinsert,
    childParentMapperPopulate:childParentMapperPopulate,
    aggregatePopulate:aggregatePopulate
};
