'use strict';

var Models = require('../Model/assessment');


var updateAssessment = function (criteria, dataToSet, options, callback) {
    options.lean = true;
    options.new = true;
    Models.update(criteria, dataToSet, options, callback);
};
//Insert User in DB
var createAssessment = function (objToSave, callback) {
    new Models(objToSave).save(callback)
};
//Delete User in DB
var deleteAssessment= function (criteria, callback) {
    Models.findOneAndRemove(criteria, callback);
};

//Get Users from DB
var getAssessment = function (criteria, projection, options, callback) {
    options.lean = true;
	logging.consolelog(criteria, projection, options);
    Models.find(criteria, projection, options, callback);
};


var getAssessmentCount = function(criteria, callback) {
    logging.consolelog("----count" ,"" ,criteria)
    Models.count(criteria, callback);
};


var getAssessmentAggregateCount = function(match, group, sort, project, matchMonthQuery, callback) {
    Models.aggregate([
        { $match : match},
        { $sort : sort },
        { $project : project},
        { $group : group },
        { $match : matchMonthQuery}
    ],callback);
};
var update = function (criteria, dataToSet, options, callback) {
    options.lean = true;
    options.new = true;
    Models.update(criteria, dataToSet, options, callback);
};
var getAllAssessment = function (criteria, projection, sortOptions,setOptions,callback)
{
    logging.consolelog("dao........",criteria, projection)
    Models.find(criteria).select(projection).sort(sortOptions).setOptions(setOptions).exec(function(err, result){
        callback(err, result);
    });
};

var getUniqueAssessment = function (criteria,distinct, callback) {

    Models.find(criteria).distinct(distinct,function(err,result){
        callback(err,result);
    });
};

var addAssessmentinsert = function (objToSave, callback) {
    logging.consolelog('insert_________',"",objToSave)
    Models.collection.insert(objToSave,callback)

};

var AssessmentPopulate= function (criteria, projection, populate, sortOptions,setOptions,callback)
{
    logging.consolelog(criteria, projection, populate)
    Models.find(criteria).select(projection).populate(populate).sort(sortOptions).setOptions(setOptions).exec(function(err, result){
        callback(err, result);
    });
};



// var aggregatePopulate= function(criteria,callback)
// {
//   Models.aggregate(criteria,callback);
// };

var createScoreBoard = function (objToSave, callback) {
    logging.consolelog('insert_________',"",objToSave)
    Models.collection.insert(objToSave,callback)

};

var updateScoreBoard = function (criteria, dataToSet, options, callback) {
    options.lean = true;
    options.new = true;
    Models.update(criteria, dataToSet, options, callback);
};

var getScoreBoard = function (criteria, projection, options, callback) {
    options.lean = true;
    logging.consolelog(criteria, projection, options);
    Models.find(criteria, projection, options, callback);
};

module.exports = {
    updateAssessment: updateAssessment,
    createAssessment: createAssessment,
    deleteAssessment: deleteAssessment,
    getAssessment:getAssessment,
    updateScoreBoard:updateScoreBoard,
    createScoreBoard:createScoreBoard,
    getScoreBoard:getScoreBoard
};
