'use strict';

var Models = require('../Model/offlineManagement');


var createkey = function (objToSave, callback) {
    new Models(objToSave).save(callback)
};



var getKey = function (criteria, projection, options, callback) {
    options.lean = true;
	logging.consolelog(criteria, projection, options);
    Models.find(criteria, projection, options, callback);
};


var updateKey = function (criteria, dataToSet, options, callback) {
    options.lean = true;
    options.new = true;
    Models.findOneAndUpdate(criteria, dataToSet, options, callback);
};





module.exports = {
   
    createkey: createkey,
    getKey:getKey,
    updateKey:updateKey
   
};
