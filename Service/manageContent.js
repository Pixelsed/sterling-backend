/**
 * Created by rishi on 9/16/16.
 */

'use strict';

var Models = require('../Model/manageContent');

var getContent = function (criteria, projection, sortOptions, populate, callback) {
	 Models.find(criteria).select(projection).populate(populate).sort(sortOptions).exec(function(err, result){
        callback(err, result);
    });
};

var updateContent=function (query, dataToSet, options, callback) {
    options.lean = true;
	Models.findOneAndUpdate(query, dataToSet, options, callback);
};
module.exports = {
	getContent: getContent,
updateContent:updateContent
};
