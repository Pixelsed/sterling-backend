/**
 * Created by priya on 8/11/16.
 */


'use strict';

//var Models = require('../Model/topic');
var mysqlConfig = require('../Library/mysqlConfig');
var queryBuilder = require('./queryBuilder').queryBuilder;

var updateTopic = function (criteria, dataToSet, options, callback) {
	var limit = " limit 1";
	logging.consolelog("logging","",JSON.stringify(criteria));
	var whereCondition = queryBuilder(criteria);
	logging.consolelog("logging","",whereCondition);
	if (options.multi && options.multi == true) {
		limit = " ";
	}
	mysqlConfig.getMysqlInstance().query('update topic set ? where 1 ' + whereCondition + limit, dataToSet, function (err, rows) {
		if (err) {
			logging.consolelog("logging","",err);
			callback(err);
		} else {
			var data = {
				_id: rows.insertId
			};
			callback(null, data);
		}
	});
};
//Insert User in DB
var createTopic = function (objToSave, callback) {
	logging.consolelog(JSON.stringify(objToSave));
	mysqlConfig.getMysqlInstance().query('insert into topic set ?', objToSave, function (err, rows) {
		if (err) {
			logging.consolelog("logging","",err);
			callback(err);
		} else {
			var data = {
				_id: rows.insertId
			};
			callback(null, data);
		}
	});
};
//Delete User in DB
var deleteTopic = function (criteria, callback) {
	Models.findOneAndRemove(criteria, callback);
};

//Get Users from DB
var getTopic = function (criteria, projection, options, callback) {
	logging.consolelog("logging","",JSON.stringify(criteria));
	var whereCondition = queryBuilder(criteria);
	logging.consolelog("logging","",whereCondition);
	logging.consolelog("logging","",'select * from topic where 1 ' + whereCondition);
	mysqlConfig.getMysqlInstance().query('select * from topic where 1 ' + whereCondition, function (err, rows) {
		logging.consolelog("getTopic data","","");
		logging.consolelog("logging","",JSON.stringify(rows));
		if (err) {
			logging.consolelog("logging","",err);
			callback(err, rows);
		} else {
			callback(null, rows);
		}
	});
};

var getValidTopics = function (listOfTopics, callback) {
	logging.consolelog("logging","",JSON.stringify(listOfTopics));
	if (!Array.isArray(listOfTopics)) {
		listOfTopics = JSON.parse(listOfTopics);
	}
	var idQuery = "( ";
	for (var i in listOfTopics) {
		idQuery += listOfTopics[i] + ",";
	}
	idQuery = idQuery.substring(0, idQuery.length - 1) + ")";
	mysqlConfig.getMysqlInstance().query('select _id from topic where isPublish = true and isDeleted = false and _id in ' + idQuery, function (err, rows) {
		logging.consolelog("getValidTopics data","","");
		logging.consolelog("logging","",JSON.stringify(rows));
		if (err) {
			logging.consolelog("logging","",err);
			callback(err, []);
		} else {
			var validTopics = [];
			if (rows && rows.length) {
				for (var i in rows) {
					validTopics.push(rows[i]["_id"]);
				}
			}
			callback(null, validTopics);
		}
	});
}

var getTopicAggregate = function (group, callback) {
	logging.consolelog("logging","",group);
	Models.aggregate([
        // { $match : match},
        // { $sort : sort },
        // { $project : project},
		{
			$group: group
		},
        // { $match : matchMonthQuery}
    ], callback);
};
var update = function (criteria, dataToSet, options, callback) {
	options.lean = true;
	options.new = true;
	Models.update(criteria, dataToSet, options, callback);
};
var getAllTopics = function (criteria, projection, sortOptions, setOptions, callback) {
	logging.consolelog("dao........", criteria, projection)
	Models.find(criteria).select(projection).sort(sortOptions).setOptions(setOptions).exec(function (err, result) {
		callback(err, result);
	});
};
var getUniqueTopics = function (criteria, distinct, callback) {
	logging.consolelog("logging","",JSON.stringify(criteria));
	var whereCondition = queryBuilder(criteria);
	logging.consolelog("logging","",whereCondition);
	mysqlConfig.getMysqlInstance().query('select distinct(' + distinct + ') from topic where 1 ' + whereCondition, function (err, rows) {
		logging.consolelog("getTopic data","","");
		logging.consolelog("logging","",JSON.stringify(rows));
		if (err) {
			logging.consolelog("err",err,"");
			callback(err, rows);
		} else {
			callback(null, rows);
		}

	});
};

// function(cb) {
//     var group = {
//         '_id': "$name",
//         'language': {
//             $push: "$language"
//         },
//         'topicId': {
//             $push: "$_id"
//         }
//     }
//     ServiceTopic.getTopicAggregate(group, function(err, data) {
//         logging.consolelog(err, data, "~~~~~~topic Listing~~~~~~~~");
//         if (!err) {
//             for (var i = 0; i < data.length; i++) {
//                 topicList.push({
//                     topicName: data[i]._id,
//                     language: data[i].language
//                 });
//             }
//             cb(null);
//         } else {
//             cb(err);
//         }
//     })
//
// }

var getTopicsByCurriculum = function (criteria, sort, limit, search, callback) {
	logging.consolelog("logging","",JSON.stringify(criteria));
	var whereCondition = queryBuilder(criteria);
	logging.consolelog("logging","",whereCondition);
	var orderBy = "";
	var limitStatement = "";
	var offsetStatement = "";
	if (sort.sortBy) {
		orderBy = " order by " + sort.sortBy + " ASC ";
	}
	if (limit.limit) {
		limitStatement = " limit " + limit.limit;
	}
	if (limit.offset) {
		offsetStatement = " offset " + limit.offset;
	}
	search = search ? search : ""
	var query = 'select boardLanguage, topic._id, sequenceOrder, topic.isDeleted, topic.isPublish, name, icon,topic.isFree,'+ 
	'authorName, averageRating, price, description, board, subject, topic.language, topic.language as topicLanguage,'+
	' grade, chapter, addedDate, isRemoved from (select board, subject, language, language as boardLanguage, grade, '+
	'chapter, topicId, isRemoved, sequenceOrder from curriculum left join topicCurriculumMapping on topicCurriculumMapping.curriculumId = curriculum._id where topicCurriculumMapping.removed=0)'+
	' curriculum inner join topic on topic._id = curriculum.topicId where 1 ' + whereCondition + search + "group by topic._id" + orderBy + limitStatement + offsetStatement;

	logging.consolelog('>>>>>>>>>',"",query);
	mysqlConfig.getMysqlInstance().query(query, function (err, rows) {
		logging.consolelog("getTopic data","","");
		logging.consolelog("logging","",JSON.stringify(rows));
		if (err) {
			logging.consolelog("logging","",err);
			callback(err, rows);
		} else {
			callback(null, rows);
		}
		/*options.lean = true;
		Models.find(criteria, projection, options, callback);*/
	});
};

var getCheckoutPrice = function (topicCriteria, curriculumCriteria, callback) {
	logging.consolelog("logging","",JSON.stringify(topicCriteria));
	var topicWhereCondition = queryBuilder(topicCriteria);
	logging.consolelog("logging","",JSON.stringify(curriculumCriteria));
	var curriculumCondition = queryBuilder(curriculumCriteria);
	var query = 'select sum(price) as price from (select sum(price) as price from (select board, subject, language, grade, chapter, topicId, isRemoved from curriculum left join topicCurriculumMapping on topicCurriculumMapping.curriculumId = curriculum._id) curriculum inner join topic on topic._id = curriculum.topicId where 1 ' + curriculumCriteria + ' union select sum(price) as price from topic where 1  ' + topicWhereCondition + ' a';
	logging.consolelog("logging","",query);
	mysqlConfig.getMysqlInstance().query(query, function (err, rows) {
		logging.consolelog("getCheckoutPrice data","","");
		logging.consolelog("logging","",JSON.stringify(rows));
		if (err) {
			logging.consolelog("logging","",err);
			callback(err, rows);
		} else {
			callback(null, rows);
		}
		/*options.lean = true;
		Models.find(criteria, projection, options, callback);*/
	});
};

var getTopicsByCurriculumCount = function (criteria, projection, limit, search, callback) {
	logging.consolelog("logging","",JSON.stringify(criteria));
	var whereCondition = queryBuilder(criteria);
	logging.consolelog("logging","",whereCondition);
	search = search ? search : ""
	var query = 'select count(*) as cnt from (select board, subject, language, grade, chapter, topicId, isRemoved from curriculum left join topicCurriculumMapping on topicCurriculumMapping.curriculumId = curriculum._id where topicCurriculumMapping.removed=0) curriculum inner join topic on topic._id = curriculum.topicId where 1 ' + whereCondition + search ;
	logging.consolelog("query","",query);
	mysqlConfig.getMysqlInstance().query(query, function (err, rows) {
		logging.consolelog("getTopic data","","");
		logging.consolelog(JSON.stringify(rows));
		if (err) {
			logging.consolelog("",err,"");
			callback(err, rows);
		} else {
			callback(null, rows);
		}
		/*options.lean = true;
		Models.find(criteria, projection, options, callback);*/
	});
};


var groupByName = function (criteria, sort, limit, search, callback) {
	logging.consolelog("logging","",JSON.stringify(criteria));
	var whereCondition = queryBuilder(criteria);
	logging.consolelog("logging","",whereCondition);
	var orderBy = "";
	var limitStatement = "";
	var offsetStatement = "";
	if (sort.sortBy) {
		orderBy = " order by " + sort.sortBy + " ASC ";
	}
	if (limit.limit) {
		limitStatement = " limit " + limit.limit;
	}
	if (limit.offset) {
		offsetStatement = " offset " + limit.offset;
	}
	search = search ? search : ""
	var query = 'select boardLanguage, topic._id, sequenceOrder, topic.isDeleted, topic.isPublish, name, icon, authorName, averageRating, price, description, board, subject, topic.language, topic.language as topicLanguage, grade, chapter, addedDate, isRemoved from (select board, subject, language,language as boardLanguage, grade, chapter, topicId, isRemoved, sequenceOrder from curriculum left join topicCurriculumMapping on topicCurriculumMapping.curriculumId = curriculum._id) curriculum inner join topic on topic._id = curriculum.topicId where 1 ' + whereCondition + search + "group by topic._id" + orderBy + limitStatement + offsetStatement;

	logging.consolelog("query","",query);
	mysqlConfig.getMysqlInstance().query(query, function (err, rows) {
		logging.consolelog("getTopic data","","");
		logging.consolelog("logging","",JSON.stringify(rows));
		if (err) {
			logging.consolelog("logging",err,"");
			callback(err, rows);
		} else {
			callback(null, rows);
		}
		/*options.lean = true;
		Models.find(criteria, projection, options, callback);*/
	});
};

var groupCnt = function (criteria, projection, limit, search, callback) {
	logging.consolelog("logging","",JSON.stringify(criteria));
	var whereCondition = queryBuilder(criteria);
	logging.consolelog("wherecondition","",whereCondition);
	search = search ? search : ""
	var query = 'select count(*) as cnt from (select board, subject, language,language as boardLanguage, grade, chapter, topicId, isRemoved, sequenceOrder from curriculum left join topicCurriculumMapping on topicCurriculumMapping.curriculumId = curriculum._id) curriculum inner join topic on topic._id = curriculum.topicId where 1 ' + whereCondition + search;

	logging.consolelog("query","",query);
	mysqlConfig.getMysqlInstance().query(query, function (err, rows) {
		logging.consolelog("getTopic data","","");
		logging.consolelog("logging","",JSON.stringify(rows));
		if (err) {
			logging.consolelog("logging","",err);
			callback(err, rows);
		} else {
			callback(null, rows);
		}
		/*options.lean = true;
		Models.find(criteria, projection, options, callback);*/
	});
};
module.exports = {
	updateTopic: updateTopic,
	createTopic: createTopic,
	deleteTopic: deleteTopic,
	getTopic: getTopic,
	getTopicAggregate: getTopicAggregate,
	getAllTopics: getAllTopics,
	update: update,
	getUniqueTopics: getUniqueTopics,
	getTopicsByCurriculum: getTopicsByCurriculum,
	getCheckoutPrice: getCheckoutPrice,
	getTopicsByCurriculumCount: getTopicsByCurriculumCount,
	getValidTopics: getValidTopics,
	groupByName: groupByName,
	groupCnt: groupCnt
};