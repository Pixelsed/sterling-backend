/**
 * Created by priya on 8/5/16.
 */


'use strict';

var constants = require('./appConstants');
var Jwt = require('jsonwebtoken');
var async = require('async');
var ServiceSuperAdmin = require('../Service/superAdmin');
var ServiceCustomer=require('../Service/customer');
var ERROR = constants.STATUS_MSG.ERROR;

var setTokenInDB = function (userId,userType,loginType, tokenToSave, callback) {
    var criteria = {
        _id: userId
    };
    var setQuery = {
        // accessToken : tokenToSave
        $set:{
        }
    };
    async.series([
        function (cb) {
            switch (loginType){
                case 'WEBAPP':
                    logging.consolelog('criteria>>>1',criteria,setQuery)
                    setQuery['$set']={'accessToken.WEBAPP':tokenToSave};
                    logging.consolelog('criteria>>>2',criteria,setQuery)
                    cb();
                    break;
                case 'ANDROIDAPP':
                    setQuery['$set']['accessToken']={'ANDROIDAPP':""};
                    setQuery['$set']={'accessToken.ANDROIDAPP':tokenToSave};
                    cb();
                    break;
                case 'IOSAPP':
                    setQuery['$set']['accessToken']={'IOSAPP':""};
                    setQuery['$set']={'accessToken.IOSAPP':tokenToSave};
                    cb();
                    break;
                default :
                    cb(ERROR.IMP_ERROR)
            }
        },
        function (cb) {
            switch(userType){
                case 'CUSTOMER':
                    logging.consolelog('criteria>>>3',criteria,setQuery)
                    ServiceCustomer.updateCustomer(criteria,setQuery,{new:true}, function (err, dataAry) {
                        if (err){
                            cb(err)
                        }else {
                            if (dataAry && dataAry._id){
                                cb();
                            }else {
                                cb(ERROR.IMP_ERROR);
                            }
                        }
                    });
                    break;
                case 'SUPER_ADMIN':
                    ServiceSuperAdmin.updateSuperAdmin(criteria,setQuery,{new:true}, function (err, dataAry) {
                        if (err){
                            cb(err)
                        }else {
                            if (dataAry && dataAry._id){
                                cb();
                            }else {
                                cb(ERROR.IMP_ERROR)
                            }
                        }

                    });
                    break;
                default :
                    cb(ERROR.IMP_ERROR)

            }
        }
    ], function (err, result) {
        if (err){
            callback(err)
        }else {
            callback()
        }

    });
};

var setToken = function (tokenData, callback) {
	logging.consolelog("token>>>","",tokenData);
    if (!tokenData.id || !tokenData.type) {
        logging.consolelog("error","","");

        callback(ERROR.IMP_ERROR);
    } else {
        var tokenToSend = Jwt.sign(tokenData, constants.COMMON_VARIABLES.JWT_SECRET_KEY);
        logging.consolelog(tokenData.id,tokenData.userType, tokenToSend);
        tokenData.loginType=tokenData.loginType?tokenData.loginType:"WEBAPP";
        setTokenInDB(tokenData.id,tokenData.userType,tokenData.loginType, tokenToSend, function (err, data) {
            logging.consolelog('token>>>>',err,data);
            callback(err, {accessToken: tokenToSend})
        })
    }
};

var getTokenFromDB = function (userId, userType,token,ip,loginType, callback) {
    logging.consolelog(userId, userType,token);
    var userData = null;
    var criteria = {
        _id: userId
        // accessToken : token
    };
    logging.consolelog('bug fixing',ip,loginType)
    async.series([
        function (cb) {
            switch (loginType){
                case 'WEBAPP':
                    criteria['accessToken.WEBAPP']=token;
                    cb()
                    break;
                case 'ANDROIDAPP':
                    criteria['accessToken.ANDROIDAPP']=token;
                    cb()
                    break;
                case 'IOSAPP':
                    criteria['accessToken.IOSAPP']=token;
                    cb()
                    break;
                default:
                    cb(ERROR.IMP_ERROR)
            }
        },
        function (cb) {
            switch(userType){
                case 'CUSTOMER' :
                    ServiceCustomer.getCustomer(criteria,{},{lean:true}, function (err, dataAry) {
                        logging.consolelog("CUSTOMER",err,dataAry);
                        if (err){
                            cb(err)
                        }else {
                            if (dataAry && dataAry.length > 0){
                                userData = dataAry[0];
                                cb();
                            }else {
                                cb(ERROR.INVALID_TOKEN)
                            }
                        }

                    });
                    break;
                case 'SUPER_ADMIN' :
                    logging.consolelog("superadmin","",criteria);
                    ServiceSuperAdmin.getSuperAdmin(criteria,{},{lean:true}, function (err, dataAry) {
                        logging.consolelog('Super admin',err, dataAry);
                        if (err){
                            callback(err)
                        }else {
                            if (dataAry && dataAry.length > 0){
                                userData = dataAry[0];
                                cb();
                            }else {
                                callback(constants.STATUS_MSG.ERROR.INVALID_TOKEN)
                            }
                        }

                    });
                    break;
                default :
                    cb(constants.STATUS_MSG.ERROR.IMP_ERROR);

            }
        }
    ], function (err, result) {
        if (err){
            callback(err)
        }else {
            if (userData && userData._id){
                userData.id = userData._id;
                userData.type = userType;
                userData.ip= ip;
                userData.name= userData.firstName + ' '+ userData.lastName;
                userData.loginType=loginType;
            }
            callback(null,{userData: userData})
        }

    });
};

var verifyToken = function (token, callback) {
  logging.consolelog("check","",token);
    var response = {
        valid: false
    };
    Jwt.verify(token, constants.COMMON_VARIABLES.JWT_SECRET_KEY, function (err, decoded) {
        logging.consolelog('jwt err',err,decoded);
        if (err) {
            callback(err)
        } else {
            getTokenFromDB(decoded.id, decoded.userType,token,decoded.ip,decoded.loginType, callback);
        }
    });
};

module.exports={
    setToken:setToken,
    verifyToken:verifyToken
}
