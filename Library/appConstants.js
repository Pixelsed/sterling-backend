/**
 * Created by priya on 8/4/16.
 File used for defining constants 
 */

'use strict';
var logFiles = {
"allLogsFilePath":"../../logs/pixelsed_all.log",
"errorLogsFilePath":"../../logs/pixelsed_error.log",
"locationLogsFilePath":"../../logs/pixelsed_location.log"
}

var COMMON_VARIABLES = {
	JWT_SECRET_KEY: 'sUPerSeCuREKeY&^$^&$^%$^%7782348723t4872t34Ends',
	UNIVERSAL_BOARD_NAME: 'UNIVERSAL',
	GOOGLE_API_KEY: 'AIzaSyC8pYcPiU9LgzvXBrzSG2QpsvZQejnooG8',
	algorithm: 'aes-256-ctr',
	password: 'd6F3Efeq2d!!',
	link: 'www.pixelsed.com'
}


var USER_TYPE = {
	SUPER_ADMIN: 'SUPER_ADMIN',
	AGENT: "AGENT",
	TEACHER: "TEACHER",
	STUDENT: "STUDENT",
	CUSTOMER: "CUSTOMER",
	AGENTDISTRICT: "DISTRICT",
	GLOBAL: "GLOBAL",
	SCHOOL: "SCHOOL",
	SUB_AGENT: "SUB_AGENT"

}

var DATABASE = {
	PACKAGE_TYPE: {
		PACKAGE: 'PACKAGE',
		OPEN_PACKAGE: 'OPEN_PACKAGE',
		CUSTOMISED_PACKAGE: 'CUSTOMISED_PACKAGE',
		CLOSED_PACKAGE:'CLOSED_PACKAGE',
		ASSIGNED_PACKAGE:'ASSIGNED_PACKAGE',
		TOPIC_PACKAGE:'TOPIC_PACKAGE',
		AGENT_PACKAGE:'AGENT_PACKAGE'
	},
	SOCIAL_TYPE: {
		FACEBOOK: 'FACEBOOK',
		GOOGLEPLUS: 'GOOGLEPLUS'
	},
	LOGIN_TYPE: {
		WEBAPP: 'WEBAPP',
		ANDROIDAPP: 'ANDROIDAPP',
		IOSAPP:"IOSAPP"
	},
	DEVICE_TYPE: {
		IOS: 'IOS',
		ANDROID: 'ANDROID'
	},
	STATUS: {
		PENDING: 'PENDING',
		COMPLETED: 'COMPLETED',
		PROCESSED: 'PROCESSED',
		CANCELLED: 'CANCELLED'
	},
	GENDER: {
		MALE: 'MALE',
		FEMALE: 'FEMALE',
		OTHERS: 'OTHERS'
	},
	CONTENT_TYPE: {
		E_SAFETY: 'E_SAFETY',
		PRIVACY_POLICIES: 'PRIVACY_POLICIES',
		TERMS_OF_USE: 'TERMS_OF_USE',
		ABOUT_US: 'ABOUT_US',
		ABOUT_APP: 'ABOUT_APP',
		WEB_TUTORIAL: 'WEB_TUTORIAL'
	},
	KEY_TYPE_SUPER_ADMIN: {
		DASHBOARD: 'DASHBOARD',
		CURRICULUM_DIRECTORY: "CURRICULUM_DIRECTORY",
		MANAGE_TOPICS: "MANAGE_TOPICS",
		MANAGE_AGENTS: "MANAGE_AGENTS",
		SETTINGS: "SETTINGS",
		MANAGE_CONTENT: "MANAGE_CONTENT",
		MANAGE_PACKAGES: "MANAGE_PACKAGES",
		MANAGE_CUSTOMERS: "MANAGE_CUSTOMERS"
	},
	KEY_TYPE_AGENT: {
		DASHBOARD: 'DASHBOARD',
		MANAGE_TOPICS: "MANAGE_TOPICS",
		SETTINGS: "SETTINGS",
		MANAGE_CUSTOMERS: "MANAGE_CUSTOMERS"
	},
	ASSESSMENT_TYPE:{
		MCQ:1,
		MTC:2,
		DRAG_DROP:3,
		SINGLE_CHOICE:4,
		MULTI_SELECT:5,
		FILL_BLANKS:6,
		DESCRIPTIVE:7
	}
}


var STATUS_MSG = {
	ERROR: {
		OLD_PASSWORD:{
			statusCode:400,
			customMessage:"New password should be different from the existing one.",
			type:"OLD_PASSWORD"
		},
		VALID_NUMBER:{
			statusCode:400,
			customMessage:'Please input valid mobile number according to input country.',
			type:'VALID_NUMBER'
		},
		ADMIN_BLOCK:{
			statusCode:400,
			customMessage:'You Id is blocked by admin.Please contact admin.',
			type:'ADMIN_BLOCK'
		},
		UNIQUE_CODE_LIMIT_REACHED: {
			statusCode: 400,
			customMessage: 'Cannot Generate Unique Code, All combinations are used',
			type: 'UNIQUE_CODE_LIMIT_REACHED'
		},
		PHONENUMBER_NOT_REGISTERED: {
			statusCode: 401,
			customMessage: 'Mobile Number is not registered with us',
			type: 'PHONENUMBER_NOT_REGISTERED'
		},
		PHONE_VERIFICATION_COMPLETE: {
			statusCode: 400,
			customMessage: 'Your mobile number verification is already completed.',
			type: 'PHONE_VERIFICATION_COMPLETE'
		},
		USER_ALREADY_REGISTERED: {
			type: "USER_ALREADY_REGISTERED",
			statusCode: 400,
			customMessage: "User already registered with us."

		},
		DUPLICATE_SOCIAL_ID: {

			type: "DUPLICATE_SOCIAL_ID",
			statusCode: 400,
			customMessage: "Duplicate social Id"

		},
		INVALID_JOBID: {
			type: "INVALID_JOBID",
			statusCode: 400,
			customMessage: "Invalid Job Id"


		},
		WAIT_CONTENT_UPLOADING: {
			type: "WAIT_CONTENT_UPLOADING",
			statusCode: 400,
			customMessage: "Content not upload"
		},
		CONTENT_UPLOAD_ERROR: {
			type: "CONTENT_UPLOAD_ERROR",
			statusCode: 400,
			customMessage: "Content upload gives error. Please upload content again."

		},
		NOT_SUPPORTED: {
			type: "NOT_SUPPORTED",
			statusCode: 400,
			customMessage: "This type of content is not supported in the system."
		},
		UPLOAD_ERROR: {
			type: "UPLOAD_ERROR",
			statusCode: 400,
			customMessage: "Uploading Error"

		},
		USER_NOT_AUTHORISED: {
			type: 'USER_NOT_AUTHORISED',
			statusCode: 400,
			customMessage: 'Not authorised user to perform this action',
		},
		TOPIC_ALREADY_EXIST: {
			statusCode: 400,
			customMessage: 'Topic Name with passed language already exists.',
			type: 'TOPIC_ALREADY_EXIST'
		},
		EMPTY_CURRICULUM: {
			statusCode: 400,
			customMessage: 'There are no curriculums in the system.',
			type: 'EMPTY_CURRICULUM'
		},
		CURRICULUM_ERROR_CSV: {
			statusCode: 400,
			customMessage: 'Please input valid code.',
			type: 'CURRICULUM_ERROR_CSV'
		},
		CURRICULUM_ERROR_EXIST: {
			statusCode: 400,
			customMessage: 'Already existing Curriculum',
			type: 'CURRICULUM_ERROR_EXIST'
		},
		INVALID_CODE: {
			statusCode: 400,
			customMessage: 'Please input valid code.',
			type: 'INVALID_CODE'
		},
		VALIDATION_FAIL: {
			statusCode: 400,
			customMessage: 'Please input required parameters',
			type: 'VALIDATION_FAIL'},
		ALREADY_REGISTERED_EMAIL: {
			statusCode: 400,
			message: 'Email Address already registered with us.',
			type: 'ALREADY_REGISTERED'
		},
		ALREADY_REGISTERED_MOBILENO: {
			statusCode: 400,
			message: 'Mobile Number already registered with us.',
			type: 'ALREADY_REGISTERED_MOBILENO'
		},
		UPLOAD_IMAGE_ERROR: {
			statusCode: 400,
			message: 'Upload Image Error',
			type: 'UPLOAD_IMAGE_ERROR'
		},
		PASSWORD_MISMATCH: {
			statusCode: 400,
			message: 'Incorrect Password',
			type: 'PASSWORD_MISMATCH'
		},
		USER_NOT_FOUND: {
			statusCode: 400,
			message: 'User Name not found',
			type: 'USER_NOT_FOUND'
		},
		IMP_ERROR: {
			statusCode: 500,
			message: 'Internal Server Error',
			type: 'IMP_ERROR'

		},
		INVALID_TOKEN: {
			statusCode: 403,
			customMessage: 'Invalid token provided',
			type: 'INVALID_TOKEN'
		},
		TOPIC_NOT_FOUND: {
			statusCode: 400,
			customMessage: 'Topic Name is not found in our system',
			type: 'TOPIC_NOT_FOUND'

		},
		ASSESSMENT_NOT_FOUND: {
			statusCode: 400,
			customMessage: 'Assessment not found in our system',
			type: 'ASSESSMENT_NOT_FOUND'

		},
		INVALID_EMAIL: {
			statusCode: 400,
			customMessage: 'Invalid Email Id',
			type: 'INVALID_EMAIL'
		},
		SOCIAL_ID_PASSWORD_ERROR: {
			statusCode: 400,
			customMessage: 'Only one field should be filled at a time, either facebookId or password',
			type: 'SOCIAL_ID_PASSWORD_ERROR'
		},
		PASSWORD_REQUIRED: {
			statusCode: 400,
			customMessage: 'Password is required',
			type: 'PASSWORD_REQUIRED'
		},
		INVALID_COUNTRY_CODE: {
			statusCode: 400,
			customMessage: 'Invalid Country Code, Should be in the format +52',
			type: 'INVALID_COUNTRY_CODE'

		},
		INVALID_PHONE_NO_FORMAT: {
			statusCode: 400,
			customMessage: 'Phone no. cannot start with 0',
			type: 'INVALID_PHONE_NO_FORMAT'
		},
		SOCIAL_ID_NOT_FOUND: {
			statusCode: 400,
			customMessage: 'User not registered with us.',
			type: 'SOCIAL_ID_NOT_FOUND'
		},
		PACKAGE_NOT_SUPPORT: {
			statusCode: 400,
			customMessage: "This type of Package is not supported by system."
		},
		CANNOT_SHARE: {
			statusCode: 400,
			customMessage: 'You do not have anymore license to share.',
			type: 'CANNOT_SHARE'
		},
		LICENSE_ERROR:{
			statusCode: 400,
			customMessage: 'Please check allocated license.',
			type: 'CANNOT_SHARE'
		},
		UNIVERSAL_LINK_ERROR: {
			statusCode: 400,
			customMessage: 'Please link topic with same universal curriculum.',
			type: 'UNIVERSAL_LINK_ERROR'
		},
		CURRICULUM_NOT_PRESENT: {
			statusCode: 400,
			customMessage: 'Universal curriculum with same topic language is not in the system.',
			type: 'CURRICULUM_NOT_PRESENT'
		},
		WRONG_PASSWORD: {
			statusCode: 400,
			customMessage: "Please enter correct old password.",
			type: "WRONG_PASSWORD"
		},
		NOT_UPDATE_PASSWORD: {
			statusCode: 400,
			customMessage: "Please enter different password from old password.",
			type: "NOT_UPDATE_PASSWORD"
		},
		RENEW_FAIL:{
			statusCode: 400,
			customMessage: "We do not give renew services on selected package.",
			type: "RENEW_FAIL"
		},
		RENEW_TOKEN:{
			statusCode:400,
			customMessage:"Please renew the token",
			type:"RENEW_TOKEN"
		}


	},
	SUCCESS: {
		// CONTENT_ADDED:{
		//   type: 'CONTENT_ADDED',
		//   statusCode: 200,
		//   customMessage: "Content has been added successfully"
		// },
		APPROVED: {
			type: 'APPROVED',
			statusCode: 200,
			customMessage: "Agent is approved successfully."

		},
		LINKING_DELETE:{
			type: 'LINKING_DELETE',
			statusCode: 200,
			customMessage: "Linking has been deleted successfully."
		},
		SENT_CODE: {
			type: 'SENT_CODE',
			statusCode: 200,
			customMessage: "Code sent on your mobile Number"

		},
		CLONED_SUCCESSFULLY: {
			type: 'CLONED_SUCCESSFULLY',
			statusCode: 200,
			customMessage: "Topic cloned successfully."
		},
		MARKED_FAV: {
			type: 'MARKED_FAV',
			statusCode: 200,
			customMessage: "Successfully marked favourite"
		},
		SAVED_NOTE: {
			type: 'SAVED_NOTE',
			statusCode: 200,
			customMessage: "Successfully saved/updated note"
		},
		UNMARKED_FAV: {
			type: 'MARKED_FAV',
			statusCode: 200,
			customMessage: "Successfully unmarked favourite"
		},
		DEFAULT: {
			type: 'CHECK_CODE',
			statusCode: 200,
			customMessage: "Success::"
		},
		PASSWORD_RESET: {
			statusCode: 200,
			customMessage: 'Password reset successfully. Please login with your new password..',
			type: 'PASSWORD_RESET'
		},
		PASSWORD_CHANGE: {
			statusCode: 200,
			customMessage: 'Password changed successfully.',
			type: 'PASSWORD_CHANGE'
		},
		CURRICULUM_ADDED: {
			type: 'CURRICULUM_ADDED',
			statusCode: 200,
			customMessage: "Curriculum added successfully."
		},
		CHAPTER_ADDED: {
			type: 'CHAPTER_ADDED',
			statusCode: 200,
			customMessage: "Chapter Added successfully."
		},
		CHAPTER_DELETED: {
			type: 'CHAPTER_DELETED',
			statusCode: 200,
			customMessage: "Chapter Deleted successfully."
		},
		SUBJECT_DELETED: {
			type: 'SUBJECT_DELETED',
			statusCode: 200,
			customMessage: "Subject Deleted successfully."
		},
		GRADE_DELETED: {
			type: 'GRADE_DELETED',
			statusCode: 200,
			customMessage: "Grade Deleted successfully."
		},
		CURRICULUM_DELETED: {
			type: 'CURRICULUM_DELETED',
			statusCode: 200,
			customMessage: "Curriculum Deleted successfully."
		},
		GRADE_VALUE_UPDATED: {
			type: 'GRADE_VALUE_UPDATED',
			statusCode: 200,
			customMessage: "Grade value Updated successfully."
		},
		SUBJECT_VALUE_UPDATED: {
			type: 'SUBJECT_VALUE_UPDATED',
			statusCode: 200,
			customMessage: "Subject value Updated successfully."
		},
		CHAPTER_VALUE_UPDATED: {
			type: 'CHAPTER_VALUE_UPDATED',
			statusCode: 200,
			customMessage: "Chapter value Updated successfully."
		},
		TOPIC_ADDED: {
			type: 'TOPIC_ADDED',
			statusCode: 200,
			customMessage: "Topic Added Successfully"
		},
		GRADE_ADDED: {
			type: 'GRADE_ADDED',
			statusCode: 200,
			customMessage: "Grade Added Successfully"
		},
		CONTENT_ADDED: {
			type: 'CONTENT_ADDED',
			statusCode: 200,
			customMessage: "Content is added successfully."
		},
		TOPIC_DELETE: {
			type: 'TOPIC_DELETE',
			statusCode: 200,
			customMessage: 'Topic deleted successfully.'
		},
		TOPIC_LINKED: {
			type: 'TOPIC_LINKED',
			statusCode: 200,
			customMessage: "Topic is linked with curriculum successfully."
		},
		CONTENT_DELETED: {
			type: 'CONTENT_DELETED',
			statusCode: 200,
			customMessage: "Content is deleted successfully."
		},
		PACKAGE_ADDED: {
			type: 'PACKAGE_ADDED',
			statusCode: 200,
			customMessage: "Package added successfully."
		},
		COMMENT_ADDED: {
			type: 'COMMENT_ADDED',
			statusCode: 200,
			customMessage: "Comment added successfully."
		},
		ZIP_UPLOADED: {
			type: 'ZIP_UPLOADED',
			statusCode: 200,
			customMessage: "Zip file is uploaded successfully."
		},
		TOPIC_SHARED: {
			type: 'TOPIC_SHARED',
			statusCode: 200,
			customMessage: "Topic has been shared successfully."
		},
		PACKAGE_DELETED: {
			type: 'PACKAGE_DELETED',
			statusCode: 200,
			customMessage: "Package has been deleted successfully."
		},
		PACKAGE_EDIT: {
			type: 'PACKAGE_EDIT',
			statusCode: 200,
			customMessage: "Package has been edited successfully."
		},
		CUSTOMER_BLOCKED: {
			type: 'CUSTOMER_BLOCKED',
			statusCode: 200,
			customMessage: "Customer has been blocked successfully."

		},
		CUSTOMER_UNBLOCKED: {
			type: 'CUSTOMER_UNBLOCKED',
			statusCode: 200,
			customMessage: "Customer has been unblocked successfully."

		},
		FEEDBACK_BLOCK: {
			type: 'CUSTOMER_UNBLOCKED',
			statusCode: 200,
			customMessage: "Customer has been unblocked successfully."
		},
		FEEDBACK_UNBLOCK: {
			type: 'FEEDBACK_UNBLOCK',
			statusCode: 200,
			customMessage: "Customer has been unblocked successfully."

		},
		PUBLISH_SUCCESSFULLY: {
			type: 'PUBLISH_SUCCESSFULLY',
			statusCode: 200,
			customMessage: "Topics publish succesfully"
		},
		ADDED_CART: {
			type: 'ADDED_CART',
			statusCode: 200,
			customMessage: "Added to cart successfully"
		},
		ADDED_CART_DUP: {
			type: 'ADDED_CART_DUP',
			statusCode: 200,
			customMessage: "Added to cart successfully with some duplicates"
		},
		REMOVED_CART: {
			type: 'REMOVED_CART',
			statusCode: 200,
			customMessage: "Item is removed from cart successfully."
		},
		AGENT_ADDED: {
			type: 'AGENT_ADDED',
			statusCode: 200,
			customMessage: "Agent is added successfully."
		},
		EDIT_SUCCESSFULLY: {
			type: 'EDIT_SUCCESSFULLY',
			statusCode: 200,
			customMessage: "Agent is edited successfully."
		}
	}
};
var AGENT_ACCESS_RIGHTS = [
				{
					"key": "MANAGE_TOPICS",
					"peer_view": false,
					"assign": true,
					"delete": false,
					"add": false,
					"edit": false,
					"view": true
        },
				{
					"key": "MANAGE_CUSTOMERS",
					"peer_view": true,
					"assign": true,
					"delete": true,
					"add": true,
					"edit": true,
					"view": true
        },
				{
					"key": "SETTINGS",
					"peer_view": true,
					"assign": true,
					"delete": true,
					"add": true,
					"edit": true,
					"view": true
        }
    ]

var paymentMSG = {
	"Invalid":"Invalid transaction. Please try again.Your transaction ID is: ",
	"Aborted":"Your transaction was aborted. Please try again.Your transaction ID is: ",
	"Failure" : "Your transaction failed."
};
var APP_CONSTANTS = {

	STATUS_MSG: STATUS_MSG,
	COMMON_VARIABLES: COMMON_VARIABLES,
	USER_TYPE: USER_TYPE,
	DATABASE: DATABASE,
	AGENT_ACCESS_RIGHTS:AGENT_ACCESS_RIGHTS,
	logFiles:logFiles,
	paymentMSG:paymentMSG
};

module.exports = APP_CONSTANTS;