var MD5 = require('MD5');
var validator = require('validator');
var async = require('async');
var constants = require('./appConstants');
var ServiceCustomer = require('../Service/customer');
var timezoner = require('timezoner');
var crypto = require('crypto');

var q = require('q');
var TokenManager = require('./tokenManager');
var ERROR = constants.STATUS_MSG.ERROR;
var SUCCESS = constants.STATUS_MSG.SUCCESS;

var encryptData = function (text) {
    var cipher = crypto.createCipher(constants.COMMON_VARIABLES.algorithm, constants.COMMON_VARIABLES.password)
    var crypted = cipher.update(text, 'utf8', 'hex')
    crypted += cipher.final('hex');
    return crypted
}

var decryptData = function decrypt(text) {
    var decipher = crypto.createDecipher(constants.COMMON_VARIABLES.algorithm, constants.COMMON_VARIABLES.password)
    var dec = decipher.update(text, 'hex', 'utf8')
    dec += decipher.final('utf8');
    return dec;
}
var CryptData = function (stringToCrypt) {
    return MD5(MD5(stringToCrypt));
};


var verifyEmailFormat = function (string) {
    return validator.isEmail(string)
};


var generateUniqueCode = function (noOfDigits, userRole, callback) {
    noOfDigits = noOfDigits || 5;
    var excludeArray = [];
    var generatedRandomCode = null;
    async.series([
        function (cb) {
            //Push All generated codes in excludeAry
            if (userRole == constants.USER_TYPE.TEACHER) {
                ServiceCustomer.getAllGeneratedCodes(function (err, dataAry) {
                    if (err) {
                        cb(err);
                    } else {
                        if (dataAry && dataAry.length > 0) {
                            excludeArray = dataAry
                        }
                        cb();
                    }
                })
            } else if (userRole == UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.CUSTOMER) {
                ServiceCustomer.getAllGeneratedCodes(function (err, dataAry) {
                    if (err) {
                        cb(err);
                    } else {
                        if (dataAry && dataAry.length > 0) {
                            excludeArray = dataAry
                        }
                        cb();
                    }
                })
            } else {
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
            }
        },
        function (cb) {
            //Generate Random Code of digits specified
            generatedRandomCode = generateRandomNumbers(noOfDigits, excludeArray);
            cb();

        }], function (err, data) {
        callback(err, {
            number: generatedRandomCode
        })
    });


};


var getOffsetViaLatLong = function (latitude, longitude, callback) {

    var rawOffset = 0;
    /* Request timezone with location coordinates */
    timezoner.getTimeZone(
        latitude, // Latitude coordinate
        longitude, // Longitude coordinate
        function (err, data) {
            if (err) {
                callback(err)
            } else {
                console.log('data', data)
                rawOffset = data.rawOffset;
                rawOffset = rawOffset + data.dstOffset;
                callback(null, {
                    rawOffset: rawOffset
                })
            }
        }, {
            language: 'es',
            key: constants.COMMON_VARIABLES.GOOGLE_API_KEY
        }
    );
};


var deleteUnnecessaryUserData = function (userObj) {
    console.log('deleting>>', userObj)
    delete userObj['__v'];
    delete userObj['password'];
    delete userObj['accessToken'];
    delete userObj['emailVerificationToken'];
    delete userObj['passwordResetToken'];
    delete userObj['registrationDate'];
    delete userObj['OTPCode'];
    delete userObj['facebookId'];
    delete userObj['codeUpdatedAt'];
    delete userObj['deviceType'];
    delete userObj['deviceToken'];
    delete userObj['appVersion'];
    delete userObj['isBlocked'];
    console.log('deleted', userObj)
    return userObj;
};

function addDaysToDate(Date, days) {
    var newDate = moment(Date).add(days, 'days').format('YYYY-MM-DD');
    return newDate;
}

var isAuthenticated = function (request) {
    var promise = q.defer();
    TokenManager.verifyToken(request.headers.authorization, function (err, token) {
        if (!err) {
            if (token.userData.ip == request.headers['x-forwarded-for']) {
                q.resolve(true); //callback(true);

            } else {
                q.resolve(false); //callback(false);
            }

        } else {
            q.resolve(false); //callback(false);
        }
    });
    return promise.promise;
}
var generateRandomStringAndNumbers = function () {
    var text = "";
    var possible = "abcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 8; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
};

module.exports = {
    CryptData: CryptData,
    verifyEmailFormat: verifyEmailFormat,
    getOffsetViaLatLong: getOffsetViaLatLong,
    generateUniqueCode: generateUniqueCode,
    addDaysToDate: addDaysToDate,
    encryptData: encryptData,
    decryptData: decryptData,
    isAuthenticated: isAuthenticated,
    generateRandomStringAndNumbers:generateRandomStringAndNumbers
}