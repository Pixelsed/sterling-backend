var debugging_enabled = true;
if (process.env.NODE_ENV === 'LIVE') debugging_enabled = true;
var constants = require('./appConstants');
exports.logDatabaseQueryError = function (eventFired, error, result) {
    if (debugging_enabled) {
        console.log(error)
        process.stderr.write("Event: " + eventFired);
        process.stderr.write("\tError: " + JSON.stringify(error));
        process.stderr.write("\tResult: " + JSON.stringify(result));
    }
};

exports.consolelog = function (eventFired, error, result) {
    if (debugging_enabled) {
        console.log(eventFired, error, result)
    }
};
//
// var log4js = require('log4js');
// log4js.clearAppenders();
// var path = require('path');
//
// log4js.configure({
//     appenders: [
//         {
//             type: 'dateFile',
//             filename: path.join(__dirname,constants.logFiles.allLogsFilePath),
//             pattern: '-yyyy-MM-dd',
//             category: 'all_logs',
//             alwaysIncludePattern: true
//         },
//         {
//             type: 'dateFile',
//             filename: path.join(__dirname,constants.logFiles.errorLogsFilePath),
//             pattern: '-yyyy-MM-dd',
//             category: 'error_logs',
//             alwaysIncludePattern: true
//         },
//         {
//             type: 'dateFile',
//             filename: path.join(__dirname,constants.logFiles.locationLogsFilePath),
//             pattern: '-yyyy-MM-dd',
//             category: 'location_logs',
//             alwaysIncludePattern: true
//         }
//     ]
// });
//
// var logger = log4js.getLogger('all_logs');
// var location = log4js.getLogger('location_logs');
// var errorLogger = log4js.getLogger('error_logs');
// var importLogger = log4js.getLogger('import_logs');
// logger.setLevel('INFO');
// errorLogger.setLevel('INFO');
//
// exports.addErrorLog = function (event, err, input) {
//     var log = {
//         event: event,
//         error: err,
//         input: input
//     }
//
//     errorLogger.info(JSON.stringify(log, undefined, 2));
// };
//
// exports.addlog = function (event, info, res) {
//
//     var log = {
//         event: event,
//         input: info,
//         res: res
//     }
//     logger.info(JSON.stringify(log, undefined, 2));
// };
//
// exports.importlogs = function (event, input, data, err) {
//
//     var log = {
//         event: event,
//         input: input,
//         data : data,
//         err: err
//     }
//     importLogger.info(JSON.stringify(log, undefined, 2));
// };