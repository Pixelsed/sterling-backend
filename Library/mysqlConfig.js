var mysql = require('mysql');
// var mysqlConfig1 = Config.get('mysqlConfig');
var mysqlDatabaseInstance;
var mysqlConfig = Config.get('mysqlConfig');

function handleDisconnect() {
	mysqlDatabaseInstance = mysql.createConnection({
	    connectionLimit: 1,
	    host: mysqlConfig.host,
	    user: mysqlConfig.user,
	    password: mysqlConfig.password,
	    database: mysqlConfig.database
	});
	// console.log('mysql connection on', process.env.NODE_ENV);

	mysqlDatabaseInstance.connect(function(err) {              // The server is either down
		if(err) {                                     // or restarting (takes a while sometimes).
//		    console.log(db_config)
		  console.log('error when connecting to db:', err);
		  setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
		}                                     // to avoid a hot loop, and to allow our node script to
	});                                     // process asynchronous requests in the meantime.
	                                          // If you're also serving http, display a 503 error.
	mysqlDatabaseInstance.on('error', function(err) {
		console.log('db error', err);
		if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
		  handleDisconnect();                         // lost due to either server restart, or a
		} else {                                      // connnection idle timeout (the wait_timeout
		  throw err;                                  // server variable configures this)
		}
	});
}
handleDisconnect();
console.log('MySQL ================ CONNECTED');
var getMysqlInstance = function () {
    return mysqlDatabaseInstance;
};

module.exports = {
    getMysqlInstance: getMysqlInstance
};

