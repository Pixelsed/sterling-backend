var awsConfig = Config.get('awsConfig');
var async = require('async');
var Path = require('path');
var knox = require('knox');
var fsExtra = require('fs-extra');
var fs = require('fs');
var AWS = require('aws-sdk');
// var credentials = new AWS.SharedIniFileCredentials({profile: 'default'});
// AWS.config.credentials = credentials;
AWS.config.update({
    accessKeyId: awsConfig.s3BucketCredentials.accessKeyId,
    secretAccessKey: awsConfig.s3BucketCredentials.secretAccessKey,
    region: awsConfig.s3BucketCredentials.region,
    correctClockSkew:true
});
var elastictranscoder = new AWS.ElasticTranscoder(),
    s3 = new AWS.S3({signatureVersion: 'v4'});
var constants = require('./appConstants');
var ERROR = constants.STATUS_MSG.ERROR;
var SUCCESS = constants.STATUS_MSG.SUCCESS;
var unzip = require('unzip');
// var zlib = require('zlib')
var fstream = require('fstream');


var deleteFile = function deleteFile(path, callback) {

    fs.unlink(path, function (err) {
        console.error("delete", err);
        if (err) {
            var error = {
                response: {
                    message: "Something went wrong",
                    data: {}
                },
                statusCode: 500
            };
            return callback(error);
        } else
            return callback(null);
    });

}
///*
// 1) Save Local Files
// 2) Create Thumbnails
// 3) Upload Files to S3
// 4) Delete Local files
// */
//

var deleteFolderRecursive = function (path) {
    console.log(path);
    if (fs.existsSync(path)) {
        fs.readdirSync(path).forEach(function (file, index) {
            var curPath = path + "/" + file;
            if (fs.lstatSync(curPath).isDirectory()) {
                deleteFolderRecursive(curPath);
            } else {
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
    }
};

function uploadPicToS3Bucket(file, folder, callback) {
    console.log('folder', file, folder)
    var fs = require('fs');
    //    var mathjs = require('mathjs');
    //    var math = mathjs();
    // var AWS = require('aws-sdk');

    if (file == undefined) {
        console.log("No file file undefined");
        return callback("No File");
    } else {
        var filename = file.name; // actual filename of file
        var path = file.path; //will be put into a temp directory
        var mimeType = file.type;
        console.log('type of path', typeof path)
        fs.readFile(path, function (error, file_buffer) {

            if (error) {
                console.log("No file");
                console.log(error);
                //  //console.log(error)
                return callback("No File");
            } else {
                var length = 5;
                var str = '';
                var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                var size = chars.length;
                for (var i = 0; i < length; i++) {

                    var randomnumber = Math.floor(Math.random() * size);
                    str = chars[randomnumber] + str;
                }

                filename = "pixelsEd-" + str + "-" + file.name;
                filename = filename.split(' ').join('-');


                // var s3bucket = new AWS.S3();
                var params = {
                    Bucket: awsConfig.s3BucketCredentials.bucket,
                    Key: folder + '/' + filename,
                    Body: file_buffer,
                    ACL: 'public-read',
                    ContentType: mimeType
                };
                console.log('params>>>>>', params.Bucket, params.Key, s3)
                s3.putObject(params, function (err, data) {
                    if (err) {
                        console.log("No file");
                        console.log(err)
                        return callback("No File");
                    } else {
                        return callback(filename);
                    }
                });
            }
        });
    }
};

function saveCSVFile(fileData, path, callback) {
    //console.log("savePath",path);
    fsExtra.copy(fileData, path, callback);
}

var videoUpload = function (fileName, callback) {
    var PipelineId, fileName, filePath, jobId;
    var out;
    async.series([
//         function(cb){
//             console.log("err::create pipeline",awsConfig.s3BucketCredentials.role);
//             var params = {
//                 InputBucket: awsConfig.s3BucketCredentials.bucket, /* required */
//                 Name: 'Video', /* required Name of pipeline you can give anything////*/
//                 Role: awsConfig.s3BucketCredentials.role, /* required */
//                 OutputBucket: awsConfig.s3BucketCredentials.bucket,
//        
//             };
//             elastictranscoder.createPipeline(params, function(err, data) {
//                 if (err) console.log(err, err.stack); // an error occurred
//                 else {
//                     PipelineId = data.Pipeline.Id;
//        
//                     console.log("pipeline::",PipelineId);
//                     cb(null);
//                 }              // successful response
//             });
//         },
        function (cb) {
            console.log("print filename::", fileName);
            var length = 5;
            var str = '';
            var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var size = chars.length;
            for (var i = 0; i < length; i++) {

                var randomnumber = Math.floor(Math.random() * size);
                str = chars[randomnumber] + str;
            }

            out = str + awsConfig.s3BucketCredentials.outputKey;
            console.log("out::", out);
            var params = {
                Input: {

                    Key: awsConfig.s3BucketCredentials.videosFolder + '/' + fileName,
                    // Encryption: {
                    //     InitializationVector: 'STRING_VALUE',
                    //     Key: 'STRING_VALUE',
                    //     KeyMd5: 'STRING_VALUE',
                    //     Mode: 'S3'
                    // }
                },

                PipelineId: awsConfig.s3BucketCredentials.pipelineId,
                Output: {
                    Key: out,
                    PresetId: awsConfig.s3BucketCredentials.presetId1027x768,
                    Rotate: 'auto',
                    SegmentDuration: awsConfig.s3BucketCredentials.segmentDuration,

                },
                OutputKeyPrefix: awsConfig.s3BucketCredentials.videosFolder + '/',
//				Playlists: [
//					{
//						Format: 'HLSv3',
//						HlsContentProtection: {
//							InitializationVector: '',
//							Key: '',
//							KeyMd5: '',
//							KeyStoragePolicy: 'WithVariantPlaylists',
//							LicenseAcquisitionUrl: '',
//							Method: 'aes-128'
//						},
//						Name: fileName,
//							OutputKeys: [
//							out
//					
//					
//				],
//			
//    },
//    /* more items */
//  ],
            }

            elastictranscoder.createJob(params, function (err, data) {
                if (err) {
                    console.log(err, err.stack);
                    cb("No File");
                } // an error occurred
                else {
                    console.log('11111111111111');
                    console.log(data);
                    jobId = data.Job.Id;
                    out = out + '.m3u8';
                    cb(null);
                }

            });

        }
    ], function (err, data) {

        if (!err) callback({
            jobIdKey: jobId,
            outputKey: out
        })
        else {
            callback("No File")
        }
    });
}

var audioUpload = function (fileName, callback) {
    var PipelineId, fileName, filePath, jobId;
    var out;
    async.series([
        // function(cb){
        //     console.log("err::create pipeline")
        //     var params = {
        //         InputBucket: awsConfig.s3BucketCredentials.bucket, /* required */
        //         Name: 'Video', /* required Name of pipeline you can give anything////*/
        //         Role: awsConfig.s3BucketCredentials.role, /* required */
        //         OutputBucket: awsConfig.s3BucketCredentials.bucket,
        //
        //     };
        //     elastictranscoder.createPipeline(params, function(err, data) {
        //         if (err) console.log(err, err.stack); // an error occurred
        //         else {
        //             PipelineId = data.Pipeline.Id;
        //
        //             console.log("pipeline::",PipelineId);
        //             cb(null);
        //         }              // successful response
        //     });
        // },
        function (cb) {
            console.log("print filename::", fileName);
            var length = 5;
            var str = '';
            var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var size = chars.length;
            for (var i = 0; i < length; i++) {

                var randomnumber = Math.floor(Math.random() * size);
                str = chars[randomnumber] + str;
            }

            out = str + awsConfig.s3BucketCredentials.outputKey;
            console.log("out::", out);
            var params = {
                Input: {
                    Key: awsConfig.s3BucketCredentials.videosFolder + '/' + fileName,
                },
                PipelineId: awsConfig.s3BucketCredentials.pipelineId,
                Output: {
                    Key: out,
                    PresetId: awsConfig.s3BucketCredentials.presetIdAudio,
                    Rotate: 'auto',
                    SegmentDuration: awsConfig.s3BucketCredentials.segmentDuration,

                },
                OutputKeyPrefix: awsConfig.s3BucketCredentials.videosFolder + '/',
            }

            elastictranscoder.createJob(params, function (err, data) {
                if (err) {
                    console.log(err, err.stack);
                    cb("No File");
                } // an error occurred
                else {
                    console.log('11111111111111');
                    console.log(data);
                    jobId = data.Job.Id;
                    out = out + '.m3u8';
                    cb(null);
                }

            });

        }
    ], function (err, data) {

        if (!err) callback({
            jobIdKey: jobId,
            outputKey: out
        })
        else {
            callback("No File")
        }
    });
}


var readJobStatus = function (jobId, callback) {
    var params = {
        Id: jobId /* required */
    };

    elastictranscoder.readJob(params, function (err, data) {
        if (err) {
            console.log(err, err.stack);
            callback(ERROR.INVALID_JOBID)
        } // an error occurred
        else {
            console.log(data);
            callback(data);
        } // successful response
    });
}


//var unzipUpload = function (file, callback) {
//	console.log(file, "content")
//	var bucketUpload = unzip.createClient({
//		key: awsConfig.s3BucketCredentials.accessKeyId,
//		secret: awsConfig.s3BucketCredentials.secretAccessKey,
//		bucket: awsConfig.s3BucketCredentials.bucket
//	});
//	var path = file.path;
//	console.log("path2", path);
//	path = Path.normalize(path).trim();
//	console.log("path1", path);
//	var zipStream = fs.createReadStream(path);
//	console.log(zipStream, "zipStream!!!");
//	bucketUpload(zipStream).on('data', function (file) {
//		console.log("unzip data of assesment~~~~~~~", file)
//		return callback(file);
//
//	}).pipe(process.stdout);
//}


function uploadVideoFile(path, folder, callback) {
    console.log("video uploading", folder);
    var fs = require('fs');
    // var AWS = require('aws-sdk');

    if (path == undefined) {
        console.log("No file file undefined");
        return callback("No File");
    } else {

        var bb = path.split("/");
        bb = bb[bb.length - 1];
        fs.readFile(path, function (error, file_buffer) {
            console.log(error, file_buffer);
            if (error) {
                console.log("No file");
                console.log(error);
                //  //console.log(error)
                return callback("No File");
            } else {

                // var s3bucket = new AWS.S3();
                var params = {
                    Bucket: awsConfig.s3BucketCredentials.bucket,
                    Key: folder + bb,
                    Body: file_buffer,
                    ACL: 'public-read',
                    ContentType: "video/mp4"
                };
                console.log(params, "s3 params");
                s3.putObject(params, function (err, data) {
                    console.log(err, data, "video uploding data");
                    if (err) {
                        console.log("No file");
                        console.log(err)
                        return callback("No File");
                    } else {
                        console.log('uploaded');
                        return callback(null);
                    }
                });
            }
        });
    }
};

var unzipUpload = function (file, callback) {
    var zip = file.originalFilename.split(".");
    var filePath = zip[0] + Date.now() + '/';
    async.series([
        function (cb) {
            var path = file.path;
            var readStream = fs.createReadStream(path);
            var writeStream = fstream.Writer(Path.resolve("..") + "/uploads/");
            // var writeStream = fs.createWriteStream(Path.resolve("..")+"/uploads/")

            var stream = readStream
                .pipe(unzip.Parse())
                // .pipe(zlib.unzip())
                .pipe(writeStream).on('close', function () {
                    console.log("finish");
                    cb(null);
                });

        },
        function (cb) {
            logging.consolelog('after unzip>>',"",file)
            setTimeout(function (file) {
                logging.consolelog(filePath);
                var s3 = require('s3');
                var awsS3Client = new AWS.S3({
                    maxAsyncS3: 5,
                    s3RetryCount: 10,
                    s3RetryDelay: 1,
                    // accessKeyId: awsConfig.s3BucketCredentials.accessKeyId,
                    // secretAccessKey: awsConfig.s3BucketCredentials.secretAccessKey,
                    // region: awsConfig.s3BucketCredentials.region,
                    multipartUploadThreshold: 809715200, // this is the default (20 MB)
                    multipartUploadSize: 457286400, // this is the default (15 MB)
                });
                var options = {
                    s3Client: awsS3Client,
                };
                var client = s3.createClient(options);

                function getS3Params(localFile, stat, S3Callback) {
                    logging.consolelog(localFile, filePath, stat);
                    if (localFile.indexOf(".mp4") != -1 || localFile.indexOf(".png") != -1 || localFile.indexOf(".jpg") != -1) {
                        var path = stat.path.split('/');
                        logging.consolelog(path);
                        var x = '';
                        for (i = 0; i < path.length - 1; i++) {
                            x += path[i] + '/';
                        }
                        logging.consolelog('????',x, "x path print");

                        if (path.length == 1) {
                            var final = filePath;
                        } else {
                            var final = filePath + x;
                        }
                        logging.consolelog("file path", "",final);
                        uploadVideoFile(localFile, final, function (err, data) {


                        });
                        S3Callback(null, null);
                    } else {
                        S3Callback(null, {});
                    }
                }

                var params = {
                    localDir: Path.resolve("..") + "/uploads/" + file,
                    deleteRemoved: true,
                    s3Params: {
                        Bucket: awsConfig.s3BucketCredentials.bucket,
                        Prefix: filePath
                    },
                    getS3Params: getS3Params
                };
                console.log("------> check",params);
                var uploader = client.uploadDir(params);
                uploader.on('error', function (err) {
                    logging.consolelog("unable to sync:", err,'');
                });
                uploader.on('progress', function () {
                    logging.consolelog("progress", uploader.progressAmount, uploader.progressTotal);
                });
                uploader.on('end', function () {
                    console.log("done uploading");

                });

            }, 30000, zip[0]);
            cb(null);

        },
        function (cb) {
            setTimeout(function (file) {
                deleteFolderRecursive(Path.resolve("..") + "/uploads/" + file);

            }, 200000, zip[0]);
            cb(null);
        }
    ], function (err, data) {
        if (!err) {
            callback(filePath);
        } else {
            callback(err);
        }

    })
}
//var unzipUpload = function (file, callback) {
//
//		var path2 = file.path;
//		var filename = file.name;
//		async.series([
//
//
//			function(cb){
//				var buffer = fs.readFileSync(path2);
//				var myZip = file;
//console.log("myZip",myZip);
//  var zip = new require('node-zip')(buffer, {base64: false, checkCRC32: true});
//  console.log("zip::",zip);
//  var knox = require('knox');
//  var s3 = knox.createClient({"key": 'TEST2/test', "secret": "/i394j5oFwOqerkuaQOXcNIQuGtEB1rN1v8uQlvd", "bucket": awsConfig.s3BucketCredentials.bucket})
//
//
//  // You might need to try something like async here to throttle the putFile command
//    for (var i = zip.files.length - 1; i >= 0; i--) {
//
//        s3.putFile(zip.files[i], path.basename(zip.files[i]), function(err, res) {
//             console.log(err,res,"response");
//        });
//    };
//			}
////		 function (cb) {
////				var readStream = fs.createReadStream(path);
////				var writeStream = fstream.Writer(Path.resolve("..") + "/uploads/");
////
////				var body = readStream
////					.pipe(unzip.Parse())
////					.pipe(writeStream)
////				cb(null);
////
////			 var AdmZip = require('adm-zip');
////
////    // reading archives
////    var zip = new AdmZip(path);
////    var zipEntries = zip.getEntries();
////			 var s3bucket = new AWS.S3();
////					var params = {
////						Bucket: awsConfig.s3BucketCredentials.bucket,
////						Key: 'TEST/test123',
////						Body: zipEntries.toBuffer(),
////						ACL: 'public-read',
////						ContentType: "text/html"
////					};
////
////					s3bucket.putObject(params, function (err, data) {
////						console.error("PUT", err, data);
////						if (err) {
////						} else {
////							console.log("just testing");
////							console.log(data);
////							cb(null)
////						}
////					});
////
////		 }
//
//	 ], function (err, data) {
//			if (!err) {
//				callback(filename)
//			} else {
//				callback(null);
//			}
//		})
//}


module.exports = {
    uploadPicToS3Bucket: uploadPicToS3Bucket,
    saveCSVFile: saveCSVFile,
    videoUpload: videoUpload,
    audioUpload: audioUpload,
    readJobStatus: readJobStatus,
    unzipUpload: unzipUpload
}