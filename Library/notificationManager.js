var smsConfig = Config.get('smsConfig');

var client = require('twilio')
	(smsConfig.twilioCredentials.accountSid, smsConfig.twilioCredentials.authToken);
var async = require('async');
var ServiceNotifications = require('../Service/customerNotifications');
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var mailConfig = Config.get('mailConfig');
var gcm = require('node-gcm');

var smtpConfig = {
	host: mailConfig.mailCredentials.host,
	port: mailConfig.mailCredentials.port,
	// secure:true,
	auth: {
		user: mailConfig.mailCredentials.user,
		pass: mailConfig.mailCredentials.pass
	}
};


console.log("=====>", JSON.stringify(smtpConfig));
var transport = nodemailer.createTransport(smtpConfig);


function renderMessageFromTemplateAndVariables(templateData, variablesData) {
	var Handlebars = require('handlebars');
	return Handlebars.compile(templateData)(variablesData);
}

function sendSMS(smsOptions, cb) {
	client.messages.create(smsOptions, function (err, message) {
		console.log('SMS RES', err, message);
		if (err) {
			console.log(err)
		} else {
			console.log(message.sid);
		}
	});
	cb(null, null); // Callback is outside as sms sending confirmation can get delayed by a lot of time
}



var sendSMSToUser = function (data, smsType, callback) {
	console.log('sendSMSToUser', data, smsType)
	var smsOptions = {
		from: smsConfig.twilioCredentials.smsFromNumber,
		To: data.mobileNo,
		Body: null
	};
	var otp = data.otp;
	var templateData;
	var variableDetails = {};
	async.series([
			 function (cb) {
			switch (smsType) {
			case 'REGISTRATION':
				templateData = 'Hi,\nYour OTP to complete PixelsEd registration is {{otp}}. \nRegards\nTeam PixelsED';
				variableDetails.otp = otp
				break;


			case 'FORGET_PASSWORD':
				templateData = 'Hi,\nYour OTP to reset your PixelsEd password is {{otp}}. \nRegards\nTeam PixelsED';
				variableDetails.otp = otp
				break;

			case 'SUB_AGENT_REGISTER':
				templateData = 'Hi,\nYou have been added as a PixelsED sub-agent by {{name}}. Login/register at www.pixelsed.com\nRegards\nTeam PixelsED'
				variableDetails.name = name
				break;

			}
			cb();

        },
			 function (cb) {
			smsOptions.Body = renderMessageFromTemplateAndVariables(templateData, variableDetails);
			cb();
        },
			 function (cb) {
			console.log("sms options", smsOptions);
			sendSMS(smsOptions, function (err, res) {
				cb(err, res);
			})
			 }
		 ], function (err, response) {
		if (err) {
			callback(err);
		} else {
			callback(null, {
				DEFAULT_SUCCESS: {
					statusCode: 200,
					customMessage: 'Success',
					type: 'DEFAULT_SUCCESS'
				},
			});
		}
	})
}








var sendEmailToUser = function (emailId, subject, body, callback) {
	console.log(emailId, subject, body);
	var mailOptions = {
		// from: 'sterlingpixels79@gmail.com',
		from: 'mail@pixelsed.com',
		to: emailId,
		subject: subject,
		html: body
	};
	async.series([
        function (cb) {
			console.log(transport);
			transport.sendMail({
				from: mailOptions.from,
				to: emailId,
				subject: mailOptions.subject,
				html: mailOptions.html
			}, function (err, info) {
				console.log("mail senr", err, info);
				cb(null);
			})

        }
    ], function (err, data) {
		if (err) {
			callback(err);
		} else {
			callback(null);
		}
	});

};

var sendNotifications = function (customerId, notificationType, variableData) {
	var data = {}
	var customerArray = [];
	if (customerId.length > 0) {
		for (var i = 0; i < customerId.length; i++) {
			customerArray.push(customerId[i]);
		}
	} else {
		customerArray.push(customerId);
	}
	async.series([
			 function (cb) {
				switch (notificationType) {
				case 'CUSTOMER_ONBOARDING':
					data.notificationType = 'REGISTRATION';
					data.notificationMessage = 'Sterling welcomes you!!';
					break;

				case 'PACKAGE_ADVERTISEMENT':
					data.notificationType = 'PACKAGE_ADVERTISEMENT';
					data.notificationMessage = 'New' + ' ' + variableData.packageName + ' ' + 'topics are available at discounted prices. Check them out now!';
					break;
				case 'COMPLETE_PROFILE':
					data.notificationMessage = 'Please complete your profile information to get relevant offers and discounts.';
					data.notificationType = 'COMPLETE_PROFILE';
					break;
				case 'FREE_TOPIC_ADVERTISEMENT':
					data.notificationMessage = 'New free topics are available. Check them out now!';
					data.notificationType = 'FREE_TOPIC_ADVERTISEMENT';
					break;
				case 'ADD_CUSTOMER':
					data.notificationMessage = 'You have been added and assigned sub-agent rights by PixelsEd. View your rights here.';
					data.notificationType = 'ADD_CUSTOMER';
					break;
				case 'PURCHASE_REQUEST':
					data.notificationMessage = 'An agent has requested to purchase topics. Please manage their request.';
					data.notificationType = 'PURCHASE_REQUEST';
					break;

				case 'NOTIFY_ADMIN':
				data.notificationMessage = 'Your request to purchase content has been '+ variableData.action;
				data.notificationType = 'NOTIFY_ADMIN';
				break;

				}


				cb();

			},
			function (cb) {
				var counter = 0;
				async.forEach(customerArray, function (item, embeddedcb) {
					counter++
					var dataSave = {
						customerId: item,
						notificationMessage: data.notificationMessage,
						notificationType: data.notificationType,
						dateTime: Date.now(),
						isRead: false

					}
					ServiceNotifications.createNotifications(dataSave, function (err, response) {

						if (err) {

							embeddedcb(null);
						} else {

							embeddedcb(null);
						}
					});


				}, function (err) {
					if (counter == customerArray.length) {
						cb();
					}

				});
			}],
		function (err, data) {
			console.log("notification data type");
		});
}


function sendAndroidPushNotification(deviceTokens, messageData, type, callback) {
	console.log("======> device tokens", deviceTokens);
	console.log(deviceTokens, messageData, type, "====> send android push notification");
	var sender = null;
	var dataToAttach = {
		message: messageData,
		title: 'PIXELSED',
		notificationType: type
	};
	console.log("push notifications");
	sender = new gcm.Sender("AIzaSyAWT_2gK5heZ6qo6Xb7fX34UjLPzFT09nY");

	var message = new gcm.Message({
		delayWhileIdle: false,
		timeToLive: 2419200,
		data: dataToAttach,
		"Content-Type": "application/json"
	});
	console.log(dataToAttach, "=====> notify data", deviceTokens);

	if (deviceTokens && deviceTokens.length > 0) {
		var device = deviceTokens;
	} else {
		var device = [deviceTokens];
	}
	sender.send(message, device, 4, function (err, result) {
		console.log("ANDROID NOTIFICATION RESULT: " + JSON.stringify(result));
		console.log("ANDROID NOTIFICATION ERROR: " + JSON.stringify(err));
	});
	callback(null);
}


module.exports = {
	sendSMSToUser: sendSMSToUser,
	sendEmailToUser: sendEmailToUser,
	sendNotifications: sendNotifications,
	sendAndroidPushNotification: sendAndroidPushNotification
}